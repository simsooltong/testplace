package com.tws.today.oilview.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.flurry.android.FlurryAgent;
import com.tws.activity.base.BaseActivity;
import com.tws.today.oil.data.DefineFlurry;
import com.tws.today.oil.data.PoiInfo;
import com.tws.today.oil.data.SearchCoordAddr;
import com.tws.today.oil.data.SearchPoiFavorite;
import com.tws.today.oil.data.SearchPoiRadius;
import com.tws.today.oil.data.SearchPoiRadiusInfo;
import com.tws.today.oil.data.SearchPoiRadiusItem;
import com.tws.today.oil.data.StationBrand;
import com.tws.today.oil.network.CommonApi;
import com.tws.today.oil.network.RetCode;
import com.tws.today.oil.util.BrandIconChooser;
import com.tws.today.oil.util.CommaGenerator;
import com.tws.today.oil.util.CustomIntroDialog;
import com.tws.today.oil.util.StationUtil;
import com.tws.today.oil.util.TimeUtil;
import com.tws.today.oil.util.WidgetController;
import com.tws.today.oil.widget.CustomWidget;
import com.tws.today.oilview.custom.CuzTextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import CoordLib.Coord;
import CoordLib.NorXY_T;
import CoordLib.Position_T;
import tws.today.lib.application.TodayApplication;
import tws.today.lib.gms.LocationDefines;
import tws.today.lib.gms.LocationGMS;
import tws.today.lib.mapview.Defines;
import tws.today.lib.mapview.Defines.MAPConst;
import tws.today.lib.mapview.WebViewSingleton;
import tws.today.lib.mapview.twsMapController;
import tws.today.lib.mgr.PrefMgr;
import tws.today.lib.util.LOG;

public class OilViewMainActivity extends BaseActivity implements OnClickListener {
    //primitive type S
    private static final String temp1 = null;
    private static final int OFFSET_VAL = 54;
    private static final int ENTER_FAVORITE = 1;
    private static final int ENTER_LOW = 2;
    private static final int ENTER_NORMAL = 0;
    private static final int DEFAULT_MAP_LEVEL = 1;
    private static final int LIMIT_MAP_LEVEL = 6;
    private static final long ADDR_TIME = 700;
    private final double WEB_RESIZE = 0.67;
    private long NewTimeStamp;
    private long OldTimeStamp;
    private final int LEVEL = 8;
    private int lastLevel = 8;
    protected boolean isRequestButton = false;
    private boolean isTouchedMarker = false;
    private int oilType;
    private boolean isLastLocation = true;// 측위 TimeOut일때 사용
    //primitive type E

    //object type S
    private static String DEFAULT_MAP_LV = "2400";
    private static final String FAVORITE_MARKER_IMAGE = "FAV";
    private static final String POPULAR_MARKER_IMAGE = "POP";
    private String addFavorite = null;
    private String removeFavorite = null;
    private String noSupportMapLevel = null;
    private String now_X;
    private String now_Y;
    private String mPopularPid = null;
    private String selectedPid = null;
    private String[] oilTypeMarker = {"map_point_01.png", "map_point_02.png", "map_point_03.png", "map_point_04.png"};
    private String[] oilTypeBubbleIcon = {"map_bubble_icon_01.png", "map_bubble_icon_02.png", "map_bubble_icon_03.png", "map_bubble_icon_04.png"};
    private String[] oilTypeStrArr = null;
    private String[] rankLowPid = {"0", "0", "0"};
    private twsMapController mMapController = null;
    private CommonApi mNetCommonApi = null;
    private ConcurrentHashMap<String, SearchPoiRadiusItem> mHashPoi = null;
    private PrefMgr mPrefMgr = null;
    private ArrayList<SearchPoiRadiusItem> mSearchPoiRadiusItems = null;
    private SearchPoiRadiusItem mSelectedRadiusItem = null;
    private ArrayList<SearchPoiRadiusItem> mTempRadiusList = new ArrayList<SearchPoiRadiusItem>();
    //object type E

    //Widget Type S
    private ImageView waypoint = null;
    private Context mContext = null;
    private RelativeLayout rootLayout = null;
    private CustomProgress waitDialog = null;
    private Display display = null;
    private WebView webview = null;
    private TextView tvName = null;
    private TextView tvPrice = null;
    private TextView tvDistance = null;
    private ImageView ivFavorite = null;
    private ImageView ivNavi = null;
    private ImageView ivMyposition = null;
    private ImageView ivSearch = null;
    private Button btOilType = null;
    private LinearLayout llUnderLayout = null;
    private ImageView ivFavoriteSoloStartIcon = null;
    private TextView tvFavoriteSoloNMCost = null;
    private ImageView ivFavoriteSoloBrandIcon = null;
    private TextView tvPostionAddr = null;
    private CustomIntroDialog customIntroDialog = null;
    private CuzTextView tvWash;
    private CuzTextView tvMart;
    private CuzTextView tvFix;
    private CuzTextView tvPopular;
    //Widget Type E

    /**
     * webview 지도에서 자바스크립트 콜백 결과 받는 핸들러.
     */
    public Handler mMapCBHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("OilViewMainActivity MapCB_Handler msg what : " + msg.what);

            switch (msg.what) {
                case MAPConst.CB_SETTOUCHEND:
                    Log.e("DSKIM", "MapCB_Handler : CB_SETTOUCHEND");
                    break;
                case MAPConst.MAP_SUCC:
                case MAPConst.MAP_RECYCLE:
                case MAPConst.CB_ISMAP:
                    // 지도 로딩 성공
                    LOG.d("MapCB_Handler : MAP_SUCC");
                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();
                    mMapController.TwsMapSetEndTouch();
                    mMapController.TwsMapSetMoveend();
                    String[] xy = null;
                    switch (mPrefMgr.getEnterType()) {
                        case ENTER_NORMAL:
                        case ENTER_LOW:
                            startUI(now_X, now_Y);
                            break;
                        case ENTER_FAVORITE:
                            System.out.println();
                            mMapController.TwsMapClearMarkers();
                            setClickableControl(true);

                            xy = mPrefMgr.getMyPosition().split("\\|");

                            if (mPrefMgr.getFavoriteName().length() < 1) {
                                requestLocation();
                            } else {
                                setMyPosition(xy[0], xy[1]);
                                LOG.d("POIID : " + mPrefMgr.getFavoritePoiId());
                                apiSearchStation(mPrefMgr.getFavoritePoiId());
                            }
                            mPrefMgr.setEnterType(0);
                            break;
                    }
                    break;

                case MAPConst.MAP_FAIL:
                    // 지도 로딩 실패

                    LOG.d("MapCB_Handler : MAP_FAIL");

                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();

                    WebViewSingleton.clear((TodayApplication) getApplication(),
                            Defines.InstanceConst.MAIN_MAP_INSTANCE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "지도 초기화 실패하여 앱을 종료합니다.");
                        }
                    }, Toast.LENGTH_SHORT);
                    // 종료 처리.

                    break;
                case MAPConst.CB_GET_EXTENT:// x1,x2,y1,y2

                    LOG.d("CB_GET_EXTENT : " + (String) msg.obj);
                    // 2014.12.17시나리오변경 반경3km extent deprecated
                    // String[] extentXY = ((String) msg.obj).split("\\|");
                    // apiSearchPoiAround(extentXY);
                    break;
                case MAPConst.CB_SETZOOMEND:
                    LOG.d("CB_SETZOOMEND : " + (String) msg.obj);
                    String[] zoomInfo = ((String) msg.obj).split("\\|");
                    now_X = zoomInfo[0];
                    now_Y = zoomInfo[1];
                    lastLevel = Integer.parseInt(zoomInfo[2]);
                    // 지도 포인트 이미지 보여주기관련 처리 S
                    // apiSearchPoiAround(zoomInfo);
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));
                    break;
                case MAPConst.CB_MOVEEND:
                    LOG.d("CB_MOVEEND : " + (String) msg.obj);
                    final String[] moved = ((String) msg.obj).split("\\|");
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));

                    OldTimeStamp = Calendar.getInstance().getTimeInMillis();

                    // Timer count;
                    CountDownTimer cdt = new CountDownTimer(ADDR_TIME, ADDR_TIME) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFinish() {
                            // TODO Auto-generated method stub

                            // ??? 怨??

                            NewTimeStamp = Calendar.getInstance().getTimeInMillis();

                            if ((NewTimeStamp - OldTimeStamp >= ADDR_TIME)
                                    && OldTimeStamp != 0) {

                                apiSearchCoorAddr(moved[0], moved[1]);

                                OldTimeStamp = 0;
                            }
                        }

                    }.start();

                    break;
                case MAPConst.CB_SETONLYMOVEEND:
                    LOG.d("CB_SETONLYMOVEEND : " + (String) msg.obj);
                    final String[] array_addr;
                    array_addr = ((String) msg.obj).split("\\|");
                    now_X = array_addr[0];
                    now_Y = array_addr[1];
                    lastLevel = Integer.parseInt(array_addr[2]);
                    String[] saved_xy = mPrefMgr.getMyPosition().split("\\|");
                    // 지도 포인트 이미지 보여주기관련 처리 S

                    if (now_X.equals(saved_xy[0]) && now_Y.equals(saved_xy[1])) {
                        waypoint.setVisibility(View.GONE);
                    } else {
                        waypoint.setVisibility(View.VISIBLE);
                    }
                    break;

                case MAPConst.CB_SETTOUCHEVENTCB:
                    LOG.d("CB_SETTOUCHEVENTCB : " + (String) msg.obj);
                    String[] setTouchEventCBarr;
                    setTouchEventCBarr = ((String) msg.obj).split("\\|");

                    if (setTouchEventCBarr[0].equals("touchend")) {
                        if (isTouchedMarker) {
                            isTouchedMarker = false;
                        } else {
//						llUnderLayout.setVisibility(View.GONE);
//						mMapController.TwsMapRemoveDivPopup("marker_select");
                        }
                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
                    break;

                case MAPConst.CB_CREATEMARKER:
                    final String[] array_marker;
                    array_marker = ((String) msg.obj).split("\\|");
                    int nMakrer_index = Integer.parseInt(array_marker[0]);
                    mMapController.TwsMapSetTouchendMarkerCB(nMakrer_index);
                    break;
                case MAPConst.CB_GETCENTER:
                    LOG.d("CB_GETCENTER : " + (String) msg.obj);
                    // return x | y

                    break;

                case MAPConst.CB_GETLEVEL:
                    LOG.d("CB_GETLEVEL : " + (String) msg.obj);
                    // return level
                    break;

                case MAPConst.CB_CREATEFEATUREMAKER:
                    LOG.d("CB_CREATEFEATUREMAKER : " + (String) msg.obj);
                    // return marker_id|param
                    break;
                case MAPConst.CB_TOUCHENDMARKER:
                    LOG.d("CB_TOUCHENDMARKER : " + (String) msg.obj);
                    break;
                case MAPConst.CB_TOUCHENDFEATUREMARKER:
                    LOG.d("CB_TOUCHENDFEATUREMARKER : " + (String) msg.obj);
                    break;

                case MAPConst.CB_GET:// 마커 터치 했을 경우 콜백
                    LOG.d("CB_GET : " + (String) msg.obj);
                    mMapController.TwsMapRemoveDivPopup("marker_select");
                    final String[] array_marker_touch;
                    array_marker_touch = ((String) msg.obj).split("\\|");

                    if (array_marker_touch.length == 3) {
                        if (array_marker_touch[1].equals("t")) {
                            isTouchedMarker = true;
                            mSelectedRadiusItem = mHashPoi.get(array_marker_touch[2]);
                            if (mPrefMgr.getFavoriteName().equals(mSelectedRadiusItem.name)) {
                                ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_selected);
                            } else {
                                ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_none);
                            }
                            drawSelectedMarker(mSelectedRadiusItem);
                        } else {
                            isTouchedMarker = false;
                            llUnderLayout.setVisibility(View.GONE);
                            mMapController.TwsMapRemoveDivPopup("marker_select");
                        }
                    } else {

                    }
                    break;

                case MAPConst.CB_CENTERMAKER:

                    LOG.d("CB_GET : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETLONLATFROMPIXEL:
                    LOG.d("CB_GETLONLATFROMPIXEL : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETPIXELFROMLONLAT:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // return param
                    break;

                case MAPConst.CB_CREATEDIVPOPUP:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // isTouchedMarker = true;
                    // return param
                    break;
            }
        }
    };

    /**
     * 하단에 세차, 매점, 정비 기능 표시.
     *
     * @param tempItem
     */
    private void showWashMartFix(SearchPoiRadiusItem tempItem) {
        if (tempItem.wash.equals("1")) {
            tvWash.setVisibility(View.VISIBLE);
        } else {
            tvWash.setVisibility(View.GONE);
        }
        if (tempItem.mart.equals("1")) {
            tvMart.setVisibility(View.VISIBLE);
        } else {
            tvMart.setVisibility(View.GONE);
        }
        if (tempItem.fix.equals("1")) {
            tvFix.setVisibility(View.VISIBLE);
        } else {
            tvFix.setVisibility(View.GONE);
        }
    }

    /**
     * 선택한 마커에 대해서 상세 데이터를 하단 레이아웃에 보여준다.
     */
    private void showDetailInfo(SearchPoiRadiusItem tempItem) {
        if (mPopularPid != null) {
            if (mPopularPid.equals(tempItem.pid)) {
                tvPopular.setVisibility(View.VISIBLE);
            } else {
                tvPopular.setVisibility(View.INVISIBLE);
            }
        }
        tvName.setText(tempItem.name);
        String priceStr = CommaGenerator.withCommaString(tempItem.pricessArr()[mPrefMgr.getOilType()]);
        tvPrice.setText(priceStr.equals("0") ? oilTypeStrArr[mPrefMgr.getOilType()] + "제공안함" : (priceStr + " 원"));
        tvDistance.setText(getDistance(tempItem.twx, tempItem.twy));
        showWashMartFix(tempItem);

        System.err.println("DSKIM" + tempItem.pid + "||" + mPrefMgr.getFavoritePoiId());
        System.err.println("DSKIM WASH " + tempItem.wash + "||" + tempItem.mart + "||" + tempItem.fix);

        if (mPrefMgr.getFavoriteName().equals(tempItem.name)) {
            ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_selected);
        } else {
            ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_none);
        }
    }

    /**
     * @param x2 선택된 마커의 X위치
     * @param y2 선택된 마커의 Y위치
     * @return 환산된 거리값.
     * @author DSKIM</br>
     * 거리값을 리턴해준다. 현재 선택된 마커의 위치와 나의 위치를
     * 기준으로 거리를 계산하여 리턴.</br>
     * <p/>
     * <참고>
     * 1km 이상일 경우 x.x km 표시.
     * 1Km 미만일 경우 xxx m 로 표시.
     */
    private String getDistance(String x2, String y2) {
        String distanceStr = null;
        Coord client = new Coord();
        String[] myXY = mPrefMgr.getMyPosition().split("\\|");
        Double distanceDouble = client.get_distance(Integer.parseInt(myXY[0]), Integer.parseInt(myXY[1]), Integer.parseInt(x2), Integer.parseInt(y2));
        if (distanceDouble >= 1000) {
            distanceStr = String.format("%.1f", (distanceDouble / 1000)) + "km";
        } else {
            distanceStr = String.format("%.0f", distanceDouble) + "m";
        }
        return distanceStr;
    }

    public boolean CheckLevel() {
        if (lastLevel >= LEVEL) {
            return true;
        } else {
            // mMapController.TwsMapClearMarkers();
            Toast.makeText(OilViewMainActivity.this,
                    "현재 지도 축적에서는 정보 결과를 제공하지 않습니다. ", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
    }

    public Handler locationResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isFinishing())
                return;
            switch (msg.what) {

                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "내 위치 확인 불가, 앱을 종료합니다.");
                            dismissIntroDialog();
                            dismissWaitDialog();
                        }
                    }, Toast.LENGTH_SHORT);
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");
                    if (msg.obj != null) {
                        isLastLocation = false;
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "location getAccuracy : "
                                + location.getAccuracy());
                        Log.i("LocationResultHandler", "location getLongitude : "
                                + location.getLongitude());
                        Log.i("LocationResultHandler", "location getLatitude : "
                                + location.getLatitude());

                        setClickableControl(true);

                        String xyStr[] = convertXY(location);
                        if (isRequestButton == true) {// 내위치 찾기
                            logFlurryAgent(DefineFlurry.keyMA_LOC, DefineFlurry.keyXY, xyStr[0] + "|" + xyStr[1]);
                            isRequestButton = false;
                        } else { // 초반 시작시
                            FlurryAgent.setLocation(Float.parseFloat(xyStr[1]), Float.parseFloat(xyStr[0]));
                        }
                        mPrefMgr.setMyPosition(xyStr[0] + "|" + xyStr[1]);
                        now_X = xyStr[0];
                        now_Y = xyStr[1];
                        // if(mPrefMgr.getIsFirst()==false){
                        // setControl();
                        // requestLocation();
                        // initData();
                        initWebview();
                        // }
                    } else {

                    }

                    break;

                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    break;

                case LocationDefines.GMS_LOCATION_TIMEOUT:// 측위 요청했지만 Timeout
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    isLastLocation = true;
                    setClickableControl(true);
                    String[] xy = mPrefMgr.getMyPosition().split("\\|");
                    now_X = xy[0];
                    now_Y = xy[1];
                    initWebview();
                    Toast.makeText(OilViewMainActivity.this, getResources().getString(R.string.gps_off), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void setClickableControl(boolean click) {
        ivFavorite.setClickable(click);
        ivMyposition.setClickable(click);
        ivSearch.setClickable(click);
        btOilType.setClickable(click);
    }

    /**
     * 네트워크 통신 결과 핸들러.
     */
    public Handler mNetResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("NetResult_Handler msg what" + msg.what);

            switch (msg.what) {
                case RetCode.RET_SEARCH_COORD_ADDR_SUCC:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC"
                            + ((SearchCoordAddr) msg.obj).cut_address);
                    // System.out.println("RET_SEARCH_COORD_ADDR_SUCC"+searchCoordAddr.address);
                    retApiSearchCoordAddr((SearchCoordAddr) msg.obj);
                    break;
                case RetCode.RET_SEARCHPOI_STATION_SUCC:
                    LOG.d("RET_SEARCHPOI_STATION_SUCC");
                    retApiSearchStationInfo((SearchPoiRadiusInfo) msg.obj);
                    break;
                case RetCode.RET_SEARCHPOI_FAVORITE_SUCC:
                    LOG.d("RET_SEARCHPOI_RADIUS_SUCC");
                    break;
                case RetCode.RET_SEARCHPOI_RADIUS_SUCC:
                    LOG.d("RET_SEARCHPOI_RADIUS_SUCC");
                    apiSearchCoorAddr(now_X, now_Y);
                    retApiSearchPoiRadius((SearchPoiRadius) msg.obj);
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_FAIL:
                    LOG.d("RET_SEARCH_COORD_ADDR_FAIL");

                    tvPostionAddr.setText(getResources().getString(R.string.no_information));

                    break;

                case RetCode.NET_ERROR_MSG:
                    dismissWaitDialog();
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR_MSG),
                            msg.obj.toString(), getPackageName());
                    if (msg.arg1 == 903) {
                        String message = "주변에 주유소(" + oilTypeStrArr[mPrefMgr.getOilType()] + ")가 없습니다.";
                        Toast.makeText(OilViewMainActivity.this, message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, msg.obj.toString() + "(" + msg.arg1 + ")", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case RetCode.NET_ERROR:
                    dismissWaitDialog();
                    LOG.d("NET_RETURN_ERROR_MSG");
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR),
                            "RetCode.NET_ERROR", getPackageName());
                    Toast.makeText(mContext, getResources().getString(R.string.network_err_request_timeout), Toast.LENGTH_SHORT).show();
                    // BaseMessagePopup(null, null, -1, false, true);
                    break;

            }
        }
    };

    private String[] convertXY(Location location) {
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1] = String.valueOf(data.y);
        return xyStr;
    }

    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        LOG.d("GetScreen 가로 : " + width + " , 세로 : " + height);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int densityDpi = (int) (metrics.density * 160f);
        LOG.d("GetScreen Desity : " + densityDpi);
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            LOG.d("GetScreen Category : Large");
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            LOG.d("GetScreen Category : XLarge");
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            LOG.d("GetScreen Category : Normal");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        StartFlurryAgent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOG.d("DSKIM resulstCode : " + resultCode);
        if (resultCode == 9999) {
            makeWaitDialog();
            if (mPrefMgr.getFavoriteName().equals("")) {// 즐겨찾기가 없음.
                ivFavoriteSoloBrandIcon.setVisibility(View.INVISIBLE);
                tvFavoriteSoloNMCost.setText(getResources().getString(R.string.main_tv_favorite_null));
            } else {// 즐겨찾기가 있음.
                if (mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]
                        .equals("0")) {
                    tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName() + " "
                            + oilTypeStrArr[mPrefMgr.getOilType()] + " 제공안함");
                } else {
                    tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName()
                            + " "
                            + CommaGenerator.withCommaString(mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]) + "원");
                }
                ivFavoriteSoloBrandIcon.setBackgroundDrawable(getResources()
                        .getDrawable(
                                BrandIconChooser.getBrandIcon(mPrefMgr
                                        .getFavoriteBrand())));
            }

            llUnderLayout.setVisibility(View.GONE);
            // singleton webview view에 추가 S
                if (webview != null) {
                ViewGroup parent = (ViewGroup) webview.getParent();
                if (parent == null) {
                    rootLayout.addView(webview, display.getWidth(),
                            display.getHeight());
                }
            }
            initWebview();
        } else if (resultCode == 9090) {
            makeWaitDialog();
            requestLocation();
        } else {

        }
        // singleton webview view에 추가 E

        // setControl();
        // requestLocation();
        // initData();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (mPrefMgr.getFavoriteName().equals("")) {// 즐겨찾기가 없음.
            ivFavoriteSoloBrandIcon.setVisibility(View.INVISIBLE);
            tvFavoriteSoloNMCost.setText(getResources().getString(R.string.main_tv_favorite_null));
        } else {// 즐겨찾기가 있음.
            if (mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]
                    .equals("0")) {
                tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName() + " "
                        + oilTypeStrArr[mPrefMgr.getOilType()] + " 제공안함");
            } else {
                tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName()
                        + " "
                        + CommaGenerator.withCommaString(mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]) + "원");
            }
            ivFavoriteSoloBrandIcon.setBackgroundDrawable(getResources()
                    .getDrawable(
                            BrandIconChooser.getBrandIcon(mPrefMgr
                                    .getFavoriteBrand())));
        }

        llUnderLayout.setVisibility(View.GONE);
        // singleton webview view에 추가 S
        if (webview != null) {
            ViewGroup parent = (ViewGroup) webview.getParent();
            if (parent == null) {
                rootLayout.addView(webview, display.getWidth(),
                        display.getHeight());
            }
        }

        // singleton webview view에 추가 E
        if (mPrefMgr.getIsFirst() == false) {
            int enterType = mPrefMgr.getEnterType();

            switch (enterType) {
                case ENTER_NORMAL:
                case ENTER_LOW:
                    makeWaitDialog();
                    requestLocation();
                    break;
                case ENTER_FAVORITE:
                    makeWaitDialog();
                    initWebview();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("DSKIM", "onCreate");
        setContentView(R.layout.activity_oilview_main);
        getScreenSize();
        mPrefMgr = new PrefMgr(OilViewMainActivity.this);
        oilType = mPrefMgr.getOilType();
        oilTypeStrArr = getResources().getStringArray(R.array.oilTypeArray);
        makeIntroDialog();
        initData();
        setControl();
        setStrings();
        tutorialCheck();

        if (mPrefMgr.getFavoriteName().equals("")) {// 즐겨찾기가 없음.
            ivFavoriteSoloBrandIcon.setVisibility(View.INVISIBLE);
            tvFavoriteSoloNMCost.setText(getResources().getString(R.string.main_tv_favorite_null));
        } else {// 즐겨찾기가 있음.
            if (mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]
                    .equals("0")) {
                tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName() + " "
                        + oilTypeStrArr[mPrefMgr.getOilType()] + " 제공안함");
            } else {
                tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName()
                        + " "
                        + CommaGenerator.withCommaString(mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()]) + "원");
            }
            ivFavoriteSoloBrandIcon.setBackgroundDrawable(getResources()
                    .getDrawable(
                            BrandIconChooser.getBrandIcon(mPrefMgr
                                    .getFavoriteBrand())));
        }

        llUnderLayout.setVisibility(View.GONE);
        // singleton webview view에 추가 S
        if (webview != null) {
            ViewGroup parent = (ViewGroup) webview.getParent();
            if (parent == null) {
                rootLayout.addView(webview, display.getWidth(),
                        display.getHeight());
            }
        }

        // singleton webview view에 추가 E
        if (mPrefMgr.getIsFirst() == false) {
            int enterType = mPrefMgr.getEnterType();

            switch (enterType) {
                case ENTER_NORMAL:
                case ENTER_LOW:
                    makeWaitDialog();
                    requestLocation();
                    break;
                case ENTER_FAVORITE:
                    initWebview();
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub

        super.onResume();
    }

    private void setStrings() {
        addFavorite = getResources().getString(R.string.add_favorite);
        removeFavorite = getResources().getString(R.string.remove_favorite);
        noSupportMapLevel = getResources().getString(R.string.no_support_map_level);
    }

    private void setControl() {

        llUnderLayout = (LinearLayout) findViewById(R.id.llUnderInfoLayout);
        tvName = (TextView) findViewById(R.id.tvName);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvDistance = (TextView) findViewById(R.id.tvDistance);
        ivFavorite = (ImageView) findViewById(R.id.ivFavorite);
        ivMyposition = (ImageView) findViewById(R.id.ivMyposition);
        ivSearch = (ImageView) findViewById(R.id.ivSearch);
        btOilType = (Button) findViewById(R.id.btGoSetting);
        ivNavi = (ImageView) findViewById(R.id.ivNavi);
        tvPostionAddr = (TextView) findViewById(R.id.tvPostionAddr);

        ivFavoriteSoloStartIcon = (ImageView) findViewById(R.id.ivFavoriteSoloStartIcon);
        tvFavoriteSoloNMCost = (TextView) findViewById(R.id.tvFavoriteSoloNMCost);
        ivFavoriteSoloBrandIcon = (ImageView) findViewById(R.id.ivFavoriteSoloBrandIcon);
        tvWash = (CuzTextView) findViewById(R.id.main_detail_info_wash);
        tvMart = (CuzTextView) findViewById(R.id.main_detail_info_mart);
        tvFix = (CuzTextView) findViewById(R.id.main_detail_info_fix);
        tvPopular = (CuzTextView) findViewById(R.id.main_detail_popular);

        waypoint = (ImageView) findViewById(R.id.frame_trf_map_waypoint);

        ivNavi.setOnClickListener(this);
        ivFavorite.setOnClickListener(this);
        ivMyposition.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        btOilType.setOnClickListener(this);

        setClickableControl(false);
    }

    private void initData() {
        mNetCommonApi = new CommonApi();
    }

    /**
     * @param longi X좌표
     * @param lati  Y좌표
     *              <p/>
     *              받아온 좌표를 my_long, my_lati에 대입한다. 추후 자주 쓸꺼라서.
     * @author DSKIM</br>
     */
    private void setMyPosition(String longi, String lati) {
        String strImagePath = null;
        try {
            if (isLastLocation) {// 측위 Timeout
                strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
            } else {// 측위 성공
                strImagePath = "/android_asset/mapicon/frm_my_point_01.png";
            }
        } catch (Exception e) {
            e.printStackTrace();
            strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
        }
        mMapController.TwsMapRemoveFeatureMarkerALL();
        mMapController.TwsMapAddCenterMarker(Integer.parseInt(longi),
                Integer.parseInt(lati), strImagePath);
    }

    /**
     * 지도 주소 정보 요청 api
     *
     * @param longi
     * @param lati
     */
    private void apiSearchCoorAddr(String longi, String lati) {
        System.err.println("apiSearchCoorAddr call");
        mNetCommonApi.apiSearchCoordAddr(mContext, longi, lati,
                new AjaxCallback<SearchCoordAddr>() {
                    @Override
                    public void callback(String url, SearchCoordAddr object,
                                         AjaxStatus status) {
                        // TODO Auto-generated method stub

                        LOG.d("apiSearchCoorAddr AQuery url : " + url);
                        LOG.d("apiSearchCoorAddr net AQuery status.getCode() : "
                                + status.getCode());

                        if (status.getCode() == RetCode.HTTP_SUCC) {
                            // http 200 성공
                            LOG.d("apiSearchCoorAddr AQuery object.result : "
                                    + object.result);
                        } else {
                            // http 오류 나 네트워크 오류
                            object = null;
                        }

                        if (object != null) {

                            SearchCoordAddr sr = (SearchCoordAddr) object;

                            // return data check
                            LOG.d("apiSearchCoorAddr sr.result : " + sr.result);
                            LOG.d("apiSearchCoorAddr sr.errormsg : "
                                    + sr.errormsg);

                            if (sr.result == RetCode.NET_SUCC) {
                                // 성공
                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_SUCC;
                                msg.obj = sr;

                                mNetResultHandler.sendMessage(msg);

                            } else {
                                // 서버 오류 메시지 출력

                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_FAIL;
                                msg.obj = sr.errormsg;
                                msg.arg1 = sr.result;
                                mNetResultHandler.sendMessage(msg);
                            }
                        } else {
                            // 실패
                        }
                        super.callback(url, object, status);
                    }
                });
    }

    /**
     * 주변 주유소 검색 API를 호출
     *
     * @param longi
     * @param lati
     */
    private void apiSearchPoiRadius(String longi, String lati) {
        mMapController.TwsMapRemoveDivPopups();
        mNetCommonApi.apiSearchPoiRadius(mContext, longi, lati, DEFAULT_MAP_LV, "0", "0", new AjaxCallback<SearchPoiRadius>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoiRadius object,
                                 AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("apiSearchPoiRadius AQuery url : " + url);
                LOG.d("apiSearchPoiRadius net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("apiSearchPoiRadius AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoiRadius sr = (SearchPoiRadius) object;

                    // return data check
                    LOG.d("apiSearchPoiRadius sr.result : " + sr.result);
                    LOG.d("apiSearchPoiRadius sr.errormsg : " + sr.errormsg);

                    if (sr.result.equals("0")) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCHPOI_RADIUS_SUCC;
                        msg.obj = sr;

                        mNetResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = mNetResultHandler.obtainMessage();

                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;

                        mNetResultHandler.sendMessage(msg);
                    }
                } else {
                    // 실패
                    mNetResultHandler
                            .sendEmptyMessage(RetCode.NET_ERROR);

                }
                super.callback(url, object, status);
            }
        });
    }

    /**
     * @author JONY</br> 즐겨찾기로 검색.</br>
     */
    private void apiSearchStation(String poiId) {
        mMapController.TwsMapRemoveDivPopups();
        mNetCommonApi.apiSearchPoiStation(mContext, poiId, new AjaxCallback<SearchPoiRadiusInfo>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoiRadiusInfo object, AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("apiSearchStation AQuery url : " + url);
                LOG.d("apiSearchStation net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("apiSearchStation AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoiRadiusInfo sr = (SearchPoiRadiusInfo) object;

                    // return data check
                    LOG.d("apiSearchStation sr.result : " + sr.result);
                    LOG.d("apiSearchStation sr.errormsg : "
                            + sr.errormsg);

                    if (sr.result.equals(String.valueOf(RetCode.NET_SUCC))) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.RET_SEARCHPOI_STATION_SUCC;
                        msg.obj = sr;
                        mNetResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;
                        mNetResultHandler.sendMessage(msg);
                    }

                } else {
                    // 실패
                    mNetResultHandler
                            .sendEmptyMessage(RetCode.NET_ERROR);
                }
                super.callback(url, object, status);
            }
        });
    }

    /**
     * @author DSKIM</br> 즐겨찾기 추가/삭제 서버로 전송</br>
     */
    private void apiAddRemoveFavorite(String pid, String type) {
        String oilType = "0";
        switch (Integer.parseInt(type)) {
            case 0://고급휘발유
                oilType = "2";
                break;
            case 1://휘발유
                oilType = "1";
                break;
            case 2://경유
                oilType = "3";
                break;
            case 3://LPG
                oilType = "4";
                break;
        }

        mNetCommonApi.apiAddRemoveFavorite(mContext, pid, oilType, new AjaxCallback<SearchPoiFavorite>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoiFavorite object, AjaxStatus status) {

                LOG.d("apiAddRemoveFavorite AQuery url : " + url);
                LOG.d("apiAddRemoveFavorite net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("apiAddRemoveFavorite AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoiFavorite sr = (SearchPoiFavorite) object;

                    // return data check
                    LOG.d("apiAddRemoveFavorite sr.result : " + sr.result);
                    LOG.d("apiAddRemoveFavorite sr.errormsg : " + sr.errormsg);

                    if (sr.result.equals(String.valueOf(RetCode.NET_SUCC))) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.RET_SEARCHPOI_FAVORITE_SUCC;
                        msg.obj = sr;
                        mNetResultHandler.sendMessage(msg);
                    } else {
                        // 서버 오류 메시지 출력
                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;
                        mNetResultHandler.sendMessage(msg);
                    }

                } else {
                    // 실패
                    mNetResultHandler
                            .sendEmptyMessage(RetCode.NET_ERROR);
                }
                super.callback(url, object, status);
            }
        });
    }

    /**
     * webview 초기화
     */

    public void initWebview() {
        LOG.d("initWebView");
        mContext = getApplicationContext();

        rootLayout = (RelativeLayout) findViewById(R.id.webview_map);

        display = getWindowManager().getDefaultDisplay();

        // 지도 로딩 시작
        // if (!(mProgDlg.isShowing()))
        // mProgDlg.show();

        // 웹뷰 불러오기. (어플리케이션에서) S

        // 콜백 확인 후 처리한다. init인지 recycle
        WebViewSingleton webViewSingleton = WebViewSingleton.getInstance(
                (TodayApplication) getApplication(),
                Defines.InstanceConst.MAIN_MAP_INSTANCE);

        webViewSingleton.setHandler(mMapCBHandler);

        webview = webViewSingleton.getWebView();

        if (webview != null) {

            // singleton webview view에 추가.
            ViewGroup parent = (ViewGroup) webview.getParent();

            if (parent == null) {
                rootLayout.addView(webview, display.getWidth(),
                        display.getHeight());
            }

        } else {
            // 예외처리.
        }

        // 웹뷰 불러오기. (어플리케이션에서) E

        // 웹뷰 컨트롤 등록
        if (webview != null) {
            mMapController = new twsMapController(webview);
            mMapController.TwsMapGetCenter();
            mMapController.TwsMapSetPantoendCB(true);
//            mMapController.TwsMapSetLevel(DEFAULT_MAP_LEVEL);
            // mMapController.TwsMapSetStartTouch();
            // mMapController.TwsMapSetEndTouch();
        } else {
//            mMapController.TwsMapSetLevel(DEFAULT_MAP_LEVEL);
            // 예외 처리 필요 (종료처리)
            // PopupDialog_Finish(AppDefines.ERROR_MSG_SET, mContext
            // .getResources().getString(R.string.ONAIR_SERVER_FAIL));
        }

    }

    /**
     * 주소 정보를 화면 표시.
     *
     * @param obj
     */
    private void retApiSearchCoordAddr(SearchCoordAddr obj) {
        String addr = obj.cut_address;
        if (addr.length() < 2) {
            tvPostionAddr.setText(getResources().getString(R.string.no_information));
        } else {
            tvPostionAddr.setText(addr);
        }
    }

    /**
     * @param obj
     */
    protected void retApiSearchStationInfo(SearchPoiRadiusInfo obj) {
        mHashPoi = new ConcurrentHashMap<String, SearchPoiRadiusItem>();
        if (obj.poi!= null && obj.poi.pid!=null) {
            mSelectedRadiusItem = obj.poi;
            mHashPoi.put(mSelectedRadiusItem.pid, obj.poi);
            mTempRadiusList = new ArrayList<SearchPoiRadiusItem>();
            mTempRadiusList.add(obj.poi);
            int favMarkerX = Integer.parseInt(mSelectedRadiusItem.twx);
            int favMarkerY = Integer.parseInt(mSelectedRadiusItem.twy);
            String favPoiId = mSelectedRadiusItem.pid;
            String favCatecode = mSelectedRadiusItem.poll;

            drawAllMarker(favMarkerX, favMarkerY, favPoiId, favCatecode);
            drawSelectedMarker(mSelectedRadiusItem);
            showDetailInfo(mSelectedRadiusItem);

            llUnderLayout.setVisibility(View.VISIBLE);
            isTouchedMarker = false;
            mMapController.TwsMapMoveTo(Integer.parseInt(mSelectedRadiusItem.twx), Integer.parseInt(mSelectedRadiusItem.twy));
        } else {
            LOG.d("size of retApiSearchStationInfo : null");
        }
        dismissIntroDialog();
        dismissWaitDialog();
    }

    /**
     * 데이터가 0인 주유소 삭제
     *
     * @param searchPoiRadius
     * @return
     */
    private ArrayList<SearchPoiRadiusItem> removeStationPriceZero(SearchPoiRadius searchPoiRadius) {
        ArrayList<SearchPoiRadiusItem> tempPoi = new ArrayList<SearchPoiRadiusItem>();
        for (SearchPoiRadiusItem poiInfo : searchPoiRadius.poilist) {
            if (!poiInfo.pricessArr()[mPrefMgr.getOilType()].equals("0")) {
                tempPoi.add(poiInfo);
            }
        }
        return tempPoi.size() > 0 ? tempPoi : null;
    }

    protected PoiInfo retFindPoi(ArrayList<PoiInfo> poiList) {
        PrefMgr prefMgr = new PrefMgr(mContext);
        prefMgr.setMyTime(TimeUtil.getInstance());
        // mPrefMgr.setMyPosition(now_x+"|"+now_y);
        PoiInfo info = null;
        for (PoiInfo item : poiList) {
            if (item.poiid.equals(prefMgr.getFavoritePoiId())) {
                info = item;
            }
        }
        return info;
    }

    /**
     * 서버로 받은 데이터를 지도에 바인딩한다. 바인딩한 데이터를 hash_oil에 넣는다
     *
     * @param obj 서버로 받은 데이터
     */
    protected void retApiSearchPoiRadius(SearchPoiRadius obj) {
        mMapController.TwsMapRemoveDivPopup("marker_select");

        mHashPoi = new ConcurrentHashMap<String, SearchPoiRadiusItem>();

        rankLowPid = new String[]{"0", "0", "0"};

        SearchPoiRadius searchPoiRadius = (SearchPoiRadius) obj;

        searchPoiRadius.poilist = removeStationPriceZero(searchPoiRadius);

        mTempRadiusList = searchPoiRadius.poilist;
        bindMarkerOnTheMap(searchPoiRadius.poilist);

        dismissIntroDialog();
        dismissWaitDialog();

        if (mHashPoi.size() > 0) {
            String message = "주변에 " + mHashPoi.size() + "개의 주유소(" + oilTypeStrArr[mPrefMgr.getOilType()] + ")가 있습니다.";
            Toast.makeText(OilViewMainActivity.this, message, Toast.LENGTH_SHORT).show();
        } else {
            String message = "주변에 주유소(" + oilTypeStrArr[mPrefMgr.getOilType()] + ")가 없습니다.";
            Toast.makeText(OilViewMainActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 모든 마커와 최저가 마커 및 인기 마커, 2등, 3등 DIV를 그린다.
     *
     * @param searchPoiRadiusItems
     */
    private void bindMarkerOnTheMap(ArrayList<SearchPoiRadiusItem> searchPoiRadiusItems) {

        if (searchPoiRadiusItems != null) {
            StationUtil stationUtil = new StationUtil(searchPoiRadiusItems);
            stationUtil.sortStation(0, mPrefMgr.getOilType());
            SearchPoiRadiusItem lowestStation = stationUtil.getLowestStation();
            ArrayList<SearchPoiRadiusItem> get23 = stationUtil.getSecondThirdPreiceStation();
            SearchPoiRadiusItem popularStation = stationUtil.getPopularStation(mPrefMgr.getOilType());
            if (popularStation != null) {
                mPopularPid = popularStation.pid;
            } else {
                mPopularPid = "";
            }
            mSearchPoiRadiusItems = stationUtil.getSortedStation();
            for (int i = mSearchPoiRadiusItems.size() - 1; i >= 0; i--) {
                SearchPoiRadiusItem item = searchPoiRadiusItems.get(i);
                mHashPoi.put(item.pid, item);
                drawAllMarker(Integer.parseInt(item.twx), Integer.parseInt(item.twy), item.pid, item.poll);
                LOG.d("이름 : " + item.name + "가격" + item.pricessArr()[mPrefMgr.getOilType()] + " 즐찾카운트 : " + item.gp_cnt + "||" + item.gn_cnt + "||" + item.dn_cnt + "||" + item.lpg_cnt + "회사 " + item.poll);
            }
            //모든 마커 그리기E

            //2등 ,3등 마커 그리기S
            if (get23 != null) {
                for (int i = 0; i < get23.size(); i++) {
                    SearchPoiRadiusItem radiusItem = get23.get(i);
                    drawSecondThirdMarker(Integer.parseInt(radiusItem.twx),
                            Integer.parseInt(radiusItem.twy), radiusItem.pid,
                            radiusItem.pricessArr()[mPrefMgr.getOilType()], i);
                }
            }
            //2등 ,3등 마커 그리기E

            //최저가 DIV 그리기S
            if (mPrefMgr.getEnterType() == ENTER_LOW) {
                if (lowestStation != null) {
                    showDetailInfo(lowestStation);
                    llUnderLayout.setVisibility(View.VISIBLE);
                    isTouchedMarker = true;
                    drawLowestMarker(Integer.parseInt(lowestStation.twx),
                            Integer.parseInt(lowestStation.twy), lowestStation.pid,
                            lowestStation.pricessArr()[mPrefMgr
                                    .getOilType()]);
                    mSelectedRadiusItem = lowestStation;
                    drawSelectedMarker(mSelectedRadiusItem);
                } else {

                }
                mPrefMgr.setEnterType(ENTER_NORMAL);
            } else if (mPrefMgr.getEnterType() == ENTER_NORMAL) {
                if (lowestStation != null) {
                    isTouchedMarker = false;
                    drawLowestMarker(Integer.parseInt(lowestStation.twx),
                            Integer.parseInt(lowestStation.twy), lowestStation.pid,
                            lowestStation.pricessArr()[mPrefMgr
                                    .getOilType()]);
                } else {

                }
                mPrefMgr.setEnterType(ENTER_NORMAL);
            }
            //최저가 DIV 그리기E
        }
    }

    /**
     * @param selstedItem
     * @author JONY
     * 선택된 마커를 그린다. 말풍선이 있을경우 말풍선을 가장 위에 layer에 그린다.
     * 선택된 말풍선이 제일 위에 올라오도록 선택된 마커와 DIV를 다시 그림.
     */
    private void drawSelectedMarker(SearchPoiRadiusItem selstedItem) {

        String selectedPid = selstedItem.pid;

        for (int i = 0; i < rankLowPid.length; i++) {
            SearchPoiRadiusItem lowItem = mHashPoi.get(rankLowPid[i]);

            if (lowItem != null && selectedPid.equals(lowItem.pid)) {
                mMapController.TwsMapRemoveDivPopup("low_" + lowItem.pid);

                int x = Integer.parseInt(lowItem.twx);
                int y = Integer.parseInt(lowItem.twy);
                String poiid = lowItem.pid;
                String price = lowItem.pricessArr()[mPrefMgr.getOilType()];


                if (i == 0) {
                    // 최저가
                    drawLowestMarker(x, y, poiid, price);
                } else if (i == 1) {
                    // 2
                    drawSecondThirdMarker(x, y, poiid, price, 1);
                } else if (i == 2) {
                    // 3
                    drawSecondThirdMarker(x, y, poiid, price, 0);
                }
            }

        }

        isTouchedMarker = true;

        llUnderLayout.setVisibility(View.VISIBLE);
        int nX = Integer.parseInt(selstedItem.twx);
        int nY = Integer.parseInt(selstedItem.twy);
        mMapController.TwsMapMoveTo(nX, nY);

        showDetailInfo(selstedItem);

        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyNAME,
                selstedItem.name);
        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyXY, nX + "|"
                + nY);

        String imgPathBrand = null;

        if (selectedPid.equals(mPrefMgr.getFavoritePoiId())) {
            imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(OilViewMainActivity.FAVORITE_MARKER_IMAGE);// 수정
        } else {
            if (mPopularPid.equals(selectedPid)) {
                imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(OilViewMainActivity.POPULAR_MARKER_IMAGE);// 수정
            } else {
                imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(selstedItem.poll);// 수정
            }
        }

        String imgPathPoint = "/android_asset/mapicon/map_point_s.png"; // 수정
        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "s|" + selstedItem.pid;

        mMapController.TwsMapOilviewDivPopupDoubleCallBack("marker_select", nX,
                nY, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
    }

    /**
     * 브랜드 아이콘을 poll에 따라 분기 친다.
     * - NH와 NCOIL은 같다.
     *
     * @param poll
     * @return 해당 poll의 이미지 path
     */
    private String getBrandIconImagePath(String poll) {
        String realPath = "frm_map_point_poi_etc.png";
        if (poll.equals(StationBrand.SKE) || poll.equals(StationBrand.SKG)) {// SK
            realPath = "map_brand_01.png";
        } else if (poll.equals(StationBrand.GS)) {// GS
            realPath = "map_brand_02.png";
        } else if (poll.equals(StationBrand.HD)) {// 현대
            realPath = "map_brand_03.png";
        } else if (poll.equals(StationBrand.SOIL)) {// S-OIL
            realPath = "map_brand_04.png";
        } else if (poll.equals(StationBrand.NH) || poll.equals(StationBrand.NC)) {// NH or NCOIL
            realPath = "map_brand_05.png";
        } else if (poll.equals(StationBrand.RTO) || poll.equals(StationBrand.RTX)) {// 알뜰
            realPath = "map_brand_06.png";
        } else if (poll.equals(StationBrand.E1)) {// E1
            realPath = "map_brand_07.png";
        } else if (poll.equals(StationBrand.ETC)) {// 기타
            realPath = "map_brand_08.png";
        } else if (poll.equals(OilViewMainActivity.FAVORITE_MARKER_IMAGE)) {
            realPath = "map_brand_09.png";
        } else if (poll.equals(OilViewMainActivity.POPULAR_MARKER_IMAGE)) {
            realPath = "map_brand_popularity.png";
        } else {

        }
        return realPath;
        // return "frm_map_point_poi_etc.png";
    }

    protected void finishUI(String title, String msg) {
        System.err.println("finishUI");
        // TODO Auto-generated method stub
        dismissWaitDialog();
        showConfirmPopup("네트워크 안내", getResources().getString(R.string.network_err_request_timeout));
//		makeDialog(title, msg, 0);

        // 종료처리.
    }

    protected void startUI(String longi, String lati) {
        // TODO Auto-generated method stub
        // poi 정보 가져오기.
        mMapController.TwsMapMoveTo(Integer.parseInt(longi),
                Integer.parseInt(lati));
        // mMapController.TWsMapGetExtent();
        String[] myXY = mPrefMgr.getMyPosition().split("\\|");
        setMyPosition(myXY[0], myXY[1]);
        if (lastLevel > LIMIT_MAP_LEVEL) {
//            apiSearchPoiAround(longi, lati);
            apiSearchPoiRadius(longi, lati);
        } else {
            toDoMapOverLevel();
            dismissWaitDialog();
        }
    }

    private void requestLocation() {
        // GMS 측위 요청.
        // makeWaitDialog();
        LocationGMS location = new LocationGMS(OilViewMainActivity.this, locationResultHandler);
        location.connect();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        if (webview != null) {
            rootLayout.removeView(webview);
        }
        // singleton webview 객체 삭제 S
        WebViewSingleton.clear((TodayApplication) getApplication(),
                Defines.InstanceConst.MAIN_MAP_INSTANCE);
        // singleton webview 객체 삭제 E
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        super.finish();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        WidgetController.updateWidget(OilViewMainActivity.this, CustomWidget.class, "android.appwidget.action.ACTION_REFRESH");
        EndFFlurryAgent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivNavi:
                try {
                    if (mSelectedRadiusItem != null) {
                        String name = URLEncoder.encode(mSelectedRadiusItem.name, "UTF-8");
                        String x = mSelectedRadiusItem.twx;
                        String y = mSelectedRadiusItem.twy;
                        CoordLib.CoordClient client = new CoordLib.CoordClient();
                        Position_T data = client.getConvertLongLati(
                                Long.valueOf(x), Long.valueOf(y));
                        startActAirNormal(name, data.sLongSec, data.sLatiSec);
                    } else {
                        Toast.makeText(OilViewMainActivity.this,
                                "먼저 검색할 주유소를 선택해주세요", Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ivFavorite:
                if (selectedPid != null) {
                    if (mPrefMgr.getFavoriteName().equals(mHashPoi.get(selectedPid).name)) {
                        showConfirmPopupTwo("즐겨찾기", mHashPoi.get(selectedPid).name + removeFavorite, 2);
                    } else {
                        showConfirmPopupTwo("즐겨찾기", mHashPoi.get(selectedPid).name + addFavorite, 1);
                    }
                } else if (mSelectedRadiusItem != null) {
                    if (mPrefMgr.getFavoriteName().equals(mSelectedRadiusItem.name)) {
                        showConfirmPopupTwo("즐겨찾기", mSelectedRadiusItem.name + removeFavorite, 4);
                    } else {
                        showConfirmPopupTwo("즐겨찾기", mSelectedRadiusItem.name + addFavorite, 3);
                    }
                } else {
                    Toast.makeText(OilViewMainActivity.this,
                            "먼저 즐겨찾기 할 주유소를 선택해주세요.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ivMyposition:
                waypoint.setVisibility(View.GONE);
                llUnderLayout.setVisibility(View.GONE);
                isRequestButton = true;
                mMapController.TwsMapClearMarkers();
                makeWaitDialog();
                requestLocation();
                if (isTouchedMarker) {
                    isTouchedMarker = false;
                } else {
                    llUnderLayout.setVisibility(View.GONE);
                }
                break;
            case R.id.ivSearch:
                waypoint.setVisibility(View.GONE);
                llUnderLayout.setVisibility(View.GONE);
                makeWaitDialog();
                searchOil();
                isRequestButton = true;
                if (isTouchedMarker) {
                    isTouchedMarker = false;
                } else {
                    llUnderLayout.setVisibility(View.GONE);
                }
//			allowSearchAgain = false;
//			ivSearch.setEnabled(allowSearchAgain);
//			ivSearch.setBackgroundDrawable(getResources().getDrawable(
//					R.drawable.map_bt_ref_p));
                break;
            case R.id.btGoSetting:
                Intent gotoSetting = new Intent(this, SettingActivity.class);
                startActivityForResult(gotoSetting, 99);
                break;
        }
    }

    /**
     * 레벨이 낮을 경우 처리하는 작업
     * 토스트 띄워주고,
     * 마커 날리기
     */
    private void toDoMapOverLevel() {
        Toast.makeText(OilViewMainActivity.this, noSupportMapLevel, Toast.LENGTH_SHORT).show();
        mMapController.TwsMapClearMarkers();
        mMapController.TwsMapRemoveDivPopups();
    }

    private void redrawMarker() {
        mMapController.TwsMapRemoveDivPopups();
        bindMarkerOnTheMap(mTempRadiusList);
        drawSelectedMarker(mSelectedRadiusItem);
    }

    private void searchOil() {
        mMapController.TwsMapClearMarkers();
        // setMyPosition(now_X, now_Y);
        logFlurryAgent(DefineFlurry.keyMA_RELOAD, DefineFlurry.keyXY, now_X + "|" + now_Y);
        tvName.setText("");
        tvPrice.setText("");
        // mMapController.TWsMapGetExtent();
        if (lastLevel > LIMIT_MAP_LEVEL) {
//            apiSearchPoiAround(now_X, now_Y);
            apiSearchPoiRadius(now_X, now_Y);
        } else {
            toDoMapOverLevel();
            dismissWaitDialog();
        }
    }

    private void makeIntroDialog() {
        customIntroDialog = new CustomIntroDialog(OilViewMainActivity.this);
        customIntroDialog.setCancelable(false);
        customIntroDialog.show();
    }

    private void tutorialCheck() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPrefMgr.getIsFirst()) {
                    dismissIntroDialog();
                    Intent intent = new Intent(OilViewMainActivity.this,
                            TutorialActivity.class);
                    startActivityForResult(intent, 0);
                }
            }
        }, Toast.LENGTH_SHORT);
    }

    private void dismissIntroDialog() {
        if (customIntroDialog != null && customIntroDialog.isShowing())
            customIntroDialog.dismiss();
        customIntroDialog = null;
    }

    /**
     * 휘발유, 고급휘발유, 경유, LPG 정보를 저장.
     *
     * @param temp
     * @return
     */
    private String extractedPrices(SearchPoiRadiusItem temp) {
        String prices = new String();
        prices += temp.gp_price + "|";
        prices += temp.gn_price + "|";
        prices += temp.dn_price + "|";
        prices += temp.lpg_price;
        return prices;
    }

    private void gotoPlayStoreDialog(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                OilViewMainActivity.this);
        builder.setTitle(title)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.thinkware.inaviair"; // getPackageName()
                        // from
                        // Context
                        // or
                        // Activity
                        // object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse("market://details?id="
                                            + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id="
                                            + appPackageName)));
                        }
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private void makeWaitDialog() {
        if (customIntroDialog != null && customIntroDialog.isShowing()) {

        } else {
            if (waitDialog != null && waitDialog.isShowing()) {

            } else {
                waitDialog = new CustomProgress(OilViewMainActivity.this);
                waitDialog.show();
            }
        }
//		waitDialog = ProgressDialog.show(OilViewMainActivity.this, "", "");
    }

    private void dismissWaitDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            waitDialog.dismiss();
            waitDialog = null;
        }
    }

    private void StartFlurryAgent() {
        FlurryAgent.onStartSession(this, DefineFlurry.FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.onPageView();
        // FlurryAgent.setUserId("20141202_1");
        logFlurryAgent(DefineFlurry.eventIntro, DefineFlurry.keyView,
                DefineFlurry.vMain);
    }

    private void EndFFlurryAgent() {
        FlurryAgent.onEndSession(this);
    }

    private void logFlurryAgent(String event) {
        FlurryAgent.logEvent(event);
    }

    private void logFlurryAgent(String event, String key, String value) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(key, value);
        FlurryAgent.logEvent(event, params);
    }

    public void startActAirNormal(String encodedPoiName, String x, String y) {
        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            // intent.setData(Uri.parse("autorunair://com.tws.today.oilview.ui?flag=10&poiName=%EC%86%8C%EC%95%BC%EC%86%94%EB%B0%AD&poiX=128.11697&poiY=36.692303&lo=OIL"));
            intent.setData(Uri
                    .parse("autorunair://com.tws.today.oilview.ui?flag=10&poiName="
                            + encodedPoiName
                            + "&poiX="
                            + x
                            + "&poiY="
                            + y
                            + "&lo=OIL"));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            gotoPlayStoreDialog("정보",
                    "아이나비에어가 설치되어 있지 않습니다. 아래 버튼을 눌러 앱을 먼저 설치하세요.");
        }
    }

    // // add jony S
    // private void drawMarkerIcon(int longi, int lati, String poiId) {
    // LOG.d("drawMarkerIcon : "+longi + "||" + lati );
    //
    // drawAllMarker(longi, lati, poiId);
    //
    // String strPrice = drawSecondThirdMarker(longi, lati, poiId);
    //
    // drawLowestMarker(longi, lati, poiId, strPrice);
    //
    // }

    /**
     * 인기 주유소 마커 그리기
     *
     * @param longi
     * @param lati
     * @param pid
     */
    private void drawPopularMarker(int longi, int lati, String pid, String poll) {
        // marker S
        String imgPathBrand = null;
        String imgPathPoint = null;

        imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(poll);// 수정
        imgPathPoint = "/android_asset/mapicon/" + oilTypeMarker[mPrefMgr.getOilType()]; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "t|" + pid;

        mMapController.TwsMapOilviewDivPopupDoubleCallBack("t_" + pid, longi,
                lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
        // marker E
    }

    /**
     * 즐겨찾기 마커 그리기
     *
     * @param longi
     * @param lati
     * @param pid
     */
    private void drawFavoriteMarker(int longi, int lati, String pid) {
        // marker S
        String imgPathBrand = null;
        String imgPathPoint = null;

        tvFavoriteSoloNMCost.setText(mHashPoi.get(pid).name + " " + CommaGenerator.withCommaString(mHashPoi.get(pid).pricessArr()[mPrefMgr.getOilType()]) + "원");
        mPrefMgr.setFavoriteCost(extractedPrices(mHashPoi.get(pid)));

        imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(OilViewMainActivity.FAVORITE_MARKER_IMAGE);// 수정
        imgPathPoint = "/android_asset/mapicon/" + oilTypeMarker[mPrefMgr.getOilType()]; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "t|" + pid;

        mMapController.TwsMapOilviewDivPopupDoubleCallBack("t_" + pid, longi,
                lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
        // marker E
    }

    /**
     * 모든 마커 그리기
     *
     * @param longi
     * @param lati
     * @param pid
     */
    private void drawAllMarker(int longi, int lati, String pid, String poll) {
        // marker S
        String imgPathBrand = null;
        String imgPathPoint = null;

        if (mPrefMgr.getFavoritePoiId().equals(pid)) {//즐겨찾기일경우
            imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(OilViewMainActivity.FAVORITE_MARKER_IMAGE);// 수정
            tvFavoriteSoloNMCost.setText(mHashPoi.get(pid).name + " " + CommaGenerator.withCommaString(mHashPoi.get(pid).pricessArr()[mPrefMgr.getOilType()]) + "원");
            mPrefMgr.setFavoriteCost(extractedPrices(mHashPoi.get(pid)));
        } else {//즐겨찾기가 아닐경우
            if (mPopularPid.equals(pid)) {
                imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(OilViewMainActivity.POPULAR_MARKER_IMAGE);// 수정
            } else {
                imgPathBrand = "/android_asset/mapicon/" + getBrandIconImagePath(poll);// 수정
            }
        }
        imgPathPoint = "/android_asset/mapicon/" + oilTypeMarker[mPrefMgr.getOilType()]; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "t|" + pid;

        mMapController.TwsMapOilviewDivPopupDoubleCallBack("t_" + pid, longi,
                lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
        // marker E
    }

    /**
     * 최저가 마커 그리기
     *
     * @param longi
     * @param lati
     * @param poiId
     * @param strPrice
     */
    private void drawLowestMarker(int longi, int lati, String poiId,
                                  String strPrice) {
        int offX;
        int offY;
        String strImgPath;
        int nIconW;
        int nIconH;

        rankLowPid[0] = poiId;

        // popup S 최저가 팝업
        strImgPath = "/android_asset/mapicon/map_bubble_01.png";
        String strOilImgPath = "/android_asset/mapicon/"
                + oilTypeBubbleIcon[mPrefMgr.getOilType()]; // 수정

        nIconW = (int) (156 * WEB_RESIZE);
        nIconH = (int) (81 * WEB_RESIZE);
        offX = -((int) (156 * WEB_RESIZE)) / 2;
        offY = -(int) ((81 + OFFSET_VAL) * WEB_RESIZE);
        mMapController.TwsMapOilviewDivPopupWithImgText("low_" + poiId, longi,
                lati, nIconW, nIconH, strImgPath, offX, offY, strOilImgPath,
                CommaGenerator.withCommaString(strPrice) + "원");
        // popup E 최저가 팝업
    }

    /**
     * 2등 3등 마커 그리기.
     *
     * @param longi
     * @param lati
     * @param poiId
     * @return
     */
    private String drawSecondThirdMarker(int longi, int lati, String poiId, String strPrice, int rank) {
        int offX;
        int offY;
        // popup S 2,3 팝업
        String strImgPath = null;

        if (mHashPoi.size() >= 3) {
            if (rank == 0) {
                rankLowPid[2] = poiId;
                strImgPath = "/android_asset/mapicon/map_bubble_03.png"; // 수정
            } else if (rank == 1) {
                rankLowPid[1] = poiId;
                strImgPath = "/android_asset/mapicon/map_bubble_02.png"; // 수정
            }
        } else if (mHashPoi.size() == 2) {
            rankLowPid[1] = poiId;
            strImgPath = "/android_asset/mapicon/map_bubble_02.png"; // 수정
        }

        int nIconW = (int) (104 * WEB_RESIZE);
        int nIconH = (int) (63 * WEB_RESIZE);
        offX = -((int) (104 * WEB_RESIZE)) / 2;
        offY = -(int) ((63 + OFFSET_VAL) * WEB_RESIZE);
        mMapController.TwsMapOilviewDivPopupWithText("low_" + poiId, longi, lati,
                nIconW, nIconH, strImgPath, offX, offY,
                CommaGenerator.withCommaString(strPrice) + "원");
        // popup E 2,3 팝업
        return strPrice;
    }


    private void showConfirmPopup(String strTitle, String strContents) {

        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
        title.setText(strTitle);
        contents.setText(strContents);
        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
        confirmPopUp.setCancelable(false);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    /**
     * 자바독이 좋지 않다
     *
     * @param strTitle
     * @param strContents
     * @param type
     */
    private void showConfirmPopupTwo(String strTitle, String strContents, final int type) {
        final SearchPoiRadiusItem selectedRadiusItem = mSelectedRadiusItem;
        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup_two);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
        title.setText(strTitle);
        contents.setText(strContents);
        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_left);
        Button btnCancle = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_right);
        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case 1:
                        String prices1 = extractedPrices(selectedRadiusItem);
                        mPrefMgr.setFavoriteName(selectedRadiusItem.name);
                        mPrefMgr.setFavoriteCost(prices1);
                        mPrefMgr.setFavoriteBrand(selectedRadiusItem.poll);
                        mPrefMgr.setFavoritePoiId(selectedRadiusItem.pid);
                        apiAddRemoveFavorite(selectedRadiusItem.pid, String.valueOf(mPrefMgr.getOilType()));
                        tvFavoriteSoloNMCost.setText(mPrefMgr.getFavoriteName()
                                + " "
                                + mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr.getOilType()] + "원");
                        ivFavoriteSoloBrandIcon.setBackgroundDrawable(getResources()
                                .getDrawable(
                                        BrandIconChooser.getBrandIcon(mPrefMgr
                                                .getFavoriteBrand())));
                        logFlurryAgent(DefineFlurry.keyMA_FAVOR,
                                DefineFlurry.keyNAME, selectedRadiusItem.poll);
                        logFlurryAgent(DefineFlurry.keyMA_FAVOR,
                                DefineFlurry.keyXY, selectedRadiusItem.twx + "|"
                                        + selectedRadiusItem.twy);
                        Toast.makeText(
                                OilViewMainActivity.this,
                                mPrefMgr.getFavoriteName()
                                        + "가 즐겨찾기에 설정되었습니다.", Toast.LENGTH_SHORT).show();
                        ivFavoriteSoloBrandIcon.setVisibility(View.VISIBLE);
                        ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_selected);
                        redrawMarker();
                        break;
                    case 2:
                        Toast.makeText(
                                OilViewMainActivity.this,
                                mPrefMgr.getFavoriteName()
                                        + "가 즐겨찾기에서 삭제되었습니다.", Toast.LENGTH_SHORT).show();
                        mPrefMgr.removeFavorite();
                        ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_none);
                        ivFavoriteSoloBrandIcon.setVisibility(View.INVISIBLE);
                        tvFavoriteSoloNMCost.setText(getResources().getString(R.string.no_favorite));
                        ivFavoriteSoloBrandIcon.setBackgroundDrawable(null);
                        redrawMarker();
                    case 3:
                        String prices2 = extractedPrices(mSelectedRadiusItem);
                        mPrefMgr.setFavoriteName(mSelectedRadiusItem.name);
                        mPrefMgr.setFavoriteCost(prices2);
                        mPrefMgr.setFavoriteBrand(selectedRadiusItem.poll);
                        mPrefMgr.setFavoritePoiId(selectedRadiusItem.pid);
                        apiAddRemoveFavorite(selectedRadiusItem.pid, String.valueOf(mPrefMgr.getOilType()));
                        tvFavoriteSoloNMCost.setText(mPrefMgr
                                .getFavoriteName()
                                + " "
                                + mPrefMgr.getFavoriteCost().split("\\|")[mPrefMgr
                                .getOilType()] + "원");
                        ivFavoriteSoloBrandIcon.setBackgroundDrawable(getResources()
                                .getDrawable(
                                        BrandIconChooser.getBrandIcon(mPrefMgr
                                                .getFavoriteBrand())));
                        logFlurryAgent(DefineFlurry.keyMA_FAVOR,
                                DefineFlurry.keyNAME,
                                mSelectedRadiusItem.poll);
                        logFlurryAgent(DefineFlurry.keyMA_FAVOR,
                                DefineFlurry.keyXY, mSelectedRadiusItem.twx
                                        + "|" + mSelectedRadiusItem.twy);
                        Toast.makeText(
                                OilViewMainActivity.this,
                                mPrefMgr.getFavoriteName()
                                        + "가 즐겨찾기에 설정되었습니다.", Toast.LENGTH_SHORT).show();
                        ivFavoriteSoloBrandIcon.setVisibility(View.VISIBLE);
                        ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_selected);
                        redrawMarker();
                        break;
                    case 4:
                        Toast.makeText(
                                OilViewMainActivity.this,
                                mPrefMgr.getFavoriteName()
                                        + "가 즐겨찾기에서 삭제되었습니다.", Toast.LENGTH_SHORT).show();
                        ivFavorite.setBackgroundResource(R.drawable.effect_main_favorite_none);
                        ivFavoriteSoloBrandIcon.setVisibility(View.INVISIBLE);
                        tvFavoriteSoloNMCost.setText("즐겨찾기된 주유소가 없습니다.");
                        ivFavoriteSoloBrandIcon.setBackgroundDrawable(null);
                        mPrefMgr.removeFavorite();
                        redrawMarker();
                }
                confirmPopUp.dismiss();
            }
        });
        btnCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 0) {
                    finish();
                    System.exit(0);
                }
                confirmPopUp.dismiss();
            }
        });

        confirmPopUp.setCancelable(true);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
}
