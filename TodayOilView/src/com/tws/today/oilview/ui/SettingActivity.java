package com.tws.today.oilview.ui;

import android.app.Dialog;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.tws.activity.base.BaseActivity;
import com.tws.today.oil.data.DefineFlurry;
import com.tws.today.oil.util.WidgetController;
import com.tws.today.oil.widget.CustomWidget;

import java.util.HashMap;
import java.util.Map;

import tws.today.lib.mgr.PrefMgr;

public class SettingActivity extends BaseActivity implements OnClickListener{
	
	private RadioButton[] mRbRowArray;
	private LinearLayout llgoBackLayout = null;
	private TextView tvSettingCurrentVer = null;
	private Button btSettingUpdate = null;
	private LinearLayout llselectOilType = null;
	private TextView tvSelectedOilType = null;
	private PrefMgr prefMgr;
	private int oilType;
	private String[] oilArray = null;
    private int lastOliType = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_oilview_setting);
		prefMgr = new PrefMgr(SettingActivity.this);
		oilType = prefMgr.getOilType();
        lastOliType = oilType;
		setControll();
	}
	
	private void setControll(){
		oilArray = getResources().getStringArray(R.array.oilTypeArray);
		tvSelectedOilType = (TextView)findViewById(R.id.tvSelectedOilType);
		tvSelectedOilType.setText(String.valueOf(getResources().getStringArray(R.array.oilTypeArray)[oilType]));
		llgoBackLayout = (LinearLayout)findViewById(R.id.llgoBackLayout);
		tvSettingCurrentVer = (TextView)findViewById(R.id.tvSettingCurrentVer);
		btSettingUpdate = (Button)findViewById(R.id.btSettingUpdate);
		llselectOilType = (LinearLayout)findViewById(R.id.llselectOilType);
		
		try {
			tvSettingCurrentVer.setText("현재 버전 "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tvSettingCurrentVer.setText("현재 버전 1.0.0");
		}
		llgoBackLayout.setOnClickListener(this);
		llselectOilType.setOnClickListener(this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	private void StartFlurryAgent() {
		FlurryAgent.onStartSession(this, DefineFlurry.FLURRY_API_KEY);
		FlurryAgent.setLogEnabled(true);
		FlurryAgent.onPageView();
//		FlurryAgent.setUserId("20141202_1");
		logFlurryAgent(DefineFlurry.eventIntro, DefineFlurry.keyView, DefineFlurry.vMain);
	}

	private void EndFFlurryAgent() {
		FlurryAgent.onEndSession(this);
	}

	private void logFlurryAgent(String event) {
		FlurryAgent.logEvent(event);
	}

	private void logFlurryAgent(String event, String key, String value) {
		Map<String, String> params = new HashMap<String, String>();
		params.put(key, value);
		FlurryAgent.logEvent(event, params);
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.llgoBackLayout:
			onBackPressed();
			break;
		case R.id.llselectOilType:
			showOilTypePopup(prefMgr.getOilType());
			break;
		}
	}

    @Override
    public void onBackPressed() {
        oilType =  prefMgr.getOilType();
        if(lastOliType != oilType){
            setResult(9999);
            WidgetController.updateWidget(this, CustomWidget.class, "android.appwidget.action.ACTION_REFRESH");
        }else{
            setResult(0);
        }
        super.onBackPressed();
    }

    private void showConfirmPopup(String strTitle, String strContents){
		
		final Dialog confirmPopUp = new Dialog(this);
		confirmPopUp.setCanceledOnTouchOutside(false);
		confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmPopUp.setContentView(R.layout.layout_common_popup);
		
		TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
		TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
		title.setText(strTitle);
		contents.setText(strContents);
		Button btnConfirm  = (Button)confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				prefMgr.setOilType(oilType);
				prefMgr.setIsFirst(false);
				confirmPopUp.dismiss();
//				setResult(0);
//				finish();
			}
		});
		confirmPopUp.setCancelable(false);
		confirmPopUp.show();
		confirmPopUp.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
	}
	
	// oiiType popup S
	private void showOilTypePopup(int initChecked) {
		// TODO Auto-generated method stub
		final Dialog oilTypePopup = new Dialog(this);
		oilTypePopup.setCanceledOnTouchOutside(false);
		oilTypePopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
		oilTypePopup.setContentView(R.layout.layout_oiltype);

		mRbRowArray = new RadioButton[4];

		Button btnOilTypeConfirm = (Button) oilTypePopup
				.findViewById(R.id.olitype_btn_confirm);
		mRbRowArray[0] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_hg);
		mRbRowArray[1] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_g);
		mRbRowArray[2] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_d);
		mRbRowArray[3] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_l);

		mRbRowArray[initChecked].setChecked(true);
		
		RelativeLayout[] mRlRowArray = new RelativeLayout[4];

		mRlRowArray[0] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_hg);
		mRlRowArray[1] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_g);
		mRlRowArray[2] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_d);
		mRlRowArray[3] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_l);

		for (int i = 0; i < mRbRowArray.length; i++) {
			mRlRowArray[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					selectRadioBtn(v, mRbRowArray);
				}
			});
			mRbRowArray[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					selectRadioBtn(v, mRbRowArray);
				}
			});
		}
		
		btnOilTypeConfirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				oilTypePopup.dismiss();
				int pos = -1;
				String type = getCheckRadioBtn(mRbRowArray);

				Log.i("jony", "type " + type);

				// return type hg / g / d /l
				if (type.equals("hg")) {
					pos = 0;
				} else if (type.equals("g")) {
					pos = 1;
				} else if (type.equals("d")) {
					pos = 2;
				} else if (type.equals("l")) {
					pos = 3;
				}
				prefMgr.setOilType(pos);
//				showConfirmPopup("안내", oilArray[prefMgr.getOilType()]+"를 선택하셨습니다.\n"+oilArray[prefMgr.getOilType()]+" 기준으로 정보가 제공됩니다.");
				logFlurryAgent(DefineFlurry.keyMA_KIND, DefineFlurry.keyKind, DefineFlurry.vGasTypeArray[pos]);
				tvSelectedOilType.setText(String.valueOf(getResources().getStringArray(R.array.oilTypeArray)[pos]));
			}
		});
		
		oilTypePopup.setCancelable(true);
		oilTypePopup.show();
		oilTypePopup.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

	}

	private void selectRadioBtn(View v, RadioButton[] arrayRdBtn) {
		if (arrayRdBtn != null && v != null) {
			String tag = (String) v.getTag();

			if (!TextUtils.isEmpty(tag)) {
				int pos = 0;

				if (tag.equals("hg")) {
					pos = 0;
				} else if (tag.equals("g")) {
					pos = 1;
				} else if (tag.equals("d")) {
					pos = 2;
				} else if (tag.equals("l")) {
					pos = 3;
				}

				for (int i = 0; i < arrayRdBtn.length; i++) {
					if (pos != i) {
						arrayRdBtn[i].setChecked(false);
					} else {

						arrayRdBtn[i].setChecked(true);

					}
				}
			}

		}

	}

	private String getCheckRadioBtn(RadioButton[] arrayRdBtn) {

		String type = "";

		for (int i = 0; i < arrayRdBtn.length; i++) {

			if (arrayRdBtn[i].isChecked()) {
				type = (String) arrayRdBtn[i].getTag();
				break;
			}
		}

		return type;

	}
	// oilType popup E
	
}
