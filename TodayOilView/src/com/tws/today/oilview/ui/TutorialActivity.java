package com.tws.today.oilview.ui;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tws.activity.base.BaseActivity;

import tws.today.lib.mgr.PrefMgr;

public class TutorialActivity extends BaseActivity implements OnClickListener{
	
	private RadioButton[] mRbRowArray;
	private Button btOilSelector = null;
	private Button btSkip;
	String[] oilArray  = null;
	PrefMgr prefMgr =null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		prefMgr = new PrefMgr(TutorialActivity.this);
		setControl();
	}
	
	private void setControl(){
		oilArray = getResources().getStringArray(R.array.oilTypeArray);
		btOilSelector = (Button)findViewById(R.id.btSelectOilType);
		btOilSelector.setOnClickListener(this);
		btSkip = (Button)findViewById(R.id.btSkip);
		btSkip.setOnClickListener(this);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
//	private AlertDialog ConfirmDialog(Context context, boolean isSkip, final int OilType){
//		
//		AlertDialog.Builder ab = new AlertDialog.Builder(this);
//		String[] array = context.getResources().getStringArray(R.array.oilTypeArray);
//		ab.setTitle("선택된 유종");
//		if(isSkip){
//			//건너뛰기 누른 경우
//			ab.setMessage("기본값 \""+array[1]+"\" 기준으로 정보가 제공됩니다.\n설정페이지에서 변경 가능합니다.");
//		}else{
//			//선택으로 누른 경우
//			ab.setMessage("선택하신 유종은 \""+array[OilType]+"\" 입니다. 위젯에 해당 유종으로 정보 제공 됩니다.");
//		}
//		ab.setCancelable(false);
//		ab.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				prefMgr.setOilType(OilType);
//				prefMgr.setIsFirst(false);
//				setResult(0);
//				finish();
//			}
//		});
//		return ab.create();
//	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSkip:
//			ConfirmDialog(TutorialActivity.this, true, 0).show();;
			prefMgr.setOilType(1);
			showConfirmPopup("안내", "기본값 \""+oilArray[1]+"\" 기준으로 정보가 제공됩니다.\n설정페이지에서 변경 가능합니다.");
			break;
		case R.id.btSelectOilType:
			showOilTypePopup(prefMgr.getOilType());
			break;
		}
	}
	
		private void showConfirmPopup(String strTitle, String strContents){
			
			final Dialog confirmPopUp = new Dialog(this);
			confirmPopUp.setCanceledOnTouchOutside(false);
			confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
			confirmPopUp.setContentView(R.layout.layout_common_popup);
			
			TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
			TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
			title.setText(strTitle);
			contents.setText(strContents);
			Button btnConfirm  = (Button)confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
			btnConfirm.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
//					prefMgr.setOilType(oilType);
					prefMgr.setIsFirst(false);
					setResult(9090);
					finish();
				}
			});
			confirmPopUp.setCancelable(false);
			confirmPopUp.show();
			confirmPopUp.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
		}
		
	// oiiType popup S
		private void showOilTypePopup(int initChecked) {
			// TODO Auto-generated method stub
			final Dialog oilTypePopup = new Dialog(this);
			oilTypePopup.setCanceledOnTouchOutside(false);
			oilTypePopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
			oilTypePopup.setContentView(R.layout.layout_oiltype);

			mRbRowArray = new RadioButton[4];

			Button btnOilTypeConfirm = (Button) oilTypePopup
					.findViewById(R.id.olitype_btn_confirm);
			mRbRowArray[0] = (RadioButton) oilTypePopup
					.findViewById(R.id.olitype_radio_hg);
			mRbRowArray[1] = (RadioButton) oilTypePopup
					.findViewById(R.id.olitype_radio_g);
			mRbRowArray[2] = (RadioButton) oilTypePopup
					.findViewById(R.id.olitype_radio_d);
			mRbRowArray[3] = (RadioButton) oilTypePopup
					.findViewById(R.id.olitype_radio_l);

			mRbRowArray[initChecked].setChecked(true);
			
			RelativeLayout[] mRlRowArray = new RelativeLayout[4];

			mRlRowArray[0] = (RelativeLayout) oilTypePopup
					.findViewById(R.id.olitype_row_hg);
			mRlRowArray[1] = (RelativeLayout) oilTypePopup
					.findViewById(R.id.olitype_row_g);
			mRlRowArray[2] = (RelativeLayout) oilTypePopup
					.findViewById(R.id.olitype_row_d);
			mRlRowArray[3] = (RelativeLayout) oilTypePopup
					.findViewById(R.id.olitype_row_l);

			for (int i = 0; i < mRbRowArray.length; i++) {
				mRlRowArray[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectRadioBtn(v, mRbRowArray);
					}
				});
				mRbRowArray[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectRadioBtn(v, mRbRowArray);
					}
				});
			}
			
			btnOilTypeConfirm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					oilTypePopup.dismiss();
					int pos = -1;
					String type = getCheckRadioBtn(mRbRowArray);

					Log.i("jony", "type " + type);

					// return type hg / g / d /l
					if (type.equals("hg")) {
						pos = 0;
					} else if (type.equals("g")) {
						pos = 1;
					} else if (type.equals("d")) {
						pos = 2;
					} else if (type.equals("l")) {
						pos = 3;
					}
					prefMgr.setOilType(pos);
//					ConfirmDialog(TutorialActivity.this, false, pos).show();
//					showConfirmPopup("선택된 유종", "선택하신 유종은 \""+oilArray[pos]+"\" 입니다.\n위젯에 해당 유종으로 정보 제공 됩니다.");
//					showConfirmPopup("안내", oilArray[prefMgr.getOilType()]+"를 선택하셨습니다.\n"+oilArray[prefMgr.getOilType()]+" 기준으로 정보가 제공됩니다.");
					prefMgr.setIsFirst(false);
					setResult(9090);
					finish();
				}
			});
			oilTypePopup.setCancelable(true);
			oilTypePopup.show();
			oilTypePopup.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

		}

		private void selectRadioBtn(View v, RadioButton[] arrayRdBtn) {
			if (arrayRdBtn != null && v != null) {
				String tag = (String) v.getTag();

				if (!TextUtils.isEmpty(tag)) {
					int pos = 0;

					if (tag.equals("hg")) {
						pos = 0;
					} else if (tag.equals("g")) {
						pos = 1;
					} else if (tag.equals("d")) {
						pos = 2;
					} else if (tag.equals("l")) {
						pos = 3;
					}

					for (int i = 0; i < arrayRdBtn.length; i++) {
						if (pos != i) {
							arrayRdBtn[i].setChecked(false);
						} else {

							arrayRdBtn[i].setChecked(true);
						}
					}
				}

			}

		}

		private String getCheckRadioBtn(RadioButton[] arrayRdBtn) {

			String type = "";

			for (int i = 0; i < arrayRdBtn.length; i++) {

				if (arrayRdBtn[i].isChecked()) {
					type = (String) arrayRdBtn[i].getTag();
					break;
				}
			}

			return type;

		}
		// oilType popup E
}
