package com.tws.today.oil.data;

/**
 * Created by dskim on 2015-03-09.
 */
public class SearchPoiRadiusItem {
    public String pid;
    public String twx;
    public String twy;
    public String distance;
    public String name;
    public String poll;
    public String gn_cnt;
    public String gp_cnt;
    public String dn_cnt;
    public String lpg_cnt;
    public String rank_cnt;
    public String address;
    public String roadaddr;
    public String tel;
    public String wash;
    public String fix;
    public String mart;
    public String updatetime;
    public String gn_price;
    public String gp_price;
    public String dn_price;
    public String lpg_price;
    public String gn_time;
    public String gp_time;
    public String dn_time;
    public String lpg_time;

    public String gn_type;
    public String gp_type;
    public String dn_type;
    public String lpg_type;

    public String[] popularArr() {
        return new String[]{gp_type, gn_type, dn_type, lpg_type};
    }
    public String[] favArr() {
        return new String[]{gp_cnt, gn_cnt, dn_cnt, lpg_cnt};
    }
    public String[] pricessArr() {
        return new String[]{gp_price, gn_price, dn_price, lpg_price};
    }

    public String result;
    public String errormsg;
}