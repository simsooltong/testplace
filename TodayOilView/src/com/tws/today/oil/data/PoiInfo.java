package com.tws.today.oil.data;

public class PoiInfo{

	public String poiid;
	public String depth;
	public String dpx;
	public String dpy;
	public String rpx;
	public String rpy;
	public String name1;
	public String name2;
	public String name3;
	public String name4;
	public String admcode;
	public String address;
	public String jibun;
	public String roadname;
	public String detailaddress;
	public String catecode;
	public String catenmae;
	public String distance;
	public String tel;
	public String hasoildata;
	public String hasdetailinfo;
	public String hassubpoi;
	public OilData oildata;
	public SubPoi subpoi;
	public String adv_count;
	public ADInfo AdInfo;

}
