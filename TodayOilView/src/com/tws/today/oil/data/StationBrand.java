package com.tws.today.oil.data;

/**
 * Created by dskim on 2015-03-12.
 */
public class StationBrand {
    public static String SKE = "SKE"  ;
    public static String GS = "GSC"  ;
    public static String HD = "HDO"  ;
    public static String SOIL = "SOL";
    public static String NH = "NHO"  ;
    public static String NC = "NCO"  ;
    public static String RTO = "RTO"  ;
    public static String RTX = "RTX"  ;
    public static String E1 = "E1G"  ;
    public static String ETC = "ETC" ;
    public static String SKG = "SKG" ;
}
