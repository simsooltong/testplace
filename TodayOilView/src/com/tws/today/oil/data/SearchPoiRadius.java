package com.tws.today.oil.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-03-09.
 */
public class SearchPoiRadius {
    public String result;
    public String errormsg;
    public String totalcount;
    public String count;
    public ArrayList<SearchPoiRadiusItem> poilist;
}
