package com.tws.today.oil.network;

import android.content.Context;

import com.androidquery.callback.AjaxCallback;
import com.tws.today.oil.data.SearchCoordAddr;
import com.tws.today.oil.data.SearchPoi;
import com.tws.today.oil.data.SearchPoiFavorite;
import com.tws.today.oil.data.SearchPoiRadius;
import com.tws.today.oil.data.SearchPoiRadiusInfo;

import org.json.JSONException;
import org.json.JSONObject;

import tws.today.lib.util.LOG;

public class CommonApi extends ApiBase {

    private final String URL_SEARCHPOI_AROUND = "/iNaviAir/air/sire/searchPoiAround";
    private final String URL_SEARCHPOI_NUMBER = "/iNaviAir/air/sire/searchPoiNumber";
    private final String URL_SEARCHPOI_DETAIL = "/iNaviAir/air/sire/searchPoiDetail";
    private final String URL_SEARCH_COOR_ADDR = "/iNaviAir/air/sire/searchCoordAddr";
    private final String URL_SEARCHPOI_RADIUS = "/TodayOil/search/radius";
    private final String URL_SEARCHPOI_STATION = "/TodayOil/station/info";
    private final String URL_SEARCHPOI_FAVORITE = "/TodayOil/favorite/manage";


    public CommonApi() {
        super();
    }

    public void apiSearchCoordAddr(Context context, String longi, String lati,
                                   AjaxCallback<SearchCoordAddr> cb) {

        cb.url(m_serverUrl.getUrlInaviAirAgent() + URL_SEARCH_COOR_ADDR);
        cb.type(SearchCoordAddr.class);

        JSONObject mJsonParam = new JSONObject();

        try {
            mJsonParam.put("coordtype", "0");
            mJsonParam.put("lonx", longi);
            mJsonParam.put("laty", lati);

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        SendAQueryAirJson(context, mJsonParam, cb);
    }

    /**
     * @param context
     * @param cateCode
     * @param spOpt
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param radius
     * @param sortOpt
     * @param curX
     * @param curY
     * @param cb
     * @deprecated 2015.03.10
     */
    public void apiSearchPoiAround(Context context, String cateCode,
                                   String spOpt, String x1, String y1, String x2, String y2,
                                   String radius, String sortOpt, String curX, String curY,
                                   AjaxCallback<SearchPoi> cb) {

        cb.url(m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_AROUND);
        cb.type(SearchPoi.class);

        JSONObject mJsonParam = new JSONObject();

        try {
            mJsonParam.put("cutflag", "0");
            mJsonParam.put("coordtype", "0");
            mJsonParam.put("startposition", "0");
            mJsonParam.put("reqcount", "100");
            mJsonParam.put("depth", "0");

            mJsonParam.put("catecode", cateCode);

            mJsonParam.put("spopt", spOpt);

            mJsonParam.put("x1", x1);
            mJsonParam.put("y1", y1);
            mJsonParam.put("x2", x2);
            mJsonParam.put("y2", y2);

            mJsonParam.put("radius", radius);

            mJsonParam.put("sortOpt", sortOpt);

            mJsonParam.put("cur_x", curX);
            mJsonParam.put("cur_y", curY);

            mJsonParam.put("home_x", "");
            mJsonParam.put("home_y", "");
            mJsonParam.put("fav_x", "");
            mJsonParam.put("fav_y", "");
            mJsonParam.put("accsrc", "MAP");

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        SendAQueryAirJson(context, mJsonParam, cb);
    }

    /**
     * @param context
     * @param name1
     * @param curX
     * @param curY
     * @param cb
     * @deprecated 2015.03.10
     */
    public void apiSearchPoiNumber(Context context, String name1, String curX, String curY, AjaxCallback<SearchPoi> cb) {
        LOG.d("REQUEST FAVORITE" + name1);
        cb.url(m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_NUMBER);
        cb.type(SearchPoi.class);

        JSONObject mJsonParam = new JSONObject();

        try {
            mJsonParam.put("cutflag", "0");
            mJsonParam.put("coordtype", "0");
            mJsonParam.put("startposition", "0");
            mJsonParam.put("reqcount", "20");
            mJsonParam.put("depth", "0");
            mJsonParam.put("option", "8");
            mJsonParam.put("query", name1);
            mJsonParam.put("cur_x", curX);
            mJsonParam.put("cur_y", curY);

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        SendAQueryAirJson(context, mJsonParam, cb);
    }

    /**
     * @param context
     * @param poiId
     * @param curX
     * @param curY
     * @param cb
     * @deprecated 2015.03.10
     */
    public void apiSearchPoiDetail(Context context, String poiId, String curX, String curY, AjaxCallback<SearchPoi> cb) {
        LOG.d("REQUEST FAVORITE" + poiId);
        cb.url(m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_DETAIL);
        cb.type(SearchPoi.class);

        JSONObject mJsonParam = new JSONObject();

        try {
            mJsonParam.put("cutflag", "0");
            mJsonParam.put("coordtype", "0");
            mJsonParam.put("startposition", "0");
            mJsonParam.put("reqcount", "20");
            mJsonParam.put("poiid", poiId);
            mJsonParam.put("cur_x", curX);
            mJsonParam.put("cur_y", curY);

            mJsonParam.put("home_x", "123456");
            mJsonParam.put("home_y", "123456");
            mJsonParam.put("fav_x", "123456");
            mJsonParam.put("fav_y", "123456");


        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        SendAQueryAirJson(context, mJsonParam, cb);
    }

    /**
     * @param context
     * @param curX
     * @param curY
     * @param radius
     * @param sort_type
     * @param sort_option
     * @param cb
     */
    public void apiSearchPoiRadius(Context context, String curX, String curY, String radius, String sort_type, String sort_option,
                                   AjaxCallback<SearchPoiRadius> cb) {
        String url = m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_RADIUS;
        cb.type(SearchPoiRadius.class);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("coordtype", "0");
            jsonObject.put("cur_x", curX);
            jsonObject.put("cur_y", curY);
            jsonObject.put("radius", radius);
            jsonObject.put("startposition", "0");
            jsonObject.put("reqcount", "100");
            jsonObject.put("sort_type", sort_type);
            jsonObject.put("sort_option ", sort_option);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SendAqueryPureJson(context, url, jsonObject, cb);
    }

    public void apiSearchPoiStation(Context context, String poiId, AjaxCallback<SearchPoiRadiusInfo> cb) {
        LOG.d("REQUEST FAVORITE" + poiId);
        String url = m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_STATION;
        cb.type(SearchPoiRadiusInfo.class);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pid", poiId);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        SendAqueryPureJsonItem(context, url, jsonObject, cb);
    }

    public void apiAddRemoveFavorite(Context context, String pid,  String type, AjaxCallback<SearchPoiFavorite> cb) {
        String url = m_serverUrl.getUrlInaviAirAgent() + URL_SEARCHPOI_FAVORITE;
        cb.type(SearchPoiFavorite.class);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pid", pid);
            jsonObject.put("type", type);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        SendAqueryPureJsonFavorite(context, url, jsonObject, cb);
    }

}
