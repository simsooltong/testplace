package com.tws.today.oil.network;

public class RetCode {

	public static final int NET_ERROR_TIMEOUT = -101; // 네트워크 타임아웃
	public static final int HTTP_SUCC = 200;

	public static final int NET_SUCC = 0;
	
	public static final int NET_ERROR = 900; // 네트워크 오류.
	public static final int NET_ERROR_MSG = 901; // 네트워크 오류(서버 문구)
	
	public static final int RET_SEARCHPOI_SUCC = 1000;
	public static final int RET_SEARCHPOI_NUMBER_SUCC = 1002;

	public static final int RET_SEARCH_COORD_ADDR_SUCC = 1003;
	public static final int RET_SEARCH_COORD_ADDR_FAIL = 1004;

	public static final int RET_SEARCHPOI_FAIL = 1005;
	public static final int RET_SEARCHPOI_NUMBER_FAIL = 1006;

    public static final int RET_SEARCHPOI_DETAIL_SUCC = 1007;
    public static final int RET_SEARCHPOI_DETAIL_FAIL = 1008;

    public static final int RET_SEARCHPOI_RADIUS_SUCC = 1101;//주변 주유소 검색 성공
    public static final int RET_SEARCHPOI_RADIUS_FAIL = 1102;//주변 주유소 검색 실패

    public static final int RET_SEARCHPOI_STATION_SUCC = 1103;//지정 주유소 검색 성공
    public static final int RET_SEARCHPOI_STATION_FAIL = 1104;//지정 주유소 검색 실패

    public static final int RET_SEARCHPOI_FAVORITE_SUCC = 1105;//즐겨찾기 추가/삭제 성공
    public static final int RET_SEARCHPOI_FAVORITE_FAIL = 1106;//즐겨찾기 추가/삭제 실패



}
