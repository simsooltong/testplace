package com.tws.today.oil.network;

import android.content.Context;
import android.os.Build;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.Transformer;
import com.google.gson.Gson;
import com.tws.today.oil.data.SearchPoiFavorite;
import com.tws.today.oil.data.SearchPoiRadius;
import com.tws.today.oil.data.SearchPoiRadiusInfo;
import com.tws.today.oil.util.DeviceInfo;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tws.today.lib.util.LOG;

public class ApiBase {

	protected ServerURL m_serverUrl;
	private int mRetry;
	private int mTimeout;

	public ApiBase() {
		mRetry = 2; // 2회
		mTimeout = 10000; // 10초
		m_serverUrl = new ServerURL();
	}

    /**
     * 네트워크 모듈 AQuery 처리
     *  2015.03.10
     *  종현씨가 새로 뽑아준 요청 규격
     *  맵형태의 값을 callback에 넣음.
     * @param context
     * @param cb
     */
    public void SendAqueryPureJsonFavorite(Context context, String url, JSONObject jsonObject, AjaxCallback<?> cb) {
        LOG.d("[ApiBase] SendAqueryPureJson NEW");

        AQuery aq = new AQuery(context);

        // 공통 설정 부분 S
        cb.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        cb.header("AuthKey", "A3tfxPqMs4OQ2itNRTXyQk3QIhCMVt22ZlgHSzdADXWEVB3ZTRxsvLlDfsJ+5yt/");

        // enc type gzip 설정
        cb.header("Accept-Encoding", "gzip");
        // cb.setGZip(true);

        // retry set
        cb.retry(mRetry);
        // 10초 타임아웃
        cb.timeout(mTimeout);
        // 공통 설정 부분 E
        // GSON 연동 처리.
        GsonTransformer transformer = new GsonTransformer();
        aq.transformer(transformer);
        // 서버 연동
//        aq.ajax(cb);
        aq.post(url, jsonObject, SearchPoiFavorite.class, (AjaxCallback<SearchPoiFavorite>) cb);
    }

    /**
     * 네트워크 모듈 AQuery 처리
     *  2015.03.10
     *  종현씨가 새로 뽑아준 요청 규격
     *  맵형태의 값을 callback에 넣음.
     * @param context
     * @param cb
     */
    public void SendAqueryPureJson(Context context, String url, JSONObject jsonObject, AjaxCallback<?> cb) {
        LOG.d("[ApiBase] SendAqueryPureJson NEW");

        AQuery aq = new AQuery(context);

        // 공통 설정 부분 S
        cb.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        cb.header("AuthKey", "A3tfxPqMs4OQ2itNRTXyQk3QIhCMVt22ZlgHSzdADXWEVB3ZTRxsvLlDfsJ+5yt/");

        // enc type gzip 설정
        cb.header("Accept-Encoding", "gzip");
        // cb.setGZip(true);

        // retry set
        cb.retry(mRetry);
        // 10초 타임아웃
        cb.timeout(mTimeout);
        // 공통 설정 부분 E
        // GSON 연동 처리.
        GsonTransformer transformer = new GsonTransformer();
        aq.transformer(transformer);
        // 서버 연동
//        aq.ajax(cb);
        aq.post(url, jsonObject, SearchPoiRadius.class, (AjaxCallback<SearchPoiRadius>) cb);
    }

    /**
     * 네트워크 모듈 AQuery 처리
     *  2015.03.10
     *  종현씨가 새로 뽑아준 요청 규격
     *  맵형태의 값을 callback에 넣음.
     * @param context
     * @param cb
     */
    public void SendAqueryPureJsonItem(Context context, String url, JSONObject jsonObject, AjaxCallback<?> cb) {
        LOG.d("[ApiBase] SendAqueryPureJsonItem NEW");

        AQuery aq = new AQuery(context);

        // 공통 설정 부분 S
        cb.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        cb.header("AuthKey", "A3tfxPqMs4OQ2itNRTXyQk3QIhCMVt22ZlgHSzdADXWEVB3ZTRxsvLlDfsJ+5yt/");

        // enc type gzip 설정
        cb.header("Accept-Encoding", "gzip");
        // cb.setGZip(true);

        // retry set
        cb.retry(mRetry);
        // 10초 타임아웃
        cb.timeout(mTimeout);
        // 공통 설정 부분 E
        // GSON 연동 처리.
        GsonTransformer transformer = new GsonTransformer();
        aq.transformer(transformer);
        // 서버 연동
//        aq.ajax(cb);
        aq.post(url, jsonObject, SearchPoiRadiusInfo.class, (AjaxCallback<SearchPoiRadiusInfo>) cb);
    }



	/**
	 * 네트워크 모듈 AQuery 처리
	 * 
	 * @param context
	 * @param mJsonParam
	 * @param cb
	 */
	public void SendAQueryAirJson(Context context, JSONObject mJsonParam, AjaxCallback<?> cb) {
		LOG.d("[ApiBase] SendAqueryPureJson OLD");

		AQuery aq = new AQuery(context);

		// 공통 설정 부분 S
		cb.header("Content-Type", "application/x-www-form-urlencoded");

//		AuthParser authParser = new AuthParser(context);
//
//		// auth ver2
//		com.thinkware.air.auth.INaviAirAuth auth = com.thinkware.air.auth.INaviAirAuth
//				.getInstance(authParser.getModel(), authParser.getIMEI(),
//						authParser.getMDN());
//
//		String strAuth = "";
//		if (auth != null)
//			strAuth = auth.getAuthString();

		cb.header("auth_key", DeviceInfo.getAuth(context));

        cb.header("source", "Android");
//		cb.header("device", authParser.getModel());
//		cb.header("mdn", authParser.getMDN());
		cb.header("device", Build.DEVICE);
		cb.header("mdn", DeviceInfo.getMDN());

		// enc type gzip 설정
		cb.header("Accept-Encoding", "gzip");
		// cb.setGZip(true);

		// retry set
		cb.retry(mRetry);

		// 10초 타임아웃
		cb.timeout(mTimeout);

		// 공통 설정 부분 E

		// 파라미터 설정 부분.
		if (mJsonParam != null) {
			Map<String, Object> params = new HashMap<String, Object>();

            params.put("request_param", mJsonParam.toString());

			LOG.d("[ApiBase] SendAqueryPureJson AQuery strParam : "
					+ mJsonParam.toString());

			// 파라미터 설정
			cb.params(params);
		}

		// GSON 연동 처리.
		GsonTransformer transformer = new GsonTransformer();
		aq.transformer(transformer);

		// 서버 연동
		aq.ajax(cb);
	}
	/**
	 * @return the mRetry
	 */
	protected int getRetry() {
		return mRetry;
	}

	/**
	 * @param mRetry
	 *            the mRetry to set
	 */
	protected void setRetry(int mRetry) {
		this.mRetry = mRetry;
	}

	/**
	 * @return the mTimeout
	 */
	protected int getTimeout() {
		return mTimeout;
	}

	/**
	 * @param mTimeout
	 *            the mTimeout to set
	 */
	protected void setTimeout(int mTimeout) {
		this.mTimeout = mTimeout;
	}

	/**
	 * GSON 처리를 위한 클랙스.
	 * 
	 * @author jonychoi
	 * 
	 */
	public class GsonTransformer implements Transformer {

		@Override
		public <T> T transform(String url, Class<T> type, String encoding,
				byte[] data, AjaxStatus status) {
			// TODO Auto-generated method stub

			Gson g = new Gson();

			return g.fromJson(new String(data), type);
		}
	}
}
