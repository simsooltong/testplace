package com.tws.today.oil.network;


public class ServerURL {
	
	// 상용 / 개발 서버 처리 (true / false)
	private final boolean bServerType = false;
		  
	////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//  Server address 
    public final String URL_REAL_DOMAIN_SERVER        	= "https://api.inavi.com:447";
    public final String URL_DEV_DOMAIN_SERVER         	= "https://dev-api.inavi.com:447";
    
//    public final String URL_REAL_AIRAGENT_SERVER      = "http://218.237.50.88/tAir";
    public final String URL_REAL_AIRAGENT_SERVER       = "https://airagt.inavi.com";
    public final String URL_DEV_AIRAGENT_SERVER       	= "http://61.33.249.180:8020";
  
	public ServerURL(){		
		
	}
	
	// web
	public String getUrlInaviApi(){
		if(bServerType) return URL_REAL_DOMAIN_SERVER;
		else return URL_DEV_DOMAIN_SERVER;
	}	
		
	// agent
	public String getUrlInaviAirAgent(){
		if(bServerType) return URL_REAL_AIRAGENT_SERVER;
		else return URL_DEV_AIRAGENT_SERVER;
	}	
	
	public boolean isReal(){
		return bServerType;
	}

	
	
}
