package com.tws.today.oil.widget;

import android.appwidget.AppWidgetProvider;

public class CustomWidgetBlack extends AppWidgetProvider {
    /*private static final String DEFAULT_MAP_LV = "2400";
    private static final int INTERVAL_REFRESH = 1800000;
    private final static String ACTION_UPDATE_DIRECT = "android.appwidget.action.ACTION_UPDATE_DIRECT";
    private final static String ACTION_REFRESH = "android.appwidget.action.ACTION_REFRESH_B";
    private final static String ACTION_GO_TO_ACTIVITY = "android.appwidget.action.ACTION_GO_TO_ACTIVITY_B";
    private final static String ACTION_GO_TO_ACTIVITY_FAVORITE = "android.appwidget.action.ACTION_GO_TO_ACTIVITY_FAVORITE_B";
    private final static String ACTION_GO_TO_ACTIVITY_LOW = "android.appwidget.action.ACTION_GO_TO_ACTIVITY_LOW_B";
    private final static String ACTION_REFRESH_LURE = "android.appwidget.action.ACTION_REFRESH_LURE_B";
    private RemoteViews rv = null;;
    private PendingIntent pendingIntent= null;
    private AlarmManager alarmManager = null;
    private CommonApi netCommonApi = null;
    private Context context = null;
    protected PoiInfo lowPoiInfo;
    protected String[] avgInfo;
    private AppWidgetManager appWidgetManager;
    private ComponentName cpName;
    private String now_x = null;
    private String now_y = null;
    private Runnable runnable = null;
    private Handler visibleHandler = null;
    private boolean isTimeOutRefresh = false;
    private String juso = null;

    private long requestTime = 0;
    private long responseTime = 0;

    private long getNowTime(){
        return System.currentTimeMillis();
    }

    private Handler locationHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            PrefMgr prefMgr = new PrefMgr(context);
            String xyStr[]  = null;
            switch (msg.what) {
                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_TIMEOUT:
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    xyStr = prefMgr.getMyPosition().split("\\|");
                    now_x = xyStr[0];
                    now_y = xyStr[1];
                    apiSearchPoiAround(now_x,now_y);
                    new PrefMgr(context).setUpdateTime(getNowTime()+10000L);
                    Toast.makeText(context, context.getResources().getString(R.string.gps_off), Toast.LENGTH_SHORT).show();
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");
                    if (msg.obj != null) {
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "location getAccuracy : "+ location.getAccuracy());
                        Log.i("LocationResultHandler", "location getLongitude : "+ location.getLongitude());
                        Log.i("LocationResultHandler", "location getLatitude : "+ location.getLatitude());
                        xyStr = convertXY(location);
                        now_x = xyStr[0];
                        now_y = xyStr[1];
                        apiSearchCoorAddr(now_x, now_y);
                        prefMgr.setMyPosition(now_x+"|"+now_y);
                        apiSearchPoiAround(now_x,now_y);
                        prefMgr.setMyTime(TimeUtil.getInstance());
                        new PrefMgr(context).setUpdateTime(getNowTime()+10000L);
					*//*	데이터 요청을 최소화하는 로직
					 * 	취소됨				
					 * 
  					if(prefMgr.getChangeOil()){
						apiSearchPoiAround(now_x,now_y);
						prefMgr.setMyTime(TimeUtil.getInstance());
						prefMgr.setChangeoil(false);
					}else{
						if(distanceValidation(now_x, now_y)){
							apiSearchPoiAround(now_x,now_y);
							prefMgr.setMyTime(TimeUtil.getInstance());
						}else{
							if(timeValidation()){
								apiSearchPoiAround(now_x,now_y);
							}else{
								new Handler().postDelayed(new Runnable() {
									@Override
									public void run() {
										rv.setViewVisibility(R.id.llProgressBar, View.GONE);
										appWidgetManager.updateAppWidget(cpName, rv);
									}
								}, 2000);
							}
						}
					}*//*

                    } else {

                    }
                    break;
                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    Toast.makeText(context, "측위실패", Toast.LENGTH_SHORT).show();
                    break;

            }

        }
    };

    *//**
     * 네트워크 통신 결과 핸들러.
     *//*
    public Handler netResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            LOG.d("NetResult_Handler msg what" + msg.what);
            switch (msg.what) {

                case RetCode.RET_SEARCH_COORD_ADDR_SUCC:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
                    SearchCoordAddr addr = (SearchCoordAddr) msg.obj;
                    String[] tempJsuo = addr.cut_address.split(" ");
                    juso = tempJsuo[tempJsuo.length-1];

                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC"+addr.cut_address );
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC"+addr.cut_address.split(" ")[tempJsuo.length-1]);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    break;

                case RetCode.RET_SEARCHPOI_NUMBER_SUCC:
                    LOG.d("RET_SEARCHPOI_NUMBER_SUCC");
                    PrefMgr prefMgr = new PrefMgr(context);
                    SearchPoi searchNumberPoi = (SearchPoi)msg.obj;
                    searchNumberPoi.poi = removeNullDataForFavorite(searchNumberPoi);

                    PoiInfo tempInfo = retFindPoi(searchNumberPoi.poi);
                    if(tempInfo!=null){
                        if(tempInfo.name1!=null){
                            rv.setTextViewText(R.id.tvFavoritenNameBlack, tempInfo.name1);
//						rv.setViewVisibility(R.id.llProgressBar, View.INVISIBLE);
                            System.err.println("DSKIM prefMgr.getOilType()"+prefMgr.getOilType());
//						System.err.println("DSKIM hg "+tempInfo.oildata.pricessArr()[0]);
//						System.err.println("DSKIM g "+tempInfo.oildata.pricessArr()[1]);
//						System.err.println("DSKIM d "+tempInfo.oildata.pricessArr()[2]);
//						System.err.println("DSKIM l "+tempInfo.oildata.pricessArr()[3]);

                            if(tempInfo.oildata.pricessArr()[prefMgr.getOilType()].equals("0")){
                                rv.setTextViewText(R.id.tvFavoriteCostBlack, "제공안함");
                            }else{
                                rv.setTextViewText(R.id.tvFavoriteCostBlack, CommaGenerator.withCommaString(tempInfo.oildata.pricessArr()[prefMgr.getOilType()]) +" 원");
                            }
                            rv.setImageViewResource(R.id.ivGasStationFavoriteBlack, BrandIconChooser.getBrandIcon(tempInfo.catecode));
                            appWidgetManager.updateAppWidget(cpName, rv);
                        }else{
                            rv.setTextViewText(R.id.tvFavoritenNameBlack, context.getResources().getString(R.string.no_favorite));
                            rv.setImageViewBitmap(R.id.ivGasStationFavoriteBlack, null);
//					rv.setViewVisibility(R.id.llFavoriteLayout, View.GONE);
                            LOG.d("tempInfo.name1이 널입니다.");
                        }
                    }else{
                        rv.setTextViewText(R.id.tvFavoritenNameBlack, context.getResources().getString(R.string.no_favorite));
                        rv.setImageViewBitmap(R.id.ivGasStationFavoriteBlack, null);
                    }
                    break;

                case RetCode.RET_SEARCHPOI_SUCC:
                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();
                    LOG.d("RET_SEARCHPOI_SUCC");
                    PrefMgr mgr = new PrefMgr(context);

                    SearchPoi searchPoi = (SearchPoi)msg.obj;
                    searchPoi.poi = removeNullDataForPoi(searchPoi);

                    CalcPrice calcPrice =  new CalcPrice(searchPoi.poi);
                    lowPoiInfo =  calcPrice.getLowestPoi(mgr.getOilType());
                    avgInfo = calcPrice.getAveragePrice(mgr.getOilType());

                    if(lowPoiInfo!=null){
                        rv.setTextViewText(R.id.tvLowNameBlack, lowPoiInfo.name1);
                        mgr.setLowName(lowPoiInfo.name1);

                        if(lowPoiInfo.oildata.pricessArr()[mgr.getOilType()].equals("0")){
                            rv.setTextViewText(R.id.tvLowNameBlack, "주변에 "+context.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()]+"주유소가 없습니다.");
                            rv.setTextViewText(R.id.tvLowCostBlack, "");
                            rv.setImageViewBitmap(R.id.ivGasLowestBlack, null);
                        }else{
                            rv.setTextViewText(R.id.tvLowCostBlack, CommaGenerator.withCommaString(lowPoiInfo.oildata.pricessArr()[mgr.getOilType()])+" 원");
                            rv.setImageViewResource(R.id.ivGasLowestBlack, BrandIconChooser.getBrandIcon(lowPoiInfo.catecode));
                        }
                    }else{
                        rv.setTextViewText(R.id.tvLowNameBlack, "주변에 "+context.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()]+"주유소가 없습니다.");
                        rv.setTextViewText(R.id.tvLowCostBlack, "");
                        rv.setImageViewBitmap(R.id.ivGasLowestBlack, null);
                    }

                    if(avgInfo!=null){
                        if((avgInfo[0].equals("0") || lowPoiInfo.oildata.pricessArr()[mgr.getOilType()].equals("0"))){
                            rv.setTextViewText(R.id.tvAverageLocationBlack, "");
                        }else{
                            rv.setTextViewText(R.id.tvAverageLocationBlack, "- "+(juso==null ? "" : juso)+" 주변 3km 평균가 "+ CommaGenerator.withCommaString(avgInfo[0])+"원");
                        }
                    }else{
                        rv.setTextViewText(R.id.tvAverageLocationBlack, "");
                    }

                    rv.setViewVisibility(R.id.llProgressBarBlack, View.INVISIBLE);
                    if(mgr.getFavoriteName().length() <1){
//					rv.setViewVisibility(R.id.llFavoriteLayout, View.GONE);
                    }else{
                        rv.setViewVisibility(R.id.llUnderFavoriteLayoutBlack, View.VISIBLE);
//					apiSearchPoiNumber(mgr.getFavoriteName(), now_x, now_y);
                    }
                    rv.setTextViewText(R.id.tvLowestAndOilBlack, "최저가 ("+context.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()]+")");
                    appWidgetManager.updateAppWidget(cpName, rv);
                    break;
                case	RetCode.RET_SEARCH_COORD_ADDR_FAIL:
                    LOG.d("RET_SEARCH_COORD_ADDR_FAIL");
//				Toast.makeText(context, "RET_SEARCH_COORD_ADDR_FAIL", Toast.LENGTH_SHORT).show();
//				Toast.makeText(context, context.getResources().getString(R.string.network_err_request_timeout), Toast.LENGTH_SHORT).show();
                    break;

                case RetCode.NET_ERROR_MSG:
                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR_MSG), msg.obj.toString(), context.getPackageName());
                    LOG.d("NET_RETURN_ERROR_MSG");
                    rv.setViewVisibility(R.id.llTimeOverBlack, View.VISIBLE);
                    rv.setViewVisibility(R.id.llProgressBarBlack, View.INVISIBLE);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    Toast.makeText(context, msg.obj.toString()+"("+msg.arg1+")", Toast.LENGTH_SHORT).show();
                    // BaseMessagePopup(title, message, code, false, true);
                    break;

                case RetCode.NET_ERROR:
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR), "RetCode.NET_ERROR", context.getPackageName());
                    rv.setViewVisibility(R.id.llTimeOverBlack, View.VISIBLE);
                    rv.setViewVisibility(R.id.llProgressBarBlack, View.INVISIBLE);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    Toast.makeText(context, context.getResources().getString(R.string.network_err_request_timeout), Toast.LENGTH_SHORT).show();
                    LOG.d("NET_ERROR");
                    break;

            }
        }

    };

    *//**
     * OilData가 없은 데이터 추출
     * @param searchPoi
     * @return
     *//*
    private ArrayList<PoiInfo> removeNullDataForPoi(SearchPoi searchPoi) {
        PrefMgr prefMgr = new PrefMgr(context);
        ArrayList<PoiInfo> tempPoi = new ArrayList<PoiInfo>();
        System.err.println("size : "+searchPoi.poi.size()+"개");
        for (PoiInfo poiInfo : searchPoi.poi) {

            if (poiInfo.oildata != null) {
                if (!poiInfo.oildata.pricessArr()[prefMgr.getOilType()].equals("0")) {
                    tempPoi.add(poiInfo);
                }
            }
        }
        return tempPoi.size() > 0 ? tempPoi : null;
    }

    private ArrayList<PoiInfo> removeNullDataForFavorite(SearchPoi searchPoi) {
        PrefMgr prefMgr = new PrefMgr(context);
        ArrayList<PoiInfo> tempPoi = new ArrayList<PoiInfo>();
        System.err.println("size : "+searchPoi.poi.size()+"개");
        for (PoiInfo poiInfo : searchPoi.poi) {
            if (poiInfo.oildata != null) {
                if( poiInfo.poiid.equals(prefMgr.getFavoritePoiId())){
                    tempPoi.add(poiInfo);
                }
            }
        }
        return tempPoi.size() > 0 ? tempPoi : null;
    }

    *//**
     * @author JONY</br> 즐겨찾기로 검색.</br>
     *
     *//*
    private void apiSearchPoiNumber(String name1, String cur_x, String cur_y) {
        netCommonApi.apiSearchPoiNumber(context, name1, cur_x, cur_y, new AjaxCallback<SearchPoi>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoi object, AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("ApiSvcFindChild AQuery url : " + url);
                LOG.d("ApiSvcFindChild net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("ApiSvcFindChild AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoi sr = (SearchPoi) object;
                    System.out.println(object.toString());
                    // return data check
                    LOG.d("apiSearchPoiAround sr.result : " + sr.result);
                    LOG.d("apiSearchPoiAround sr.errormsg : "
                            + sr.errormsg);

                    if (sr.result == RetCode.NET_SUCC) {
                        // 성공
                        Message msg = netResultHandler.obtainMessage();
                        msg.what = RetCode.RET_SEARCHPOI_NUMBER_SUCC;
                        msg.obj = sr;
                        netResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = netResultHandler.obtainMessage();
                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;
                        msg.arg1 = sr.result;
                        netResultHandler.sendMessage(msg);
                    }

                } else {
                    // 실패
                    netResultHandler.sendEmptyMessage(RetCode.NET_ERROR);
                }
                super.callback(url, object, status);
            }
        });
    }


    protected PoiInfo retFindPoi(ArrayList<PoiInfo> poiList) {
        PrefMgr prefMgr = new PrefMgr(context);
        prefMgr.setMyTime(TimeUtil.getInstance());
        String poiId = prefMgr.getFavoritePoiId();
        PoiInfo info = new PoiInfo();
        if(poiList!=null){
            for (PoiInfo item : poiList) {
                if(item.poiid.equals(poiId)){
                    info = item;
                }
            }
            return info;
        }else{
            return null;
        }
    }

    *//**
     * 지도 주소 정보 요청 api
     *
     * @param longi
     * @param lati
     *//*
    private void apiSearchCoorAddr(String longi, String lati) {
        System.err.println("apiSearchCoorAddr call");
        netCommonApi.apiSearchCoordAddr(context, longi, lati,
                new AjaxCallback<SearchCoordAddr>() {
                    @Override
                    public void callback(String url, SearchCoordAddr object,
                                         AjaxStatus status) {
                        // TODO Auto-generated method stub

                        LOG.d("ApiSvcFindChild AQuery url : " + url);
                        LOG.d("ApiSvcFindChild net AQuery status.getCode() : " + status.getCode());

                        if (status.getCode() == RetCode.HTTP_SUCC) {
                            // http 200 성공
                            LOG.d("ApiSvcFindChild AQuery object.result : "
                                    + object.result);
                        } else {
                            // http 오류 나 네트워크 오류
                            object = null;
                        }

                        if (object != null) {

                            SearchCoordAddr sr = (SearchCoordAddr) object;

                            // return data check
                            LOG.d("apiSearchPoiAround sr.result : " + sr.result);
                            LOG.d("apiSearchPoiAround sr.errormsg : "
                                    + sr.errormsg);

                            if (sr.result == RetCode.NET_SUCC) {
                                // 성공
                                Message msg = netResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_SUCC;
                                msg.obj = sr;

                                netResultHandler.sendMessage(msg);

                            } else {
                                // 서버 오류 메시지 출력

                                Message msg = netResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_FAIL;
                                msg.obj = sr.errormsg;
                                msg.arg1 = sr.result;

                            }
                        } else {
                            // 실패
                        }
                        super.callback(url, object, status);
                    }
                });
    }

    private void apiSearchPoiAround(String longi, String lati) {
        // TODO Auto-generated method stub
        // api.apiSearchPoiAround(context, cateCode, spOpt, x1, y1, x2, y2,
        // radius, sortOpt, curX, curY, cb)
        // 네트워크 연결 여부 확인.

        netCommonApi.apiSearchPoiAround(context, "C0100", "2", longi,
                lati, "0", "0", DEFAULT_MAP_LV, "1", longi, lati,
                new AjaxCallback<SearchPoi>() {
                    // callback 처리.
                    @Override
                    public void callback(String url, SearchPoi object, AjaxStatus status) {
                        // TODO Auto-generated method stub

                        LOG.d("ApiSvcFindChild AQuery url : " + url);
                        LOG.d("ApiSvcFindChild net AQuery status.getCode() : "+ status.getCode());

                        int retCode = status.getCode();

                        if (retCode == RetCode.HTTP_SUCC) {
                            // http 200 성공
                            LOG.d("ApiSvcFindChild AQuery object.result : "
                                    + object.result);
                        } else {
                            // http 오류 나 네트워크 오류
                            object = null;
                        }

                        if (object != null) {

                            final SearchPoi sr = (SearchPoi) object;

                            // return data check
                            LOG.d("apiSearchPoiAround sr.result : " + sr.result);
                            LOG.d("apiSearchPoiAround sr.errormsg : "
                                    + sr.errormsg);

                            if (sr.result == RetCode.NET_SUCC) {
                                // 성공
                                Message msg = netResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCHPOI_SUCC;
                                msg.obj = sr;

                                netResultHandler.sendMessage(msg);

                            } else {
                                // 서버 오류 메시지 출력

                                Message msg = netResultHandler.obtainMessage();

                                msg.what = RetCode.NET_ERROR_MSG;
                                msg.obj = sr.errormsg;
                                msg.arg1 = sr.result;

                                netResultHandler.sendMessage(msg);
                            }
                        } else {

                            if(retCode == RetCode.NET_ERROR_TIMEOUT)
                            {
                                // 네트워트 타임아웃.
                                netResultHandler
                                        .sendEmptyMessage(RetCode.NET_ERROR_TIMEOUT);
                            }
                            else
                            {
                                // 실패
                                netResultHandler
                                        .sendEmptyMessage(RetCode.NET_ERROR);
                            }
                        }
                        super.callback(url, object, status);
                    }
                });
    }

    private String[] convertXY(Location location){
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1]= String.valueOf(data.y);
        return xyStr;
    }

    public CustomWidgetBlack() {
        netCommonApi = new CommonApi();
    }

    private void requestLocation(Context context) {
        // GMS 측위 요청.
        LocationGMS location = new LocationGMS(context, locationHandler);
        location.connect();
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        this.context = context;
        initUi(context);
        String action = intent.getAction();
        if(action.equals(ACTION_UPDATE_DIRECT)){
            PrefMgr prefMgr = new PrefMgr(context);
            isTimeOutRefresh = false;
            logFlurryAgent(DefineFlurry.eventEX_RELOAD);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOverBlack, View.INVISIBLE);
            rv.setViewVisibility(R.id.llProgressBarBlack, View.VISIBLE);
            rv.setViewVisibility(R.id.llUnderFavoriteLayoutBlack, View.VISIBLE);
            String favoriteNM = new PrefMgr(context).getFavoriteName();
            if(favoriteNM.equals("")){
                rv.setTextViewText(R.id.tvFavoritenNameBlack, context.getResources().getString(R.string.no_favorite));
                rv.setTextViewText(R.id.tvFavoriteCostBlack, null);
                rv.setImageViewBitmap(R.id.ivGasStationFavoriteBlack, null);
            }else{
                System.err.println("DSKIM FAVNAME"+ favoriteNM);
                apiSearchPoiNumber(favoriteNM, "0", "0");
            }
            System.out.println("DSKIM getNowTime "+getNowTime());
            System.out.println("DSKIM getThisTime "+new PrefMgr(context).getUpdateTime());
            if(getNowTime()> new PrefMgr(context).getUpdateTime()){
                System.out.println("DSKIM 측위함");
                requestLocation(context);
                new PrefMgr(context).setUpdateTime(getNowTime()+10000L);
            }else{
                String savedPostion[] = prefMgr.getMyPosition().split("\\|");
                apiSearchCoorAddr(savedPostion[0], savedPostion[1]);
                apiSearchPoiAround(savedPostion[0],savedPostion[1]);
            }
            createUIUpdateHandler(runnable, visibleHandler);

//			if(getNowTime() > prefMgr.getUpdateLureTime()){
//				prefMgr.setUpdateLureTime(getNowTime()+INTERVAL_REFRESH);
//				rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
//			}else{
//				
//			}
        }else if(action.equals(ACTION_REFRESH)){
            isTimeOutRefresh = true;
            PrefMgr prefMgr = new PrefMgr(context);
            logFlurryAgent(DefineFlurry.eventEX_RELOAD);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOverBlack, View.INVISIBLE);
            rv.setViewVisibility(R.id.llProgressBarBlack, View.VISIBLE);
            String favoriteNM = new PrefMgr(context).getFavoriteName();
            if(favoriteNM.equals("")){
                rv.setTextViewText(R.id.tvFavoritenNameBlack, context.getResources().getString(R.string.no_favorite));
                rv.setTextViewText(R.id.tvFavoriteCostBlack, null);
                rv.setImageViewBitmap(R.id.ivGasStationFavoriteBlack, null);
            }else{
                System.err.println("DSKIM FAVNAME"+ favoriteNM);
                apiSearchPoiNumber(favoriteNM, "0", "0");
            }
            if(getNowTime() > new PrefMgr(context).getUpdateTime()){
                System.out.println("DSKIM getNowTime "+getNowTime());
                System.out.println("DSKIM new PrefMgr(context).getUpdateTime() "+new PrefMgr(context).getUpdateTime());
                requestLocation(context);
            }else{
                String savedPostion[] = prefMgr.getMyPosition().split("\\|");
                apiSearchCoorAddr(savedPostion[0], savedPostion[1]);
                apiSearchPoiAround(savedPostion[0],savedPostion[1]);
            }
            createUIUpdateHandler(runnable, visibleHandler);
//			if(getNowTime() > prefMgr.getUpdateLureTime()){
//				prefMgr.setUpdateLureTime(getNowTime()+INTERVAL_REFRESH);
//				rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
//			}else{
//				
//			}
        }else if(action.equals(ACTION_GO_TO_ACTIVITY)){
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOverBlack, View.INVISIBLE);
            Intent gotoActivity = new  Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            new PrefMgr(context).setEnterType(0);
            context.startActivity(gotoActivity);

        }else if(action.equals(ACTION_GO_TO_ACTIVITY_FAVORITE)){
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOverBlack, View.INVISIBLE);
            Intent gotoActivity = new  Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            new PrefMgr(context).setEnterType(1);
            context.startActivity(gotoActivity);

        }else if(action.equals(ACTION_GO_TO_ACTIVITY_LOW)){
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOverBlack, View.INVISIBLE);
            Intent gotoActivity = new  Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			gotoActivity.putExtra("callType", "low");
            new PrefMgr(context).setEnterType(2);
            context.startActivity(gotoActivity);

        }else if(action.equals(AppWidgetManager.ACTION_APPWIDGET_ENABLED)){
            createUIUpdateHandler(runnable, visibleHandler);
        }
        rv.setOnClickPendingIntent(R.id.llTimeOverBlack, getPendingIntent(context, R.id.llTimeOverBlack));
        rv.setOnClickPendingIntent(R.id.ivRefreshButtonDirectBlack, getPendingIntent(context, R.id.ivRefreshButtonDirectBlack));
//		rv.setOnClickPendingIntent(R.id.llAvgLayout, getPendingIntent(context, R.id.llAvgLayout));
        rv.setOnClickPendingIntent(R.id.llUnderFavoriteLayoutBlack, getPendingIntent(context, R.id.llUnderFavoriteLayoutBlack));
        rv.setOnClickPendingIntent(R.id.llLowLayoutBlack, getPendingIntent(context, R.id.llLowLayoutBlack));

        appWidgetManager = AppWidgetManager.getInstance(context);
        cpName = new ComponentName(context, CustomWidgetBlack.class);
        appWidgetManager.updateAppWidget(cpName, rv);
    }

    private PendingIntent getPendingIntent(Context context, final int id) {
        final Intent intent = new Intent(context, CustomWidgetBlack.class);
        switch (id) {
            case R.id.llTimeOverBlack:
                StartFlurryAgent(context);
                intent.setAction(ACTION_REFRESH);
                intent.putExtra("viewId", id);
                break;
            case R.id.ivRefreshButtonDirectBlack:
                StartFlurryAgent(context);
                intent.setAction(ACTION_UPDATE_DIRECT);
                intent.putExtra("viewId", id);
                break;
//		case R.id.llAvgLayout:
//			StartFlurryAgent(context);
//			intent.setAction(ACTION_GO_TO_ACTIVITY);
//			intent.putExtra("viewId", id);
//			break;
            case R.id.llUnderFavoriteLayoutBlack:
                StartFlurryAgent(context);
                intent.setAction(ACTION_GO_TO_ACTIVITY_FAVORITE);
                intent.putExtra("viewId", id);
                break;
            case R.id.llLowLayoutBlack:
                StartFlurryAgent(context);
                intent.setAction(ACTION_GO_TO_ACTIVITY_LOW);
                intent.putExtra("viewId", id);
                break;

        }
        return PendingIntent.getBroadcast(context, id, intent, 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.e("DSKIM", "onDeleted");
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.e("DSKIM", "onDeleted");
        super.onDeleted(context, appWidgetIds);
        PrefMgr prefMgr = new PrefMgr(context);
        prefMgr.setChangeoil(true);//사실 이런용도가 아니지만 절대 업데이트를 위해 변경
        removePreviousAlarm();
    }

    public void initUi(Context context) {
        rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout_black);
    }

    private void createUIUpdateHandler(Runnable runnable, Handler visibleHandler){
        removePreHandler(runnable, visibleHandler);
        runnable = new Runnable() {
            @Override
            public void run() {
                rv.setViewVisibility(R.id.llTimeOverBlack, View.VISIBLE);
                rv.setViewVisibility(R.id.llProgressBarBlack, View.INVISIBLE);
                appWidgetManager.updateAppWidget(cpName, rv);
            }
        };
        visibleHandler = new Handler();
        visibleHandler.postDelayed(runnable, INTERVAL_REFRESH);
    }

    private void removePreHandler(Runnable runnable, Handler visibleHandler){
        if(runnable!=null && visibleHandler!=null){
            visibleHandler.removeCallbacks(runnable);
        }
    }

    private void removePreviousAlarm(){
        if(alarmManager!=null && pendingIntent !=null){
            pendingIntent.cancel();
            alarmManager.cancel(pendingIntent);
        }
    }

    private void StartFlurryAgent(Context context) {
        FlurryAgent.onStartSession(context, DefineFlurry.FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.onPageView();
//		FlurryAgent.setUserId("20141202_1");
        logFlurryAgent(DefineFlurry.eventIntro, DefineFlurry.keyView, DefineFlurry.vappWidget);
    }

    private void EndFFlurryAgent(Context context) {
        FlurryAgent.onEndSession(context);
    }

    private void logFlurryAgent(String event) {
        FlurryAgent.logEvent(event);
    }

    private void logFlurryAgent(String event, String key, String value) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(key, value);
        FlurryAgent.logEvent(event, params);
    }

    *//**
     * @param strX2
     * @param strY2
     * @return 거리가 1km보다 크면 true, 작으면 false
     *//*
    private boolean distanceValidation(String strX2, String strY2){
        PrefMgr prefMgr = new PrefMgr(context);
        String[] savedXY = prefMgr.getMyPosition().split("\\|");
//		CoordClient client = new CoordClient();
//		Position_T xy = client.getConvertLongLati(Long.parseLong(savedXY[0]), Long.parseLong(savedXY[1]));

        int x1 = Integer.parseInt(savedXY[0]);
        int y1 = Integer.parseInt(savedXY[1]);

        int x2 = Integer.parseInt(strX2);
        int y2 = Integer.parseInt(strY2);

        CoordClient coordClient = new CoordClient();
        Double distance =  coordClient.getDistance(x1, y1, x2, y2);

        return distance < 0.01 ? false : true;
    }


    *//**
     * @author DSKIM</br>
     * 사용자의 이동 반경이 1km 미만일 경우,
     * 데이터가 변경되는 시간을 체크하여, 업데이트 최소화.
     * 데이터가 변경되지 않은 시간에서 리프레쉬했을 경우 서버에 요청 안함.
     * @return 변경시간에 안에 있음 false, 변경시간대 밖에 있음 true;
     *//*
    private boolean timeValidation(){
        boolean isValid = false;
        PrefMgr prefMgr = new PrefMgr(context);

        String[] strSavedTimeArr = prefMgr.getMyTime().split("\\|");
        String[] strNowTimeArr = TimeUtil.getInstance().split("\\|");
        Log.e("DSKIM", prefMgr.getMyTime()+"=="+TimeUtil.getInstance());

        int savedYMD = Integer.parseInt(strSavedTimeArr[0]);
        int savedHMS = Integer.parseInt(strSavedTimeArr[1]);

        int nowYMD = Integer.parseInt(strNowTimeArr[0]);
        int nowHMS = Integer.parseInt(strNowTimeArr[1]);

        Log.e("DSKIM", "sYMD :"+strSavedTimeArr[0]+" sHMD : "+strSavedTimeArr[1]+"\nnYMD : "+strNowTimeArr[0]+" nHMS : "+strNowTimeArr[1]);

        if(savedHMS>=420 && savedHMS<=920){
            if(nowHMS>=420 && nowHMS<=920){
                if(savedYMD<nowYMD){
                    //업데이트함
                    isValid = true;
                }else{
                    //업데이트 안함
                    isValid = false;
                }
            }else{
                //업데이트함
                isValid = true;
            }
        }else if(savedHMS>=920 && savedHMS<=1420){
            if(nowHMS>=920 && nowHMS<=1420){
                if(savedYMD<nowYMD){
                    //업데이트함
                    isValid = true;
                }else{
                    //업데이트 안함
                    isValid = false;
                }
            }else{
                //업데이트함
                isValid = true;
            }
        }else if(savedHMS>=1420 && savedHMS<=1920){
            if(nowHMS>=1420 && nowHMS<=1920){
                if(savedYMD<nowYMD){
                    //업데이트함
                    isValid = true;
                }else{
                    //업데이트 안함
                    isValid = false;
                }
            }else{
                //업데이트함
                isValid = true;
            }
        }else if(savedHMS>=1920 && savedHMS<=420){
            if(nowHMS>=1920 && nowHMS<=420){
                if(savedYMD<nowYMD){
                    //업데이트함
                    isValid = true;
                }else{
                    //업데이트 안함
                    isValid = false;
                }
            }else{
                //업데이트함
                isValid = true;
            }
        }else{
            isValid = true;
        }
        new PrefMgr(context).setMyTime(TimeUtil.getInstance());
        return isValid;
    }*/

}



