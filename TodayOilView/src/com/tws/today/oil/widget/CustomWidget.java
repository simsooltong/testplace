package com.tws.today.oil.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.flurry.android.FlurryAgent;
import com.tws.today.oil.data.DefineFlurry;
import com.tws.today.oil.data.PoiInfo;
import com.tws.today.oil.data.SearchCoordAddr;
import com.tws.today.oil.data.SearchPoi;
import com.tws.today.oil.data.SearchPoiRadius;
import com.tws.today.oil.data.SearchPoiRadiusInfo;
import com.tws.today.oil.data.SearchPoiRadiusItem;
import com.tws.today.oil.network.CommonApi;
import com.tws.today.oil.network.RetCode;
import com.tws.today.oil.util.BrandIconChooser;
import com.tws.today.oil.util.CommaGenerator;
import com.tws.today.oil.util.StationUtil;
import com.tws.today.oil.util.TimeUtil;
import com.tws.today.oilview.ui.OilViewMainActivity;
import com.tws.today.oilview.ui.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import CoordLib.NorXY_T;
import tws.today.lib.gms.LocationDefines;
import tws.today.lib.gms.LocationGMS;
import tws.today.lib.mgr.PrefMgr;
import tws.today.lib.util.LOG;

public class CustomWidget extends AppWidgetProvider {

    //primitive type S
    private static final int INTERVAL_REFRESH = 1800000;
    private boolean isTimeOutRefresh = false;
    //primitive type E

    //object type S
    private static final String DEFAULT_MAP_LV = "2400";
    private final static String ACTION_UPDATE_DIRECT = "android.appwidget.action.ACTION_UPDATE_DIRECT";
    private final static String ACTION_REFRESH = "android.appwidget.action.ACTION_REFRESH";
    private final static String ACTION_GO_TO_ACTIVITY = "android.appwidget.action.ACTION_GO_TO_ACTIVITY";
    private final static String ACTION_GO_TO_ACTIVITY_FAVORITE = "android.appwidget.action.ACTION_GO_TO_ACTIVITY_FAVORITE";
    private final static String ACTION_GO_TO_ACTIVITY_LOW = "android.appwidget.action.ACTION_GO_TO_ACTIVITY_LOW";
    private final static String ACTION_REFRESH_LURE = "android.appwidget.action.ACTION_REFRESH_LURE";
    private SearchPoiRadiusItem loowRadiusItem;
    private CommonApi mNetCommonApi = null;
    private AlarmManager alarmManager = null;
    private SearchPoiRadiusItem lowRadiusItem;
    private AppWidgetManager appWidgetManager;
    private ComponentName cpName;
    private String juso = null;
    private String[] avgInfo;
    private String now_x = null;
    private String now_y = null;
    private Runnable runnable = null;
    private Handler visibleHandler = null;
    //object type E

    //widget type S
    private RemoteViews rv = null;
    ;
    private PendingIntent pendingIntent = null;
    private Context mContext = null;
    //widget type E

    private long getNowTime() {
        return System.currentTimeMillis();
    }

    private Handler locationHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            PrefMgr prefMgr = new PrefMgr(mContext);
            String xyStr[] = null;
            switch (msg.what) {
                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_TIMEOUT:
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    xyStr = prefMgr.getMyPosition().split("\\|");
                    now_x = xyStr[0];
                    now_y = xyStr[1];
                    apiSearchPoiRadius(now_x, now_y);
                    new PrefMgr(mContext).setUpdateTime(getNowTime() + 10000L);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.gps_off), Toast.LENGTH_SHORT).show();
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");
                    if (msg.obj != null) {
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "location getAccuracy : " + location.getAccuracy());
                        Log.i("LocationResultHandler", "location getLongitude : " + location.getLongitude());
                        Log.i("LocationResultHandler", "location getLatitude : " + location.getLatitude());
                        xyStr = convertXY(location);
                        now_x = xyStr[0];
                        now_y = xyStr[1];
                        apiSearchCoorAddr(now_x, now_y);
                        prefMgr.setMyPosition(now_x + "|" + now_y);
                        apiSearchPoiRadius(now_x, now_y);
                        prefMgr.setMyTime(TimeUtil.getInstance());
                        new PrefMgr(mContext).setUpdateTime(getNowTime() + 10000L);
                    } else {

                    }
                    break;
                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    Toast.makeText(mContext, "측위실패", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     * 네트워크 통신 결과 핸들러.
     */
    public Handler mNetResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            LOG.d("NetResult_Handler msg what" + msg.what);
            switch (msg.what) {

                case RetCode.RET_SEARCH_COORD_ADDR_SUCC:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
                    SearchCoordAddr addr = (SearchCoordAddr) msg.obj;
                    String[] tempJsuo = addr.cut_address.split(" ");
                    juso = tempJsuo[tempJsuo.length - 1];
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC" + addr.cut_address);
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC" + addr.cut_address.split(" ")[tempJsuo.length - 1]);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    break;
                case RetCode.RET_SEARCHPOI_STATION_SUCC:
                    LOG.d("RET_SEARCHPOI_STATION_SUCC");
                    PrefMgr prefMgr = new PrefMgr(mContext);
                    SearchPoiRadiusInfo info = (SearchPoiRadiusInfo)msg.obj;
                    SearchPoiRadiusItem tempInfo = info.poi;
                    if (tempInfo != null) {
                        if (tempInfo.name != null) {
                            rv.setTextViewText(R.id.tvFavoritenName, tempInfo.name);
                            if (tempInfo.pricessArr()[prefMgr.getOilType()].equals("0")) {
                                rv.setTextViewText(R.id.tvFavoriteCost, "제공안함");
                            } else {
                                rv.setTextViewText(R.id.tvFavoriteCost, CommaGenerator.withCommaString(tempInfo.pricessArr()[prefMgr.getOilType()]) + " 원");
                            }
                            rv.setImageViewResource(R.id.ivGasStationFavorite, BrandIconChooser.getBrandIcon(tempInfo.poll));
                            appWidgetManager.updateAppWidget(cpName, rv);
                        } else {
                            rv.setTextViewText(R.id.tvFavoritenName, mContext.getResources().getString(R.string.no_favorite));
                            rv.setImageViewBitmap(R.id.ivGasStationFavorite, null);
                            //					rv.setViewVisibility(R.id.llFavoriteLayout, View.GONE);
                            LOG.d("tempInfo.name1이 널입니다.");
                        }
                    } else {
                        rv.setTextViewText(R.id.tvFavoritenName, mContext.getResources().getString(R.string.no_favorite));
                        rv.setImageViewBitmap(R.id.ivGasStationFavorite, null);
                    }
                    break;

                case RetCode.RET_SEARCHPOI_RADIUS_SUCC:
                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();
                    LOG.d("RET_SEARCHPOI_SUCC");
                    PrefMgr mgr = new PrefMgr(mContext);

                    SearchPoiRadius serachPoiRadius = (SearchPoiRadius) msg.obj;
                    serachPoiRadius.poilist = removeStationPriceZero(serachPoiRadius);
                    StationUtil stationUtil = new StationUtil(serachPoiRadius.poilist);
                    stationUtil.sortStation(0, mgr.getOilType());
                    SearchPoiRadiusItem lowestStation = stationUtil.getLowestStation();
                    avgInfo = stationUtil.getAveragePrice(mgr.getOilType());
                    if (lowestStation != null) {
                        rv.setTextViewText(R.id.tvLowName, lowestStation.name);
                        mgr.setLowName(lowestStation.name);

                        if (lowestStation.pricessArr()[mgr.getOilType()].equals("0")) {
                            rv.setTextViewText(R.id.tvLowName, "주변에 " + mContext.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()] + "주유소가 없습니다.");
                            rv.setTextViewText(R.id.tvLowCost, "");
                            rv.setImageViewBitmap(R.id.ivGasLowest, null);
                        } else {
                            rv.setTextViewText(R.id.tvLowCost, CommaGenerator.withCommaString(lowestStation.pricessArr()[mgr.getOilType()]) + " 원");
                            rv.setImageViewResource(R.id.ivGasLowest, BrandIconChooser.getBrandIcon(lowestStation.poll));
                        }
                    } else {
                        rv.setTextViewText(R.id.tvLowName, "주변에 " + mContext.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()] + "주유소가 없습니다.");
                        rv.setTextViewText(R.id.tvLowCost, "");
                        rv.setImageViewBitmap(R.id.ivGasLowest, null);
                    }

                    if (avgInfo != null) {
                        if ((avgInfo[0].equals("0") || lowestStation.pricessArr()[mgr.getOilType()].equals("0"))) {
                            rv.setTextViewText(R.id.tvAverageLocation, "");
                        } else {
                            rv.setTextViewText(R.id.tvAverageLocation, "- " + (juso == null ? "" : juso) + " 주변 3km 평균가 " + CommaGenerator.withCommaString(avgInfo[0]) + "원");
                        }
                    } else {
                        rv.setTextViewText(R.id.tvAverageLocation, "");
                    }

                    rv.setViewVisibility(R.id.llProgressBar, View.INVISIBLE);
                    if (mgr.getFavoriteName().length() < 1) {
//					rv.setViewVisibility(R.id.llFavoriteLayout, View.GONE);
                    } else {
                        rv.setViewVisibility(R.id.llUnderFavoriteLayout, View.VISIBLE);
//					apiSearchPoiNumber(mgr.getFavoriteName(), now_x, now_y);
                    }
                    rv.setTextViewText(R.id.tvLowestAndOil, "최저가 (" + mContext.getResources().getStringArray(R.array.oilTypeArray)[mgr.getOilType()] + ")");
                    appWidgetManager.updateAppWidget(cpName, rv);
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_FAIL:
                    LOG.d("RET_SEARCH_COORD_ADDR_FAIL");
//				Toast.makeText(mContext, "RET_SEARCH_COORD_ADDR_FAIL", Toast.LENGTH_SHORT).show();
//				Toast.makeText(mContext, mContext.getResources().getString(R.string.network_err_request_timeout), Toast.LENGTH_SHORT).show();
                    break;

                case RetCode.NET_ERROR_MSG:
                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR_MSG), msg.obj.toString(), mContext.getPackageName());
                    LOG.d("NET_RETURN_ERROR_MSG");
                    rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
                    rv.setViewVisibility(R.id.llProgressBar, View.INVISIBLE);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    Toast.makeText(mContext, msg.obj.toString() + "(" + msg.arg1 + ")", Toast.LENGTH_SHORT).show();
                    // BaseMessagePopup(title, message, code, false, true);
                    break;

                case RetCode.NET_ERROR:
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR), "RetCode.NET_ERROR", mContext.getPackageName());
                    rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
                    rv.setViewVisibility(R.id.llProgressBar, View.INVISIBLE);
                    appWidgetManager.updateAppWidget(cpName, rv);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.network_err_request_timeout), Toast.LENGTH_SHORT).show();
                    LOG.d("NET_ERROR");
                    break;

            }
        }

    };

    /**
     * 데이터가 0인 주유소 삭제
     *
     * @param searchPoiRadius
     * @return
     */
    private ArrayList<SearchPoiRadiusItem> removeStationPriceZero(SearchPoiRadius searchPoiRadius) {
        PrefMgr mPrefMgr = new PrefMgr(mContext);
        ArrayList<SearchPoiRadiusItem> tempPoi = new ArrayList<SearchPoiRadiusItem>();
        for (SearchPoiRadiusItem poiInfo : searchPoiRadius.poilist) {
            if (!poiInfo.pricessArr()[mPrefMgr.getOilType()].equals("0")) {
                tempPoi.add(poiInfo);
            }
        }
        return tempPoi.size() > 0 ? tempPoi : null;
    }

    private ArrayList<PoiInfo> removeNullDataForFavorite(SearchPoi searchPoi) {
        PrefMgr prefMgr = new PrefMgr(mContext);
        ArrayList<PoiInfo> tempPoi = new ArrayList<PoiInfo>();
        System.err.println("size : " + searchPoi.poi.size() + "개");
        for (PoiInfo poiInfo : searchPoi.poi) {
            if (poiInfo.oildata != null) {
                if (poiInfo.poiid.equals(prefMgr.getFavoritePoiId())) {
                    tempPoi.add(poiInfo);
                }
            }
        }
        return tempPoi.size() > 0 ? tempPoi : null;
    }

    /**
     * @author JONY</br> 즐겨찾기로 검색.</br>
     *
     *//*
	private void apiSearchPoiNumber(String name1, String cur_x, String cur_y) {
		mNetCommonApi.apiSearchPoiNumber(mContext, name1, cur_x, cur_y, new AjaxCallback<SearchPoi>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoi object, AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("ApiSvcFindChild AQuery url : " + url);
                LOG.d("ApiSvcFindChild net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("ApiSvcFindChild AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoi sr = (SearchPoi) object;
                    System.out.println(object.toString());
                    // return data check
                    LOG.d("apiSearchPoiAround sr.result : " + sr.result);
                    LOG.d("apiSearchPoiAround sr.errormsg : "
                            + sr.errormsg);

                    if (sr.result == RetCode.NET_SUCC) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.RET_SEARCHPOI_NUMBER_SUCC;
                        msg.obj = sr;
                        mNetResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;
                        msg.arg1 = sr.result;
                        mNetResultHandler.sendMessage(msg);
                    }

                } else {
                    // 실패
                    mNetResultHandler.sendEmptyMessage(RetCode.NET_ERROR);
                }
                super.callback(url, object, status);
            }
        });
	}*/

    /**
     * @author JONY</br> 즐겨찾기로 검색.</br>
     */
    private void apiSearchStation(String poiId) {
        mNetCommonApi.apiSearchPoiStation(mContext, poiId, new AjaxCallback<SearchPoiRadiusInfo>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoiRadiusInfo object, AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("apiSearchStation AQuery url : " + url);
                LOG.d("apiSearchStation net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("apiSearchStation AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoiRadiusInfo sr = (SearchPoiRadiusInfo) object;

                    // return data check
                    LOG.d("apiSearchStation sr.result : " + sr.result);
                    LOG.d("apiSearchStation sr.errormsg : "
                            + sr.errormsg);

                    if (sr.result.equals(String.valueOf(RetCode.NET_SUCC))) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.RET_SEARCHPOI_STATION_SUCC;
                        msg.obj = sr;
                        mNetResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = mNetResultHandler.obtainMessage();
                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;
                        mNetResultHandler.sendMessage(msg);
                    }

                } else {
                    // 실패
                    mNetResultHandler
                            .sendEmptyMessage(RetCode.NET_ERROR);
                }
                super.callback(url, object, status);
            }
        });
    }

    protected PoiInfo retFindPoi(ArrayList<PoiInfo> poiList) {
        PrefMgr prefMgr = new PrefMgr(mContext);
        prefMgr.setMyTime(TimeUtil.getInstance());
        String poiId = prefMgr.getFavoritePoiId();
        PoiInfo info = new PoiInfo();
        if (poiList != null) {
            for (PoiInfo item : poiList) {
                if (item.poiid.equals(poiId)) {
                    info = item;
                }
            }
            return info;
        } else {
            return null;
        }
    }

    /**
     * 지도 주소 정보 요청 api
     *
     * @param longi
     * @param lati
     */
    private void apiSearchCoorAddr(String longi, String lati) {
        System.err.println("apiSearchCoorAddr call");
        mNetCommonApi.apiSearchCoordAddr(mContext, longi, lati,
                new AjaxCallback<SearchCoordAddr>() {
                    @Override
                    public void callback(String url, SearchCoordAddr object,
                                         AjaxStatus status) {
                        // TODO Auto-generated method stub

                        LOG.d("apiSearchCoorAddr AQuery url : " + url);
                        LOG.d("apiSearchCoorAddr net AQuery status.getCode() : " + status.getCode());

                        if (status.getCode() == RetCode.HTTP_SUCC) {
                            // http 200 성공
                            LOG.d("apiSearchCoorAddr AQuery object.result : "
                                    + object.result);
                        } else {
                            // http 오류 나 네트워크 오류
                            object = null;
                        }

                        if (object != null) {

                            SearchCoordAddr sr = (SearchCoordAddr) object;

                            // return data check
                            LOG.d("apiSearchCoorAddr sr.result : " + sr.result);
                            LOG.d("apiSearchCoorAddr sr.errormsg : "
                                    + sr.errormsg);

                            if (sr.result == RetCode.NET_SUCC) {
                                // 성공
                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_SUCC;
                                msg.obj = sr;

                                mNetResultHandler.sendMessage(msg);

                            } else {
                                // 서버 오류 메시지 출력

                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCH_COORD_ADDR_FAIL;
                                msg.obj = sr.errormsg;
                                msg.arg1 = sr.result;

                            }
                        } else {
                            // 실패
                        }
                        super.callback(url, object, status);
                    }
                });
    }
	
	/*private void apiSearchPoiAround(String longi, String lati) {
		// TODO Auto-generated method stub
		// api.apiSearchPoiAround(mContext, cateCode, spOpt, x1, y1, x2, y2,
		// radius, sortOpt, curX, curY, cb)
		// 네트워크 연결 여부 확인.
		
		mNetCommonApi.apiSearchPoiAround(mContext, "C0100", "2", longi,
                lati, "0", "0", DEFAULT_MAP_LV, "1", longi, lati,
                new AjaxCallback<SearchPoi>() {
                    // callback 처리.
                    @Override
                    public void callback(String url, SearchPoi object, AjaxStatus status) {
                        // TODO Auto-generated method stub

                        LOG.d("ApiSvcFindChild AQuery url : " + url);
                        LOG.d("ApiSvcFindChild net AQuery status.getCode() : " + status.getCode());

                        int retCode = status.getCode();

                        if (retCode == RetCode.HTTP_SUCC) {
                            // http 200 성공
                            LOG.d("ApiSvcFindChild AQuery object.result : "
                                    + object.result);
                        } else {
                            // http 오류 나 네트워크 오류
                            object = null;
                        }

                        if (object != null) {

                            final SearchPoi sr = (SearchPoi) object;

                            // return data check
                            LOG.d("apiSearchPoiAround sr.result : " + sr.result);
                            LOG.d("apiSearchPoiAround sr.errormsg : "
                                    + sr.errormsg);

                            if (sr.result == RetCode.NET_SUCC) {
                                // 성공
                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.RET_SEARCHPOI_SUCC;
                                msg.obj = sr;

                                mNetResultHandler.sendMessage(msg);

                            } else {
                                // 서버 오류 메시지 출력

                                Message msg = mNetResultHandler.obtainMessage();

                                msg.what = RetCode.NET_ERROR_MSG;
                                msg.obj = sr.errormsg;
                                msg.arg1 = sr.result;

                                mNetResultHandler.sendMessage(msg);
                            }
                        } else {

                            if (retCode == RetCode.NET_ERROR_TIMEOUT) {
                                // 네트워트 타임아웃.
                                mNetResultHandler
                                        .sendEmptyMessage(RetCode.NET_ERROR_TIMEOUT);
                            } else {
                                // 실패
                                mNetResultHandler
                                        .sendEmptyMessage(RetCode.NET_ERROR);
                            }
                        }
                        super.callback(url, object, status);
                    }
                });
	}*/

    /**
     * 주변 주유소 검색 API를 호출
     *
     * @param longi
     * @param lati
     */
    private void apiSearchPoiRadius(String longi, String lati) {
        mNetCommonApi.apiSearchPoiRadius(mContext, longi, lati, DEFAULT_MAP_LV, "0", "0", new AjaxCallback<SearchPoiRadius>() {
            // callback 처리.
            @Override
            public void callback(String url, SearchPoiRadius object,
                                 AjaxStatus status) {
                // TODO Auto-generated method stub

                LOG.d("apiSearchPoiRadius AQuery url : " + url);
                LOG.d("apiSearchPoiRadius net AQuery status.getCode() : "
                        + status.getCode());

                if (status.getCode() == RetCode.HTTP_SUCC) {
                    // http 200 성공
                    LOG.d("apiSearchPoiRadius AQuery object.result : "
                            + object.result);
                } else {
                    // http 오류 나 네트워크 오류
                    object = null;
                }

                if (object != null) {

                    final SearchPoiRadius sr = (SearchPoiRadius) object;

                    // return data check
                    LOG.d("apiSearchPoiRadius sr.result : " + sr.result);
                    LOG.d("apiSearchPoiRadius sr.errormsg : " + sr.errormsg);

                    if (sr.result.equals("0")) {
                        // 성공
                        Message msg = mNetResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCHPOI_RADIUS_SUCC;
                        msg.obj = sr;

                        mNetResultHandler.sendMessage(msg);

                    } else {
                        // 서버 오류 메시지 출력

                        Message msg = mNetResultHandler.obtainMessage();

                        msg.what = RetCode.NET_ERROR_MSG;
                        msg.obj = sr.errormsg;

                        mNetResultHandler.sendMessage(msg);
                    }
                } else {
                    // 실패
                    mNetResultHandler
                            .sendEmptyMessage(RetCode.NET_ERROR);

                }
                super.callback(url, object, status);
            }
        });
    }


    private String[] convertXY(Location location) {
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1] = String.valueOf(data.y);
        return xyStr;
    }

    public CustomWidget() {
        mNetCommonApi = new CommonApi();
    }

    private void requestLocation(Context context) {
        // GMS 측위 요청.
        LocationGMS location = new LocationGMS(context, locationHandler);
        location.connect();
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        this.mContext = context;
        initUi(context);
        String action = intent.getAction();
        if (action.equals(ACTION_UPDATE_DIRECT)) {
            PrefMgr prefMgr = new PrefMgr(context);
            isTimeOutRefresh = false;
            logFlurryAgent(DefineFlurry.eventEX_RELOAD);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOver, View.INVISIBLE);
            rv.setViewVisibility(R.id.llProgressBar, View.VISIBLE);
            rv.setViewVisibility(R.id.llUnderFavoriteLayout, View.VISIBLE);
            String favoritePoiId = new PrefMgr(context).getFavoritePoiId();
            if (favoritePoiId.equals("")) {
                rv.setTextViewText(R.id.tvFavoritenName, context.getResources().getString(R.string.no_favorite));
                rv.setTextViewText(R.id.tvFavoriteCost, null);
                rv.setImageViewBitmap(R.id.ivGasStationFavorite, null);
            } else {
                System.err.println("DSKIM FAVNAME : " + favoritePoiId);
                apiSearchStation(favoritePoiId);
            }
            System.out.println("DSKIM getNowTime " + getNowTime());
            System.out.println("DSKIM getThisTime " + new PrefMgr(context).getUpdateTime());
            if (getNowTime() > new PrefMgr(context).getUpdateTime()) {
                System.out.println("DSKIM 측위함");
                requestLocation(context);
                new PrefMgr(context).setUpdateTime(getNowTime() + 10000L);
            } else {
                String savedPostion[] = prefMgr.getMyPosition().split("\\|");
                apiSearchCoorAddr(savedPostion[0], savedPostion[1]);
                apiSearchPoiRadius(savedPostion[0], savedPostion[1]);
            }
            createUIUpdateHandler(runnable, visibleHandler);

//			if(getNowTime() > prefMgr.getUpdateLureTime()){
//				prefMgr.setUpdateLureTime(getNowTime()+INTERVAL_REFRESH);
//				rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
//			}else{
//				
//			}
        } else if (action.equals(ACTION_REFRESH)) {
            isTimeOutRefresh = true;
            PrefMgr prefMgr = new PrefMgr(context);
            logFlurryAgent(DefineFlurry.eventEX_RELOAD);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOver, View.INVISIBLE);
            rv.setViewVisibility(R.id.llProgressBar, View.VISIBLE);
            String favoritePoiId = new PrefMgr(context).getFavoritePoiId();
            if (favoritePoiId.equals("")) {
                rv.setTextViewText(R.id.tvFavoritenName, context.getResources().getString(R.string.no_favorite));
                rv.setTextViewText(R.id.tvFavoriteCost, null);
                rv.setImageViewBitmap(R.id.ivGasStationFavorite, null);
            } else {
                System.err.println("DSKIM FAVNAME" + favoritePoiId);
                apiSearchStation(favoritePoiId);
            }
            if (getNowTime() > new PrefMgr(context).getUpdateTime()) {
                System.out.println("DSKIM getNowTime " + getNowTime());
                System.out.println("DSKIM new PrefMgr(mContext).getUpdateTime() " + new PrefMgr(context).getUpdateTime());
                requestLocation(context);
            } else {
                String savedPostion[] = prefMgr.getMyPosition().split("\\|");
                apiSearchCoorAddr(savedPostion[0], savedPostion[1]);
                apiSearchPoiRadius(savedPostion[0], savedPostion[1]);
            }
            createUIUpdateHandler(runnable, visibleHandler);
//			if(getNowTime() > prefMgr.getUpdateLureTime()){
//				prefMgr.setUpdateLureTime(getNowTime()+INTERVAL_REFRESH);
//				rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
//			}else{
//				
//			}
        } else if (action.equals(ACTION_GO_TO_ACTIVITY)) {
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOver, View.INVISIBLE);
            Intent gotoActivity = new Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            new PrefMgr(context).setEnterType(0);
            context.startActivity(gotoActivity);

        } else if (action.equals(ACTION_GO_TO_ACTIVITY_FAVORITE)) {
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOver, View.INVISIBLE);
            Intent gotoActivity = new Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            new PrefMgr(context).setEnterType(1);
            context.startActivity(gotoActivity);

        } else if (action.equals(ACTION_GO_TO_ACTIVITY_LOW)) {
            logFlurryAgent(DefineFlurry.eventEX_CLICK);
            EndFFlurryAgent(context);
            rv.setViewVisibility(R.id.llTimeOver, View.INVISIBLE);
            Intent gotoActivity = new Intent(context, OilViewMainActivity.class);
            gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			gotoActivity.putExtra("callType", "low");
            new PrefMgr(context).setEnterType(2);
            context.startActivity(gotoActivity);

        } else if (AppWidgetManager.ACTION_APPWIDGET_ENABLED.equals(action)) {
            createUIUpdateHandler(runnable, visibleHandler);
        }
        rv.setOnClickPendingIntent(R.id.llTimeOver, getPendingIntent(context, R.id.llTimeOver));
        rv.setOnClickPendingIntent(R.id.ivRefreshButtonDirect, getPendingIntent(context, R.id.ivRefreshButtonDirect));
//		rv.setOnClickPendingIntent(R.id.llAvgLayout, getPendingIntent(mContext, R.id.llAvgLayout));
        rv.setOnClickPendingIntent(R.id.llUnderFavoriteLayout, getPendingIntent(context, R.id.llUnderFavoriteLayout));
        rv.setOnClickPendingIntent(R.id.llLowLayout, getPendingIntent(context, R.id.llLowLayout));

        appWidgetManager = AppWidgetManager.getInstance(context);
        cpName = new ComponentName(context, CustomWidget.class);
        appWidgetManager.updateAppWidget(cpName, rv);
    }

    private PendingIntent getPendingIntent(Context context, final int id) {
        final Intent intent = new Intent(context, CustomWidget.class);
        switch (id) {
            case R.id.llTimeOver:
                StartFlurryAgent(context);
                intent.setAction(ACTION_REFRESH);
                intent.putExtra("viewId", id);
                break;
            case R.id.ivRefreshButtonDirect:
                StartFlurryAgent(context);
                intent.setAction(ACTION_UPDATE_DIRECT);
                intent.putExtra("viewId", id);
                break;
//		case R.id.llAvgLayout:
//			StartFlurryAgent(mContext);
//			intent.setAction(ACTION_GO_TO_ACTIVITY);
//			intent.putExtra("viewId", id);
//			break;
            case R.id.llUnderFavoriteLayout:
                StartFlurryAgent(context);
                intent.setAction(ACTION_GO_TO_ACTIVITY_FAVORITE);
                intent.putExtra("viewId", id);
                break;
            case R.id.llLowLayout:
                StartFlurryAgent(context);
                intent.setAction(ACTION_GO_TO_ACTIVITY_LOW);
                intent.putExtra("viewId", id);
                break;

        }
        return PendingIntent.getBroadcast(context, id, intent, 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.e("DSKIM", "onDeleted");
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.e("DSKIM", "onDeleted");
        super.onDeleted(context, appWidgetIds);
        PrefMgr prefMgr = new PrefMgr(context);
        prefMgr.setChangeoil(true);//사실 이런용도가 아니지만 절대 업데이트를 위해 변경
        removePreviousAlarm();
    }

    public void initUi(Context context) {
        rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
    }

    private void createUIUpdateHandler(Runnable runnable, Handler visibleHandler) {
        removePreHandler(runnable, visibleHandler);
        runnable = new Runnable() {
            @Override
            public void run() {
                rv.setViewVisibility(R.id.llTimeOver, View.VISIBLE);
                rv.setViewVisibility(R.id.llProgressBar, View.INVISIBLE);
                appWidgetManager.updateAppWidget(cpName, rv);
            }
        };
        visibleHandler = new Handler();
        visibleHandler.postDelayed(runnable, INTERVAL_REFRESH);
    }

    private void removePreHandler(Runnable runnable, Handler visibleHandler) {
        if (runnable != null && visibleHandler != null) {
            visibleHandler.removeCallbacks(runnable);
        }
    }

    private void removePreviousAlarm() {
        if (alarmManager != null && pendingIntent != null) {
            pendingIntent.cancel();
            alarmManager.cancel(pendingIntent);
        }
    }

    private void StartFlurryAgent(Context context) {
        FlurryAgent.onStartSession(context, DefineFlurry.FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.onPageView();
//		FlurryAgent.setUserId("20141202_1");
        logFlurryAgent(DefineFlurry.eventIntro, DefineFlurry.keyView, DefineFlurry.vappWidget);
    }

    private void EndFFlurryAgent(Context context) {
        FlurryAgent.onEndSession(context);
    }

    private void logFlurryAgent(String event) {
        FlurryAgent.logEvent(event);
    }

    private void logFlurryAgent(String event, String key, String value) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(key, value);
        FlurryAgent.logEvent(event, params);
    }
}



