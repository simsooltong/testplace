package com.tws.today.oil.util;

import java.util.Calendar;

public class TimeUtil {
	public static String getInstance(){
		Calendar calendar = Calendar.getInstance();
		String getYMD = String.valueOf(calendar.get(Calendar.YEAR))+checkStringSize(String.valueOf(calendar.get(Calendar.MONTH)))+checkStringSize(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
		String getHMS = checkStringSize(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)))+checkStringSize(String.valueOf(calendar.get(Calendar.MINUTE)));
		return getYMD+"|"+getHMS;
	}
	
	private static String checkStringSize(String str){
		String num= null;
		if(str.length()<2){
		 num = "0"+str;
		}else{
			num = str;
		}
		return num;
	}
}
