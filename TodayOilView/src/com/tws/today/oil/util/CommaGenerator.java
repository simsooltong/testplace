package com.tws.today.oil.util;

public class CommaGenerator {
	public static String withCommaString(String costValue){
		int intval = Integer.parseInt(costValue);
		return String.format("%,d", intval);
	}
}
