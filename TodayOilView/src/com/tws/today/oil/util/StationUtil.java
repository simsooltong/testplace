package com.tws.today.oil.util;

import com.tws.today.oil.data.SearchPoiRadiusItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tws.today.lib.util.LOG;

/**
 * 해당 클래스에서는 주유소를 정렬(최저가)정렬하고
 * 최저가 주유소, 2등3등최저가 주유소, 인기주유소를 반환한다.
 * <p/>
 * Created by dskim on 2015-03-10.
 */
public class StationUtil {
    private ArrayList<SearchPoiRadiusItem> mRadiusItems = null;
    private String[] stationType = {"고급휘발유", "휘발유", "경유", "LPG"};

    /**
     * ArayList의 길이가 0이면 StationUtil에 진입하지 않음.
     * 생성자
     *
     * @param radiusItems
     */
    public StationUtil(ArrayList<SearchPoiRadiusItem> radiusItems) {
        this.mRadiusItems = radiusItems;
    }

    /**
     * 정렬된 주유소를 반환한다.
     *
     * @return
     */
    public ArrayList<SearchPoiRadiusItem> getSortedStation() {
        return mRadiusItems;
    }

    /**
     * 주유소를 Sort 타입에 따라 정렬한다.
     *
     * @param sortType 0 : 가격, 1 : 거리 , 2 : 인기
     * @param oilType  0 : 고휘 , 1 : 일반휘발유, 2 : 경유, 3 : LPG
     * @return 정련된 주유소 리스트
     */
    public ArrayList<SearchPoiRadiusItem> sortStation(int sortType, int oilType) {
        if (sortType == 0) {
            switch (oilType) {
                case 0:
                    Collections.sort(mRadiusItems, new Desc_gp_price());
                    for (SearchPoiRadiusItem item : mRadiusItems) {
                        System.err.println("고휘발유 : " + item.name + "," + item.gp_price);
                    }
                    break;
                case 1:
                    Collections.sort(mRadiusItems, new Desc_gn_price());
                    for (SearchPoiRadiusItem item : mRadiusItems) {
                        System.err.println("휘발유 : " + item.name + "," + item.gn_price);
                    }
                    break;
                case 2:
                    Collections.sort(mRadiusItems, new Desc_dn_price());
                    for (SearchPoiRadiusItem item : mRadiusItems) {
                        System.err.println("경유 : " + item.name + "," + item.dn_price);
                    }
                    break;
                case 3:
                    Collections.sort(mRadiusItems, new Desc_lpg_price());
                    for (SearchPoiRadiusItem item : mRadiusItems) {
                        System.err.println("LPG : " + item.name + "," + item.lpg_price);
                    }
                    break;
            }
        } else if (sortType == 1) {
            //거리 미사용
        } else {
            //인기 미사용
        }
        return mRadiusItems;
    }

    private String getOilType(String oiltype) {
        int intType = Integer.parseInt(oiltype);
        String strType = "0";
        switch (intType) {
            case 0:
                strType = "2";
                break;
            case 1:
                strType = "1";
                break;
            case 2:
                strType = "3";
                break;
            case 3:
                strType = "4";
                break;
        }
        return strType;
    }

    /**
     * 즐겨찾기가 가장 높은 인기 주유소를 반환
     *
     * @return
     */
    public SearchPoiRadiusItem getPopularStation(int oilType) {
        SearchPoiRadiusItem popularItem = null;
        if (mRadiusItems.size() > 0) {
            for (int i = 0; i < mRadiusItems.size(); i++) {
                String popYN = mRadiusItems.get(i).popularArr()[oilType];
                if (popYN.equals("1")) {
                    LOG.d("인기주유소" + mRadiusItems.get(i).name);
                    popularItem = mRadiusItems.get(i);
                }
            }
            return popularItem;
        } else {
            return null;
        }

    }

    /**
     * 정렬된 주유소중, 최저가 주유소를 구한다.
     *
     * @return 최저가주유소
     */
    public SearchPoiRadiusItem getLowestStation() {
        return mRadiusItems.get(0);
    }

    /**
     * 2등 3등 주유소를 반환
     *
     * @return 2등, 3등 주유소
     */
    public ArrayList<SearchPoiRadiusItem> getSecondThirdPreiceStation() {
        ArrayList<SearchPoiRadiusItem> secondThirdStation = new ArrayList<SearchPoiRadiusItem>();
        if (mRadiusItems.size() >= 3) {
            try {
                secondThirdStation.add(mRadiusItems.get(2));
                secondThirdStation.add(mRadiusItems.get(1));
            } catch (Exception e) {
                return secondThirdStation.size() > 0 ? secondThirdStation : null;
            }
        } else if (mRadiusItems.size() == 2) {
            try {
                secondThirdStation.add(mRadiusItems.get(1));
            } catch (Exception e) {
                return secondThirdStation.size() > 0 ? secondThirdStation : null;
            }
        }
        return secondThirdStation.size() > 0 ? secondThirdStation : null;
    }

    /**
     * 주유소의 평균가를 구함
     *
     * @param oilType 유종
     * @return 평균유가, 주소
     */
    public String[] getAveragePrice(int oilType) {
        String retValue[] = new String[2];
        int sumVlaue = 0;
        if (mRadiusItems != null) {
            if (mRadiusItems.size() == 0) {
                retValue[0] = "0";
                retValue[1] = "주변에 주유소(" + stationType[oilType] + ") 가 없습니다.";
                return retValue;
            } else {
                for (SearchPoiRadiusItem item : mRadiusItems) {
                    switch (oilType) {
                        case 0:
                            sumVlaue += Integer.parseInt(item.gp_price);
                            break;
                        case 1:
                            sumVlaue += Integer.parseInt(item.gn_price);
                            break;
                        case 2:
                            sumVlaue += Integer.parseInt(item.dn_price);
                            break;
                        case 3:
                            sumVlaue += Integer.parseInt(item.lpg_price);
                            break;
                    }
                }
                retValue[0] = String.valueOf((sumVlaue / mRadiusItems.size()));
                retValue[1] = mRadiusItems.get(0).address;
                return retValue;
            }
        } else {
            retValue[0] = "0";
            retValue[1] = "주변에 주유소(" + stationType[oilType] + ") 가 없습니다.";
            return retValue;
        }
    }

    static class Desc_gp_price implements Comparator<SearchPoiRadiusItem> {
        @Override
        public int compare(SearchPoiRadiusItem lhs, SearchPoiRadiusItem rhs) {
            return lhs.gp_price.compareTo(rhs.gp_price);
        }
    }

    static class Desc_gn_price implements Comparator<SearchPoiRadiusItem> {
        @Override
        public int compare(SearchPoiRadiusItem lhs, SearchPoiRadiusItem rhs) {
            return lhs.gn_price.compareTo(rhs.gn_price);
        }
    }

    static class Desc_dn_price implements Comparator<SearchPoiRadiusItem> {
        @Override
        public int compare(SearchPoiRadiusItem lhs, SearchPoiRadiusItem rhs) {
            return lhs.dn_price.compareTo(rhs.dn_price);
        }
    }

    static class Desc_lpg_price implements Comparator<SearchPoiRadiusItem> {
        @Override
        public int compare(SearchPoiRadiusItem lhs, SearchPoiRadiusItem rhs) {
            return lhs.lpg_price.compareTo(rhs.lpg_price);
        }
    }
}
