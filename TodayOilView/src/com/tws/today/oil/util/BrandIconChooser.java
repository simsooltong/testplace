package com.tws.today.oil.util;

import com.tws.today.oil.data.StationBrand;
import com.tws.today.oilview.ui.R;

public class BrandIconChooser {
	
	/**
	 * @author DSKIM</br>
	 * 
	 * 밴드아이콘의 Drawable Id 값을 리턴해준다.
	 * 
	 * @param poll 서버로 부터 받은 주유소 카테코드
	 * @return 이미지 리소스 아이디
	 */
	public static int getBrandIcon(String poll){
		int realPath  = 0;
        if (poll.equals(StationBrand.SKE)||poll.equals(StationBrand.SKG)) {// SK
            realPath=R.drawable.today_icon_01;
        } else if (poll.equals(StationBrand.GS)) {// GS
            realPath=R.drawable.today_icon_02;
        } else if (poll.equals(StationBrand.HD) ) {// 현대
            realPath=R.drawable.today_icon_03;
        } else if (poll.equals(StationBrand.SOIL)) {// S-OIL
            realPath=R.drawable.today_icon_04;
        } else if (poll.equals(StationBrand.NH)||poll.equals(StationBrand.NC)) {// NH or NCOIL
            realPath=R.drawable.today_icon_05;
        } else if (poll.equals(StationBrand.RTO)||poll.equals(StationBrand.RTX)) {// 알뜰
            realPath=R.drawable.today_icon_06;
        } else if (poll.equals(StationBrand.E1)) {// E1
            realPath=R.drawable.today_icon_07;
        } else if (poll.equals(StationBrand.ETC)) {// 기타
            realPath=R.drawable.today_icon_08;
        }  else {

        }
        return realPath;
	}
}
