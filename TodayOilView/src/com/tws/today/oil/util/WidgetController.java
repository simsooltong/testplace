package com.tws.today.oil.util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by dskim on 2015-03-13.
 */
public class WidgetController {
    public static void updateWidget(Context from, Class<?> to, String updateAction ){
        Intent intent = new Intent(from, to);
        intent.setAction("android.appwidget.action.ACTION_REFRESH");
        from.sendBroadcast(intent);
    }
}
