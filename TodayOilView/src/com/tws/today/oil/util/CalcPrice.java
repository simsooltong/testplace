package com.tws.today.oil.util;

import com.tws.today.oil.data.PoiInfo;
import com.tws.today.oil.data.SearchPoiRadiusItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CalcPrice {
	private ArrayList<PoiInfo> poi = null;
    private ArrayList<SearchPoiRadiusItem> raduisItems = null;
	private String[] gasType = {"고급휘발유", "휘발유", "경유", "LPG"};

    /*public CalcPrice(ArrayList<SearchPoiRadiusItem raduisItems>){
        this.raduisItems  = raduisItems;
    }*/

	public CalcPrice(ArrayList<PoiInfo> poi) {
		this.poi = poi;
	}
	
	public ArrayList<PoiInfo> getPoi() {
		return poi;
	}

    public ArrayList<SearchPoiRadiusItem> getRaduisItems(){
        return raduisItems;
    }

	/**
	 * 최저가 2,3등의 PoinInfo를 리턴한다.
	 * @return 최저가 2,3 PoiInfo List
	 */
	public ArrayList<PoiInfo> get23(){
		ArrayList<PoiInfo> temp234 = new ArrayList<PoiInfo>();
		if(poi.size() >= 3){
			try {
				temp234.add(poi.get(2));
				temp234.add(poi.get(1));
			} catch (Exception e) {
				return temp234.size() > 0 ? temp234 : null;
			}
		}else if(poi.size() == 2){
			try {
				temp234.add(poi.get(1));
			} catch (Exception e) {
				return temp234.size() > 0 ? temp234 : null;
			}
		}
		return temp234.size() > 0 ? temp234 : null;
	}

	/**
	 * @param oilType
	 *            유종</br> 0: 휘발유, 1: 고급 휘발유, 2: 경유 , 3: LGP
	 * @return 정렬된 데이터의 최저가
	 */
	public PoiInfo getLowestPoi(int oilType) {
//		poi = extractPureList(oilType);
		if(poi==null ||poi.size()==0){
//			PoiInfo temp = new PoiInfo();
//			temp.name1 = "주변에 "+gasType[oilType]+" 주유소가 없습니다.";
//			OilData data = new OilData();
//			temp.oildata = data;
			return null;
		}else{
			switch (oilType) {
			case 0:
				Collections.sort(poi, new Desc_hg_price());
				for (PoiInfo item : poi) {
					System.err.println("고휘발유 : "+item.name1+","+item.oildata.hg_price);
				}
				break;
			case 1:
				Collections.sort(poi, new Desc_g_price());
				for (PoiInfo item : poi) {
					System.err.println("휘발유 : "+item.name1+","+item.oildata.g_price);
				}
				break;
			case 2:
				Collections.sort(poi, new Desc_d_price());
				for (PoiInfo item : poi) {
					System.err.println("경유 : "+item.name1+","+item.oildata.d_price);
				}
				break;
			case 3:
				Collections.sort(poi, new Desc_l_price());
				for (PoiInfo item : poi) {
					System.err.println("LPG : "+item.name1+","+item.oildata.l_price);
				}
				break;
			}
			return poi.get(0);
		}
	}

	/**
	 * @param oilType
	 *            유종</br> 0: 휘발유, 1: 고급 휘발유, 2: 경유 , 3: LGP
	 * @return 평균 유가
	 */
	public String[] getAveragePrice(int oilType) {

		String retValue[] = new String[2];
		int sumVlaue = -1;
		
		if(poi!=null){
			if(poi.size()==0){
				retValue[0] ="0";
				retValue[1] = "주변에 주유소("+gasType[oilType]+") 가 없습니다.";
				return retValue;
			}else{
				for (PoiInfo item : poi) {
					switch (oilType) {
					case 0:
						sumVlaue += Integer.parseInt(item.oildata.hg_price);
						break;
					case 1:
						sumVlaue += Integer.parseInt(item.oildata.g_price);
						break;
					case 2:
						sumVlaue += Integer.parseInt(item.oildata.d_price);
						break;
					case 3:
						sumVlaue += Integer.parseInt(item.oildata.l_price);
						break;
					}
				}
				retValue[0] = String.valueOf((sumVlaue / poi.size()));
				retValue[1] = poi.get(0).address;
				return retValue;
			}
		}else{
			retValue[0] ="0";
			retValue[1] = "주변에 주유소("+gasType[oilType]+") 가 없습니다.";
			return retValue;
		}
	}


	static class Desc_hg_price implements Comparator<PoiInfo> {
		@Override
		public int compare(PoiInfo lhs, PoiInfo rhs) {
			return lhs.oildata.hg_price.compareTo(rhs.oildata.hg_price);
		}
	}
	
	static class Desc_g_price implements Comparator<PoiInfo> {
		@Override
		public int compare(PoiInfo lhs, PoiInfo rhs) {
			return lhs.oildata.g_price.compareTo(rhs.oildata.g_price);
		}
	}

	static class Desc_d_price implements Comparator<PoiInfo> {
		@Override
		public int compare(PoiInfo lhs, PoiInfo rhs) {
			return lhs.oildata.d_price.compareTo(rhs.oildata.d_price);
		}
	}

	static class Desc_l_price implements Comparator<PoiInfo> {
		@Override
		public int compare(PoiInfo lhs, PoiInfo rhs) {
			return lhs.oildata.l_price.compareTo(rhs.oildata.l_price);
		}
	}

}
