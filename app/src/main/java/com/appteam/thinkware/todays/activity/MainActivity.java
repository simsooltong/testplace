package com.appteam.thinkware.todays.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.appteam.thinkware.todays.R;

import de.greenrobot.event.EventBus;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(MainActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(MainActivity.this);
    }

}
