package com.appteam.thinkware.todaycommute.activitys;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appteam.thinkware.todaycommute.R;
import com.appteam.thinkware.todaycommute.customUI.CustomIntroDialog;
import com.appteam.thinkware.todaycommute.customUI.CustomProgress;
import com.appteam.thinkware.todaycommute.data.MapCCTV;
import com.appteam.thinkware.todaycommute.data.MapCCTV_Item;
import com.appteam.thinkware.todaycommute.data.SearchCoordAddr;
import com.appteam.thinkware.todaycommute.network.ApiAgent;
import com.appteam.thinkware.todaycommute.network.RetCode;
import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;

import CoordLib.NorXY_T;
import tws.today.lib.application.TodayApplication;
import tws.today.lib.gms.LocationDefines;
import tws.today.lib.gms.LocationGMS;
import tws.today.lib.mapview.Defines;
import tws.today.lib.mapview.Defines.MAPConst;
import tws.today.lib.mapview.WebViewSingleton;
import tws.today.lib.mapview.twsMapController;
import tws.today.lib.mgr.PrefMgr;
import tws.today.lib.util.LOG;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    // S Primitive//
    private static final int OFFSET_VAL = 54;
    private static final int ENTER_FAVORITE = 1;
    private static final int ENTER_LOW = 2;
    private static final int ENTER_NORMAL = 0;
    private static final int DEFAULT_MAP_LEVEL = 9;
    private static final long ADDR_TIME = 700;
    public static final String STR_LEVEL = "9";
    private final double WEB_RESIZE = 0.67;
    private long NewTimeStamp;
    private long OldTimeStamp;
    private String now_X;
    private String now_Y;
    private int lastLevel = 9;
    private boolean isLastLocation = true;
    // E Primitive//

    // S Objet//
    private twsMapController mapController = null;
    private ApiAgent apiAgent = null;
    private PrefMgr prefMgr = null;
    private Context context = null;
    private Display display = null;
    private ConcurrentHashMap<String, MapCCTV_Item> hash_CCTV = null;
    private MapCCTV_Item selectedCCTVItem = null;
    // E Objet//

    //S Widget//
    private RelativeLayout rootLayout = null;
    private CustomProgress waitDialog = null;
    private CustomIntroDialog customIntroDialog = null;
    private WebView webview = null;
    private VideoView vvCCTVVideo = null;
    private ImageView ivFavorite = null;
    private LinearLayout llUnderInfoLayout = null;
    private ImageView ivDetail = null;
    //E Widget//

    private TextView tvCCTVName = null;
    public Handler locationResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isFinishing())
                return;
            switch (msg.what) {

                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "내 위치 확인 불가, 앱을 종료합니다.");
                            dismissIntroDialog();
                            dismissWaitDialog();
                        }
                    }, Toast.LENGTH_SHORT);
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");
                    isLastLocation = false;
                    if (msg.obj != null) {
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "location getAccuracy : "
                                + location.getAccuracy());
                        Log.i("LocationResultHandler", "location getLongitude : "
                                + location.getLongitude());
                        Log.i("LocationResultHandler", "location getLatitude : "
                                + location.getLatitude());

                        setClickableControl(true);

                        String xyStr[] = convertXY(location);
                       /* if (isRequestButton == true) {// 내위치 찾기
                            logFlurryAgent(DefineFlurry.keyMA_LOC,DefineFlurry.keyXY, xyStr[0] + "|" + xyStr[1]);
                            isRequestButton = false;
                        } else { // 초반 시작시
                            FlurryAgent.setLocation(Float.parseFloat(xyStr[1]), Float.parseFloat(xyStr[0]));
                        }*/
                        prefMgr.setMyPosition(xyStr[0] + "|" + xyStr[1]);
                        now_X = xyStr[0];
                        now_Y = xyStr[1];
                        initWebview();
                        // }
                    } else {

                    }

                    break;

                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    break;

                case LocationDefines.GMS_LOCATION_TIMEOUT:// 측위 요청했지만 Timeout
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    isLastLocation = true;
                    setClickableControl(true);
                    String[] xy = prefMgr.getMyPosition().split("\\|");
                    now_X = xy[0];
                    now_Y = xy[1];
                    initWebview();
                    break;
            }
        }
    };
    /**
     * 네트워크 통신 결과 핸들러.
     */
    public Handler netResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("NetResult_Handler msg what" + msg.what);

            switch (msg.what) {
                case RetCode.RET_SEARCH_CCTV_SUCC:
                    LOG.d("RET_SEARCH_CCTV_SUCC");
                    MapCCTV mapCCTV = (MapCCTV) msg.obj;
                    ArrayList<MapCCTV_Item> cctvList = mapCCTV.row;
                    dismissIntroDialog();
                    dismissWaitDialog();
                    retApiTconCCTV(cctvList);
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_SUCC:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC"
                            + ((SearchCoordAddr) msg.obj).cut_address);
                    // System.out.println("RET_SEARCH_COORD_ADDR_SUCC"+searchCoordAddr.address);
//                    retApiSearchCoordAddr((SearchCoordAddr) msg.obj);
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_FAIL:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
//				tvPostionAddr.setText("정보 없음");
                    break;
                case RetCode.RET_SEARCHPOI_NUMBER_SUCC:
                    LOG.d("RET_SEARCHPOI_NUMBER_SUCC");
//                    retApiSearchPoiNumber((SearchPoi) msg.obj);
                    break;
                case RetCode.RET_SEARCHPOI_SUCC:
                    LOG.d("RET_SEARCHPOI_SUCC");
//                    apiSearchCoorAddr(now_X, now_Y);
//                    retApiSearchPoiAround((SearchPoi) msg.obj);
                    break;

                case RetCode.NET_ERROR_MSG:
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR_MSG),
                            msg.obj.toString(), getPackageName());
                    Toast.makeText(MainActivity.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    dismissWaitDialog();
                    // Toast.makeText(context, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    // LOG.d("NET_RETURN_ERROR_MSG");
                    // new Handler().postDelayed(new Runnable() {
                    // @Override
                    // public void run() {
                    // finishUI("네트워크 에러", "네트워크 불안정으로 앱을 종료합니다.");
                    // }
                    // }, Toast.LENGTH_SHORT);
                    break;

                case RetCode.NET_ERROR:

                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();

                    LOG.d("NET_RETURN_ERROR_MSG");
                    FlurryAgent.onError(String.valueOf(RetCode.NET_ERROR),
                            "RetCode.NET_ERROR", getPackageName());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "네트워크가 불안정하여 앱을 종료합니다.");
                        }
                    }, Toast.LENGTH_SHORT);
                    // BaseMessagePopup(null, null, -1, false, true);

                    break;

            }
        }
    };
    /**
     * webview 지도에서 자바스크립트 콜백 결과 받는 핸들러.
     */
    public Handler mapCBHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("OilViewMainActivity MapCB_Handler msg what : " + msg.what);

            switch (msg.what) {
                case MAPConst.CB_SETTOUCHEND:
                    Log.e("DSKIM", "MapCB_Handler : CB_SETTOUCHEND");
                    break;
                case MAPConst.MAP_SUCC:
                case MAPConst.MAP_RECYCLE:
                case MAPConst.CB_ISMAP:
                    // 지도 로딩 성공
                    LOG.d("MapCB_Handler : MAP_SUCC");

                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();
                    mapController.TwsMapSetLevel(DEFAULT_MAP_LEVEL);
                    mapController.TwsMapSetEndTouch();
                    mapController.TwsMapSetMoveend();
                    String[] xy = null;

                    switch (prefMgr.getEnterType()) {
                        case ENTER_NORMAL:
                        case ENTER_LOW:
                            startUI(now_X, now_Y);
                            break;
                        case ENTER_FAVORITE:
                            mapController.TwsMapClearMarkers();
                            setClickableControl(true);

                            xy = prefMgr.getMyPosition().split("\\|");

                            if (prefMgr.getFavoriteName().length() < 1) {
                                requestLocation();
                            } else {
                                setMyPosition(xy[0], xy[1]);
                                /*apiSearchPoiNumber(prefMgr.getFavoriteName(), xy[0], xy[1]);*/
                            }
                            prefMgr.setEnterType(0);
                            break;
                /*
                 * case 2: mapController.TwsMapClearMarkers();
				 * setClickableControl(true); xy =
				 * prefMgr.getMyPosition().split("\\|");
				 * apiSearchPoiNumber(prefMgr.getLowName(), xy[0], xy[1]);
				 * setMyPosition(xy[0], xy[1]); prefMgr.setEnterType(0); break;
				 */
                    }

                    // if(callType==null){
                    // }else{
                    //
                    // if(callType.equals("favorite")){
                    // if(prefMgr.getFavoriteName().length()<1){
                    // callType = null;
                    // requestLocation();
                    // }else{
                    // apiSearchPoiNumber(prefMgr.getFavoriteName(), xy[0], xy[1]);
                    // }
                    // }else if(callType.equals("low")){
                    //
                    // }
                    // }
                    break;

                case MAPConst.MAP_FAIL:
                    // 지도 로딩 실패

                    LOG.d("MapCB_Handler : MAP_FAIL");

                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();

                    WebViewSingleton.clear((TodayApplication) getApplication(),
                            Defines.InstanceConst.MAIN_MAP_INSTANCE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "지도 초기화 실패하여 앱을 종료합니다.");
                        }
                    }, Toast.LENGTH_SHORT);
                    // 종료 처리.

                    break;
                case MAPConst.CB_GET_EXTENT:// x1,x2,y1,y2

                    LOG.d("CB_GET_EXTENT : " + (String) msg.obj);
                    // 2014.12.17시나리오변경 반경3km extent deprecated
                    // String[] extentXY = ((String) msg.obj).split("\\|");
                    // apiSearchPoiAround(extentXY);
                    break;
                case MAPConst.CB_SETZOOMEND:
                    LOG.d("CB_SETZOOMEND : " + (String) msg.obj);
                    String[] zoomInfo = ((String) msg.obj).split("\\|");
                    // apiSearchPoiAround(zoomInfo);
                    lastLevel = Integer.parseInt(zoomInfo[2]);

                    if (checkMapLevel() == false) {
                        apiTconCCTV(zoomInfo[0], zoomInfo[1]);
//                        Toast.makeText(MainActivity.this, getResources().getString(R.string.no_support_map_level), Toast.LENGTH_SHORT).show();
                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));
                    break;
                case MAPConst.CB_MOVEEND:
                    LOG.d("CB_MOVEEND : " + (String) msg.obj);
                    final String[] moved = ((String) msg.obj).split("\\|");
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));

                    OldTimeStamp = Calendar.getInstance().getTimeInMillis();

                    // Timer count;
                    CountDownTimer cdt = new CountDownTimer(ADDR_TIME, ADDR_TIME) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFinish() {
                            // TODO Auto-generated method stub

                            // ??? 怨??

                            NewTimeStamp = Calendar.getInstance().getTimeInMillis();

                            if ((NewTimeStamp - OldTimeStamp >= ADDR_TIME)
                                    && OldTimeStamp != 0) {

//                                apiSearchCoorAddr(moved[0], moved[1]);
                                apiTconCCTV(moved[0], moved[1]);
                                OldTimeStamp = 0;
                            }
                        }

                    }.start();

                    break;
                case MAPConst.CB_SETONLYMOVEEND:
                    LOG.d("CB_SETONLYMOVEEND : " + (String) msg.obj);
                    final String[] array_addr;
                    array_addr = ((String) msg.obj).split("\\|");
                    // now_X = Integer.parseInt(array_addr[0]);
                    // now_Y = Integer.parseInt(array_addr[1]);
                    now_X = array_addr[0];
                    now_Y = array_addr[1];
                    lastLevel = Integer.parseInt(array_addr[2]);
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(().getDrawable(
//						R.xml.effect_search_button));

                    break;

                case MAPConst.CB_SETTOUCHEVENTCB:
                    LOG.d("CB_SETTOUCHEVENTCB : " + (String) msg.obj);
                    String[] setTouchEventCBarr;
                    setTouchEventCBarr = ((String) msg.obj).split("\\|");

                    if (setTouchEventCBarr[0].equals("touchend")) {
                      /*  if (isTouchedMarker) {
                            isTouchedMarker = false;
                        } else {
                            llUnderLayout.setVisibility(View.GONE);
                            mapController.TwsMapRemoveDivPopup("marker_select");
                        }*/
                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
                    break;

                case MAPConst.CB_CREATEMARKER:
                    final String[] array_marker;
                    array_marker = ((String) msg.obj).split("\\|");
                    int nMakrer_index = Integer.parseInt(array_marker[0]);
                    mapController.TwsMapSetTouchendMarkerCB(nMakrer_index);
                    break;
                case MAPConst.CB_GETCENTER:
                    LOG.d("CB_GETCENTER : " + (String) msg.obj);
                    // return x | y

                    break;

                case MAPConst.CB_GETLEVEL:
                    LOG.d("CB_GETLEVEL : " + (String) msg.obj);
                    // return level
                    break;

                case MAPConst.CB_CREATEFEATUREMAKER:
                    LOG.d("CB_CREATEFEATUREMAKER : " + (String) msg.obj);
                    // return marker_id|param
                    break;
                case MAPConst.CB_TOUCHENDMARKER:
                    LOG.d("CB_TOUCHENDMARKER : " + (String) msg.obj);

                    break;

                case MAPConst.CB_TOUCHENDFEATUREMARKER:
                    LOG.d("CB_TOUCHENDFEATUREMARKER : " + (String) msg.obj);
                    // return marker_id|param

                    break;

                case MAPConst.CB_GET:// 마커 터치 했을 경우 콜백
                    LOG.d("CB_GET : " + (String) msg.obj);
                    mapController.TwsMapRemoveDivPopup("marker_select");
                    final String[] array_marker_touch;
                    array_marker_touch = ((String) msg.obj).split("\\|");
                    llUnderInfoLayout.setVisibility(View.VISIBLE);
                    if (array_marker_touch.length == 3) {
                        if (array_marker_touch[1].equals("c")) {
                            selectedCCTVItem = hash_CCTV.get(array_marker_touch[2]);
                            if (selectedCCTVItem != null) {
                                drawSelectedMarker(selectedCCTVItem);
                                showDetailInfo(selectedCCTVItem);
                            } else {

                            }
                        } else {
                            mapController.TwsMapRemoveDivPopup("marker_select");
                        }
                    } else {

                    }
                    break;

                case MAPConst.CB_CENTERMAKER:

                    LOG.d("CB_GET : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETLONLATFROMPIXEL:
                    LOG.d("CB_GETLONLATFROMPIXEL : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETPIXELFROMLONLAT:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_CREATEDIVPOPUP:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // isTouchedMarker = true;
                    // return param
                    break;
            }
        }
    };

    private boolean checkMapLevel() {
        boolean validation = false;
        if (6 < lastLevel) {
            validation = true;
        } else {
            validation = false;
            initMarker();
        }
        return validation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commute_main);
        initData();
        createSplashoDialog();
        requestLocation();
        setControl();


    }

    @Override
    protected void onResume() {
        vvCCTVVideo.resume();
        vvCCTVVideo.start();
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        vvCCTVVideo.stopPlayback();
        vvCCTVVideo.pause();
//        if (webview != null) {
//            rootLayout.removeView(webview);
//        }
        // singleton webview view에 제거 E

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (webview != null) {
            rootLayout.removeView(webview);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void finishUI(String title, String msg) {
        // TODO Auto-generated method stub
        dismissWaitDialog();
        // 종료처리.
    }

    private void setControl() {
        prefMgr = new PrefMgr(MainActivity.this);
        hash_CCTV = new ConcurrentHashMap<String, MapCCTV_Item>();

        ivDetail = (ImageView) findViewById(R.id.ivDetail);
        ivFavorite = (ImageView) findViewById(R.id.ivFavorite);
        tvCCTVName = (TextView) findViewById(R.id.tvCCTVName);
        vvCCTVVideo = (VideoView) findViewById(R.id.vvCCTVVideo);

        llUnderInfoLayout = (LinearLayout) findViewById(R.id.llUnderInfoLayout);
        llUnderInfoLayout.setVisibility(View.GONE);

        ivDetail.setOnClickListener(this);
        ivFavorite.setOnClickListener(this);
        vvCCTVVideo.setOnClickListener(this);
    }

    private void setClickableControl(boolean click) {

    }

    private String[] convertXY(Location location) {
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1] = String.valueOf(data.y);
        return xyStr;
    }

    protected void startUI(String longi, String lati) {
        // TODO Auto-generated method stub

        // poi 정보 가져오기.
        mapController.TwsMapMoveTo(Integer.parseInt(longi), Integer.parseInt(lati));
        // mapController.TWsMapGetExtent();
        setMyPosition(longi, lati);
        apiTconCCTV(longi, lati);
        /*apiSearchPoiAround(longi, lati);*/
    }

    private void requestLocation() {
        // GMS 측위 요청.
        // makeWaitDialog();
        LocationGMS location = new LocationGMS(MainActivity.this, locationResultHandler);
        location.connect();
    }

    /**
     * @param longi X좌표
     * @param lati  Y좌표
     *              <p/>
     *              받아온 좌표를 my_long, my_lati에 대입한다. 추후 자주 쓸꺼라서.
     * @author DSKIM</br>
     */
    private void setMyPosition(String longi, String lati) {
        String strImagePath = null;
        try {
            if (isLastLocation) {// 측위 Timeout
                strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
            } else {// 측위 성공
                strImagePath = "/android_asset/mapicon/frm_my_point_01.png";
            }
        } catch (Exception e) {
            e.printStackTrace();
            strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
        }
        mapController.TwsMapRemoveFeatureMarkerALL();
        mapController.TwsMapAddCenterMarker(Integer.parseInt(longi),
                Integer.parseInt(lati), strImagePath);
    }

    private void dismissIntroDialog() {
        if (customIntroDialog != null)
            customIntroDialog.dismiss();
    }

    private void createSplashoDialog() {
        customIntroDialog = new CustomIntroDialog(MainActivity.this);
        customIntroDialog.show();
        customIntroDialog.setCancelable(false);
    }

    private void dismissWaitDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            waitDialog.dismiss();
            waitDialog = null;
        }
    }

    private void makeWaitDialog() {
        waitDialog = new CustomProgress(MainActivity.this);
        waitDialog.show();
    }

    private void initData() {
        apiAgent = new ApiAgent();
    }

    public void initWebview() {

        context = getApplicationContext();

        rootLayout = (RelativeLayout) findViewById(R.id.webview_map);

        display = getWindowManager().getDefaultDisplay();

        // 지도 로딩 시작
        // if (!(mProgDlg.isShowing()))
        // mProgDlg.show();

        // 웹뷰 불러오기. (어플리케이션에서) S

        // 콜백 확인 후 처리한다. init인지 recycle
        WebViewSingleton webViewSingleton = WebViewSingleton.getInstance((TodayApplication) getApplication(), Defines.InstanceConst.MAIN_MAP_INSTANCE);

        webViewSingleton.setHandler(mapCBHandler);

        webview = webViewSingleton.getWebView();

        if (webview != null) {

            // singleton webview view에 추가.
            ViewGroup parent = (ViewGroup) webview.getParent();

            if (parent == null) {
                rootLayout.addView(webview, display.getWidth(),
                        display.getHeight());
            }

        } else {
            // 예외처리.
        }

        // 웹뷰 불러오기. (어플리케이션에서) E

        // 웹뷰 컨트롤 등록
        if (webview != null) {
            mapController = new twsMapController(webview);
            mapController.TwsMapGetCenter();
            mapController.TwsMapSetPantoendCB(true);

            // mapController.TwsMapSetStartTouch();
            // mapController.TwsMapSetEndTouch();
        } else {
            // 예외 처리 필요 (종료처리)
            // PopupDialog_Finish(AppDefines.ERROR_MSG_SET, mContext
            // .getResources().getString(R.string.ONAIR_SERVER_FAIL));
        }
    }

    private void apiTconCCTV(String strX, String strY) {
        ApiAgent api = new ApiAgent();

        if (checkMapLevel() == false) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.no_support_map_level), Toast.LENGTH_SHORT).show();
            llUnderInfoLayout.setVisibility(View.GONE);
            return;
        }
 /*       String strLevel = Integer.toString(getmMapLevel());

        if (!CheckLevel(1, strLevel))
            return;

        // 네트워크 체크
        boolean bFlag = NetworkUtils.NetUnAvailable(mActivity, false);
        if (!bFlag) {
            PopupDialog_Finish(AppDefines.ERROR_MSG_NETWORK, "");
            return;
        }*/
        if (api != null) {
            api.apiTconCCTV(this, strX, strY, STR_LEVEL, new Response.Listener<MapCCTV>() {

                @Override
                public void onResponse(MapCCTV retCode) {

                    LOG.d("retCode.result : " + retCode.result);
                    LOG.d("retCode.errormsg : " + retCode.errormsg);

                    if (retCode.result == 1) {
                        // success

                    } else {
                        // fail
                        LOG.d("apiSetUserLoc Fail " + retCode.result);
                        Toast.makeText(MainActivity.this, retCode.errormsg + "(" + retCode.result + ")", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiSetUserLoc VolleyError " + volleyError.getMessage());
                    Toast.makeText(MainActivity.this, "네트워크 오류", Toast.LENGTH_SHORT).show();
                }
            });
        }

//        api.apiTconCCTV(MainActivity.this, strX, strY, STR_LEVEL,
//                new AjaxCallback<MapCCTV>() {
//
//                    // callback 처리.
//                    @Override
//                    public void callback(String url, MapCCTV object,
//                                         AjaxStatus status) {
//                        // TODO Auto-generated method stub
//
//                        LOG.d("apiTconCCTV AQuery url : " + url);
//                        LOG.d("apiTconCCTV net AQuery status.getCode() : "
//                                + status.getCode());
//
//                        if (status.getCode() == RetCode.HTTP_SUCC) {
//                            // http 200 성공
//                            LOG.d("apiTconCCTV AQuery object.result : "
//                                    + object.result);
//                        } else {
//                            // http 오류 나 네트워크 오류
//                            object = null;
//                        }
//
//                        if (object != null) {
//
//                            final MapCCTV sr = (MapCCTV) object;
//
//                            // return data check
//                            LOG.d("apiTconCCTV sr.result : " + sr.result);
//                            LOG.d("apiTconCCTV sr.errormsg : " + sr.errormsg);
//                            LOG.d("apiTconCCTV sr.rescnt : " + sr.rescnt);
//
//                            if (sr.result == 0) {
//                                // 성공
//                                Message msg = netResultHandler.obtainMessage();
//
//                                msg.what = RetCode.RET_SEARCH_CCTV_SUCC;
//                                msg.obj = sr;
//
//                                netResultHandler.sendMessage(msg);
//
////                                ArrayList<MapCCTV_Item> cctvInfo = sr.row;
////                                for (int i = 0; i <cctvInfo.size() ; i++) {
////                                    System.err.println("DSKIM : "+cctvInfo.get(i).cctv_id);
////                                }
////                                put_array_CCTVInfo(CCTVInfo);
//
//                            } else if (sr.result == RetCode.NET_ERROR_MSG) {
//                                // 조회 결과 없음
//
//                            } else {
//                                // 서버 오류 메시지 출력
////                                PopupDialog_Finish(AppDefines.ERROR_MSG_TOAST,
////                                        sr.errormsg + "(" + sr.result + ")");
//                            }
//
//                        } else {
//                            // 실패
////                            PopupDialog_Finish(AppDefines.ERROR_MSG_NETWORK, "");
//                        }
//                        super.callback(url, object, status);
//                    }
//                });

    }


    private void retApiTconCCTV(ArrayList<MapCCTV_Item> mapCCTV_items) {
        int longi = 0;
        int lati = 0;
        String cctvId = null;
        for (MapCCTV_Item item : mapCCTV_items) {
            if (item.cctv_id != null) {
                if (hash_CCTV.get(item.cctv_id) != null) {

                } else {
                    hash_CCTV.put(item.cctv_id, item);
                    System.err.println("DSKIM cctv_id " + item.cctv_id);
                    longi = Integer.parseInt(item.longitude);
                    lati = Integer.parseInt(item.latitude);
                    cctvId = item.cctv_id;
                    drawCCTVMarker(longi, lati, cctvId);
                }
            }
        }
    }

    /**
     * 지도 위에 생성된 마커들 모두 삭제(마커와 Div마커 모두 삭제)
     */
    public void initMarker() {
        LOG.d("InitMarkers");
        hash_CCTV.clear();
        mapController.TwsMapRemoveDivPopup("marker_select");
        mapController.TwsMapRemoveDivPopups();
    }

    private void drawCCTVMarker(int longi, int lati, String cctvId) {
        System.err.println("DSKIM drawCCTVMarker ID : " + cctvId);
        // marker S
        String imgPathBrand = "/android_asset/mapicon/frm_map_point_cctv.png";// 수정
//        String imgPathPoint = "/android_asset/mapicon/" + oilTypeMarker[prefMgr.getOilType()]; // 수정
        String imgPathPoint = "/android_asset/mapicon/frm_map_point_cctv.png"; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "c|" + cctvId;

        mapController.TwsMapOilviewDivPopupDoubleCallBack("c_" + cctvId, longi,
                lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
    }

    private void drawSelectedMarker(MapCCTV_Item tempInfo) {
        int nX = Integer.parseInt(tempInfo.longitude);
        int nY = Integer.parseInt(tempInfo.latitude);
        mapController.TwsMapMoveTo(nX, nY);

//        showDetailInfo(tempInfo);

//        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyNAME,tempInfo.catenmae);
//        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyXY, nX + "|"
//                + nY);

//        String imgPathBrand = "/android_asset/mapicon/"+ getImagePath(tempInfo.catecode);// 수정
        String imgPathBrand = "/android_asset/mapicon/map_point_s.png";// 수정
        String imgPathPoint = "/android_asset/mapicon/map_point_s.png"; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "s|" + tempInfo.cctv_id;

        mapController.TwsMapOilviewDivPopupDoubleCallBack("marker_select", nX,
                nY, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
    }

    /**
     * 선택한 마커에 대해서 상세 데이터를 하단 레이아웃에 보여준다.
     */
    private void showDetailInfo(MapCCTV_Item tempInfo) {
        tvCCTVName.setText(tempInfo.location);
        Uri video_url = Uri.parse(tempInfo.stream_url);
        playVideoCCTV(video_url);
    }

    private void playVideoCCTV(Uri video_url) {
        vvCCTVVideo.setVideoURI(video_url);
        vvCCTVVideo.requestFocus();
        vvCCTVVideo.start();

        vvCCTVVideo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub

                //finish();
                return false;
            }
        });

        vvCCTVVideo
                .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(final MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        LOG.d("onPrepared");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                             /*   if (mProDlg.isShowing())
                                    mProDlg.dismiss();*/
                            }
                        });
                        // m_text_cctv_time.postDelayed(getMovieTime, 1000);
                    }
                });


        vvCCTVVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*if (mProDlg.isShowing())
                            mProDlg.dismiss();*/
                    }
                });

//                PopupDialog_MSG("정보 제공처 사정으로 CCTV 영상이 제공되지 않는 구간입니다.");

                return true;
            }
        });

        vvCCTVVideo
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        // 재생 완료
                        LOG.d("onCompletion");

                        // 무한 루프
                        vvCCTVVideo.start();

                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivFavorite:
                //즐겨찾기에 추가, 팝업 표시

                break;
            case R.id.ivDetail:
                vvCCTVVideo.pause();
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("selectItem", selectedCCTVItem);
                startActivity(intent);
                //상세 화면 이동
                break;
        }

    }

    private void slideToAction(View view, float fFromXDelta, float fToXDelta,float fFromYDelta, float fToYDelta, int nVisibility) {
        TranslateAnimation animate = new TranslateAnimation(fFromXDelta, fToXDelta, fFromYDelta, fToYDelta);
        animate.setFillAfter(false);
        animate.setDuration(500);
        view.startAnimation(animate);
        view.setVisibility(nVisibility);
    }
}
