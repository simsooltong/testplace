package com.appteam.thinkware.todaycommute.util;

public class CommaGenerator {
    public static String withCommaString(String costValue) {
        int intval = Integer.parseInt(costValue);
        return String.format("%,d", intval);
    }
}
