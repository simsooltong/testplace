package com.appteam.thinkware.todaycommute.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-01-13.
 */
public class MapCCTV {

    public int result;            //  결과코드
    public String errormsg;        //  오류 메시지
    public int retcode;            //  결과코드
    public String retmsg;        //  오류 메시지
    public int rescnt;            //  검색 결과 갯수

    public ArrayList<MapCCTV_Item> row;        // 결과 리스트

}
