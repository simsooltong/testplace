package com.appteam.thinkware.todaycommute.activitys;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.VideoView;

import com.appteam.thinkware.todaycommute.R;
import com.appteam.thinkware.todaycommute.data.MapCCTV_Item;
import com.bumptech.glide.Glide;

import tws.today.lib.util.LOG;

/**
 * Created by dskim on 2015-01-15.
 */
public class DetailActivity extends BaseActivity {

    private VideoView vvDetailCCTVVideo = null;
    private MapCCTV_Item mapCCTV_item;
    private ImageView ivDetailScreenShot1 = null;
    private ImageView ivDetailScreenShot2 = null;
    private SeekBar sbDaySeeker = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commute_detail);
        setControl();

        Intent getCCTVData = getIntent();
        mapCCTV_item = (MapCCTV_Item) getCCTVData.getSerializableExtra("selectItem");
        playVideoCCTV(mapCCTV_item.stream_url);
        bindingURLImage(ivDetailScreenShot2, mapCCTV_item.thumbnail);
        bindingURLImage(ivDetailScreenShot1, mapCCTV_item.thumbnail);
    }

    private void setControl() {
        sbDaySeeker = (SeekBar)findViewById(R.id.sbDaySeeker);
        sbDaySeeker.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                return true;
            }
        });
        vvDetailCCTVVideo = (VideoView) findViewById(R.id.vvDetailCCTVVideo);
        ivDetailScreenShot1 = (ImageView)findViewById(R.id.ivDetailScreenShot1);
        ivDetailScreenShot2 = (ImageView)findViewById(R.id.ivDetailScreenShot2);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void bindingURLImage(ImageView imageView, String photo_url) {
        try{
            Glide.with(this).load(photo_url).into(imageView);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void playVideoCCTV(String video_url) {
        Uri uri = Uri.parse(video_url);
        vvDetailCCTVVideo.setVideoURI(uri);
        vvDetailCCTVVideo.requestFocus();
        vvDetailCCTVVideo.start();

        vvDetailCCTVVideo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub

                //finish();
                return false;
            }
        });

        vvDetailCCTVVideo
                .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(final MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        LOG.d("onPrepared");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                             /*   if (mProDlg.isShowing())
                                    mProDlg.dismiss();*/
                            }
                        });
                        // m_text_cctv_time.postDelayed(getMovieTime, 1000);
                    }
                });


        vvDetailCCTVVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*if (mProDlg.isShowing())
                            mProDlg.dismiss();*/
                    }
                });

//                PopupDialog_MSG("정보 제공처 사정으로 CCTV 영상이 제공되지 않는 구간입니다.");

                return true;
            }
        });

        vvDetailCCTVVideo
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        // 재생 완료
                        LOG.d("onCompletion");

                        // 무한 루프
                        vvDetailCCTVVideo.start();

                    }
                });
    }
}
