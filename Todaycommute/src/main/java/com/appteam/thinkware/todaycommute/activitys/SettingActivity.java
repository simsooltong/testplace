package com.appteam.thinkware.todaycommute.activitys;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.appteam.thinkware.todaycommute.data.DefineFlurry;
import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

public class SettingActivity extends BaseActivity implements OnClickListener {

    //	private String[] oilArray = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    private void setControll() {

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    private void StartFlurryAgent() {
        FlurryAgent.onStartSession(this, DefineFlurry.FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.onPageView();
//		FlurryAgent.setUserId("20141202_1");
        logFlurryAgent(DefineFlurry.eventIntro, DefineFlurry.keyView, DefineFlurry.vMain);
    }

    private void EndFFlurryAgent() {
        FlurryAgent.onEndSession(this);
    }

    private void logFlurryAgent(String event) {
        FlurryAgent.logEvent(event);
    }

    private void logFlurryAgent(String event, String key, String value) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(key, value);
        FlurryAgent.logEvent(event, params);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    private void showConfirmPopup(String strTitle, String strContents) {

	/*	final Dialog confirmPopUp = new Dialog(this);
		confirmPopUp.setCanceledOnTouchOutside(false);
		confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmPopUp.setContentView(R.layout.layout_common_popup);
		
		TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
		TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
		title.setText(strTitle);
		contents.setText(strContents);
		Button btnConfirm  = (Button)confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				prefMgr.setOilType(oilType);
				prefMgr.setIsFirst(false);
				confirmPopUp.dismiss();
//				setResult(0);
//				finish();
			}
		});
		confirmPopUp.setCancelable(false);
		confirmPopUp.show();
		confirmPopUp.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));*/
    }

    // oiiType popup S
    private void showOilTypePopup(int initChecked) {
		/*// TODO Auto-generated method stub
		final Dialog oilTypePopup = new Dialog(this);
		oilTypePopup.setCanceledOnTouchOutside(false);
		oilTypePopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
		oilTypePopup.setContentView(R.layout.layout_oiltype);

		mRbRowArray = new RadioButton[4];

		Button btnOilTypeConfirm = (Button) oilTypePopup
				.findViewById(R.id.olitype_btn_confirm);
		mRbRowArray[0] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_hg);
		mRbRowArray[1] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_g);
		mRbRowArray[2] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_d);
		mRbRowArray[3] = (RadioButton) oilTypePopup
				.findViewById(R.id.olitype_radio_l);

		mRbRowArray[initChecked].setChecked(true);
		
		RelativeLayout[] mRlRowArray = new RelativeLayout[4];

		mRlRowArray[0] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_hg);
		mRlRowArray[1] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_g);
		mRlRowArray[2] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_d);
		mRlRowArray[3] = (RelativeLayout) oilTypePopup
				.findViewById(R.id.olitype_row_l);

		for (int i = 0; i < mRbRowArray.length; i++) {
			mRlRowArray[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					selectRadioBtn(v, mRbRowArray);
				}
			});
			mRbRowArray[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					selectRadioBtn(v, mRbRowArray);
				}
			});*/
    }

    // oilType popup E

}
