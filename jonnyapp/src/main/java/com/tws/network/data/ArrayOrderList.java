package com.tws.network.data;

import java.util.ArrayList;

/**
 * Created by Jony on 2015-01-15.
 */
public class ArrayOrderList {

    public String index;
    public String regdate;
    public String userid;
    public String storeid;
    public String arrivaltime;
    public String orderkey;
    public String status;
    public String distance;

    public ArrayList<ArrayOrderData> orderdata;
}
