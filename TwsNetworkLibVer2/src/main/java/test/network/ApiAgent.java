package test.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.tws.network.api.ApiBase;
import com.tws.network.lib.AirGsonRequest;
import com.tws.network.lib.JsonGsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

import test.data.CoreGetPublicKey;
import test.data.retData;
import test.util.CommonParams;
import test.util.DeviceInfo;

/**
 * Created by jonychoi on 14. 11. 21..
 */
public class ApiAgent {

    private static final String TAG = "VolleyRequest";

    // into Domain
    //private static final String URL_DOMAIN = "https://airagt.inavi.com";
    private static final String URL_DOMAIN = "http://61.33.249.180:8020";

    // publickey info
    private static final String URL_PUBLICKEY = "/iNaviAir/air/inaviweb/PublicKey";

    private static final String URL_ORDER = "/SoulBrown/sb/ordersoul";
    private static final String URL_REACH = "/SoulBrown/sb/reachsoul";

    // apiOrder
    public void apiOrder(Context context,String name,String store,String storeMenu, String pay, String nearby,  Response.Listener<retData> succListener, Response.ErrorListener failListener) {

        String url = URL_DOMAIN + URL_ORDER;

        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        header.put("version", "soul_v1.0");

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();
        try {

            jsonParams.put("phone","01092868090");
            jsonParams.put("name",name);
            jsonParams.put("store",store);
            jsonParams.put("store_menu",storeMenu);
            jsonParams.put("pay_yn",pay);
            jsonParams.put("nearby_yn",nearby);

        } catch (Exception e) {
            Log.d("soul","apiOrder error:"+e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        Log.d("soul","reqParams " + reqParams);


        // set params E

        // request!
        JsonGsonRequest<retData> gsObjRequest = new JsonGsonRequest<retData>(
                Request.Method.POST,
                url,
                retData.class, header, reqParams,
                succListener, failListener

        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);

    }

    // apiReach
    public void apiReach(Context context, String nearby,  Response.Listener<retData> succListener, Response.ErrorListener failListener) {

        String url = URL_DOMAIN + URL_REACH;

        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        header.put("version", "soul_v1.0");

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();
        try {

            jsonParams.put("phone","01092868090");
            jsonParams.put("nearby_yn",nearby);

        } catch (Exception e) {
            Log.d("soul","apiReach error:"+e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        Log.d("soul","reqParams " + reqParams);


        // set params E

        // request!
        JsonGsonRequest<retData> gsObjRequest = new JsonGsonRequest<retData>(
                Request.Method.POST,
                url,
                retData.class, header, reqParams,
                succListener, failListener

        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);

    }

    // get publickey
    public void apiPublicKey(Context context, Response.Listener<CoreGetPublicKey> succListener, Response.ErrorListener failListener) {

        String url = URL_DOMAIN + URL_PUBLICKEY;

        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        String auth = DeviceInfo.getAuth(context);

        if (!TextUtils.isEmpty(auth))
            header.put("auth_key", auth);

        header.put("mdn", DeviceInfo.getMDN(context));

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();
        try {

            jsonParams = CommonParams.getCommonParams(context, jsonParams);

        } catch (Exception e) {

            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        // set params E

        // request!
        AirGsonRequest<CoreGetPublicKey> gsObjRequest = new AirGsonRequest<CoreGetPublicKey>(
                Request.Method.POST,
                url,
                CoreGetPublicKey.class, header, reqParams,
                succListener, failListener

        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }
}
