package tws.today.lib.mapview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import tws.today.lib.application.TodayApplication;
import tws.today.lib.mapview.Defines.MAPConst;
import tws.today.lib.mgr.NetworkManager;
import tws.today.lib.mgr.PrefMgr;
import tws.today.lib.util.LOG;

/**
 * @author jonychoi
 */
@SuppressLint({ "JavascriptInterface", "SetJavaScriptEnabled" })
public class WebViewSingleton {

	private WebView _WebView = null;

	private Context mContext = null;

	private twsMapController m_TwsMapControl = null;
	private Handler m_MapHandler = null;

	/** webview timeout check! **/
	private boolean mTimeout = true;
	
	private boolean mInitTimeout = true;
	private PrefMgr prefMgr = null;
    private static String mlongi = null;
    private static String mlati = null;

	public static WebViewSingleton getInstance(TodayApplication app, String strInstance) {

		LOG.d("WebViewSingleton getInstance");
		WebViewSingleton svc = null;
		Object obj = app.getObject(strInstance,
				WebViewSingleton.class.getName());
		if (obj != null) {
			LOG.d("WebViewSingleton obj is not null");

			svc = (WebViewSingleton) obj;

			if (svc.m_TwsMapControl != null) {
				LOG.d("WebViewSingleton TwsMapIsMap");

				svc.m_TwsMapControl.TwsMapIsMap();
			} else {

				// �?기�?? ???�?.
				app.removeObject(strInstance);
				svc = new WebViewSingleton();
				svc.initialize(app.getApplicationContext());
				app.addObject(strInstance, svc);

			}

		} else {
			LOG.d("WebViewSingleton obj is null");

			svc = new WebViewSingleton();
			svc.initialize(app.getApplicationContext());
			app.addObject(strInstance, svc);
		}
		return svc;
	}

	/**
	 * singletone clear
	 * 
	 * @param app
	 */
	public static void clear(TodayApplication app, String strInstance) {

		LOG.d("WebViewSingleton clear");

		Object obj = app.getObject(strInstance,
				WebViewSingleton.class.getName());

		if (obj != null) {
			if (app != null)
				LOG.d("WebViewSingleton strInstance : " + strInstance
						+ " removeObject");

			app.removeObject(strInstance);

			obj = null;
		}

	}

	/**
	 * �?기�?? �?�?.
	 * 
	 * @param context
	 */
	private void initialize(Context context) {
		// TODO Auto-generated method stub

		LOG.d("WebViewSingleton initialize");

		mContext = context;
		
		mTimeout = true;

		// init webview
		_WebView = new WebView(context.getApplicationContext());

		// userAgent setting
		_WebView.getSettings().setUserAgentString("Android");

		// m_WebView.getSettings().setSupportZoom(true);
		// _WebView.getSettings().setJavaScriptEnabled(true);

		// image auto reloading
		_WebView.getSettings().setLoadsImagesAutomatically(true);

		// meta tag use
		_WebView.getSettings().setUseWideViewPort(true);
		// _WebView.getSettings().setLoadWithOverviewMode(true);

		// foucs false
		_WebView.setFocusableInTouchMode(false);

		_WebView.setFocusable(false);

		// javascript enabled
		_WebView.getSettings().setJavaScriptEnabled(true);

		 //ZoomDensity
		_WebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);

		_WebView.getSettings().setRenderPriority(
				WebSettings.RenderPriority.HIGH);
		
//		_WebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

		// 2013.09.09 webview setting S
		_WebView.getSettings().setDatabaseEnabled(false);
		_WebView.getSettings().setDomStorageEnabled(false);
		_WebView.getSettings().setGeolocationEnabled(false);
		_WebView.getSettings().setSaveFormData(false);
		// 2013.09.09 webview setting E

		// no cache
		// _WebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

		// interface setting
		JavascriptInterface MapJavaIF = new JavascriptInterface(MapCB_Handler);
		_WebView.addJavascriptInterface(MapJavaIF, "tmjscall");

		// Load the URLs inside the WebView, not in the external web browser
		_WebView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);

				// webview time out

				new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						try {
							// 10sec time out!
							Thread.sleep(15 * 1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (mTimeout) {
							webview_handler
									.sendEmptyMessage(MAPConst.PAGE_TIMEOUT);

						}

					}
				}).start();

				// webview time out

			}

			@Override
			public void onPageFinished(WebView view, String url) {
				// webview onPageFinished

				// cache clear! S
				if (mContext != null) {
					java.io.File dir = mContext.getCacheDir();
					java.io.File[] children = dir.listFiles();

					if (children.length == 0) {

						view.clearCache(true);

					}
				}
				// cache clear! E

				if (mTimeout) {
					mTimeout = false;
					webview_handler.sendEmptyMessage(MAPConst.PAGE_FINISH);

				} else {

				}

				super.onPageFinished(view, url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				view.loadUrl(url);
				return true;

			}

			// SSL ??? �??��
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {

			}

		});

		_WebView.loadUrl("file:///android_asset/android.html");

		m_TwsMapControl = new twsMapController(_WebView);

	}

	private WebViewSingleton() {

	}

	/**
	 * ??�뷰 리�??.
	 * 
	 * @return
	 */
	public WebView getWebView() {
		return _WebView;
	}

	/**
	 * ?????? �???? ??��?��?? ??��??.
	 * 
	 * @param handler
	 * @return
	 */
	public boolean setHandler(Handler handler) {
		this.m_MapHandler = handler;

		if (m_MapHandler != null) {
			Message msg = m_MapHandler.obtainMessage();
			msg.what = MAPConst.MAP_SET_HANDLER;
			msg.obj = !mTimeout;
			m_MapHandler.sendMessage(msg);
		}

		return true;

	}

	/**
	 * Map handler
	 */
	public Handler MapCB_Handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			LOG.d("MapCB_Handler msg what : " + msg.what);
			LOG.d("MapCB_Handler msg obj : " + msg.obj);

			if (m_MapHandler != null) {

				if (msg.what == MAPConst.CB_INIT) {
					
					mInitTimeout = false;
					
					// success init
					LOG.d("MapCB_Handler MAP INIT SUCC!!!");

					// �???? ??�벤??? ??��??.
					m_TwsMapControl.TwsMapSetOnlyMoveend();

					m_TwsMapControl.TwsMapSetZoomend();

					m_MapHandler.sendEmptyMessage(MAPConst.MAP_SUCC);

				} else if (msg.what == MAPConst.CB_ISMAP) {
					// success init
					LOG.d("MapCB_Handler MAP INIT CB_ISMAP!!!");

					m_MapHandler.sendEmptyMessage(MAPConst.MAP_RECYCLE);

				} else {
				
					Message msg_pass = m_MapHandler.obtainMessage();
					msg_pass.what = msg.what;
					msg_pass.obj = msg.obj;
					m_MapHandler.sendMessage(msg_pass);

				}
			} else {
				// ??��?? (??��?��?? null) set ??????.
				// m_MapHandler.sendEmptyMessage(MAPConst.MAP_FAIL);
			}

		}

	};

	/**
	 * webview singleton handler!
	 */
	protected Handler webview_handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {

			if (msg.what == MAPConst.PAGE_FINISH) {

				// page finish!!! (init!)

				LOG.d("webview_handler PAGE_FINISH");

				// �?기�?? ???�?.
				Map_Init();

			} else if (msg.what == MAPConst.PAGE_TIMEOUT) {
				// time out!!!
				LOG.d("webview_handler PAGE_TIMEOUT");

				m_MapHandler.sendEmptyMessage(MAPConst.MAP_FAIL);
				
				
			}

		};
	};

	/**
	 * �???? �?기�??
	 */
	private void Map_Init() {

		// init map
		NetworkManager NetworkMgr = null;
		
		int m_MapLocX = 0;
		int m_MapLocY = 0;
		
		// set level
		int m_MapLevel = Defines.InitMapConst.MAP_DEFAULT_LEVEL;

		if (mContext != null) {
			prefMgr = new PrefMgr(mContext);

			String strPosition[] = prefMgr.getMyPosition().split("\\|");
			
			m_MapLocX = Integer.parseInt(strPosition[0]);
			m_MapLocY = Integer.parseInt(strPosition[1]);

			if (m_MapLocX <= 0 || m_MapLocY <= 0) {
				m_MapLocX = 169050;
				m_MapLocY = 517950;
			}

			NetworkMgr = new NetworkManager();
		}

		// ??��?��????? 체�??.

		if (NetworkMgr != null) {

			if(!NetworkMgr.checkingNetwork(mContext))
			{
				m_MapHandler.sendEmptyMessage(MAPConst.MAP_FAIL);
			}
			else
			{
				m_TwsMapControl.TwsMapInit(m_MapLocX, m_MapLocY, m_MapLevel);
				new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						try {
							// 10sec time out!
							Thread.sleep(15 * 1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						/*
						if (mInitTimeout) {
							
							m_MapHandler
									.sendEmptyMessage(MAPConst.MAP_FAIL);

						}
						*/

					}
				}).start();
			}
			
		}
		else
		{
			m_MapHandler.sendEmptyMessage(MAPConst.MAP_FAIL);
		}

	}

}
