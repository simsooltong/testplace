package tws.today.lib.mgr;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefMgr {

	private final String PREF_NAME_FRAME_USER = "AFK_Frame_User_Info";
	private final String PREF_OIL_TYPE = "oil_type";
	private final String PREF_IS_FIRST = "is_first";
	private final String PREF_FAVORITE_INFO_NAME = "favorite_name";
	private final String PREF_FAVORITE_INFO_COST = "favorite_cost";
	private final String PREF_FAVORITE_INFO_BRAND = "favorite_brand";
	private final String PREF_FAVORITE_INFO_POIID = "favorite_poiid";
	private final String PREF_MY_POSITION = "my_position";
	private final String PREF_MY_TIME = "my_time";
	private final String PREF_ENTER_TYPE = "enter_type";
	private final String PREF_LOW_NM = "low_nm";
	private final String PREF_CHANGE_OIL = "change_oil";
	private final String PREF_UPDATE_TIME = "my_update_time";
	private final String PREF_UPDATE_LURE_TIME = "my_update_lure_time";

	private Context mContext = null;

	private SharedPreferences mPrefFrame = null;
	private SharedPreferences.Editor mEditor = null;

	public PrefMgr(Context context) {
		mContext = context;
		mPrefFrame = mContext.getSharedPreferences(PREF_NAME_FRAME_USER, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
		mEditor = mPrefFrame.edit();
	}

	public void removeAll() {
		mEditor.clear();
		mEditor.commit();
	}
	
	public int getOilType() {
		return mPrefFrame.getInt(PREF_OIL_TYPE, 0);
	}

	public void setOilType(int strId) {
		mEditor.putInt(PREF_OIL_TYPE, strId);
		mEditor.commit();
	}
	
	public boolean getChangeOil(){
		return mPrefFrame.getBoolean(PREF_CHANGE_OIL, false);
	}
	
	public void setChangeoil(boolean boolOil){
		mEditor.putBoolean(PREF_CHANGE_OIL, boolOil);
		mEditor.commit();
	}
	
	public String getLowName(){
		return mPrefFrame.getString(PREF_LOW_NM, "");
	}
	
	public void setLowName(String strLow){
		mEditor.putString(PREF_LOW_NM, strLow);
		mEditor.commit();
	}
	
	public boolean getIsFirst() {
		return mPrefFrame.getBoolean(PREF_IS_FIRST, true);
	}
	
	public String getMyPosition(){
		return mPrefFrame.getString(PREF_MY_POSITION, "169050|517950"); //Default Thinkware Position
	}
	
	public void setMyPosition(String myPosition){
		mEditor.putString(PREF_MY_POSITION, myPosition);
		mEditor.commit();
	}
	
	public String getMyTime(){
		return mPrefFrame.getString(PREF_MY_TIME, "19860416|0419"); //Default Thinkware Position
	}
	
	public void setMyTime(String myTime){
		mEditor.putString(PREF_MY_TIME, myTime);
		mEditor.commit();
	}
	
	public void setUpdateTime(long myTime){
		mEditor.putLong(PREF_UPDATE_TIME, myTime);
		mEditor.commit();
	}
	
	public long getUpdateTime(){
		return mPrefFrame.getLong(PREF_UPDATE_LURE_TIME, 0l);
	}
	
	public void setUpdateLureTime(long myTime){
		mEditor.putLong(PREF_UPDATE_LURE_TIME, myTime);
		mEditor.commit();
	}
	
	public long getUpdateLureTime(){
		return mPrefFrame.getLong(PREF_UPDATE_LURE_TIME, 0l);
	}
	
	/**
	 * 0 : 정상 또는 평균가
	 * 1 : 즐겨찾기
	 * 2 : 최저가
	 * @param enterType
	 */
	public void setEnterType(int enterType){
		mEditor.putInt(PREF_ENTER_TYPE, enterType);
		mEditor.commit();
	}
	
	/**
	 * 0 : 정상 또는 평균가
	 * 1 : 즐겨찾기
	 * 2 : 최저가
	 * @return
	 */
	public int getEnterType(){
		return mPrefFrame.getInt(PREF_ENTER_TYPE, 0); //Default Enter Type 0
	}
	
	public void removeFavorite(){
		mEditor.remove(PREF_FAVORITE_INFO_NAME);
		mEditor.remove(PREF_FAVORITE_INFO_BRAND);
		mEditor.remove(PREF_FAVORITE_INFO_COST);
		mEditor.remove(PREF_FAVORITE_INFO_POIID);
		mEditor.commit();
	}
	
	public void removeMyTime(){
		mEditor.remove(PREF_MY_TIME);
		mEditor.commit();
	}
	public void removeMyPosition(){
		mEditor.remove(PREF_MY_POSITION);
		mEditor.commit();
	}
	
	public void setIsFirst(boolean strId) {
		mEditor.putBoolean(PREF_IS_FIRST, strId);
		mEditor.commit();
	}
	
	public void setFavoriteName(String favoriteName){
		mEditor.putString(PREF_FAVORITE_INFO_NAME, favoriteName);
		mEditor.commit();
	}
	
	public void setFavoritePoiId(String favoritePoiId){
		mEditor.putString(PREF_FAVORITE_INFO_POIID, favoritePoiId);
		mEditor.commit();
	}
	
	public void setFavoriteCost(String favoriteCost){
		mEditor.putString(PREF_FAVORITE_INFO_COST, favoriteCost);
		mEditor.commit();
	}
	
	public void setFavoriteBrand(String favoriteBrandCode){
		mEditor.putString(PREF_FAVORITE_INFO_BRAND, favoriteBrandCode);
		mEditor.commit();
	}
	
	public String getFavoriteBrand(){
		return mPrefFrame.getString(PREF_FAVORITE_INFO_BRAND, "");
	}
	
	public String getFavoriteName(){
		return mPrefFrame.getString(PREF_FAVORITE_INFO_NAME, "");
	}
	
	public String getFavoritePoiId(){
		return mPrefFrame.getString(PREF_FAVORITE_INFO_POIID, "");
	}
	
	public String getFavoriteCost(){
		return mPrefFrame.getString(PREF_FAVORITE_INFO_COST, "");
	}
	
}
