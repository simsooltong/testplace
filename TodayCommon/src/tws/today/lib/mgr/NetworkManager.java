package tws.today.lib.mgr;

import android.content.Context;
import android.net.ConnectivityManager;


public class NetworkManager {
	
	public NetworkManager() {
	}

	public boolean checkingNetwork(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {

			return true;
		} else
			return false;

	}

}
