package tws.today.lib.util;

import android.text.TextUtils;

import java.security.Key;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public class DESUtil {

	private static String key() {
		return "SKTBbox_Tsafe_thinkware_";
	}

	private static Key getKey() throws Exception {
		DESedeKeySpec desKeySpec = new DESedeKeySpec(DESUtil.key().getBytes());
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
		Key key = keyFactory.generateSecret(desKeySpec);

		return key;
	}

	public static String encrypt(String ID) throws Exception {
		if (TextUtils.isEmpty(ID))
			return "";

		String instance = "DESede/ECB/PKCS5Padding";
		javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(instance);
		cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, getKey());
		String amalgam = ID;

		byte[] inputBytes1 = amalgam.getBytes("UTF8");
		byte[] outputBytes1 = cipher.doFinal(inputBytes1);
		String outputStr1 = Base64Coder.encodeLines(outputBytes1);
		return outputStr1;
	}

	public static String decrypt(String codedID) throws Exception {
		if (TextUtils.isEmpty(codedID))
			return "";

		String instance = "DESede/ECB/PKCS5Padding";
		javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(instance);
		cipher.init(javax.crypto.Cipher.DECRYPT_MODE, getKey());

		byte[] inputBytes1 = Base64Coder.decodeLines(codedID);
		byte[] outputBytes2 = cipher.doFinal(inputBytes1);

		String strResult = new String(outputBytes2, "UTF8");
		return strResult;
	}
}
