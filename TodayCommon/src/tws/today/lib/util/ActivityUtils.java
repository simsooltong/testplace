package tws.today.lib.util;

import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;

public class ActivityUtils {
	
	public static String getBaseClassName(Activity activity) {

		ComponentName baseActivity = activity.getComponentName();
		String BaseClassName = baseActivity.getClassName();
		LOG.d("ActivityUtils BaseClassName " + BaseClassName);
		return BaseClassName;
	}

	public static String getTopClassName(Activity activity) {

		ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> Info = am.getRunningTasks(1);
		ComponentName topActivity = Info.get(0).topActivity;
		String TopClassName = topActivity.getClassName();
		LOG.d("ActivityUtils TopClassName " + TopClassName);

		return TopClassName;
	}
	
	

	

	

}
