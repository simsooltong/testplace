package tws.today.lib.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.res.AssetManager;

public class HtmlUtils {

	public static String loadTextFromAsset(Context context, String strAssetFileName) {

		AssetManager assetMgr = context.getAssets();
		String strContent = "";
		try {
			// get text content in asset.
			InputStream is = assetMgr.open(strAssetFileName);
			InputStreamReader isr = new InputStreamReader(is);

			BufferedReader br = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String strLine = null;
			while ((strLine = br.readLine()) != null) {
				sb.append(strLine);
			}
			strContent = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return strContent;
	}


}
