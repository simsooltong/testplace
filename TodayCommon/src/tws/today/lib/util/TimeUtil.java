package tws.today.lib.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.text.TextUtils;

public class TimeUtil {

	private static SimpleDateFormat sdf;
	private static Calendar calendar;
	
	public static String getCurrentSimpleDateFormat(String pattern) 
	{
		sdf = new SimpleDateFormat(pattern, Locale.KOREA);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		Date currentTime = new Date();
		String mTime = sdf.format(currentTime);
		return mTime;
	}

	public static String getNewSimpleDateFormat(String pattern, String time) {

		String mTime = "";

		if (TextUtils.isEmpty(pattern) || TextUtils.isEmpty(time))
			return mTime;

		sdf = new SimpleDateFormat(pattern, Locale.KOREA);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		calendar = Calendar.getInstance();
		calendar.setTimeZone(sdf.getTimeZone());
		calendar.setTimeInMillis(Long.parseLong(time) * 1000L);

		mTime = sdf.format(calendar.getTime());
		return mTime;
	}

	public void setKoreaSimpleDateFormat(String pattern) {
		sdf = new SimpleDateFormat(pattern, Locale.KOREA);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		calendar = Calendar.getInstance();
		calendar.setTimeZone(sdf.getTimeZone());
	}

	public static String getTimeSetSimpleDateFormat(String time) {

		calendar.setTimeInMillis(Long.parseLong(time) * 1000L);

		return sdf.format(calendar.getTime());
	}

	public static String getSimpleDateFormatAMPM(String time) {

		String mTime = "";

		mTime = getNewSimpleDateFormat("yyyy D a hh??? mm�?", time);

		return mTime;
	}

	public static String getDateYYYYMMDD(int gap) {

		Calendar cal = Calendar.getInstance();

		cal.add(cal.DATE, gap);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd",
				Locale.KOREA);
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		return dateFormat.format(cal.getTime());
	}

	public static long getUpdateTimeToUnixTime(String time) {

		sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		long a = -1;

		if (time.length() < 14) {
			return a;
		}

		try {
			a = sdf.parse(time).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return a;
	}

	/*
	 * ??�멧 �?�?. - ??��?? ??��?��?��?? ??��?? ??��?��?��?? 경�?? ??? ??��?? ??��??/??��?? HH??? MM�? - ??��?? ??��?��?��?? ??��?? ??��?��?��?? 경�?? - ??��??
	 * ??��??/??��?? HH??? MM�? - ??��?? ??��?��?��?? 2??��??~??��?��?? ??��?��?��?? 경�?? ??? MM??? DD??? ??��??/??��?? HH??? MM�? - ??��?? ??��?��?��??
	 * ??��?? ??��????? ??��?��?��?? 경�?? ??? YYYY??? MM??? DD??? ??��??/??��?? HH??? MM�?
	 */
	public static String getUpdateTimeInfo(String strUpdateTime) {
		String strTime = "";

		long unixTime = System.currentTimeMillis() / 1000L;

		String updateTime = "";

		updateTime = getNewSimpleDateFormat("yyyy D MM dd a hh mm",
				strUpdateTime);

		String nowTime = "";

		nowTime = getNewSimpleDateFormat("yyyy D MM dd a hh mm",
				Long.toString(unixTime));

		String[] updateTimeSplited = updateTime.split(" ");

		String[] nowTimeSplited = nowTime.split(" ");

		int nYear = Integer.parseInt(updateTimeSplited[0]);

		int nMonth = Integer.parseInt(updateTimeSplited[2]);
		int nDate = Integer.parseInt(updateTimeSplited[3]);

		String AmPm = updateTimeSplited[4];

		int nHH = Integer.parseInt(updateTimeSplited[5]);
		int nMM = Integer.parseInt(updateTimeSplited[6]);

		if (updateTimeSplited[0].equalsIgnoreCase(nowTimeSplited[0])) {

			int NowDay = Integer.parseInt(nowTimeSplited[1]);
			int UpdateDay = Integer.parseInt(updateTimeSplited[1]);

			// ??��??.
			if (NowDay == UpdateDay) {
				// ??��?? ??��?��?��?? 경�?? ??? ??��?? ??��??/??��?? HH??? MM�?
	
				strTime = "??��?? " + AmPm + " " + nHH + "??? ";
				
				strTime += (nMM>0)?nMM + "�?":"";

			} else if (NowDay == (UpdateDay + 1)) {
				// ??��?? ??��?��?��?? 경�?? - ??��?? ??��??/??��?? HH??? MM�?
				strTime = "??��?? " + AmPm + " " + nHH + "??? ";
				
				strTime += (nMM>0)?nMM + "�?":"";
			} else {
				// 2??��??~??��?��?? ??��?��?��?? 경�?? ??? MM??? DD??? ??��??/??��?? HH??? MM�?
				strTime = nMonth + "??? " + nDate + "??? " + AmPm + " " + nHH
						+ "??? ";
				
				strTime += (nMM>0)?nMM + "�?":"";
			}
		} else {
			// ??��?? ??��????? ??��?��?��?? 경�?? ??? YYYY??? MM??? DD??? ??��??/??��?? HH??? MM�?
			strTime = nYear + "??? " + nMonth + "??? " + nDate + "??? " + AmPm + " "
					+ nHH + "??? ";
			
			strTime += (nMM>0)?nMM + "�?":"";

		}

		return strTime;
	}

}
