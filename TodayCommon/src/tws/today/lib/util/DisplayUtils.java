package tws.today.lib.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class DisplayUtils {

	private DisplayMetrics outMetrics = null;
	
	private float scale;
	
	public DisplayUtils(Context context){	
		outMetrics = context.getResources().getDisplayMetrics();
		scale = context.getResources().getDisplayMetrics().density;
	}
	
	public float PixelToDp(float pixel){		
		return (pixel / scale);
	}
	
	public float DpToPixel(float dp){
		return (dp * scale);
	}
	
	public static int dpToPx(int dp)
	{
	    return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static int pxToDp(int px)
	{
	    return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}
}
