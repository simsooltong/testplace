package tws.today.lib.util;

public class LOG {

    public static final String TAG = "Commute";
    public static final boolean DEBUG = true;

    public static void d(String msg) {
    	if( DEBUG )
    		android.util.Log.d(TAG, msg);
    }
}
