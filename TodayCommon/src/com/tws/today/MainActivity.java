package com.tws.today;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import tws.today.lib.gms.LocationDefines;
import tws.today.lib.gms.LocationGMS;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);

		LocationGMS location = new LocationGMS(MainActivity.this,
				LocationResultHandler);

		location.connect();

	}

	public Handler LocationResultHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (isFinishing())
				return;

			switch (msg.what) {

			case LocationDefines.GMS_CONNECT_SUCC:
				Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");

				break;
			case LocationDefines.GMS_CONNECT_FAIL:
				Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");

				break;
			case LocationDefines.GMS_DISCONNECT_SUCC:
				Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
				break;
			case LocationDefines.GMS_LOCATION_NEED_SETTING:
				Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
				break;
			case LocationDefines.GMS_LOCATION_SUCC:
				Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");

				if (msg.obj != null) {
					Location location = (Location) msg.obj;

					Log.i("LocationResultHandler", "location getAccuracy : "
							+ location.getAccuracy());
					Log.i("LocationResultHandler", "location getLongitude : "
							+ location.getLongitude());
					Log.i("LocationResultHandler", "location getLatitude : "
							+ location.getLatitude());
				} else {

				}

				break;
			case LocationDefines.GMS_LOCATION_FAIL:
				Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
				break;

			}

		}
	};

}
