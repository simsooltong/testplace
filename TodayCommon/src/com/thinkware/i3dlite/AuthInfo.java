package com.thinkware.i3dlite;

public class AuthInfo extends Object {
	String UUID;
	String DeviceName;
	String SoftwareSerial;
	String RegisterVersion;
	String AppVersion;
	String AppBuildNum;
	String MapVersion;
	String OSVersion;
	String ExpireDate;
	String User;
	String SDRM4DP;
	String SDRM4MMC;
	int	PhoneType;
	
	public void SetUUID(String Value)
	{
		UUID = Value;
	}
	public void SetDeviceName(String Value)
	{
		DeviceName = Value;
	}
	public void SetAppVersion(String Value)
	{
		AppVersion = Value;
	}
	public void SetAppBuildNum(String Value)
	{
		AppBuildNum = Value;
	}
	public void SetOSVersion(String Value)
	{
		OSVersion = Value;
	}
	public void SetExpireDate(String Value)
	{
		ExpireDate = Value;
	}
	public void SetMapVersion(String Value)
	{
		MapVersion = Value;
	}
	public void SetSoftwareSerial(String Value)
	{
		SoftwareSerial = Value;
	}
	public void SetRegVer(String Value)
	{
		RegisterVersion = Value;
	}
	public void SetSDRM4MMC(String Value)
	{
		SDRM4MMC = Value;
	}
	public void SetSDRM4DP(String Value)
	{
		SDRM4DP = Value;
	}
	public void SetPhoneType(int Value)
	{
		PhoneType = Value;
	}
}
