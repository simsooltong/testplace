package com.appteam.thinkware.todaycommuter.ui;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appteam.thinkware.todaycommuter.R;
import com.appteam.thinkware.todaycommuter.data.FavoriteCCTVData;
import com.appteam.thinkware.todaycommuter.data.MapCCTVNow;
import com.appteam.thinkware.todaycommuter.data.MapCCTV_Item;
import com.appteam.thinkware.todaycommuter.data.RetCode;
import com.appteam.thinkware.todaycommuter.mgr.ApiMgr;
import com.appteam.thinkware.todaycommuter.mgr.PrefMgr;
import com.appteam.thinkware.todaycommuter.util.LOG;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomWidget extends AppWidgetProvider {

    //Primitive S
    private final static String ACTION_UPDATE_DIRECT = "android.appwidget.action.ACTION_COMMUTER_UPDATE_DIRECT";
    private final static String ACTION_REFRESH = "android.appwidget.action.ACTION_COMMUTER_REFRESH";
    private final static String ACTION_GO_TO_ACTIVITY_FAVORITE = "android.appwidget.action.ACTION_COMMUTER_GO_TO_ACTIVITY_FAVORITE";
    private final static String ACTION_LOAD_LEFT_IMG = "android.appwidget.action.ACTION_COMMUTER_LOAD_LEFT_IMG";
    private final static String ACTION_LOAD_RIGHT_IMG = "android.appwidget.action.ACTION_COMMUTER_LOAD_RIGHT_IMG";
    private final static String ACTION_REFRESH_LURE = "android.appwidget.action.ACTION_COMMUTE_REFRESH_LURE";
    public static int selectedCCTVIndex = 0;
    //Primitive S

    //WIDGET S
    private RemoteViews remoteViews = null;
    Context mContext = null;
    //WIDGET E

    //OBJECT S
    private AppWidgetManager mAppWidgetManager;
    private ComponentName mCPName;
    private ApiMgr mApiMgr;
    private PrefMgr mPrefMgr;
    private MapCCTV_Item mResultItem;
    //OBJECT E
    /**
     * 네트워크 통신 결과 핸들러.
     */
    public Handler netResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            LOG.d("NetResult_Handler msg what" + msg.what);
            switch (msg.what) {
                case RetCode.RET_CCTV_NOW_SUCC:
                    LOG.d("netResultHandler RET_CCTV_NOW_SUCC");
                    MapCCTVNow mapCCTVNow = (MapCCTVNow) msg.obj;
                    if (mapCCTVNow != null) {
                        mResultItem = mapCCTVNow.List.get(0);
                        if (mResultItem.available.equals("Y")) {
                            String cctvImgUrl = mResultItem.thumbnail;//무조건 리스트라서 0번째임(신경 ㄴㄴ 추후 I/F 변경할 예정
                            String cctvName = mResultItem.name;
                            String cctvUpdateTime = mResultItem.update_time;
                            LOG.d("지금 CCTV url : " + cctvImgUrl);
                            LOG.d("지금 CCTV 이름 : " + cctvName);
                            LOG.d("지금 CCTV time : " + cctvUpdateTime);
                            remoteViews.setTextViewText(R.id.tv_widget_cctv_title, cctvName);
                            remoteViews.setViewVisibility(R.id.ll_time_over, View.GONE);
                            remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
                            remoteViews.setTextViewText(R.id.tv_widget_update_time_text, substractHHMM(cctvUpdateTime));
                            bindingURLImage(mContext, R.id.iv_widget_cctv_img, cctvImgUrl);
                        } else {
                            Toast.makeText(mContext, "제공 가능한 CCTV가 아닙니다.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case RetCode.NET_ERROR_MSG:
                    Toast.makeText(mContext, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    break;

                case RetCode.NET_ERROR:
                    LOG.d("netResultHandler NET_RETURN_ERROR_MSG");
                    break;
            }
        }
    };

    private String substractHHMM(String time) {
        String first = time.substring(8, 10);
        String second = time.substring(10, 12);
        return first + ":" + second;
    }

    private PendingIntent getPendingIntent(Context context, final int id) {
        final Intent intent = new Intent(context, CustomWidget.class);
        switch (id) {
            case R.id.ll_widget_img:
                intent.setAction(ACTION_GO_TO_ACTIVITY_FAVORITE);
                intent.putExtra("viewId", id);
                break;
            case R.id.ll_widget_left_button:
                intent.setAction(ACTION_LOAD_LEFT_IMG);
                intent.putExtra("viewId", id);
                break;
            case R.id.ll_widget_right_button:
                intent.setAction(ACTION_LOAD_RIGHT_IMG);
                intent.putExtra("viewId", id);
                break;
            case R.id.iv_widget_refresh_direct_button:
                intent.setAction(ACTION_UPDATE_DIRECT);
                intent.putExtra("viewId", id);
                break;
            case R.id.ll_time_over:
                intent.setAction(ACTION_REFRESH);
                intent.putExtra("viewId", id);
                break;

        }
        return PendingIntent.getBroadcast(context, id, intent, 0);
    }

    @Override
    public void onEnabled(Context context) {
        LOG.d("onEnabled");
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        LOG.d("onReceive");
        super.onReceive(context, intent);
        this.mContext = context;
        initUi(context);
        String action = intent.getAction();
//        Toast.makeText(context, action, Toast.LENGTH_SHORT).show();
        if (AppWidgetManager.ACTION_APPWIDGET_ENABLED.equals(action)) {
            LOG.d("Receive ACTION_APPWIDGET_ENABLED");
            remoteViews.setViewVisibility(R.id.ll_time_over, View.VISIBLE);
            //처음 생성 되었을때
        } else if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {
            LOG.d("Receive ACTION_APPWIDGET_UPDATE");
            //업데이트 되었을때
//            AppWidgetManager manager = AppWidgetManager.getInstance(mContext);
//            initUI(mContext, manager, manager.getAppWidgetIds(new ComponentName(mContext, getClass())));
        } else if (AppWidgetManager.ACTION_APPWIDGET_DELETED.equals(action)) {
            LOG.d("Receive ACTION_APPWIDGET_DELETED");
            //삭제되었을때
        } else if (AppWidgetManager.ACTION_APPWIDGET_DISABLED.equals(action)) {
            LOG.d("Receive ACTION_APPWIDGET_DISABLED");
            //바탕화면에서 없어졌을때
        }

        // Custom Recevier
        else if (ACTION_GO_TO_ACTIVITY_FAVORITE.equals(action)) {
            LOG.d("Receive ACTION_GO_TO_ACTIVITY_FAVORITE");
//            Toast.makeText(context, "Receiver 수신 완료!!.", Toast.LENGTH_SHORT).show();
            gotoDetailActivity(context);
        } else if (ACTION_UPDATE_DIRECT.equals(action)) {
            LOG.d("Receive ACTION_UPDATE_DIRECT");
            remoteViews.setViewVisibility(R.id.ll_progress_bar, View.VISIBLE);
            if (getCCTVList(context).size() != 0) {
                updateThisCCTV(context);
            } else {
                //즐겨찾기 없음
                Toast.makeText(context, context.getString(R.string.widget_no_cctv_goto_app), Toast.LENGTH_SHORT).show();
                gotoMainActivity(context);
            }
        } else if (ACTION_REFRESH.equals(action)) {
            LOG.d("Receive ACTION_UPDATE_DIRECT");
            if (getCCTVList(context).size() != 0) {
                updateThisCCTV(context);
            } else {
                //즐겨찾기 없음
                Toast.makeText(context, context.getString(R.string.widget_no_cctv_goto_app), Toast.LENGTH_SHORT).show();
                gotoMainActivity(context);
            }
        } else if (ACTION_LOAD_LEFT_IMG.equals(action)) {
            LOG.d("Receive ACTION_LOAD_LEFT_IMG");
            LOG.d("ACTION_LOAD_LEFT_IMG select " + selectedCCTVIndex);
            remoteViews.setViewVisibility(R.id.ll_progress_bar, View.VISIBLE);
            if (selectedCCTVIndex - 1 < 0) {
                Toast.makeText(context, context.getString(R.string.string_widget_first_cctv), Toast.LENGTH_SHORT).show();
                remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
            } else {
                ArrayList<MapCCTV_Item> mapCCTV_items = getCCTVList(context);
                --selectedCCTVIndex;
                LOG.d("LEFT1 INDEX : " + selectedCCTVIndex);
                ArrayList<String> idList = new ArrayList<String>();
                for (MapCCTV_Item mapCCTV_item : mapCCTV_items) {
                    idList.add(mapCCTV_item.cctv_id);
                }
                apiCurrentCCTVData(context, idList.get(selectedCCTVIndex));
            }
            LOG.d("LEFT INDEX : " + selectedCCTVIndex);
        } else if (ACTION_LOAD_RIGHT_IMG.equals(action)) {
            LOG.d("Receive ACTION_LOAD_RIGHT_IMG");
            LOG.d("ACTION_LOAD_RIGHT_IMG select " + selectedCCTVIndex);
            remoteViews.setViewVisibility(R.id.ll_progress_bar, View.VISIBLE);
            ArrayList<MapCCTV_Item> mapCCTV_items = getCCTVList(context);
            if (selectedCCTVIndex + 1 >= mapCCTV_items.size()) {
                Toast.makeText(context, context.getString(R.string.string_widget_last_cctv), Toast.LENGTH_SHORT).show();
                remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
            } else {
                ++selectedCCTVIndex;
                LOG.d("RIGHT1 INDEX : " + selectedCCTVIndex);
                ArrayList<String> idList = new ArrayList<String>();
                for (MapCCTV_Item mapCCTV_item : mapCCTV_items) {
                    idList.add(mapCCTV_item.cctv_id);
                    LOG.d("즐찾 CCTV : " + mapCCTV_item.cctv_id);
                }
                apiCurrentCCTVData(context, idList.get(selectedCCTVIndex));
            }
            LOG.d("RIGHT INDEX : " + selectedCCTVIndex);
        } else if (ACTION_REFRESH_LURE.equals(action)) {
            LOG.d("Receive ACTION_REFRESH_LURE");
            remoteViews.setViewVisibility(R.id.ll_time_over, View.VISIBLE);
            selectedCCTVIndex = 0;
        }
        mAppWidgetManager = AppWidgetManager.getInstance(context);
        mCPName = new ComponentName(context, CustomWidget.class);
        mAppWidgetManager.updateAppWidget(mCPName, remoteViews);
    }

    private void gotoDetailActivity(Context context) {
        Intent gotoActivity = new Intent(context, MainActivity.class);
        gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MapCCTV_Item tempItem = getCCTVList(context).get(selectedCCTVIndex);
        gotoActivity.putExtra("selectItem", tempItem);
        context.startActivity(gotoActivity);
//        initWidgetResource();
    }

    private void gotoMainActivity(Context context) {
        Intent gotoActivity = new Intent(context, MainActivity.class);
        gotoActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(gotoActivity);
//        initWidgetResource();
    }

    private void initWidgetResource() {
        remoteViews.setViewVisibility(R.id.ll_time_over, View.VISIBLE);
        remoteViews.setImageViewBitmap(R.id.iv_widget_cctv_img, null);
        remoteViews.setTextViewText(R.id.tv_widget_cctv_title, "");
        remoteViews.setTextViewText(R.id.tv_widget_update_time_text, "");
    }

    private void updateThisCCTV(Context context) {
        ArrayList<MapCCTV_Item> mapCCTV_items = getCCTVList(context);
        ArrayList<String> idList = new ArrayList<String>();
        for (MapCCTV_Item mapCCTV_item : mapCCTV_items) {
            idList.add(mapCCTV_item.cctv_id);
        }
        apiCurrentCCTVData(context, idList.get(selectedCCTVIndex));
    }

    private ArrayList<MapCCTV_Item> getCCTVList(Context context) {
        LOG.d("DSKIM getCCTVList");
        Gson gson = new Gson();
        mPrefMgr = new PrefMgr(context);
        String favoriteGson = mPrefMgr.getFavoriteCCTV();
        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
        if (favoriteCCTVData == null) {
            favoriteCCTVData = new FavoriteCCTVData();
        }
        return favoriteCCTVData.getFavoriteCCTVItems();
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        LOG.d("onUpdate");
        // 현재 클래스로 등록된 모든 위젯의 리스트를 가져옴
        appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
//            Toast.makeText(context, "onUpdate(): [" + String.valueOf(i) + "] " + String.valueOf(appWidgetId), Toast.LENGTH_SHORT).show();
        }
    }

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.e("DSKIM", "onDeleted");
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.e("DSKIM", "onDeleted");
        super.onDeleted(context, appWidgetIds);
    }

    public void initUi(Context context) {
        LOG.d("initUi RemoteView init");
        remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        remoteViews.setOnClickPendingIntent(R.id.ll_widget_img, getPendingIntent(context, R.id.ll_widget_img));
        remoteViews.setOnClickPendingIntent(R.id.iv_widget_refresh_direct_button, getPendingIntent(context, R.id.iv_widget_refresh_direct_button));
        remoteViews.setOnClickPendingIntent(R.id.ll_widget_left_button, getPendingIntent(context, R.id.ll_widget_left_button));
        remoteViews.setOnClickPendingIntent(R.id.ll_widget_right_button, getPendingIntent(context, R.id.ll_widget_right_button));
        remoteViews.setOnClickPendingIntent(R.id.ll_time_over, getPendingIntent(context, R.id.ll_time_over));
    }

    private void bindingURLImage(Context context, int viewId, String photo_url) {
        Log.i("bindingURLImage", "request Picasso");
        try {
            int[] appWidgetIds = mAppWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));
            Picasso.with(context).load(photo_url).into(remoteViews, R.id.iv_widget_cctv_img, appWidgetIds);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("bindingURLImage", "GLIDE ERR", e);
        }
    }

    private void apiCurrentCCTVData(final Context context, String cctv_id) {
        mApiMgr = new ApiMgr();
        mApiMgr.apiTodayCurrentCCTV(context, cctv_id, new Response.Listener<MapCCTVNow>() {
            @Override
            public void onResponse(MapCCTVNow response) {
                LOG.d("apiCurrentCCTVData retCode.result : " + response.result);
                LOG.d("apiCurrentCCTVData retCode.errormsg : " + response.errormsg);
                remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
                if (response.result == 0) {
                    Message msg = netResultHandler.obtainMessage();
                    msg.what = RetCode.RET_CCTV_NOW_SUCC;
                    msg.obj = response;
                    netResultHandler.sendMessage(msg);
                } else if (response.result == 2003) {
                    remoteViews.setViewVisibility(R.id.ll_time_over, View.GONE);
                    remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
//                    remoteViews.setTextViewText(R.id.tv_widget_update_time_text, substractHHMM(cctvUpdateTime));
                } else {
                    // fail
                    LOG.d("apiTconCCTV Fail " + response.result);
                    Toast.makeText(context, response.errormsg + "(" + response.result + ")", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LOG.d("apiCurrentCCTVData VolleyError " + error.getMessage());
                Toast.makeText(context, context.getResources().getString(R.string.map_err_init_fail), Toast.LENGTH_SHORT).show();
                remoteViews.setViewVisibility(R.id.ll_progress_bar, View.GONE);
                mAppWidgetManager = AppWidgetManager.getInstance(context);
                mCPName = new ComponentName(context, CustomWidget.class);
                mAppWidgetManager.updateAppWidget(mCPName, remoteViews);
            }
        });
    }
}



