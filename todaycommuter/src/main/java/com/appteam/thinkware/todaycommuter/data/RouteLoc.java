package com.appteam.thinkware.todaycommuter.data;

/**
 * Created by dskim on 2015-02-06.
 */
public class RouteLoc {
    public String linkid ="0";
    public String lonx;
    public String laty;
    public String direction="0";
    public String mapid="0";
    public String nodeid="0";
    public String name="0";

    public RouteLoc(String lonX, String latY){
        this.lonx = lonX;
        this.laty = latY;
    }
}
