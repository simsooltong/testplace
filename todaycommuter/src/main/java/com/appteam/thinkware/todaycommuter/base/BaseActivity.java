package com.appteam.thinkware.todaycommuter.base;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Jony on 2015-01-18.
 */
public class BaseActivity extends Activity {

    public SweetAlertDialog mBaseProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseInitProgresDialog();
    }

    private void baseInitProgresDialog()
    {
        mBaseProgressDialog  = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mBaseProgressDialog.getProgressHelper().setBarColor(Color.parseColor("#2996CC"));
        mBaseProgressDialog.setTitleText("Loading");
        mBaseProgressDialog.setCancelable(false);

    }

    public static Typeface mTypeface = null;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (BaseActivity.mTypeface == null){
            BaseActivity.mTypeface = Typeface.createFromAsset(getAssets(), "inavi_rixgo_m.ttf");
        }
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        setGlobalFont(root);
    }

    void setGlobalFont(ViewGroup root) {
        for (int i = 0; i < root.getChildCount(); i++) {
            View child = root.getChildAt(i);
            if (child instanceof TextView)
                ((TextView)child).setTypeface(mTypeface);
            else if (child instanceof ViewGroup)
                setGlobalFont((ViewGroup)child);
        }
    }
}
