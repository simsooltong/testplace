package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-02-05.
 */
public class SearchRouteAroundMulti {
    public String result;
    public String errormsg;
    public int totalcount;
    public int count;
    public ArrayList<PoiInfo> poi;
}
