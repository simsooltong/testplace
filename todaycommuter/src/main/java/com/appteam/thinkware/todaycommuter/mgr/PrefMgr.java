package com.appteam.thinkware.todaycommuter.mgr;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefMgr {

	private final String PREF_NAME_FRAME_USER = "AFK_Frame_User_Info";
	private final String PREF_MY_POSITION = "my_position";
	private final String PREF_ENTER_TYPE = "enter_type";
	private final String PREF_FAVORITE_CCTV_DATA = "favorite_cctv_data";
	private final String PREF_HOME_LON_X = "home_x";
	private final String PREF_HOME_LAT_Y = "home_y";
	private final String PREF_WORK_LON_X = "work_x";
	private final String PREF_WORK_LAT_Y = "work_y";
    private final String PREF_HOME_WORKPLACE_CHANGE = "home_workplace_change";
    private final String PREF_IS_FIRST = "is_first";

	private Context mContext = null;

	private SharedPreferences mPrefFrame = null;
	private SharedPreferences.Editor mEditor = null;

	public PrefMgr(Context context) {
		mContext = context;
		mPrefFrame = mContext.getSharedPreferences(PREF_NAME_FRAME_USER, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
		mEditor = mPrefFrame.edit();
	}

	public void removeAll() {
		mEditor.clear();
		mEditor.commit();
	}

	public String getMyPosition(){
        return mPrefFrame.getString(PREF_MY_POSITION, "169050|517950"); //Default Thinkware Position
    }

    public void setMyPosition(String myPosition){
        mEditor.putString(PREF_MY_POSITION, myPosition);
        mEditor.commit();
    }

    public boolean getPREF_ISFIRST(){
        return mPrefFrame.getBoolean(PREF_IS_FIRST, true); //Default Thinkware Position
    }

    public void setPREF_IS_FIRST(boolean isFirst){
        mEditor.putBoolean(PREF_IS_FIRST, isFirst);
        mEditor.commit();
    }

    public boolean getPREF_HOME_WORKPLACE_CHANGE(){
        return mPrefFrame.getBoolean(PREF_HOME_WORKPLACE_CHANGE, false); //Default false
    }

    public void setPREF_HOME_WORKPLACE_CHANGE(boolean change){
        mEditor.putBoolean(PREF_HOME_WORKPLACE_CHANGE, change);
        mEditor.commit();
    }

    public String getFavoriteCCTV(){
        return mPrefFrame.getString(PREF_FAVORITE_CCTV_DATA, "");
    }

    public void setFavoriteCCTV(String jsonString){
        mEditor.putString(PREF_FAVORITE_CCTV_DATA, jsonString);
        mEditor.commit();
    }

    public void setPREF_HOME_LON_X(String lon_x){
        mEditor.putString(PREF_HOME_LON_X, lon_x);
        mEditor.commit();
    }
    public void setPREF_HOME_LAT_Y(String lat_y){
        mEditor.putString(PREF_HOME_LAT_Y, lat_y);
        mEditor.commit();
    }
    public void setPREF_WORK_LON_X(String lon_x){
        mEditor.putString(PREF_WORK_LON_X, lon_x);
        mEditor.commit();
    }
    public void setPREF_WORK_LAT_Y(String lat_y){
        mEditor.putString(PREF_WORK_LAT_Y, lat_y);
        mEditor.commit();
    }

    public String getPREF_HOME_LON_X(){
        return mPrefFrame.getString(PREF_HOME_LON_X, "173701"); //Default Thinkware Position
    }
    public String getPREF_HOME_LAT_Y(){
        return mPrefFrame.getString(PREF_HOME_LAT_Y, "512406"); //Default Thinkware Position
    }
    public String getPREF_WORK_LON_X(){
        return mPrefFrame.getString(PREF_WORK_LON_X, "169027"); //Default Thinkware Position
    }
    public String getPREF_WORK_LAT_Y(){
        return mPrefFrame.getString(PREF_WORK_LAT_Y, "517907"); //Default Thinkware Position
    }

	/**
	 * 0 : 정상 또는 평균가
	 * 1 : 즐겨찾기
	 * 2 : 최저가
	 * @param enterType
	 */
	public void setEnterType(int enterType){
		mEditor.putInt(PREF_ENTER_TYPE, enterType);
		mEditor.commit();
	}
	
	/**
	 * 0 : 정상 또는 평균가
	 * 1 : 즐겨찾기
	 * 2 : 최저가
	 * @return
	 */
	public int getEnterType(){
		return mPrefFrame.getInt(PREF_ENTER_TYPE, 0); //Default Enter Type 0
	}

	public void removeMyPosition(){
		mEditor.remove(PREF_MY_POSITION);
		mEditor.commit();
	}
}
