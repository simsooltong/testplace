package com.appteam.thinkware.todaycommuter.util;

import android.util.Log;

import CoordLib.CoordClient;
import CoordLib.Position_T;

/**
 * Created by dskim on 2015-02-06.
 */
public class PositionConverter {
    /**
     * 정규화 좌표를 WSG84 Degree로 변환한다.
     * @param strX 정규화 좌표 X
     * @param strY 정규화 좌표 Y
     * @return WSG84 Degree X,Y 해쉬맵
     */
    public static String[] getWsg84DegreePosition(String strX, String strY) {
        CoordClient coordClient = new CoordClient();
        Position_T wsg84XY =  coordClient.getConvertLongLati(Long.parseLong(strX), Long.parseLong(strY));
        float lonX_f = Float.parseFloat(wsg84XY.sLatiSec);
        float latY_f = Float.parseFloat(wsg84XY.sLongSec);
        int lonX_i = (int)(lonX_f * 360000);
        int latY_i = (int)(latY_f * 360000);
        Log.i("getWsg84DegreePosition", strX + " Convert --> " + lonX_i);
        Log.i("getWsg84DegreePosition", strY+" Convert --> "+ latY_i);
        String[] converted = new String[2];
        converted[0] = String.valueOf(lonX_i);
        converted[1] = String.valueOf(latY_i);
        return converted;
    }
}
