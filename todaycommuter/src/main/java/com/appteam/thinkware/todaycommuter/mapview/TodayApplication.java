/**
 * 
 */
package com.appteam.thinkware.todaycommuter.mapview;

/**
 * @author jonychoi
 *
 */

import android.app.Application;

import java.util.concurrent.ConcurrentHashMap;



public class TodayApplication extends Application {

	private ConcurrentHashMap<String, Object> m_Objects = new ConcurrentHashMap<String, Object>();

	@Override
	public void onCreate() {
		super.onCreate();

//		setAQueryInit();
	}
/*
	*//**
	 * AQuery 설정.
	 *//*

	private void setAQueryInit() {
	
		// Cache Config Ext
		File ext = Environment.getExternalStorageDirectory();
		File cacheDir = new File(ext, "bbox/tchildsafe/_img");
		
		if(LOG.DEBUG)
			AQUtility.setDebug(true);
		

		AQUtility.setCacheDir(cacheDir);

		// set the max number of concurrent network connections, default is 4
		AbstractAjaxCallback.setNetworkLimit(4);

		// set the max number of icons (image width <= 50) to be cached in
		// memory, default is 20
		BitmapAjaxCallback.setIconCacheLimit(100);

		// set the max number of images (image width > 50) to be cached in
		// memory, default is 20
		BitmapAjaxCallback.setCacheLimit(100);

		// set the max size of an image to be cached in memory, default is 1600
		// pixels (ie. 400x400)
		BitmapAjaxCallback.setPixelLimit(400 * 400);

		// set the max size of the memory cache, default is 1M pixels (4MB)
		BitmapAjaxCallback.setMaxPixelLimit(2000000);

	}*/

	@Override
	public void onLowMemory() {
		// TODO Auto-generated method stub

//		BitmapAjaxCallback.clearCache();

		super.onLowMemory();
	}

	// /////////////////////////////////////////////////////////////////////////////
	// Global Object Management Methods
	// /////////////////////////////////////////////////////////////////////////////
	public boolean containsObject(String key) {
		return m_Objects.contains(key);
	}

	public void addObject(String key, Object value) {
		m_Objects.put(key, value);
	}

	public Object getObject(String key) {
		return m_Objects.get(key);
	}

	public Object getObject(String key, String clsName) {
		Object obj = m_Objects.get(key);
		if (obj != null && obj.getClass().getName().equals(clsName) == false) {

			obj = null;
		}
		return obj;
	}

	public void removeObject(String key) {
		m_Objects.remove(key);
	}
	
	

	
}