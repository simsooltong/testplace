package com.appteam.thinkware.todaycommuter.mapview;

public class Defines {

	// //////////////////////////////////////////////////////////////////////////////////////////////
	//
	// MAP handler defines
	//
	// //////////////////////////////////////////////////////////////////////////////////////////////
	
	public class InstanceConst {
		

	public static final String MAIN_MAP_INSTANCE = "MAIN_MAP_INSTANCE";
	
	public static final String PUSH_MAP_INSTANCE = "PUSH_MAP_INSTANCE";
	
	public static final String FIND_MAP_INSTANCE = "FIND_MAP_INSTANCE";
	
	}
	
	public class InitMapConst {
		
		public static final int MAP_DEFAULT_LEVEL = 8;
		
	}
	
	
	public class MAPConst {
		// Map Handler CB Info
		public static final int MAP_CIRCLE = 500;
		
		public static final int MAP_SUCC = 0;
		public static final int MAP_RECYCLE = 1;
		public static final int MAP_FAIL = 2;
		public static final int MAP_SET_HANDLER = 3;
		
		public static final int CB_GETLEVEL = 100;
		public static final int CB_GETCENTER = 101;
		public static final int CB_INIT = 102;
		public static final int CB_ISMAP = 103;
		public static final int CB_MOVEEND = 104;
		public static final int CB_CREATEFEATUREMAKER = 105;
		public static final int CB_GET = 106;
		public static final int CB_SETZOOMEND = 107;
		public static final int CB_TOUCHENDFEATUREMARKER = 108;
		public static final int CB_GETPIXELFROMLONLAT = 109;
		public static final int CB_GETLONLATFROMPIXEL = 110;
		public static final int CB_CENTERMAKER = 111;

//		public static final int CB_TYPE_DISMISS_LOADING = 111;
		public static final int CB_DIV_POP = 112;

		public static final int CB_DIV_INDEX_CCTV = 113;
		public static final int CB_DIV_INDEX_GAS_LOW = 114;
		public static final int CB_SETTOUCHEVENTCB = 115;

		public static final int CB_CREATEMARKER = 116;
		public static final int CB_TOUCHENDMARKER = 117;
		public static final int CB_CREATEANDADDOFFSETDOUBLE = 118;
		
		public static final int CB_SETONLYMOVEEND = 119;
		public static final int CB_SETZOOMIN = 120;
		public static final int CB_SETZOOMOUT = 121;
		
		
		public static final int CB_CREATEDIVPOPUP = 122;
		public static final int CB_ADDCIRCLE = 123;
		
		public static final int CB_PANTOEND = 124;
		public static final int CB_GET_EXTENT = 125;
		public static final int CB_SETTOUCHEND = 126;
		

		public static final int PAGE_FINISH = 200;
		public static final int PAGE_TIMEOUT = 201;

		public static final int HANDLER_MARKER = 500;
		public static final int HANDLER_MARKER_REMOVE = 501;
		public static final int HANDLER_MARKER_FINISH = 502;
		public static final int HANDLER_MARKER_GAS_LOW = 503;

		public static final int RESULT_FINISH = 900;
		
	}
	
	public static class MapTouch_Const
	{
		// mMap(webView) touch event
		public final static int TOUCH_MODE_NONE = 0;
		public final static int TOUCH_MODE_DRAG = 1;
		public final static int TOUCH_MODE_ZOOM = 2;
		public final static int TOUCH_MODE_CANCEL = 3;
		public static int mode = TOUCH_MODE_NONE;
		public static float m_fOldDist = 1f;
	}

	public class ActConst {

		// roadview url
		public static final String ROADVIEW_URL = "http://api.map.inavi.com/Map/GetApi?key=b141c5eab425026f50bd0a2e6ad8eb568a4b338b&rvUrl=1";

	}

}

