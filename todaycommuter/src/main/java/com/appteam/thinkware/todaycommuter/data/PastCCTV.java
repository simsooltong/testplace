package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-02-03.
 */
public class PastCCTV {
    public int result;            //  결과코드
    public String errormsg;        //  오류 메시지
    public ArrayList<PastCCTV_Item> List ;
}
