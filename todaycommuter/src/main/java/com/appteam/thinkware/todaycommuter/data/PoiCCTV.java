package com.appteam.thinkware.todaycommuter.data;

/**
 * Created by dskim on 2015-02-05.
 */
public class PoiCCTV {

    public String id ;
    public String name ;
    public String nosun;
    public String offer;
    public String thumbnail;
    public String stream_url;
    public String updatetime;
}
