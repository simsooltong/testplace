package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-02-12.
 */
public class FavoriteCCTVData {
    ArrayList<MapCCTV_Item> favoriteCCTVItems = new ArrayList<MapCCTV_Item>();

    public ArrayList<MapCCTV_Item> getFavoriteCCTVItems() {
        return favoriteCCTVItems;
    }

    public void setFavoriteCCTVItems(ArrayList<MapCCTV_Item> favoriteCCTVItems) {
        this.favoriteCCTVItems = favoriteCCTVItems;
    }
}
