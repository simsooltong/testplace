package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-02-22.
 */
public class WeatherPast {
    public String message;
    public String cod;
    public String city_id;
    public String calctime;
    public String cnt;
    public ArrayList<WeatherDataItem> list = new ArrayList<WeatherDataItem>();

    public class WeatherDataItem{
        public WeatherDataMain main;
        public WeatherDataWind wind;
        public WeatherDataClouds clouds;
        public ArrayList<WeatherDataWeather> weather = new ArrayList<WeatherDataWeather>();
    }

    public class WeatherDataWeather{
        public String id;
        public String main;
        public String description;
        public String icon;
    }

    public class WeatherDataMain{
        public String temp;
        public String temp_min;
        public String temp_max;
        public String pressure;
        public String sea_level;
        public String grnd_level;
        public String humidity;
    }
    public class WeatherDataWind{
        public String speed;
        public String deg;
    }

    public class WeatherDataClouds{
        public String all;
    }
}
