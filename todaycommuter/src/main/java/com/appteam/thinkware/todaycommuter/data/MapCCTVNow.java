package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by dskim on 2015-02-13.
 */
public class MapCCTVNow {
    public int result;            //  결과코드
    public String errormsg;        //  오류 메시지

    public ArrayList<MapCCTV_Item> List;        // 결과 리스트
}
