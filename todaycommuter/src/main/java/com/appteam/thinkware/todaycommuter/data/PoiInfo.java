package com.appteam.thinkware.todaycommuter.data;

public class PoiInfo {
	public String poiid;
	public String depth;
	public String dpx;
	public String dpy;
	public String rpx;
	public String rpy;
	public String name1;
	public String name2;
	public String name3;
	public String name4;
	public String admcode;
	public String address;
	public String jibun;
	public String roadname;
    public String roadjibun;
	public String detailaddress;
	public String catecode;
	public String catenmae;
	public int distance;
	public int direction;
	public String tel;
	public boolean hasoildata;
	public boolean hasdetailinfo;
	public boolean hassubpoi;
	public boolean hascctvdata;
    public OilData oildata;
	public String adv_count;
    public PoiCCTV cctvdata;
}
