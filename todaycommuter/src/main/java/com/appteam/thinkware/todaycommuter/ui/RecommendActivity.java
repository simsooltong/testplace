package com.appteam.thinkware.todaycommuter.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.ListPopupWindow;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appteam.thinkware.todaycommuter.R;
import com.appteam.thinkware.todaycommuter.base.BaseActivity;
import com.appteam.thinkware.todaycommuter.customui.CustomIntroDialog;
import com.appteam.thinkware.todaycommuter.customui.CustomProgress;
import com.appteam.thinkware.todaycommuter.data.FavoriteCCTVData;
import com.appteam.thinkware.todaycommuter.data.MapCCTV;
import com.appteam.thinkware.todaycommuter.data.MapCCTV_Item;
import com.appteam.thinkware.todaycommuter.data.PoiCCTV;
import com.appteam.thinkware.todaycommuter.data.PoiInfo;
import com.appteam.thinkware.todaycommuter.data.RetCode;
import com.appteam.thinkware.todaycommuter.data.RouteExtensionItem;
import com.appteam.thinkware.todaycommuter.data.RouteLoc;
import com.appteam.thinkware.todaycommuter.data.SearchCoordAddr;
import com.appteam.thinkware.todaycommuter.data.SearchRouteAroundMulti;
import com.appteam.thinkware.todaycommuter.data.SearchRouteList;
import com.appteam.thinkware.todaycommuter.data.WeatherCode;
import com.appteam.thinkware.todaycommuter.data.WeatherNow;
import com.appteam.thinkware.todaycommuter.mapview.Defines;
import com.appteam.thinkware.todaycommuter.mapview.Defines.MAPConst;
import com.appteam.thinkware.todaycommuter.mapview.TodayApplication;
import com.appteam.thinkware.todaycommuter.mapview.WebViewSingleton;
import com.appteam.thinkware.todaycommuter.mapview.twsMapController;
import com.appteam.thinkware.todaycommuter.mgr.ApiMgr;
import com.appteam.thinkware.todaycommuter.mgr.PrefMgr;
import com.appteam.thinkware.todaycommuter.util.LOG;
import com.appteam.thinkware.todaycommuter.util.PositionConverter;
import com.google.gson.Gson;
import com.tws.common.lib.gms.LocationDefines;
import com.tws.common.lib.gms.LocationGmsClient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;

import CoordLib.CoordClient;
import CoordLib.NorXY_T;
import CoordLib.Position_T;

public class RecommendActivity extends BaseActivity implements View.OnClickListener {
    // S Primitive//
    private final static String ACTION_REFRESH_LURE = "android.appwidget.action.ACTION_COMMUTE_REFRESH_LURE";
    private static final int OFFSET_VAL = 54;
    private static final int ENTER_FAVORITE = 1;
    private static final int ENTER_LOW = 2;
    private static final int ENTER_NORMAL = 0;
    private static final int DEFAULT_MAP_LEVEL = 9;
    private static final long ADDR_TIME = 700;
    public static final String STR_LEVEL = "9";
    private final double WEB_RESIZE = 0.67;
    private long NewTimeStamp;
    private long OldTimeStamp;
    private String now_X;
    private String now_Y;
    private int lastLevel = 9;
    private boolean isLastLocation = true;
    private boolean mBFinish = false;
    private int HOME_ALARM_TIME = 9000;
    private int WORKPALCE_ALARM_TIME = 9001;
    private final int APP_EXIT_TYPE = 9999;
    private final int resultCode = 8000;
    private ArrayList<String> idLIst = new ArrayList<String>();
    private float density;
    private float dpHeight;
    private float dpWidth;
    private final int FAVORITE_ADD_TYPE = 1111;
    private final int FAVORITE_DEL_TYPE = 2222;
    public static final int CALL_TYPE_MAIN_TO_DETAIL = 8000;
    public static final int CALL_TYPE_WIDGET_TO_DETAIL = 8001;
    private static int LIMIT_FAVORITE_COUNT = 4;
    // E Primitive//
    // S Objet//
    private Context mContext = null;
    private twsMapController mMapController = null;
    private ApiMgr mApiMgr = null;
    private Display mDisplay = null;
    private ConcurrentHashMap<String, PoiInfo> hash_CCTV = null;
    private PoiInfo mSelectedCCTVPoi = null;
    private MapCCTV_Item mSelectedCCTVItem= null;
    private ArrayList<MapCCTV_Item> mCctvList;
    private ArrayList<MapCCTV_Item> mFavoriteList = new ArrayList<MapCCTV_Item>();
    private SearchRouteAroundMulti mSearchRouteAroundMulti = null;
    // E Objet//

    //S Widget//
    private RelativeLayout rootLayout = null;
    private CustomProgress waitDialog = null;
    private CustomIntroDialog customIntroDialog = null;
    private WebView webview = null;
    private VideoView vvCCTVVideo = null;
    private RelativeLayout rlFavorite = null;
    private LinearLayout llUnderInfoLayout = null;
    private RelativeLayout rlDetail = null;
    private TextView tvPostionAddr = null;
    private TextView tvCCTVName = null;
    private ImageView mIV_map_waypoint = null;
    private ImageView ivMyPositionButton = null;
    private ImageView ivFavoriteList = null;
    private ImageView ivCctvCloseButton = null;
    private ListPopupWindow listPopupWindow = null;
    private RelativeLayout rl_cctv_video_wraper;
    private ImageView ivCctvWeather = null;
    private ImageView ivRecFavoriteImg = null;
    //E Widget//
    private RouteLoc mRouteStart;
    private RouteLoc mRouteEnd;
    private PrefMgr mPrefMgr;
    //TEST S
    private Button startButton, endButton, searchButton;
    private TextView testStartTxt, testEndTxt;
    private String startX, startY, goalX, goalY;
    private int MAP_POINT_NORMAL = 0;
    private int MAP_POINT_START = 1;
    private int MAP_POINT_GOAL = 2;

    public Handler locationResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isFinishing())
                return;
            switch (msg.what) {

                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "내 위치 확인 불가, 앱을 종료합니다.");
                            dismissSplashDialog();
                            dismissWaitDialog();
                        }
                    }, Toast.LENGTH_SHORT);
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("LocationResultHandler", "GMS_LOCATION_SUCC");
                    isLastLocation = false;
                    if (msg.obj != null) {
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "location getAccuracy : "
                                + location.getAccuracy());
                        Log.i("LocationResultHandler", "location getLongitude : "
                                + location.getLongitude());
                        Log.i("LocationResultHandler", "location getLatitude : "
                                + location.getLatitude());

                        setClickableControl(true);

                        String xyStr[] = convertXY(location);
                       /* if (isRequestButton == true) {// 내위치 찾기
                            logFlurryAgent(DefineFlurry.keyMA_LOC,DefineFlurry.keyXY, xyStr[0] + "|" + xyStr[1]);
                            isRequestButton = false;
                        } else { // 초반 시작시
                            FlurryAgent.setLocation(Float.parseFloat(xyStr[1]), Float.parseFloat(xyStr[0]));
                        }*/
                        mPrefMgr.setMyPosition(xyStr[0] + "|" + xyStr[1]);
                        now_X = xyStr[0];
                        now_Y = xyStr[1];
                        initWebview();
                        // }
                    } else {
                        Log.i("LocationResultHandler", "MSG NULL");
                    }

                    break;

                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    break;

                case LocationDefines.GMS_LOCATION_TIMEOUT:// 측위 요청했지만 Timeout
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    isLastLocation = true;
                    setClickableControl(true);
                    String[] xy = mPrefMgr.getMyPosition().split("\\|");
                    now_X = xy[0];
                    now_Y = xy[1];
                    initWebview();
                    break;
            }
        }
    };
    ;

    /**
     * 네트워크 통신 결과 핸들러.
     */
    public Handler netResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("NetResult_Handler msg what" + msg.what);

            switch (msg.what) {
                case RetCode.RET_SEARCH_ROUTE_LIST_MUTI_SUCC:
                    LOG.d("RET_SEARCH_ROUTE_LIST_MUTI_SUCC");
                    mSearchRouteAroundMulti = (SearchRouteAroundMulti) msg.obj;
                    int sX = Integer.parseInt(startX);
                    int sY = Integer.parseInt(startY);
                    int eX = Integer.parseInt(goalX);
                    int eY = Integer.parseInt(goalY);
                    mMapController.TwsMapSetMapExtent(sX, sY, eX, eY);
                    drawCCTVMarker(Integer.parseInt(startX), Integer.parseInt(startY), "0000", MAP_POINT_START);
                    drawCCTVMarker(Integer.parseInt(goalX), Integer.parseInt(goalY), "9999", MAP_POINT_GOAL);

                    drawCCTVMarkers(mSearchRouteAroundMulti);

                    dismissWaitDialog();
//                    for (Map.Entry<String, PoiInfo> entry : hash_CCTV.entrySet()) {
//                        drawCCTVMarker(Integer.parseInt(entry.getValue().dpx), Integer.parseInt(entry.getValue().dpy), entry.getValue().cctvdata.id);
//                    }
                    break;
                case RetCode.RET_SEARCH_ROUTE_LIST_SUCC:
                    LOG.d("RET_SEARCH_ROUTE_LIST_SUCC");
                    String[] myPostion = mPrefMgr.getMyPosition().split("\\|");
                    apiSearchRouteAroundMulti(myPostion[0], myPostion[1], msg.obj.toString(), startX, startY, goalX, goalY);
                    break;
                case RetCode.RET_SEARCH_ROUTE_LIST_FAIL:
                    LOG.d("RET_SEARCH_ROUTE_LIST_FAIL");
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_SUCC:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
                    System.out.println("RET_SEARCH_COORD_ADDR_SUCC"
                            + ((SearchCoordAddr) msg.obj).cut_address);
                    retApiSearchCoordAddr((SearchCoordAddr) msg.obj);
                    break;
                case RetCode.RET_SEARCH_COORD_ADDR_FAIL:
                    LOG.d("RET_SEARCH_COORD_ADDR_SUCC");
//				tvPostionAddr.setText("정보 없음");
                    break;
                case RetCode.RET_SEARCHPOI_NUMBER_SUCC:
                    LOG.d("RET_SEARCHPOI_NUMBER_SUCC");
//                    retApiSearchPoiNumber((SearchPoi) msg.obj);
                    break;
                case RetCode.RET_SEARCHPOI_SUCC:
                    LOG.d("RET_SEARCHPOI_SUCC");
//                    apiSearchCoorAddr(now_X, now_Y);
//                    retApiSearchPoiAround((SearchPoi) msg.obj);
                    break;

                case RetCode.NET_ERROR_MSG:
                    Toast.makeText(RecommendActivity.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    dismissWaitDialog();
                    // Toast.makeText(mContext, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    // LOG.d("NET_RETURN_ERROR_MSG");
                    // new Handler().postDelayed(new Runnable() {
                    // @Override
                    // public void run() {
                    // finishUI("네트워크 에러", "네트워크 불안정으로 앱을 종료합니다.");
                    // }
                    // }, Toast.LENGTH_SHORT);
                    break;

                case RetCode.NET_ERROR:

                    // if (loadingPopup.isShowing())
                    // loadingPopup.dismiss();

                    LOG.d("NET_RETURN_ERROR_MSG");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "네트워크가 불안정하여 앱을 종료합니다.");
                        }
                    }, Toast.LENGTH_SHORT);
                    // BaseMessagePopup(null, null, -1, false, true);
                    break;
            }
        }
    };

    private void drawCCTVMarkers(SearchRouteAroundMulti mSearchRouteAroundMulti) {
        drawCCTVMarker(Integer.parseInt(startX), Integer.parseInt(startY), "0000", MAP_POINT_START);
        drawCCTVMarker(Integer.parseInt(goalX), Integer.parseInt(goalY), "9999", MAP_POINT_GOAL);

        for (int i = 0; i < mSearchRouteAroundMulti.poi.size(); i++) {
            PoiInfo poiInfo = mSearchRouteAroundMulti.poi.get(i);
            if (poiInfo.hascctvdata) {
                hash_CCTV.put(poiInfo.cctvdata.id, poiInfo);
                drawCCTVMarker(Integer.parseInt(poiInfo.dpx), Integer.parseInt(poiInfo.dpy), poiInfo.cctvdata.id, MAP_POINT_NORMAL);
            }
        }
    }

    /**
     * webview 지도에서 자바스크립트 콜백 결과 받는 핸들러.
     */
    public Handler mapCBHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (isFinishing())
                return;

            LOG.d("TodayCommuters MapCB_Handler msg what : " + msg.what);

            switch (msg.what) {
                case MAPConst.CB_SETTOUCHEND:
                    Log.e("DSKIM", "MapCB_Handler : CB_SETTOUCHEND");
                    break;
                case MAPConst.MAP_SUCC:
                case MAPConst.MAP_RECYCLE:
                case MAPConst.CB_ISMAP:
                    // 지도 로딩 성공
                    LOG.d("MapCB_Handler : MAP_SUCC");

                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();
//                    mMapController.TwsMapSetLevel(DEFAULT_MAP_LEVEL);
                    mMapController.TwsMapSetEndTouch();
                    mMapController.TwsMapSetMoveend();
                    String[] xy = null;

//                    switch (mPrefMgr.getEnterType()) {
////                        case ENTER_NORMAL:
////                        case ENTER_LOW:
////                            startUI(now_X, now_Y);
////                            break;
////                        case ENTER_FAVORITE:
////                            mMapController.TwsMapClearMarkers();
////                            setClickableControl(true);
////
////                            xy = mPrefMgr.getMyPosition().split("\\|");
////
////                            mPrefMgr.setEnterType(0);
////                            break;
//                    }
                    startUI(now_X, now_Y);
                    break;

                case MAPConst.MAP_FAIL:
                    // 지도 로딩 실패

                    LOG.d("MapCB_Handler : MAP_FAIL");

                    // if (mProgDlg.isShowing())
                    // mProgDlg.dismiss();

                    WebViewSingleton.clear((TodayApplication) getApplication(),
                            Defines.InstanceConst.MAIN_MAP_INSTANCE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishUI("네트워크 에러", "지도 초기화 실패하여 앱을 종료합니다.");
                        }
                    }, Toast.LENGTH_SHORT);
                    // 종료 처리.

                    break;
                case MAPConst.CB_GET_EXTENT:// x1,x2,y1,y2

                    LOG.d("CB_GET_EXTENT : " + (String) msg.obj);
                    // 2014.12.17시나리오변경 반경3km extent deprecated
                    // String[] extentXY = ((String) msg.obj).split("\\|");
                    // apiSearchPoiAround(extentXY);
                    break;
                case MAPConst.CB_SETZOOMEND:
                    LOG.d("CB_SETZOOMEND : " + (String) msg.obj);
                    String[] zoomInfo = ((String) msg.obj).split("\\|");
                    // apiSearchPoiAround(zoomInfo);
                    lastLevel = Integer.parseInt(zoomInfo[2]);
//                    if (checkMapLevel() == false) {
//                        apiTconCCTV(zoomInfo[0], zoomInfo[1]);
//                        Toast.makeText(MainActivity.this, getResources().getString(R.string.no_support_map_level), Toast.LENGTH_SHORT).show();
//                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));
                    break;
                case MAPConst.CB_MOVEEND:
                    LOG.d("CB_MOVEEND : " + (String) msg.obj);
                    final String[] moved = ((String) msg.obj).split("\\|");
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(getResources().getDrawable(
//						R.xml.effect_search_button));

                    OldTimeStamp = Calendar.getInstance().getTimeInMillis();

                    // Timer count;
                    CountDownTimer cdt = new CountDownTimer(ADDR_TIME, ADDR_TIME) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFinish() {
                            // TODO Auto-generated method stub

                            // ??? 怨??

                            NewTimeStamp = Calendar.getInstance().getTimeInMillis();

                            if ((NewTimeStamp - OldTimeStamp >= ADDR_TIME) && OldTimeStamp != 0) {
                                apiSearchCoorAddr(moved[0], moved[1]);
//                                apiTconCCTV(moved[0], moved[1]);
                                CoordClient coordClient = new CoordClient();
                                Position_T position_t = coordClient.getConvertLongLati(Long.parseLong(moved[0]), Long.parseLong(moved[1]));
                                apiWeatherNow(String.valueOf(position_t.sLatiSec), String.valueOf(position_t.sLongSec));
                                OldTimeStamp = 0;
                            }
                        }

                    }.start();

                    break;
                case MAPConst.CB_SETONLYMOVEEND:
                    LOG.d("CB_SETONLYMOVEEND : " + (String) msg.obj);
                    final String[] array_addr;
                    array_addr = ((String) msg.obj).split("\\|");
                    // now_X = Integer.parseInt(array_addr[0]);
                    // now_Y = Integer.parseInt(array_addr[1]);
                    now_X = array_addr[0];
                    now_Y = array_addr[1];
                    lastLevel = Integer.parseInt(array_addr[2]);
                    String[] saved_xy = mPrefMgr.getMyPosition().split("\\|");
                    // 지도 포인트 이미지 보여주기관련 처리 S
                    if (now_X.equals(saved_xy[0]) && now_Y.equals(saved_xy[1])) {
                        mIV_map_waypoint.setVisibility(View.GONE);
                    } else {
                        mIV_map_waypoint.setVisibility(View.VISIBLE);
                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
//				ivSearch.setBackgroundDrawable(().getDrawable(
//						R.xml.effect_search_button));

                    break;

                case MAPConst.CB_SETTOUCHEVENTCB:
                    LOG.d("CB_SETTOUCHEVENTCB : " + (String) msg.obj);
                    String[] setTouchEventCBarr;
                    setTouchEventCBarr = ((String) msg.obj).split("\\|");

                    if (setTouchEventCBarr[0].equals("touchend")) {
                      /*  if (isTouchedMarker) {
                            isTouchedMarker = false;
                        } else {
                            llUnderLayout.setVisibility(View.GONE);
                            mMapController.TwsMapRemoveDivPopup("marker_select");
                        }*/
                    }
//				allowSearchAgain = true;
//				ivSearch.setEnabled(allowSearchAgain);
                    break;

                case MAPConst.CB_CREATEMARKER:
                    final String[] array_marker;
                    array_marker = ((String) msg.obj).split("\\|");
                    int nMakrer_index = Integer.parseInt(array_marker[0]);
                    mMapController.TwsMapSetTouchendMarkerCB(nMakrer_index);
                    break;
                case MAPConst.CB_GETCENTER:
                    LOG.d("CB_GETCENTER : " + (String) msg.obj);
                    // return x | y

                    break;

                case MAPConst.CB_GETLEVEL:
                    LOG.d("CB_GETLEVEL : " + (String) msg.obj);
                    // return level
                    break;

                case MAPConst.CB_CREATEFEATUREMAKER:
                    LOG.d("CB_CREATEFEATUREMAKER : " + (String) msg.obj);
                    // return marker_id|param
                    break;
                case MAPConst.CB_TOUCHENDMARKER:
                    LOG.d("CB_TOUCHENDMARKER : " + (String) msg.obj);

                    break;

                case MAPConst.CB_TOUCHENDFEATUREMARKER:
                    LOG.d("CB_TOUCHENDFEATUREMARKER : " + (String) msg.obj);
                    // return marker_id|param

                    break;

                case Defines.MAPConst.CB_GET:// 마커 터치 했을 경우 콜백
                    LOG.d("CB_GET : " + (String) msg.obj);
                    mMapController.TwsMapRemoveDivPopup("marker_select");
                    final String[] array_marker_touch;
                    array_marker_touch = ((String) msg.obj).split("\\|");
                    llUnderInfoLayout.setVisibility(View.VISIBLE);
                    if (array_marker_touch.length == 3) {
                        if (array_marker_touch[1].equals("c")) {
                            mSelectedCCTVPoi = hash_CCTV.get(array_marker_touch[2]);
                            mSelectedCCTVItem = transplantMapCCTV_Item(mSelectedCCTVPoi);
                            if (mSelectedCCTVPoi != null) {
                                drawSelectedMarker(mSelectedCCTVPoi);
                                showDetailInfo(mSelectedCCTVItem);
                            } else {
                                LOG.d("CB_GET ERR(ITEM NULL) ");
                            }
                        } else {
                            LOG.d("CB_GET ERR(NOT _C FORMAT) ");
//                            mMapController.TwsMapRemoveDivPopup("marker_select");
//                            llUnderInfoLayout.setVisibility(View.GONE);
                        }
                    } else {
                        LOG.d("CB_GET ERR(ITEM LENGTH NOT 3) ");
                    }
                    break;

                case MAPConst.CB_CENTERMAKER:

                    LOG.d("CB_GET : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETLONLATFROMPIXEL:
                    LOG.d("CB_GETLONLATFROMPIXEL : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_GETPIXELFROMLONLAT:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // return param

                    break;

                case MAPConst.CB_CREATEDIVPOPUP:
                    LOG.d("CB_GETPIXELFROMLONLAT : " + (String) msg.obj);
                    // isTouchedMarker = true;
                    // return param
                    break;
            }
        }
    };


    private boolean checkMapLevel() {
        boolean validation = false;
        if (6 < lastLevel) {
            validation = true;
        } else {
            validation = false;
            initMarker();
        }
        return validation;
    }

    @Override
    protected void onStart() {
        super.onStart();
        LOG.d("CALLBACK onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        LOG.d("CALLBACK onRestart");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LOG.d("CALLBACK onCreate");
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        setContentView(R.layout.activity_commute_recommend);
        initData();
        initMgr();
        setControl();
        makeWaitDialog();
        requestLocation();
//        mSelectedCCTVItem = (MapCCTV_Item) getIntent().getSerializableExtra("selectItem");
    }

    private void initMgr() {
        mApiMgr = new ApiMgr();
        mPrefMgr = new PrefMgr(RecommendActivity.this);
        String[] initPosition = mPrefMgr.getMyPosition().split("\\|");
        now_X = initPosition[0];
        now_Y = initPosition[1];
        mPrefMgr.setPREF_HOME_WORKPLACE_CHANGE(false);
    }

    private void runSample() {
        String homeX = mPrefMgr.getPREF_HOME_LON_X();
        String homeY = mPrefMgr.getPREF_HOME_LAT_Y();
        String workX = mPrefMgr.getPREF_WORK_LON_X();
        String workY = mPrefMgr.getPREF_WORK_LAT_Y();

        String[] conStartXY = PositionConverter.getWsg84DegreePosition(homeX, homeY);
        mRouteStart = new RouteLoc(conStartXY[1], conStartXY[0]);
        testStartTxt.setText(conStartXY[0] + " , " + conStartXY[1]);
        startX = homeX;
        startY = homeY;

        String[] conEndXY = PositionConverter.getWsg84DegreePosition(workX, workY);
        mRouteEnd = new RouteLoc(conEndXY[1], conEndXY[0]);
        testEndTxt.setText(conEndXY[0] + " , " + conEndXY[1]);
        goalX = workX;
        goalY = workY;
//        mRouteStart = new RouteLoc("45756196", "13466597");
//        mRouteEnd = new RouteLoc("46533300", "12748497");
        apiSearchRouteList(mRouteStart, mRouteEnd, new RouteExtensionItem());
    }

    @Override
    protected void onResume() {
        LOG.d("CALLBACK onResume");
        vvCCTVVideo.resume();
        vvCCTVVideo.start();
        super.onResume();
    }

    @Override
    protected void onStop() {
        LOG.d("CALLBACK onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        LOG.d("CALLBACK onPause");
        vvCCTVVideo.stopPlayback();
        vvCCTVVideo.pause();
//        if (webview != null) {
//            rootLayout.removeView(webview);
//        }
        // singleton webview view에 제거 E

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        LOG.d("CALLBACK onDestroy");
//        if (webview != null) {
//            rootLayout.removeView(webview);
//        }
        WebViewSingleton.clear((TodayApplication) getApplication(),
                Defines.InstanceConst.FIND_MAP_INSTANCE);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(resultCode);
        finish();
//        super.onBackPressed();
/*        if (!mBFinish) {
            mBFinish = true;
            Toast.makeText(RecommendActivity.this,
                    R.string.backpress_close_app_ment, Toast.LENGTH_SHORT)
                    .show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    mBFinish = false;
                }
            }, 3000);
        } else {
            if (vvCCTVVideo != null && vvCCTVVideo.isPlaying()) {
                vvCCTVVideo.pause();
            }
            finish();
        }*/
    }

    protected void finishUI(String title, String msg) {
        // TODO Auto-generated method stub
        dismissWaitDialog();
        showConfirmPopup(getString(R.string.network_info), getResources().getString(R.string.map_err_init_fail), APP_EXIT_TYPE);
        // 종료처리.
    }

    private void setControl() {
        getDisplayMetrics();
        hash_CCTV = new ConcurrentHashMap<String, PoiInfo>();
        mIV_map_waypoint = (ImageView) findViewById(R.id.frame_trf_rec_map_waypoint);
        rlDetail = (RelativeLayout) findViewById(R.id.rl_rec_detail);
        rlFavorite = (RelativeLayout) findViewById(R.id.rl_rec_favorite);
        tvCCTVName = (TextView) findViewById(R.id.tv_rec_cctv_name);
        vvCCTVVideo = (VideoView) findViewById(R.id.vv_rec_cctv_video);
        tvPostionAddr = (TextView) findViewById(R.id.tv_rec_position_addr);
        ivMyPositionButton = (ImageView) findViewById(R.id.iv_rec_my_position);
        llUnderInfoLayout = (LinearLayout) findViewById(R.id.ll_rec_under_info_layout);
        ivFavoriteList = (ImageView) findViewById(R.id.iv_rec_favorite_list);
        ivCctvCloseButton = (ImageView) findViewById(R.id.iv_rec_cctv_close_button);
        rl_cctv_video_wraper = (RelativeLayout) findViewById(R.id.rl_rec_cctv_video_wraper);
        ivCctvWeather = (ImageView) findViewById(R.id.iv_rec_cctv_weather);
        ivRecFavoriteImg = (ImageView) findViewById(R.id.iv_rec_favorite_img);

        //Test S
        startButton = (Button) findViewById(R.id.test_start_button);
        startButton.setOnClickListener(this);
        endButton = (Button) findViewById(R.id.test_end_button);
        endButton.setOnClickListener(this);
        searchButton = (Button) findViewById(R.id.test_search_button);
        searchButton.setOnClickListener(this);
        testStartTxt = (TextView) findViewById(R.id.test_start_txt);
        testEndTxt = (TextView) findViewById(R.id.test_end_txt);
        //Test E
        setOnlyWidth400DpDevice();
        initPopUpWindow();

        rlDetail.setOnClickListener(this);
        rlFavorite.setOnClickListener(this);
        vvCCTVVideo.setOnClickListener(this);
        ivMyPositionButton.setOnClickListener(this);
        ivFavoriteList.setOnClickListener(this);
        ivCctvCloseButton.setOnClickListener(this);
    }

    private void setOnlyWidth400DpDevice() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                (int) getResources().getDimension(R.dimen.size_260),
                (int) getResources().getDimension(R.dimen.size_160)
        );
        if (dpWidth == 400) {
            rl_cctv_video_wraper.setLayoutParams(params);
        }
    }

    private void getDisplayMetrics() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        density = getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
        dpWidth = outMetrics.widthPixels / density;
    }

    private void setClickableControl(boolean click) {

    }

    private String[] convertXY(Location location) {
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1] = String.valueOf(data.y);
        return xyStr;
    }

    protected void startUI(String longi, String lati) {
        // TODO Auto-generated method stub
        // poi 정보 가져오기.
        mMapController.TwsMapMoveTo(Integer.parseInt(longi), Integer.parseInt(lati));
        mMapController.TWsMapGetExtent();
        setMyPosition(longi, lati);
//        apiTconCCTV(longi, lati);
        apiSearchCoorAddr(longi, lati);
        runSample();
    }

    private void requestLocation() {
        // GMS 측위 요청.
        // makeWaitDialog();
        LocationGmsClient location = new LocationGmsClient(RecommendActivity.this, locationResultHandler);
        location.connect();
    }

    /**
     * @param longi X좌표
     * @param lati  Y좌표
     *              <p/>
     *              받아온 좌표를 my_long, my_lati에 대입한다. 추후 자주 쓸꺼라서.
     * @author DSKIM</br>
     */
    private void setMyPosition(String longi, String lati) {
        String strImagePath = null;
        try {
            if (isLastLocation) {// 측위 Timeout
                strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
            } else {// 측위 성공
                strImagePath = "/android_asset/mapicon/frm_my_point_01.png";
            }
        } catch (Exception e) {
            e.printStackTrace();
            strImagePath = "/android_asset/mapicon/frm_my_point_01_off.png";
        }
        mMapController.TwsMapRemoveFeatureMarkerALL();
        mMapController.TwsMapAddCenterMarker(Integer.parseInt(longi),
                Integer.parseInt(lati), strImagePath);
    }

    private void dismissSplashDialog() {
        if (customIntroDialog != null)
            customIntroDialog.dismiss();
    }

    private void createSplashoDialog() {
        customIntroDialog = new CustomIntroDialog(RecommendActivity.this);
        customIntroDialog.show();
        customIntroDialog.setCancelable(false);
    }

    private void dismissWaitDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            waitDialog.dismiss();
            waitDialog = null;
        }
    }

    private void makeWaitDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            waitDialog.dismiss();
            waitDialog = null;
        }
        waitDialog = new CustomProgress(RecommendActivity.this);
        waitDialog.show();
    }

    private void initData() {
        mApiMgr = new ApiMgr();
        mPrefMgr = new PrefMgr(RecommendActivity.this);
        Gson gson = new Gson();
        String favoriteGson = mPrefMgr.getFavoriteCCTV();
        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
        if (favoriteCCTVData == null) {
            favoriteCCTVData = new FavoriteCCTVData();
        }
        mFavoriteList = favoriteCCTVData.getFavoriteCCTVItems();
        idListInit(favoriteCCTVData);

        System.out.println("DSKIM : " + mPrefMgr.getPREF_HOME_LON_X());
        System.out.println("DSKIM : " + mPrefMgr.getPREF_HOME_LAT_Y());
        System.out.println("DSKIM : " + mPrefMgr.getPREF_WORK_LON_X());
        System.out.println("DSKIM : " + mPrefMgr.getPREF_WORK_LAT_Y());
    }

    private void initPopUpWindow() {
        listPopupWindow = new ListPopupWindow(RecommendActivity.this);
        listPopupWindow.setAnchorView(ivFavoriteList);
        float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 208, getResources().getDisplayMetrics());
        listPopupWindow.setHeight((int) height);
        listPopupWindow.setWidth(500);
        listPopupWindow.setContentWidth(500);
        listPopupWindow.setModal(true);
    }

    public void initWebview() {

        mContext = getApplicationContext();

        rootLayout = (RelativeLayout) findViewById(R.id.rec_webview_map);

        mDisplay = getWindowManager().getDefaultDisplay();

        // 지도 로딩 시작
        // if (!(mProgDlg.isShowing()))
        // mProgDlg.show();

        // 웹뷰 불러오기. (어플리케이션에서) S

        // 콜백 확인 후 처리한다. init인지 recycle
        WebViewSingleton webViewSingleton = WebViewSingleton.getInstance((TodayApplication) getApplication(), Defines.InstanceConst.FIND_MAP_INSTANCE);

        webViewSingleton.setHandler(mapCBHandler);

        webview = webViewSingleton.getWebView();

        if (webview != null) {
            // singleton webview view에 추가.
            ViewGroup parent = (ViewGroup) webview.getParent();

            if (parent == null) {
                rootLayout.addView(webview, mDisplay.getWidth(),
                        mDisplay.getHeight());
            }

        } else {
            // 예외처리.
        }

        // 웹뷰 불러오기. (어플리케이션에서) E

        // 웹뷰 컨트롤 등록
        if (webview != null) {
            mMapController = new twsMapController(webview);
            mMapController.TwsMapGetCenter();
            mMapController.TwsMapSetPantoendCB(true);

            // mapController.TwsMapSetStartTouch();
            // mapController.TwsMapSetEndTouch();
        } else {
            // 예외 처리 필요 (종료처리)
            // PopupDialog_Finish(AppDefines.ERROR_MSG_SET, mContext
            // .getResources().getString(R.string.ONAIR_SERVER_FAIL));
        }
    }

    private void apiWeatherNow(String strX, String strY) {
        if (mApiMgr != null) {
            mApiMgr.apiWeatherNow(this, strX, strY, new Response.Listener<WeatherNow>() {
                @Override
                public void onResponse(WeatherNow retCode) {
                    LOG.d("apiWeatherNow retCode " + retCode.weather.get(0).id);
                    int weatherCode = Integer.valueOf(retCode.weather.get(0).id);
                    setWeatherImage(ivCctvWeather, weatherCode);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiTconCCTV VolleyError " + volleyError.getMessage());
//                    Toast.makeText(RecommendActivity.this, "apiTconCCTV 네트워크 오류", Toast.LENGTH_SHORT).show();
                    dismissWaitDialog();
                }
            });
        }
    }

    private void setWeatherImage(ImageView imgView, int weatherCode) {
        if (weatherCode / 100 == 8) {
            if (weatherCode == WeatherCode.sunny) {
                imgView.setBackgroundResource(R.drawable.icon_weather_sun);
            } else if (weatherCode == WeatherCode.fewClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_day_cloud);
            } else if (weatherCode == WeatherCode.scatteredClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            } else if (weatherCode == WeatherCode.brokenClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            } else if (weatherCode == WeatherCode.overcastClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            }
        } else if (weatherCode / 100 == WeatherCode.mist) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        } else if (weatherCode / 100 == WeatherCode.snow) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_snow);
        } else if (weatherCode / 100 == WeatherCode.rain) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_rain);
        } else if (weatherCode / 100 == WeatherCode.sotrm) {
            imgView.setBackgroundResource(R.drawable.icon_weather_windy_thunderstorm);
        } else if (weatherCode / 100 == WeatherCode.mist) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        } else if (weatherCode / 100 == WeatherCode.dizzle) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        }
    }


    private void apiTconCCTV(String strX, String strY) {

        if (checkMapLevel() == false) {
            Toast.makeText(RecommendActivity.this, getResources().getString(R.string.no_support_map_level), Toast.LENGTH_SHORT).show();
            llUnderInfoLayout.setVisibility(View.GONE);
            return;
        }

        if (mApiMgr != null) {
            mApiMgr.apiTconCCTV(this, strX, strY, STR_LEVEL, new Response.Listener<MapCCTV>() {

                @Override
                public void onResponse(MapCCTV retCode) {

                    LOG.d("apiTconCCTV retCode.result : " + retCode.result);
                    LOG.d("apiTconCCTV retCode.errormsg : " + retCode.errormsg);

                    if (retCode.result == 0 || retCode.result == 903) {
                        Message msg = netResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCH_CCTV_SUCC;
                        msg.obj = retCode;

                        netResultHandler.sendMessage(msg);
                    } else {
                        // fail
                        LOG.d("apiTconCCTV Fail " + retCode.result);
                        Toast.makeText(RecommendActivity.this, retCode.errormsg + "(" + retCode.result + ")", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiTconCCTV VolleyError " + volleyError.getMessage());
                    Toast.makeText(RecommendActivity.this, "apiTconCCTV 네트워크 오류", Toast.LENGTH_SHORT).show();
                    dismissWaitDialog();
                }
            });
        }
    }

    private void apiSearchCoorAddr(String strX, String strY) {

        if (mApiMgr != null) {
            mApiMgr.apiSearchCoordAddr(this, strX, strY, new Response.Listener<SearchCoordAddr>() {

                @Override
                public void onResponse(SearchCoordAddr retCode) {

                    LOG.d("apiSearchCoorAddr retCode.result : " + retCode.result);
                    LOG.d("retCode.errormsg : " + retCode.errormsg);

                    if (retCode.result.equals("0")) {
                        Message msg = netResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCH_COORD_ADDR_SUCC;
                        msg.obj = retCode;

                        netResultHandler.sendMessage(msg);
                    } else {
                        // fail
                        LOG.d("apiSearchCoorAddr Fail " + retCode.result);
                        Toast.makeText(RecommendActivity.this, retCode.errormsg + "(" + retCode.result + ")", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiSearchCoorAddr VolleyError " + volleyError.getMessage());
                    Toast.makeText(RecommendActivity.this, "apiSearchCoorAddr 네트워크 오류", Toast.LENGTH_SHORT).show();
                    dismissWaitDialog();
                }
            });
        }
    }

    private void redrawCCTVMarker() {
        hash_CCTV.clear();
        mMapController.TwsMapRemoveDivPopups();
        drawCCTVMarkers(mSearchRouteAroundMulti);
        drawSelectedMarker(mSelectedCCTVPoi);
    }


    /**
     * 지도 위에 생성된 마커들 모두 삭제(마커와 Div마커 모두 삭제)
     */
    public void initMarker() {
        LOG.d("InitMarkers");
        hash_CCTV.clear();
        mMapController.TwsMapRemoveDivPopup("marker_select");
        mMapController.TwsMapRemoveDivPopups();
    }

    /**
     * @param longi
     * @param lati
     * @param cctvId
     * @param flag   0 : 일반, 1 : 시작, 2: 골(끝)
     */
    private void drawCCTVMarker(int longi, int lati, String cctvId, int flag) {
        // marker S
        String imgPathBrand = null;
        String imgPathPoint = null;

        if (flag == 1) {//시작
            imgPathBrand = "/android_asset/mapicon/map_point_start.png";// 수정
            imgPathPoint = "/android_asset/mapicon/map_point_start.png"; // 수정
        } else if (flag == 2) {//끝
            imgPathBrand = "/android_asset/mapicon/map_point_goal.png";// 수정
            imgPathPoint = "/android_asset/mapicon/map_point_goal.png"; // 수정
        } else {//일반
            imgPathBrand = "/android_asset/mapicon/ico_map_point_cctv_01.png";// 수정
            imgPathPoint = "/android_asset/mapicon/map_point_green.png"; // 수정
            for (String item : idLIst) {
                if (item.equals(cctvId)) {
                    imgPathBrand = "/android_asset/mapicon/ico_map_point_favorite.png";
                }
            }
        }
//        String imgPathPoint = "/android_asset/mapicon/ico_map_point_cctv_01.png"; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);
        String strParam = null;

        if (flag == 1) {//시작
            strParam = "s|" + cctvId;
            mMapController.TwsMapOilviewDivPopupDoubleCallBack("s_" + cctvId, longi,
                    lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                    imgPathPoint, offX, offY, strParam);
        } else if (flag == 2) {//끝
            strParam = "g|" + cctvId;
            mMapController.TwsMapOilviewDivPopupDoubleCallBack("g_" + cctvId, longi,
                    lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                    imgPathPoint, offX, offY, strParam);
        } else {//일반
            strParam = "c|" + cctvId;
            mMapController.TwsMapOilviewDivPopupDoubleCallBack("c_" + cctvId, longi,
                    lati, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                    imgPathPoint, offX, offY, strParam);
        }
    }

    private void drawSelectedMarker(PoiInfo tempInfo) {
        int nX = Integer.parseInt(tempInfo.dpx);
        int nY = Integer.parseInt(tempInfo.dpy);

        String[] converted = PositionConverter.getWsg84DegreePosition(tempInfo.dpx, tempInfo.dpy);
        Log.i("drawSelectedMarker", "selected Position LonX : " + converted[0]);
        Log.i("drawSelectedMarker", "selected Position LatY : " + converted[1]);
        mMapController.TwsMapMoveTo(nX, nY);
//        showDetailInfo(tempInfo);

//        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyNAME,tempInfo.catenmae);
//        logFlurryAgent(DefineFlurry.keyMA_MARKER, DefineFlurry.keyXY, nX + "|"
//                + nY);

//        String imgPathBrand = "/android_asset/mapicon/"+ getImagePath(tempInfo.catecode);// 수정
        String imgPathBrand = "/android_asset/mapicon/ico_map_point_cctv_click_01.png";// 수정
        for (String item : idLIst) {
            if (item.equals(tempInfo.cctvdata.id)) {
                imgPathBrand = "/android_asset/mapicon/ico_map_point_favorite.png";
            }
        }
        String imgPathPoint = "/android_asset/mapicon/map_point_red_click.png"; // 수정

        int nFiconW = (int) (48 * WEB_RESIZE);
        int nFiconH = (int) (57 * WEB_RESIZE);

        int offX = -((int) (48 * WEB_RESIZE)) / 2;
        int offY = -(int) (57 * WEB_RESIZE);

        int nSiconW = (int) (48 * WEB_RESIZE);
        int nSiconH = (int) (57 * WEB_RESIZE);

        String strParam = "s|" + tempInfo.poiid;

        mMapController.TwsMapOilviewDivPopupDoubleCallBack("marker_select", nX,
                nY, nFiconW, nFiconH, imgPathBrand, nSiconW, nSiconH,
                imgPathPoint, offX, offY, strParam);
    }

    /**
     * 선택한 마커에 대해서 상세 데이터를 하단 레이아웃에 보여준다.
     */
    private void showDetailInfo(MapCCTV_Item tempInfo) {
        boolean isFavorite = false;
        for (MapCCTV_Item item : mFavoriteList) {
            if (tempInfo.cctv_id.equals(item.cctv_id)) {
                isFavorite = true;
            }
        }
        if (isFavorite) {
            ivRecFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite_selected);
        } else {
            ivRecFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite);
        }

        tvCCTVName.setText(tempInfo.location);
        Uri video_url = Uri.parse(tempInfo.stream_url);
        playVideoCCTV(video_url);
    }

    private void playVideoCCTV(Uri video_url) {
        vvCCTVVideo.setVideoURI(video_url);
        vvCCTVVideo.requestFocus();
        vvCCTVVideo.start();

        vvCCTVVideo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub

                //finish();
                return false;
            }
        });

        vvCCTVVideo
                .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(final MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        LOG.d("onPrepared");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                             /*   if (mProDlg.isShowing())
                                    mProDlg.dismiss();*/
                            }
                        });
                        // m_text_cctv_time.postDelayed(getMovieTime, 1000);
                    }
                });


        vvCCTVVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*if (mProDlg.isShowing())
                            mProDlg.dismiss();*/
                    }
                });

//                PopupDialog_MSG("정보 제공처 사정으로 CCTV 영상이 제공되지 않는 구간입니다.");

                return true;
            }
        });

        vvCCTVVideo
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        // 재생 완료
                        LOG.d("onCompletion");

                        // 무한 루프
                        vvCCTVVideo.start();

                    }
                });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LOG.d("onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case CALL_TYPE_MAIN_TO_DETAIL:
                //메인 -> 디테일
                initData();
                redrawCCTVMarker();
                if (mSelectedCCTVItem != null) {
                    showDetailInfo(mSelectedCCTVItem);
                }
                break;
            case CALL_TYPE_WIDGET_TO_DETAIL:
                //위젯 -> 디테일
                initData();
                mSelectedCCTVItem = (MapCCTV_Item) data.getSerializableExtra("selectItem");
                int nX = Integer.parseInt(mSelectedCCTVItem.longitude);
                int nY = Integer.parseInt(mSelectedCCTVItem.latitude);
                mMapController.TwsMapMoveTo(nX, nY);
                showDetailInfo(mSelectedCCTVItem);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_rec_my_position:
                initMarker();
                mIV_map_waypoint.setVisibility(View.GONE);
                llUnderInfoLayout.setVisibility(View.GONE);
                makeWaitDialog();
                requestLocation();
                break;
            case R.id.rl_rec_favorite:
                //즐겨찾기에 추가, 팝업 표시
                addDeleteFavorite();
                break;
            case R.id.rl_rec_detail:
                vvCCTVVideo.pause();
                Intent gotoDetail = new Intent(RecommendActivity.this, DetailActivity.class);
                gotoDetail.putExtra("selectItem", mSelectedCCTVItem);
                gotoDetail.putExtra("call_type", CALL_TYPE_MAIN_TO_DETAIL);
                startActivityForResult(gotoDetail, CALL_TYPE_MAIN_TO_DETAIL);
                //상세 화면 이동
                break;
            case R.id.iv_rec_favorite_list:
                updateFavoriteList();
                break;
            case R.id.iv_rec_cctv_close_button:
                mSelectedCCTVPoi = null;
                mMapController.TwsMapRemoveDivPopup("marker_select");
                vvCCTVVideo.pause();
                llUnderInfoLayout.setVisibility(View.GONE);
                break;
            case R.id.test_start_button:
                String[] conStartXY = PositionConverter.getWsg84DegreePosition(now_X, now_Y);
                mRouteStart = new RouteLoc(conStartXY[1], conStartXY[0]);
                testStartTxt.setText(conStartXY[0] + " , " + conStartXY[1]);
                mPrefMgr.setPREF_HOME_LON_X(now_X);
                mPrefMgr.setPREF_HOME_LAT_Y(now_Y);
                startX = now_X;
                startY = now_Y;
                break;
            case R.id.test_end_button:
                String[] conEndXY = PositionConverter.getWsg84DegreePosition(now_X, now_Y);
                mRouteEnd = new RouteLoc(conEndXY[1], conEndXY[0]);
                testEndTxt.setText(conEndXY[0] + " , " + conEndXY[1]);
                mPrefMgr.setPREF_WORK_LON_X(now_X);
                mPrefMgr.setPREF_WORK_LAT_Y(now_Y);
                goalX = now_X;
                goalY = now_Y;
                break;
            case R.id.test_search_button:
                makeWaitDialog();
                initMarker();
                apiSearchRouteList(mRouteStart, mRouteEnd, new RouteExtensionItem());
                break;
        }
    }

    private void updateFavoriteList() {

        if (mFavoriteList.size() < 1) {
            Toast.makeText(RecommendActivity.this, getString(R.string.plz_add_favorite_first), Toast.LENGTH_SHORT).show();
        } else {

            final ArrayList<String> cctvName = new ArrayList<String>();

            for (MapCCTV_Item item : mFavoriteList) {
                cctvName.add(item.location);
                System.err.println("###"+item.location);
            }

            listPopupWindow.setHeight((60) * (cctvName.size()) * (int) density);
            listPopupWindow.setWidth(180 * (int) density);
            listPopupWindow.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, cctvName));

            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    mMapController.TwsMapRemoveDivPopup("marker_select");
//                Toast.makeText(MainActivity.this, cctvName.get(position), Toast.LENGTH_SHORT).show();
                    MapCCTV_Item mapCCTV_item = mFavoriteList.get(position);
                    int lonX = Integer.parseInt(mapCCTV_item.longitude);
                    int latY = Integer.parseInt(mapCCTV_item.latitude);
                    int intX = Integer.parseInt(now_X);
                    int intY = Integer.parseInt(now_Y);
                    if (intX == lonX && intY == latY) {
                        //동일한 위치임
                    } else {
//                        makeWaitDialog();
//                        mMapController.TwsMapMoveTo(lonX, latY);
//                        mSelectedCCTVItem = mapCCTV_item;
//                        llUnderInfoLayout.setVisibility(View.VISIBLE);
//                        showDetailInfo(mSelectedCCTVItem);
                    }
                    listPopupWindow.dismiss();
                }
            });
            listPopupWindow.show();
        }
    }

    /**
     * 즐겨찾기를 추가 삭제한다.
     * 즐겨찾기 리스트 업데이트
     */
    private void addDeleteFavorite() {
        Gson gson = new Gson();
        int seekIndex = -1;
        boolean isDuplicate = false;
        //즐겨찾기 초기화
        String favoriteGson = mPrefMgr.getFavoriteCCTV();
        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
        if (favoriteCCTVData == null) {
            favoriteCCTVData = new FavoriteCCTVData();
        }
        //즐겨찾기 초기화 끝
        //선택된 CCTV가 즐찾에 있는 지 확인
        for (MapCCTV_Item item : favoriteCCTVData.getFavoriteCCTVItems()) {
            seekIndex++;
            if (item.cctv_id.equals(mSelectedCCTVPoi.cctvdata.id)) {
                //삭제 일경우 삭제
                isDuplicate = true;
                break;
            }
        }

        //중복여부에따라 추가/삭제 결정
        if (isDuplicate == true) {
            showConfirmPopupTwo(getResources().getString(R.string.favorite), mSelectedCCTVPoi.cctvdata.name + getResources().getString(R.string.remove_favorite), FAVORITE_DEL_TYPE, seekIndex);
        } else {
            showConfirmPopupTwo(getResources().getString(R.string.favorite), mSelectedCCTVPoi.cctvdata.name + getResources().getString(R.string.add_favorite), FAVORITE_ADD_TYPE, seekIndex);
        }
    }

    private void idListInit(FavoriteCCTVData favoriteCCTVData) {
        idLIst.clear();
        for (MapCCTV_Item item : favoriteCCTVData.getFavoriteCCTVItems()) {
            idLIst.add(item.cctv_id);
            System.out.println("###" + item.cctv_id);
        }
    }

    /**
     * 주소 정보를 화면 표시.
     *
     * @param obj
     */
    private void retApiSearchCoordAddr(SearchCoordAddr obj) {
        String addr = obj.cut_address;
        if (addr.length() < 2) {
            tvPostionAddr.setText(getResources().getString(R.string.no_information));
        } else {
            tvPostionAddr.setText(addr);
        }
    }

    /**
     * @param strTitle
     * @param strContents
     * @param type
     */
    private void showConfirmPopup(String strTitle, String strContents, final int type) {
        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
        title.setText(strTitle);
        contents.setText(strContents);
        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 9999) {
                    finish();
                    System.exit(0);
                } else {
                    confirmPopUp.dismiss();
                }
            }
        });
        confirmPopUp.setCancelable(false);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    private void showConfirmPopupTwo(String strTitle, String strContents, final int type, final int seekIndex) {
        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        final Gson gson = new Gson();
        final String favoriteGson = mPrefMgr.getFavoriteCCTV();

        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup_two);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
        title.setText(strTitle);
        contents.setText(strContents);
        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_left);
        Button btnCancle = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_right);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case FAVORITE_ADD_TYPE:
                        //4개 미만일 경우에만 추가
                        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
                        if (favoriteCCTVData == null) {
                            favoriteCCTVData = new FavoriteCCTVData();
                        }
                        if (favoriteCCTVData.getFavoriteCCTVItems().size() < LIMIT_FAVORITE_COUNT) {
                            favoriteCCTVData.getFavoriteCCTVItems().add(mSelectedCCTVItem);
                            String jsonStr = gson.toJson(favoriteCCTVData);
                            System.out.println(jsonStr);
                            mPrefMgr.setFavoriteCCTV(jsonStr);
                            mFavoriteList = favoriteCCTVData.getFavoriteCCTVItems();
                            idListInit(favoriteCCTVData);
                            Toast.makeText(RecommendActivity.this, mSelectedCCTVItem.location + getResources().getString(R.string.add_favorite_done), Toast.LENGTH_SHORT).show();
                            ivRecFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite_selected);
                            redrawCCTVMarker();
                        } else {
                            Toast.makeText(RecommendActivity.this, getResources().getString(R.string.max_favorite), Toast.LENGTH_SHORT).show();
                            String jsonStr = gson.toJson(favoriteCCTVData);
                            System.out.println(jsonStr);
                        }
                        break;
                    case FAVORITE_DEL_TYPE:
                        FavoriteCCTVData favoriteCCTVData1 = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
                        if (favoriteCCTVData1 == null) {
                            favoriteCCTVData1 = new FavoriteCCTVData();
                        }
                        favoriteCCTVData1.getFavoriteCCTVItems().remove(seekIndex);
                        String jsonStrrr = gson.toJson(favoriteCCTVData1);
                        mPrefMgr.setFavoriteCCTV(jsonStrrr);
                        mFavoriteList = favoriteCCTVData1.getFavoriteCCTVItems();
                        idListInit(favoriteCCTVData1);
                        redrawCCTVMarker();
                        Toast.makeText(RecommendActivity.this, mSelectedCCTVItem.location + getResources().getString(R.string.remove_favorite_done), Toast.LENGTH_SHORT).show();
                        ivRecFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite_selected);
                        sendWidgetRefresh();
                        break;
                }
                confirmPopUp.dismiss();
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPopUp.dismiss();
            }
        });
        confirmPopUp.setCancelable(true);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    /**
     * 데이터셋이 안맞아서 이렇게 함
     * @return
     */
    private MapCCTV_Item transplantMapCCTV_Item(PoiInfo selectedCCTVItem) {
        MapCCTV_Item addTemp = new MapCCTV_Item();
        PoiCCTV poiCCTV =  selectedCCTVItem.cctvdata;
        addTemp.cctv_id = poiCCTV.id;
        addTemp.latitude = selectedCCTVItem.dpy;
        addTemp.longitude = selectedCCTVItem.dpx;
        addTemp.thumbnail = poiCCTV.thumbnail;
        addTemp.stream_url = poiCCTV.stream_url;
        addTemp.location = poiCCTV.name;
        addTemp.offer = poiCCTV.offer;
        addTemp.update_time = poiCCTV.updatetime;
        return addTemp;
    }


    /**
     * 경로 주변 CCTV
     *
     * @param myLonX
     * @param myLatY
     * @param rid
     */
    private void apiSearchRouteAroundMulti(String myLonX, String myLatY, String rid, final String startX, final String startY, final String goalX, final String goalY) {

        if (mApiMgr != null) {
            mApiMgr.apisearchRouteAroundMulti(RecommendActivity.this, myLonX, myLatY, rid, startX, startY, goalX, goalY, new Response.Listener<SearchRouteAroundMulti>() {
                @Override
                public void onResponse(SearchRouteAroundMulti response) {

                    LOG.d("retCode.result : " + response.result);
                    LOG.d("retCode.errormsg : " + response.errormsg);

                    if (response.result.equals("0")) {
                        Log.i("apiSearchRouteAroundMulti", "DSKIM 경로 주변 CCTV 개수 : " + response.poi.size());
                        Message msg = netResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCH_ROUTE_LIST_MUTI_SUCC;
                        msg.obj = response;

                        netResultHandler.sendMessage(msg);
                    } else {
                        // fail
                        LOG.d("apiSearchRouteAroundMulti Fail " + response.result);
                        dismissWaitDialog();
//                        Toast.makeText(RecommendActivity.this, response.errormsg + "(" + response.result + ")", Toast.LENGTH_SHORT).show();
                        Toast.makeText(RecommendActivity.this, "추천 경로가 없습니다.", Toast.LENGTH_SHORT).show();
                        drawCCTVMarker(Integer.parseInt(startX), Integer.parseInt(startY), "0000", MAP_POINT_START);
                        drawCCTVMarker(Integer.parseInt(goalX), Integer.parseInt(goalY), "9999", MAP_POINT_GOAL);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiSearchRouteAroundMulti VolleyError " + volleyError.getMessage());
                    dismissWaitDialog();
                    Toast.makeText(RecommendActivity.this, "네트워크 오류", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * 두지점의 경로의 RID 구하기
     *
     * @param start
     * @param end
     * @param routeExtensionItem
     */
    private void apiSearchRouteList(RouteLoc start, RouteLoc end, RouteExtensionItem routeExtensionItem) {

        if (mApiMgr != null) {
            mApiMgr.apisearchRouteList(this, start, end, routeExtensionItem, new Response.Listener<SearchRouteList>() {
                @Override
                public void onResponse(SearchRouteList response) {

                    LOG.d("retCode.result : " + response.result);
                    LOG.d("retCode.errormsg : " + response.errormsg);

                    if (response.result.equals("0")) {
                        Log.i("apisearchRouteList", "경로탐색 RID : " + response.rid);
                        Message msg = netResultHandler.obtainMessage();

                        msg.what = RetCode.RET_SEARCH_ROUTE_LIST_SUCC;
                        msg.obj = response.rid;

                        netResultHandler.sendMessage(msg);

                    } else {
                        // fail
                        LOG.d("apiSearchRouteList Fail " + response.result);
                        dismissWaitDialog();
                        Toast.makeText(RecommendActivity.this, response.errormsg + "(" + response.result + ")", Toast.LENGTH_SHORT).show();
                        drawCCTVMarker(Integer.parseInt(startX), Integer.parseInt(startY), "0000", MAP_POINT_START);
                        drawCCTVMarker(Integer.parseInt(goalX), Integer.parseInt(goalY), "9999", MAP_POINT_GOAL);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiSearchRouteList VolleyError " + volleyError.getMessage());
                    dismissWaitDialog();
                    Toast.makeText(RecommendActivity.this, "네트워크 오류", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void sendWidgetRefresh() {
        Intent intent = new Intent(mContext, CustomWidget.class);
        intent.setAction(ACTION_REFRESH_LURE);
        intent.putExtra("viewId", R.id.ll_time_over);
        sendBroadcast(intent);
    }
}
