package com.appteam.thinkware.todaycommuter.data;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-02-22.
 */
public class WeatherNow {
    public WeatherDataCoord coord;
    public WeatherDataSys sys;
    public ArrayList<WeatherDataWeather> weather = new ArrayList<WeatherDataWeather>();
    public String base;
    public WeatherDataMain main;
    public WeatherDataWind wind;
    public WeatherDataClouds clouds ;
    public WeatherDataRain rain;
    public String dt;
    public String id;
    public String name;
    public String cod;

    public class WeatherDataCoord{
        public String lon;
        public String lat;
    }

    public class WeatherDataSys{
        public String message;
        public String country;
        public String sunrise;
        public String sunset;
    }

    public class WeatherDataWeather{
        public String id;
        public String main;
        public String description;
        public String icon;
    }

    public class WeatherDataMain{
        public String temp;
        public String temp_min;
        public String temp_max;
        public String pressure;
        public String sea_level;
        public String grnd_level;
        public String humidity;
    }

    public class WeatherDataWind{
        public String speed;
        public String deg;
    }

    public class WeatherDataClouds{
        public String all;
    }
    public class WeatherDataRain{
        public String rain;
    }
}



