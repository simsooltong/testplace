package com.appteam.thinkware.todaycommuter.data;

/**
 * Created by jonychoi on 15. 1. 14..
 */
public class RetCode {

    public int result;
    public String errormsg;

    public static final int NET_ERROR_TIMEOUT = -101; // 네트워크 타임아웃
    public static final int HTTP_SUCC = 200;

    public static final int NET_SUCC = 0;

    public static final int NET_ERROR = 900; // 네트워크 오류.
    public static final int NET_ERROR_MSG = 901; // 네트워크 오류(서버 문구)

    public static final int RET_SEARCHPOI_SUCC = 1000;
    public static final int RET_SEARCHPOI_NUMBER_SUCC = 1002;

    public static final int RET_SEARCH_COORD_ADDR_SUCC = 1003;
    public static final int RET_SEARCH_COORD_ADDR_FAIL = 1004;

    public static final int RET_SEARCH_CCTV_SUCC = 1007;
    public static final int RET_SEARCH_CCTV_FAIL = 1008;

    public static final int RET_SEARCH_ROUTE_LIST_SUCC = 1009;
    public static final int RET_SEARCH_ROUTE_LIST_FAIL = 1010;

    public static final int RET_SEARCH_ROUTE_LIST_MUTI_SUCC = 1011;
    public static final int RET_SEARCH_ROUTE_LIST_MUTI_FAIL = 1012;

    public static final int RET_CCTV_NOW_SUCC = 1013;
    public static final int RET_CCTV_NOW_FAIL = 1014;
}
