package com.appteam.thinkware.todaycommuter.data;

public class SearchCoordAddr{

    public String result;        // 결과코드
    public String errormsg;    // 결과메시지
    public String admcode;
    public String x;
    public String y;
    public String address;
    public String cut_address;
}
