package com.appteam.thinkware.todaycommuter.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appteam.thinkware.todaycommuter.R;
import com.appteam.thinkware.todaycommuter.base.BaseActivity;
import com.appteam.thinkware.todaycommuter.customui.CustomTimePickerDialog;
import com.appteam.thinkware.todaycommuter.data.FavoriteCCTVData;
import com.appteam.thinkware.todaycommuter.data.MapCCTV_Item;
import com.appteam.thinkware.todaycommuter.data.PastCCTV;
import com.appteam.thinkware.todaycommuter.data.PastCCTV_Item;
import com.appteam.thinkware.todaycommuter.data.WeatherCode;
import com.appteam.thinkware.todaycommuter.data.WeatherNow;
import com.appteam.thinkware.todaycommuter.data.WeatherPast;
import com.appteam.thinkware.todaycommuter.mgr.ApiMgr;
import com.appteam.thinkware.todaycommuter.mgr.PrefMgr;
import com.appteam.thinkware.todaycommuter.util.LOG;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import CoordLib.CoordClient;
import CoordLib.Position_T;

/**
 * Created by dskim on 2015-01-15.
 */
public class DetailActivity extends BaseActivity implements View.OnClickListener {
    //PRIMITY S
    private final static String ACTION_REFRESH_LURE = "android.appwidget.action.ACTION_COMMUTE_REFRESH_LURE";
    int mYear, mMonth, mDay, mHour, mMinute;
    private static int LIMIT_FAVORITE_COUNT = 4;
    private int resultCode;
    private int weatherLocationCode;
    //PRIMITY E

    //OBJECT S
    private MapCCTV_Item mSelectedCCTVItem = null;
    private ApiMgr mApiMgr = null;
    private PrefMgr mPrefMgr = null;
    private Context mContext = null;
    private GregorianCalendar calendar = null;
    private ArrayList<PastCCTV_Item> pastImgaeList = null;
    private Runnable runnable = null;

    //OBJECT E

    //WiDGET S
    private VideoView vvDetailCCTVVideo = null;
    private ImageView ivDetailScreenShot1 = null;
    private ImageView ivDetailScreenShot2 = null;
    private Button btDetailClose = null;
    private TextView tvDetailCCTVName = null;
    private LinearLayout llDetailSetTime = null;
    private LinearLayout llDetailAddFavorite = null;
    private ImageView ivDetailWeatherNow = null;
    private ImageView ivDetailWeatherYesterday = null;
    private ImageView ivDetailWeatherLastWeek = null;
    private ImageView ivDetailFavoriteImg = null;
    private TextView
            tvDetailImgYesterdayTimeTxt;
    private TextView tvDetailVideoTodayTimeTxt;
    private TextView tvDetailImgLastweekdayTimeTxt;
    //WiDGET E

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commute_detail);
        mContext = getApplicationContext();
        getCalendar();
        initApiMgr();
        Intent getCCTVData = getIntent();
        resultCode = getCCTVData.getIntExtra("call_type", 8000);
        mSelectedCCTVItem = (MapCCTV_Item) getCCTVData.getSerializableExtra("selectItem");
        setControl();
        playVideoCCTV(mSelectedCCTVItem.stream_url);
        getWeatherImageNow();
        tvDetailCCTVName.setText(mSelectedCCTVItem.location);
        apiPastImage(lpad(mSelectedCCTVItem.cctv_id, 4, "0"), getHHMM());
    }

    private void getWeatherImageNow() {
        LOG.d("DSKIM detail lati" + String.valueOf(mSelectedCCTVItem.latitude));
        LOG.d("DSKIM detail longi " + String.valueOf(mSelectedCCTVItem.longitude));
        CoordClient coordClient = new CoordClient();
        Position_T position_t = coordClient.getConvertLongLati(Long.parseLong(mSelectedCCTVItem.longitude), Long.parseLong(mSelectedCCTVItem.latitude));
        LOG.d("DSKIM convert lati " + String.valueOf(position_t.sLatiSec));
        LOG.d("DSKIM convert longi " + String.valueOf(position_t.sLongSec));
        apiWeatherNow(ivDetailWeatherNow, String.valueOf(position_t.sLatiSec), String.valueOf(position_t.sLongSec));
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        LOG.d("CALLBACK CODE : " + requestCode);
    }

    private void getCalendar() {
        calendar = new GregorianCalendar();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
    }

    /**
     * lpad 함수
     *
     * @param str 대상문자열, len 길이, addStr 대체문자
     * @return 문자열
     */
    public static String lpad(String str, int len, String addStr) {
        String result = str;
        int templen = len - result.length();

        for (int i = 0; i < templen; i++) {
            result = addStr + result;
        }
        return result;
    }

    private String getHHMM() {
        LOG.d("before set hh" + mHour);
        LOG.d("before set mm" + mMinute);
        String hourStr = String.format("%02d", mHour);
        String minuteStr = String.format("%02d", (mMinute / 10) * 10);
        LOG.d("after set hh" + hourStr);
        LOG.d("after set mm" + minuteStr);
        return hourStr + minuteStr;
    }

    private void setControl() {
        mPrefMgr = new PrefMgr(DetailActivity.this);
        vvDetailCCTVVideo = (VideoView) findViewById(R.id.vv_detail_cctv_video);
        ivDetailScreenShot1 = (ImageView) findViewById(R.id.iv_detail_screenshot1);
        ivDetailScreenShot2 = (ImageView) findViewById(R.id.iv_detail_screenshot2);
        btDetailClose = (Button) findViewById(R.id.bt_detail_close);
        tvDetailCCTVName = (TextView) findViewById(R.id.tv_detail_cctv_name);
        llDetailSetTime = (LinearLayout) findViewById(R.id.ll_detail_time_set);
        llDetailAddFavorite = (LinearLayout) findViewById(R.id.ll_detail_add_favorite);
        ivDetailWeatherNow = (ImageView) findViewById(R.id.iv_detail_weather_now);
        ivDetailWeatherYesterday = (ImageView) findViewById(R.id.iv_detail_weather_yesterday);
        ivDetailWeatherLastWeek = (ImageView) findViewById(R.id.iv_detail_weather_last_week);
        ivDetailFavoriteImg = (ImageView) findViewById(R.id.iv_detail_favorite_img);
        tvDetailVideoTodayTimeTxt = (TextView) findViewById(R.id.tv_detail_video_today_time_txt);
        tvDetailImgYesterdayTimeTxt = (TextView) findViewById(R.id.tv_detail_img_yesterday_time_txt);
        tvDetailImgLastweekdayTimeTxt = (TextView) findViewById(R.id.tv_detail_img_lastweekday_time_txt);

        tvDetailVideoTodayTimeTxt.setText(String.format("%02d", mHour) + "시" + String.format("%02d", mMinute) + "분");
        tvDetailImgYesterdayTimeTxt.setText(String.format("%02d", mHour) + "시" + String.format("%02d", mMinute) + "분");
        tvDetailImgLastweekdayTimeTxt.setText(String.format("%02d", mHour) + "시" + String.format("%02d", mMinute) + "분");

        llDetailSetTime.setOnClickListener(this);
        btDetailClose.setOnClickListener(this);
        llDetailAddFavorite.setOnClickListener(this);

        ivDetailScreenShot1.setOnClickListener(this);
        ivDetailScreenShot2.setOnClickListener(this);
        setDetailScreenshotVisible(false);
        setFavoriteImg();
    }

    private void setDetailScreenshotVisible(boolean visible){
        ivDetailScreenShot1.setClickable(visible);
        ivDetailScreenShot2.setClickable(visible);
    }

    private void setFavoriteImg() {
        Gson gson = new Gson();
        String favoriteGson = mPrefMgr.getFavoriteCCTV();
        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
        if (favoriteCCTVData == null) {
            favoriteCCTVData = new FavoriteCCTVData();
        }
        boolean isFavorite = false;
        for (MapCCTV_Item item : favoriteCCTVData.getFavoriteCCTVItems()) {
            if (item.cctv_id.equals(mSelectedCCTVItem.cctv_id)) {
                //삭제 일경우 삭제
                isFavorite = true;
                break;
            }
        }
        if (isFavorite == true) {
            ivDetailFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite_selected);
        } else {
            ivDetailFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite);
        }
    }

    private void initApiMgr() {
        mApiMgr = new ApiMgr();
    }

    @Override
    protected void onResume() {
        if (vvDetailCCTVVideo != null) {
            vvDetailCCTVVideo.resume();
            vvDetailCCTVVideo.start();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        vvDetailCCTVVideo.stopPlayback();
        vvDetailCCTVVideo.pause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (resultCode == 8000) {
            setResult(resultCode);
            finish();
        } else if (resultCode == 8001) {
            Intent intent = new Intent();
            intent.putExtra("selectItem", mSelectedCCTVItem);
            setResult(resultCode, intent);
            finish();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void bindingURLImage(ImageView imageView, String photo_url) {
        try {
            Log.i("bindingURLImage", "request Glide");
            Glide.with(DetailActivity.this).load(photo_url).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("bindingURLImage", "GLIDE ERR", e);
//            imageView.setBackgroundResource(R.drawable.ico_widget_refresh_01_p);
        }
    }

    /**
     * 서버로 부터 데이터가 하나일때만 사용
     * 유닉스 타임을 보고 일주전인지, 하루전 데이터인지 확인.
     *
     * @param unixTime
     * @return
     */
    private int dateCalc(String unixTime) {
        return 0;
    }

    /**
     * 과거 일주전, 하루전 데이터를 가져오는 인터페이스
     * index 0 : 하루전
     * index 1 : 일주전
     *
     * @param cctv_id 해당 CCTV 아이디
     * @param time    시간 값 ex) 1430
     */
    private void apiPastImage(String cctv_id, String time) {
        if (mApiMgr != null) {
            mApiMgr.apiPastCCTVImage(DetailActivity.this, cctv_id, time, new Response.Listener<PastCCTV>() {
                @Override
                public void onResponse(PastCCTV response) {

                    LOG.d("apiPastImage retCode.result : " + response.result);
                    LOG.d("apiPastImage retCode.errormsg : " + response.errormsg);
                    if (response.result == 0) {
                        //success
                        pastImgaeList = response.List;

                        if (pastImgaeList.size() == 2) {
                            try {
                                Log.i("apiPastImage", "Response List Size : 2");
                                Log.i("apiPastImage", "Response 하루전 : " + pastImgaeList.get(1).img_Url);
                                Log.i("apiPastImage", "Response 일주전 : " + pastImgaeList.get(0).img_Url);
                                //일주전, 하루전 데이터 둘다 존재
                                setDetailScreenshotVisible(true);
                                bindingURLImage(ivDetailScreenShot1, pastImgaeList.get(1).img_Url);
                                bindingURLImage(ivDetailScreenShot2, pastImgaeList.get(0).img_Url);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("apiPastImage", "response img_Url Fail");
                            }
                        } else if (pastImgaeList.size() == 1) {
                            Log.i("apiPastImage", "Response List Size : 1");
                            //둘중 하나만 들어있음
                        }
                    } else {
                        //fail

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
    }

    /**
     * @param city_id
     * @param start
     * @param end
     */
    private void apiWeatherPast(String city_id, String start, String end, final ImageView imageView) {
        if (mApiMgr != null) {
            mApiMgr.apiWeatherPast(DetailActivity.this, city_id, start, end, new Response.Listener<WeatherPast>() {
                @Override
                public void onResponse(WeatherPast response) {
                    if (response.list.size() != 0) {
                        int weatherId = Integer.valueOf(response.list.get(0).weather.get(0).id);
                        LOG.d("날씨 코드 : " + weatherId);
                        setWeatherImage(imageView, weatherId);
                    } else {
//                        Toast.makeText(DetailActivity.this, getString(R.string.fail_load_weather_info), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(DetailActivity.this, getString(R.string.fail_load_weather_info), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void playVideoCCTV(String video_url) {
        Uri uri = Uri.parse(video_url);
        vvDetailCCTVVideo.setVideoURI(uri);
        vvDetailCCTVVideo.requestFocus();
        vvDetailCCTVVideo.start();

        vvDetailCCTVVideo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub

                //finish();
                return false;
            }
        });

        vvDetailCCTVVideo
                .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(final MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        LOG.d("onPrepared");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                             /*   if (mProDlg.isShowing())
                                    mProDlg.dismiss();*/
                            }
                        });
                        // m_text_cctv_time.postDelayed(getMovieTime, 1000);
                    }
                });


        vvDetailCCTVVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*if (mProDlg.isShowing())
                            mProDlg.dismiss();*/
                    }
                });

//                PopupDialog_MSG("정보 제공처 사정으로 CCTV 영상이 제공되지 않는 구간입니다.");

                return true;
            }
        });

        vvDetailCCTVVideo
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        // 재생 완료
                        LOG.d("onCompletion");

                        // 무한 루프
                        vvDetailCCTVVideo.start();

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_detail_close:
                onBackPressed();
                break;
            case R.id.ll_detail_add_favorite:
                addDeleteFavorite();
                break;
            case R.id.ll_detail_time_set:
                final CustomTimePickerDialog customTimePickerDialog = new CustomTimePickerDialog(DetailActivity.this, TimePickerDialog.THEME_HOLO_LIGHT, timeSetListener, mHour, mMinute, false);
//                final CustomTimePickerDialog customTimePickerDialog = new CustomTimePickerDialog(DetailActivity.this, timeSetListener, mHour, mMinute, false);
                customTimePickerDialog.setTitle(getString(R.string.set_time));
                customTimePickerDialog.show();
                break;
            case R.id.iv_detail_screenshot1:
                Log.i("jony", "pastImgaeList.get(1).img_Url : " + pastImgaeList.get(1).img_Url);
                showConfirmPopup("어제 " + String.format("%02d", mHour) + "시 " + String.format("%02d", mMinute) + "분", pastImgaeList.get(1).img_Url, ivDetailWeatherYesterday, 0);
                break;
            case R.id.iv_detail_screenshot2:
                showConfirmPopup("지난주 " + String.format("%02d", mHour) + "시 " + String.format("%02d", mMinute) + "분", pastImgaeList.get(0).img_Url, ivDetailWeatherLastWeek, 0);
                break;
        }
    }

    private CustomTimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // TODO Auto-generated method stub
            mHour = hourOfDay;
            mMinute = minute;

            calendar.set(Calendar.HOUR_OF_DAY, mHour);
            calendar.set(Calendar.MINUTE, mMinute);

            Toast.makeText(DetailActivity.this, "어제, 지난주 " + mHour + "시 " + mMinute + "분의 " + "CCTV 이미지를 가져옵니다", Toast.LENGTH_SHORT).show();

            apiPastImage(lpad(mSelectedCCTVItem.cctv_id, 4, "0"), getHHMM());
            long nowMilis = calendar.getTimeInMillis() / 1000L;
            long yesterDayMillis = nowMilis - 86400;
            long lastWeekMillis = nowMilis - 604800;

            apiWeatherPast(String.valueOf(weatherLocationCode), String.valueOf(yesterDayMillis), String.valueOf(yesterDayMillis+7200), ivDetailWeatherYesterday);
            apiWeatherPast(String.valueOf(weatherLocationCode), String.valueOf(lastWeekMillis), String.valueOf(lastWeekMillis + 7200), ivDetailWeatherLastWeek);

            tvDetailImgYesterdayTimeTxt.setText(String.format("%02d", mHour) + "시" + String.format("%02d", mMinute) + "분");
            tvDetailImgLastweekdayTimeTxt.setText(String.format("%02d", mHour) + "시" + String.format("%02d", mMinute) + "분");
//            apiWeatherPast
        }
    };

    /**
     * 즐겨찾기를 추가 삭제한다.
     * 즐겨찾기 리스트 업데이트
     */
    private void addDeleteFavorite() {
        Gson gson = new Gson();
        int seekIndex = -1;
        boolean isDuplicate = false;
        //즐겨찾기 초기화
        String favoriteGson = mPrefMgr.getFavoriteCCTV();
        FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
        if (favoriteCCTVData == null) {
            favoriteCCTVData = new FavoriteCCTVData();
        }
        //즐겨찾기 초기화 끝
        //선택된 CCTV가 즐찾에 있는 지 확인
        for (MapCCTV_Item item : favoriteCCTVData.getFavoriteCCTVItems()) {
            seekIndex++;
            if (item.cctv_id.equals(mSelectedCCTVItem.cctv_id)) {
                //삭제 일경우 삭제
                isDuplicate = true;
                break;
            }
        }
        //중복여부에따라 추가/삭제 결정
        if (isDuplicate == true) {
            showConfirmPopupTwo(getResources().getString(R.string.favorite), mSelectedCCTVItem.location + getResources().getString(R.string.remove_favorite), isDuplicate, seekIndex);
        } else {
            showConfirmPopupTwo(getResources().getString(R.string.favorite), mSelectedCCTVItem.location + getResources().getString(R.string.add_favorite), isDuplicate, seekIndex);
        }
    }

    /**
     * @param strTitle
     * @param type
     */
    private void showConfirmPopup(String strTitle, String img_url, ImageView weatherImg, final int type) {
        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup_for_img);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        title.setText(strTitle);

        ImageView contents = (ImageView) confirmPopUp.findViewById(R.id.iv_common_contents_img);
        contents.setBackgroundResource(R.drawable.frm_tmon_noimage);
        try {
//            Glide.with(DetailActivity.this).load(img_url).into(contents).onLoadFailed(new RuntimeException(), getResources().getDrawable(R.drawable.frm_tmon_noimage));
            Glide.with(DetailActivity.this).load(img_url).into(contents);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(DetailActivity.this, "이미지를 가져오기 실패", Toast.LENGTH_SHORT).show();
        }
        ImageView weather = (ImageView) confirmPopUp.findViewById(R.id.iv_common_weather_img);
        weather.setBackground(weatherImg.getBackground());

        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 9999) {
                    finish();
                    System.exit(0);
                } else {
                    confirmPopUp.dismiss();
                }
            }
        });
        confirmPopUp.setCancelable(false);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    private void showConfirmPopupTwo(String strTitle, String strContents, final boolean isDuplicate, final int seekIndex) {
        final Dialog confirmPopUp = new Dialog(this);
        confirmPopUp.setCanceledOnTouchOutside(false);
        final Gson gson = new Gson();
        final String favoriteGson = mPrefMgr.getFavoriteCCTV();

        confirmPopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPopUp.setContentView(R.layout.layout_common_popup_two);

        TextView title = (TextView) confirmPopUp.findViewById(R.id.tvCommonTitle);
        TextView contents = (TextView) confirmPopUp.findViewById(R.id.tvCommonContents);
        title.setText(strTitle);
        contents.setText(strContents);

        Button btnConfirm = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_left);
        Button btnCancle = (Button) confirmPopUp.findViewById(R.id.common_popup_btn_right);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDuplicate == true) {
                    FavoriteCCTVData favoriteCCTVData1 = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
                    if (favoriteCCTVData1 == null) {
                        favoriteCCTVData1 = new FavoriteCCTVData();
                    }
                    favoriteCCTVData1.getFavoriteCCTVItems().remove(seekIndex);
                    String jsonStrrr = gson.toJson(favoriteCCTVData1);
                    mPrefMgr.setFavoriteCCTV(jsonStrrr);
                    Toast.makeText(DetailActivity.this, mSelectedCCTVItem.location + getResources().getString(R.string.remove_favorite_done), Toast.LENGTH_SHORT).show();
                    ivDetailFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite);
                    sendWidgetRefresh();
                } else {
                    FavoriteCCTVData favoriteCCTVData = gson.fromJson(favoriteGson, FavoriteCCTVData.class);
                    if (favoriteCCTVData == null) {
                        favoriteCCTVData = new FavoriteCCTVData();
                    }
                    if (favoriteCCTVData.getFavoriteCCTVItems().size() < LIMIT_FAVORITE_COUNT) {
                        favoriteCCTVData.getFavoriteCCTVItems().add(mSelectedCCTVItem);
                        String jsonStr = gson.toJson(favoriteCCTVData);
                        System.out.println(jsonStr);
                        mPrefMgr.setFavoriteCCTV(jsonStr);
                        Toast.makeText(DetailActivity.this, mSelectedCCTVItem.location + getResources().getString(R.string.add_favorite_done), Toast.LENGTH_SHORT).show();
                        ivDetailFavoriteImg.setBackgroundResource(R.drawable.ico_map_cctv_video_favorite_selected);
                    } else {
                        Toast.makeText(DetailActivity.this, getResources().getString(R.string.max_favorite), Toast.LENGTH_SHORT).show();
                        String jsonStr = gson.toJson(favoriteCCTVData);
                        System.out.println(jsonStr);
                    }
                }
                confirmPopUp.dismiss();
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPopUp.dismiss();
            }
        });
        confirmPopUp.setCancelable(true);
        confirmPopUp.show();
        confirmPopUp.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    private void apiWeatherNow(final ImageView imgView, String strX, String strY) {
        if (mApiMgr != null) {
            mApiMgr.apiWeatherNow(this, strX, strY, new Response.Listener<WeatherNow>() {
                @Override
                public void onResponse(WeatherNow retCode) {
                    LOG.d("apiWeatherNow retCode " + retCode.weather.get(0).id);
                    int weatherCode = Integer.valueOf(retCode.weather.get(0).id);
                    weatherLocationCode = Integer.valueOf(retCode.id);
                    setWeatherImage(imgView, weatherCode);
                    long nowMilis = calendar.getTimeInMillis() / 1000L;
                    long yesterDayMillis = nowMilis - 86400;
                    long lastWeekMillis = nowMilis - 604800;
//                    apiWeatherPast(String.valueOf(weatherLocationCode), String.valueOf(yesterDayMillis), String.valueOf(nowMilis), ivDetailWeatherYesterday);
//                    apiWeatherPast(String.valueOf(weatherLocationCode), String.valueOf(lastWeekMillis), String.valueOf(nowMilis), ivDetailWeatherLastWeek);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    LOG.d("apiTconCCTV VolleyError " + volleyError.getMessage());
                    Toast.makeText(DetailActivity.this, "네트워크 오류", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setWeatherImage(ImageView imgView, int weatherCode) {
        if (weatherCode / 100 == 8) {
            if (weatherCode == WeatherCode.sunny) {
                imgView.setBackgroundResource(R.drawable.icon_weather_sun);
            } else if (weatherCode == WeatherCode.fewClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_day_cloud);
            } else if (weatherCode == WeatherCode.scatteredClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            } else if (weatherCode == WeatherCode.brokenClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            } else if (weatherCode == WeatherCode.overcastClouds) {
                imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
            }
        } else if ((weatherCode / 100) * 100 == WeatherCode.mist) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        } else if ((weatherCode / 100) * 100 == WeatherCode.snow) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_snow);
        } else if ((weatherCode / 100) * 100 == WeatherCode.rain) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_rain);
        } else if ((weatherCode / 100) * 100 == WeatherCode.sotrm) {
            imgView.setBackgroundResource(R.drawable.icon_weather_windy_thunderstorm);
        } else if ((weatherCode / 100) * 100 == WeatherCode.mist) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        } else if ((weatherCode / 100) * 100 == WeatherCode.dizzle) {
            imgView.setBackgroundResource(R.drawable.icon_weather_cloud_fog);
        }
    }

    private void sendWidgetRefresh() {
        Intent intent = new Intent(mContext, CustomWidget.class);
        intent.setAction(ACTION_REFRESH_LURE);
        intent.putExtra("viewId", R.id.ll_time_over);
        sendBroadcast(intent);
    }
}
