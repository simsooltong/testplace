package com.appteam.thinkware.todaycommuter.customui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.appteam.thinkware.todaycommuter.R;

public class CustomProgress extends Dialog {
    private Context context;
    private CuzTextView mTitleView;

    private String mTitle = "";
    private boolean isCancelable = true;
    private boolean isFinishActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.5f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.dialog_custom_progress);

        setLayout();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        if (!isCancelable) return;
        else {
            if (isFinishActivity) {
                this.dismiss();
                ((Activity) context).finish();
            } else super.onBackPressed();
        }
    }

    public CustomProgress(Context context) {
        // Dialog 배경을 투명 처리 해준다.
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context = context;
        this.mTitle = context.getResources().getString(R.string.dailgo_progress_contents);
    }

    public CustomProgress(Context context, String title) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context = context;
        this.mTitle = title;
    }

    public void setCancelable(boolean isCancelable) {
        this.isCancelable = isCancelable;
    }

    public void setCancelListener(OnCancelListener listener) {
        this.setOnCancelListener(listener);
    }

    /**
     * 취소 시 activity 종료를 위한 설정
     *
     * @param isFinishActivity
     */
    public void setCancelableForActFinish(boolean isFinishActivity) {
        this.isFinishActivity = isFinishActivity;
    }

    /**
     * @param isCancelable
     * @param isFinishActivity
     */
    public void setCancelableForActFinish(boolean isCancelable, boolean isFinishActivity) {
        this.isCancelable = isCancelable;
        this.isFinishActivity = isFinishActivity;
    }

    /*
     * Layout
     */
    private void setLayout() {

        mTitleView = (CuzTextView) findViewById(R.id.tvContents);
        mTitleView.setText(mTitle);
        if (mTitleView != null && mTitle.length() < 1) {
            mTitleView.setVisibility(View.GONE);
        }
    }

}
