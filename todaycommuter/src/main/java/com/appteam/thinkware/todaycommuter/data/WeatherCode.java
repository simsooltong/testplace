package com.appteam.thinkware.todaycommuter.data;

/**
 * Created by Administrator on 2015-02-22.
 */
public class WeatherCode {
    public static int sunny = 800; //맑음
    public static int fewClouds = 801; //구름조금
    public static int scatteredClouds = 802;//구름
    public static int brokenClouds = 803;//먹구름
    public static int overcastClouds = 804;//먹구름

    public static int mist = 701;//안개

    public static int snow = 601;//눈

    public static int rain = 500;//비

    public static int sotrm = 200;//폭풍

    public static int dizzle = 300;//구름비
}
