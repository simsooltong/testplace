package com.appteam.thinkware.todaycommuter.mgr;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.tws.common.lib.gms.LocationDefines;
import com.tws.common.lib.gms.LocationGmsClient;

import java.util.Calendar;
import java.util.Locale;

import CoordLib.NorXY_T;

/**
 * Created by dskim on 2015-02-09.
 */
public class MyBroadCastReceiver extends BroadcastReceiver {
    private PrefMgr mPrefMgr = null;
    private int HOME_ALARM_TIME = 9000;
    private int WORKPALCE_ALARM_TIME = 9001;

    public Handler locationResultHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LocationDefines.GMS_CONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_CONNECT_SUCC");
                    break;
                case LocationDefines.GMS_CONNECT_FAIL:
                    Log.i("LocationResultHandler", "GMS_CONNECT_FAIL");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, Toast.LENGTH_SHORT);
                    break;
                case LocationDefines.GMS_DISCONNECT_SUCC:
                    Log.i("LocationResultHandler", "GMS_DISCONNECT_SUCC");
                    break;
                case LocationDefines.GMS_LOCATION_NEED_SETTING:
                    Log.i("LocationResultHandler", "GMS_LOCATION_NEED_SETTING");
                    break;
                case LocationDefines.GMS_LOCATION_SUCC:
                    Log.i("MyBroadCastReceiver LocationResultHandler", "GMS_LOCATION_SUCC");
                    if (msg.obj != null) {
                        Location location = (Location) msg.obj;
                        Log.i("LocationResultHandler", "MyBroadCastReceiver location getAccuracy : "
                                + location.getAccuracy());
                        Log.i("LocationResultHandler", "MyBroadCastReceiver location getLongitude : "
                                + location.getLongitude());
                        Log.i("LocationResultHandler", "MyBroadCastReceiver location getLatitude : "
                                + location.getLatitude());
                        // }
                        String[] xy = convertXY(location);
                        Calendar calendar = Calendar.getInstance(Locale.KOREA);
                        int time = calendar.get(Calendar.HOUR_OF_DAY);
                        if (time < 9) {
                            if (mPrefMgr.getPREF_HOME_LON_X().equals(xy[0]) == false || mPrefMgr.getPREF_HOME_LAT_Y().equals(xy[1]) == false) {
                                if (mPrefMgr.getPREF_WORK_LON_X().equals(xy[0]) == false || mPrefMgr.getPREF_WORK_LAT_Y().equals(xy[1])) {
                                    mPrefMgr.setPREF_HOME_LON_X(xy[0]);
                                    mPrefMgr.setPREF_HOME_LAT_Y(xy[1]);
                                    mPrefMgr.setPREF_HOME_WORKPLACE_CHANGE(true);
                                }
                            }
                        } else {
                            if (mPrefMgr.getPREF_WORK_LON_X().equals(xy[0]) == false || mPrefMgr.getPREF_WORK_LAT_Y().equals(xy[1]) == false) {
                                if (mPrefMgr.getPREF_HOME_LON_X().equals(xy[0]) == false || mPrefMgr.getPREF_HOME_LAT_Y().equals(xy[1]) == false) {
                                    mPrefMgr.setPREF_WORK_LON_X(xy[0]);
                                    mPrefMgr.setPREF_WORK_LAT_Y(xy[1]);
                                    mPrefMgr.setPREF_HOME_WORKPLACE_CHANGE(true);
                                }
                            }
                        }
                    } else {
                        Log.i("LocationResultHandler", "MSG NULL");
                    }

                    break;

                case LocationDefines.GMS_LOCATION_FAIL:
                    Log.i("LocationResultHandler", "GMS_LOCATION_FAIL");
                    break;

                case LocationDefines.GMS_LOCATION_TIMEOUT:// 측위 요청했지만 Timeout
                    Log.i("LocationResultHandler", "GMS_LOCATION_TIMEOUT");
                    break;
            }
        }
    };


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("onReceive", "MyBroadCastReceiver 컁컁" + intent.getStringExtra("time"));
        String action = intent.getAction();
        mPrefMgr = new PrefMgr(context);
        if (action.equals("HOME_ALARM_TIME") || action.equals("WORKPALCE_ALARM_TIME")) {
            requestLocation(context);
        }
        if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
            setHomeAlarm(context);
            setWorkPlaceAlarm(context);
        }
    }

    private void requestLocation(Context context) {
        // GMS 측위 요청.
        // makeWaitDialog();
        LocationGmsClient location = new LocationGmsClient(context, locationResultHandler);
        location.connect();
    }

    private String[] convertXY(Location location) {
        CoordLib.CoordClient cc = new CoordLib.CoordClient();
        NorXY_T data = cc.getConvertXY(String.valueOf(location.getLongitude()),
                String.valueOf(location.getLatitude()));
        String[] xyStr = new String[2];
        xyStr[0] = String.valueOf(data.x);
        xyStr[1] = String.valueOf(data.y);
        return xyStr;
    }

    private void setHomeAlarm(Context context) {
        Log.i("setHomeAlarm", "alarm set start");
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MyBroadCastReceiver.class);
        intent.putExtra("time", "HOME");
        intent.setAction("HOME_ALARM_TIME");
        Calendar calendar = Calendar.getInstance(Locale.KOREAN);
        calendar.set(calendar.HOUR_OF_DAY, 05);
        calendar.set(calendar.MINUTE, 00);
        PendingIntent pi = PendingIntent.getBroadcast(context, HOME_ALARM_TIME, intent, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 60 * 24, pi);
    }

    private void setWorkPlaceAlarm(Context context) {
        Log.i("setWorkPlaceAlarm", "alarm set start");
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MyBroadCastReceiver.class);
        intent.putExtra("time", "WORKPLACE");
        intent.setAction("WORKPALCE_ALARM_TIME");
        Calendar calendar = Calendar.getInstance(Locale.KOREAN);
        calendar.set(calendar.HOUR_OF_DAY, 15);
        calendar.set(calendar.MINUTE, 00);
        PendingIntent pi = PendingIntent.getBroadcast(context, WORKPALCE_ALARM_TIME, intent, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 60 * 24, pi);
    }
}
