package com.appteam.thinkware.todaycommuter.mapview;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.appteam.thinkware.todaycommuter.mapview.Defines.MAPConst;

public class JavascriptInterface {

	Handler mHandler;

	public JavascriptInterface(Handler myhandler) {
		// TODO Auto-generated constructor stub
        Log.d("JavascriptInterface", "JavascriptInterface Init");
		mHandler = myhandler;
	}

	// init info
	@android.webkit.JavascriptInterface
	public void initCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_INIT;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// ismap info
	@android.webkit.JavascriptInterface
	public void isMapCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_ISMAP;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// moveend info
	@android.webkit.JavascriptInterface
	public void setMoveendCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_MOVEEND;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// moveend info
	@android.webkit.JavascriptInterface
	public void setZoomendCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETZOOMEND;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// map center info
	@android.webkit.JavascriptInterface
	public void getCenterCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GETCENTER;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}
	
	// map center info
	@android.webkit.JavascriptInterface
	public void getExtentCB(String coords) {
		
		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GET_EXTENT;
		
		msg.obj = coords;
		mHandler.sendMessage(msg);
		
	}

	// map level info
	@android.webkit.JavascriptInterface
	public void getLevelCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GETLEVEL;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// create maker info
	@android.webkit.JavascriptInterface
	public void createFeatureMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_CREATEFEATUREMAKER;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// maker touch
	@android.webkit.JavascriptInterface
	public void setTouchendFeatureMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_TOUCHENDFEATUREMARKER;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// marker touch
	@android.webkit.JavascriptInterface
	public void touchendMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_TOUCHENDMARKER;

		msg.obj = coords;
		mHandler.sendMessage(msg);

	}

	// custom call back
	@android.webkit.JavascriptInterface
	public void getCB(int nType, String strCBData) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GET;

		msg.obj = nType + "|" + strCBData;
		mHandler.sendMessage(msg);
	}

	// center marker
	@android.webkit.JavascriptInterface
	public void createCenterFeatureMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_CENTERMAKER;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// createAndAddMarker
	@android.webkit.JavascriptInterface
	public void createMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_CREATEMARKER;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	@android.webkit.JavascriptInterface
	public void createAndAddOffsetDoubleMarkerCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_CREATEANDADDOFFSETDOUBLE;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// lonlat pixel info
	@android.webkit.JavascriptInterface
	public void getPixelFromLonLatCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GETPIXELFROMLONLAT;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// pixel lonlat info
	@android.webkit.JavascriptInterface
	public void getLonLatFromPixelCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_GETLONLATFROMPIXEL;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// ??��?? ??�벤???
	@android.webkit.JavascriptInterface
	public void setTouchEventCB(String strCBData) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETTOUCHEVENTCB;

		msg.obj = strCBData;
		mHandler.sendMessage(msg);
	}

	// ??��?? ??�벤???
	@android.webkit.JavascriptInterface
	public void setOnlyMoveendCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETONLYMOVEEND;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// ??��?? ??�벤???
	@android.webkit.JavascriptInterface
	public void setZoominCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETZOOMIN;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// ??��?? ??�벤???
	@android.webkit.JavascriptInterface
	public void setZoomoutCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETZOOMOUT;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	@android.webkit.JavascriptInterface
	public void createDivPopupCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_CREATEDIVPOPUP;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	@android.webkit.JavascriptInterface
	public void addCircleCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_ADDCIRCLE;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

	// Panto ??��?????�???? ??�벤???
	@android.webkit.JavascriptInterface
	public void setPantoendCB(String coords) {

		Message msg = Message.obtain();
		msg.what = MAPConst.CB_PANTOEND;

		msg.obj = coords;
		mHandler.sendMessage(msg);
	}
	
	// Panto ??��?????�???? ??�벤???
	@android.webkit.JavascriptInterface
	public void setTouchendCB(String coords) {
		
		Message msg = Message.obtain();
		msg.what = MAPConst.CB_SETTOUCHEND;
		
		msg.obj = coords;
		mHandler.sendMessage(msg);
	}

}
