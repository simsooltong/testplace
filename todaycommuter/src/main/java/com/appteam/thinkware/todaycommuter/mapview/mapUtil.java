package com.appteam.thinkware.todaycommuter.mapview;


public class mapUtil {

	public static String getMapScale(int level) {

		String scale = "";

		switch (level) {
		case 1:
			scale = "80km";
			break;
		case 2:
			scale = "45km";
			break;
		case 3:
			scale = "25km";
			break;
		case 4:
			scale = "10km";
			break;
		case 5:
			scale = "5km";
			break;
		case 6:
			scale = "2.5km";
			break;
		case 7:
			scale = "1.2km";
			break;
		case 8:
			scale = "600m";
			break;
		case 9:
			scale = "280m";
			break;
		case 10:
			scale = "140m";
			break;
		case 11:
			scale = "80m";
			break;
		case 12:
			scale = "40m";
			break;

		case 13:
			scale = "20m";
			break;
		default:
			break;
		}

		return scale;
	}

}
