package com.appteam.thinkware.todaycommuter.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;

import com.appteam.thinkware.todaycommuter.base.BaseActivity;
import com.appteam.thinkware.todaycommuter.mgr.PrefMgr;


public class TutorialActivity extends BaseActivity implements OnClickListener {

    private RadioButton[] mRbRowArray;
    private Button btOilSelector = null;
    private Button btSkip;
    String[] oilArray = null;
    PrefMgr prefMgr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setControl() {
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    private void showConfirmPopup(String strTitle, String strContents) {

    }

    // oiiType popup S
    private void showOilTypePopup(int initChecked) {

    }

    private void selectRadioBtn(View v, RadioButton[] arrayRdBtn) {

    }
    // oilType popup E
}
