package com.appteam.thinkware.todaycommuter.mapview;

import android.webkit.WebView;

/**
 * @author jonychoi
 * 
 */
public class twsMapController {

	private WebView webview = null;

	/**
	 * ?????��??
	 */
	public twsMapController(WebView webview) {
		// TODO Auto-generated constructor stub
		this.webview = webview;

	}

	/**
	 * �???? 컨�?�롤 �?�?
	 */

	/**
	 * �?기�?? 2013. 4. 24. TwsMapInit void
	 * 
	 * @param lLocX
	 * @param lLocY
	 * @param nlevel
	 */
	public void TwsMapInit(int nX, int nY, int nlevel) {
		String strJavaScript = String.format(
				"javascript:TMWA.initMap(\"%s\", %d, %d, %d,'2','m')",
				"div_map", nX, nY, nlevel);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * set level
	 * 
	 * @param nlevel
	 */
	public void TwsMapSetLevel(int nlevel) {
		String strJavaScript = String.format(
				"javascript:THINKMAP.setLevel( %d)", nlevel);

		webview.loadUrl(strJavaScript);
	}
	
	public void TwsMapGetLevel() {
		String strJavaScript = String.format(
				"javascript:TMWA.getLevel()");

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapIsMap() {
		String strJavaScript = String.format("javascript:TMWA.isMap()");

		webview.loadUrl(strJavaScript);
	}

	/**
	 * ??��?? �???? ??????
	 */
	public void TwsMapSimpleMap() {
		String strJavaScript = String.format("javascript:THINKMAP.imageMap()");

		webview.loadUrl(strJavaScript);

		// 주기 �??????��??
		strJavaScript = String
				.format("javascript:THINKMAP.setAerialHybrid(false)");

		webview.loadUrl(strJavaScript);
	}

	/**
	 * ???�? �???? ??????
	 */
	public void TwsMapAerialMap() {
		String strJavaScript = String.format("javascript:THINKMAP.aerialMap()");

		webview.loadUrl(strJavaScript);

		// 주기 ?????��??
		strJavaScript = String
				.format("javascript:THINKMAP.setAerialHybrid(true)");

		webview.loadUrl(strJavaScript);

	}

	/**
	 * ?????? 2013. 4. 24. TwsMapZoomIn void
	 */
	public void TwsMapZoomIn() {
		webview.loadUrl("javascript:THINKMAP.fixedZoomIn();");
	}
	
	public void TWsMapGetExtent(){
		webview.loadUrl("javascript:TMWA.getExtent();");
	}
	
	/**
	 * �???? 2013. 4. 24. TwsMapZoomOut void
	 */
	public void TwsMapZoomOut() {
		// webview.loadUrl("javascript:THINKMAP.fixedZoomOut();");
		webview.loadUrl("javascript:THINKMAP.setLevel(THINKMAP.getLevel()-1);");
	}

	/**
	 * �???? moveend event 2013. 4. 24. TwsMapSetMoveEnd void
	 */
	public void TwsMapSetMoveEnd(boolean bType) {

		if (bType)
			webview.loadUrl("javascript:TMWA.setMoveend();");
		else
			webview.loadUrl("javascript:TMWA.removeMoveend();");
	}

	/**
	 * ???�? �???? �???? �???? ??�기 2013. 4. 24. TwsMapGetCenter void
	 */
	public void TwsMapGetCenter() {
		webview.loadUrl("javascript:TMWA.getCenter();");
	}

	/**
	 * �???��????? 보�?�주�? on/off 2013. 4. 24. TwsMapSetTrafficRoad void
	 * 
	 * @param bType
	 */
	public void TwsMapSetTrafficRoad(boolean bType) {

		if (bType)
			webview.loadUrl("javascript:THINKMAP.changeTrafficRoad('',true);");
		else
			webview.loadUrl("javascript:THINKMAP.changeTrafficRoad('',false);");
	}

	/**
	 * ????????? 거리 �???? 그리�?
	 * 
	 * @param strPoint
	 */
	public void TwsMapAddPolyLine(int nX, int nY, int nXX,
			int nYY, String color) {
		
		String strPoint = nX + "," + nY + "|" + nXX + "," + nYY;
		String strJavaScript = String
				.format("javascript:TMWA.addPolyLine(\"%s\", \"%s\", 0.7, 3, 'solid','');",
						strPoint, color);
		
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �??????? ??? 그리�? 2013. 4. 24. TwsMapAddCircle void
	 * 
	 * @param nX
	 * @param nY
	 * @param nRadius
	 */
	public void TwsMapAddCircle(int nX, int nY, int nRadius) {

		// 존�????? ??? �????.
		String strZoneColor = "#2188d7";
		
		String strZoneStrokeColor = "#0d92fd";

		String point = nX + "," + nY;

		// �???? / �?�? / ??�곽??? ??? / ??��????? / ???�? / ??��????? / �???�기??? / �???�기 ??��????? / ?????��?��??
		String strJavaScript = String
				.format("javascript:TMWA.addCircle(\"%s\", %d, \"%s\", 0.3, 1.34, 'solid', \"%s\", 0.15, 100);",
						point, nRadius, strZoneStrokeColor, strZoneColor);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * FeatureMarker ?????? 2013. 4. 25. TwsMapAddCCTVMarker void
	 * 
	 * @param strCx
	 * @param strCy
	 * @param nMarker_index
	 */
	public void TwsMapAddCCTVMarker(int nX, int nY, String strImgPath,
			String strParam) {

		int image_width = (int) (48 * 0.67);
		int image_height = (int) (57 * 0.67);

		int x_offset = 0;
		int y_offset = 0;

		/*
		 * Parameters: lon 경�??(x�?) lat ?????? imageUrl ?????��????��?��????? Url image_opacity �?�?
		 * ??��?��?? ??��????? (�???? : 0 ~ 1 ) image_width ?????��????? ?????? image_height ?????��????? ??????
		 * x_offset x보�??�? y_offset y보�??�? param ??��?��?? ?????��?��??
		 * 
		 * Returns: createFeatureMarkerCB �?백�?��??�? marker_id|param �?커�??�?id�?|??��?��???????��?��??
		 */
		String strJavaScript = String
				.format("javascript:TMWA.createFeatureMarker(%d, %d,\"%s\",'1',%d,%d,%d,%d,\"%s\");",
						nX, nY, strImgPath, image_width, image_height,
						x_offset, y_offset, strParam);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * FeatureMarker ?????? 2013. 4. 25. TwsMapAddCCTVMarker void
	 * 
	 * @param strCx
	 * @param strCy
	 * @param nMarker_index
	 */
	public void TwsMapCreateFeatureMarker(int nX, int nY, String strImgPath,
			int image_width, int image_height, int x_offset, int y_offset,
			String strParam) {

		/*
		 * Parameters: lon 경�??(x�?) lat ?????? imageUrl ?????��????��?��????? Url image_opacity �?�?
		 * ??��?��?? ??��????? (�???? : 0 ~ 1 ) image_width ?????��????? ?????? image_height ?????��????? ??????
		 * x_offset x보�??�? y_offset y보�??�? param ??��?��?? ?????��?��??
		 * 
		 * Returns: createFeatureMarkerCB �?백�?��??�? marker_id|param �?커�??�?id�?|??��?��???????��?��??
		 */
		String strJavaScript = String
				.format("javascript:TMWA.createFeatureMarker(%d, %d,\"%s\",'1',%d,%d,%d,%d,\"%s\");",
						nX, nY, strImgPath, image_width, image_height,
						x_offset, y_offset, strParam);

		webview.loadUrl(strJavaScript);
	}
	public void TwsMapSetTouchEnd(){
		String strJavaScript = String.format("javascript:TMWA.setTouchend()");
		webview.loadUrl(strJavaScript);
	}
	/**
	 * FeatureMarker ??��?? ??�벤??? ??��?? 2013. 4. 25. TwsMapSetTouchendFeatureMarker void
	 * 
	 * @param nMarker_index
	 */
	public void TwsMapSetTouchendFeatureMarker(int nMarker_index) {
		/*
		 * Parameters: marker_id ??��?��??�?id
		 * 
		 * Returns: setTouchendFeatureMarkerCB �?백�?��?? marker_id|param
		 * ??��?��??�?id|??��?��???????��?��??
		 */
		String strJavaScript = String.format(
				"javascript:TMWA.setTouchendFeatureMarkerCB(%d);",
				nMarker_index);
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �?�? ??��?? ??�벤??? ??��??
	 * 
	 * @param nMarker_index
	 */
	public void TwsMapSetTouchendMarkerCB(int nMarker_index) {
		/*
		 * Parameters: marker_id �?�? id
		 * 
		 * Returns: touchendMarkerCB �?백�?��?? marker_id|param ??��?��??�?id|??��?��???????��?��??
		 */
		String strJavaScript = String.format(
				"javascript:TMWA.setTouchendMarkerCB(%d);", nMarker_index);
		webview.loadUrl(strJavaScript);
	}

	public void TwsMapRemoveFeatureMarkerALL() {
		String strJavaScript = String
				.format("javascript:THINKMAP.removeImageFeatures();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * FeatureMarker ?????? 2013. 4. 25. TwsMapRemoveFeatureMarker void
	 * 
	 * @param nMarker_index
	 */
	public void TwsMapRemoveFeatureMarker(int nMarker_index) {
		String strJavaScript = String.format(
				"javascript:THINKMAP.removeFeatureById(%d);", nMarker_index);
		webview.loadUrl(strJavaScript);
	}

	/**
	 * Div ?????? ???�? ??????
	 */
	public void TwsMapRemoveDivPopups() {
		String strJavaScript = String
				.format("javascript:THINKMAP.removeDivPopups();");
		webview.loadUrl(strJavaScript);

		// div �?�? �?기�??
		strJavaScript = String
				.format("javascript:removeAllInfoExtDivMarker();");
		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// CCTV ???보�????? DIV S

	public void TwsMapAddCCTVInfoDIVMarker(int nX, int nY, String strImgPath_n,
			String strImgPath_p, String strTitle, int offX, int offY,
			String arrayIndex) {

		String strContent = "<div>"
				+ "<img  id='CCTVMarker_bg' src='"
				+ strImgPath_n
				+ "' />"
				+ "<img  id='CCTVMarker_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div id='CCTVMarker_title' type='text' style='float:center;font-size:11px'>"
				+ strTitle + "</div>"

				+ "</div>";

		String strJavaScript = String
				.format("javascript:addCCTVInfoMarker(%d, %d, \"%s\", %d, %d, \"%s\");",
						nX, nY, strContent, offX, offY, arrayIndex);

		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// CCTV ???보�????? DIV E

	// ///////////////////////////////////////// POI ???보�????? DIV S

	public void TwsMapAddPOIInfoDIVMarker(int nX, int nY, String strImgPath_n,
			String strImgPath_p, String strTitle, int offX, int offY,
			String arrayIndex, String strType) {

		String strContent = "<div>"
				+ "<img  id='CCTVMarker_bg' src='"
				+ strImgPath_n
				+ "' />"
				+ "<img  id='CCTVMarker_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div id='CCTVMarker_title' type='text' style='float:center;font-size:11px'>"
				+ strTitle + "</div>"

				+ "</div>";

		String strJavaScript = String
				.format("javascript:addPOIInfoMarker(%d, %d, \"%s\", %d, %d, \"%s\", \"%s\");",
						nX, nY, strContent, offX, offY, arrayIndex, strType);

		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// POI ???보�????? DIV E

	// ///////////////////////////////////////// POI GRP ???보�????? DIV S

	public void TwsMapAddPOIGrpDIVMarker(int nX, int nY, String strImgPath,
			String strTitle, int offX, int offY, String arrayIndex,
			String strType) {

		String strCntImgPath = "/android_asset/mapicon/frm_map_point_num_00.png";

		String strContent = "<div>" + "<img  id='GroupMarker_bg' src='"
				+ strImgPath + "' />"
				+ "<div id='GroupMarker_grp' align='center'>";

		if (strTitle.length() < 3) {

			for (int i = 0; i < strTitle.length(); i++) {
				strCntImgPath = GroupCountImge(strTitle.charAt(i), false);

				strContent += "<img id='GroupMarker_title' src='";
				strContent += strCntImgPath + "' />";

			}

			strContent += "</div>";

		} else {
			// 그룹??? 건�??�? 100??��????? 경�??
			for (int i = 0; i < strTitle.length(); i++) {
				strCntImgPath = GroupCountImge(strTitle.charAt(i), true);

				strContent += "<img id='GroupMarker_title_s' src='";
				strContent += strCntImgPath + "' />";
			}
			strContent += "</div>";
		}

		strContent = strContent + "</div>";

		String strJavaScript = String
				.format("javascript:addPOIGRPMarker(%d, %d, \"%s\", %d, %d, \"%s\", \"%s\");",
						nX, nY, strContent, offX, offY, arrayIndex, strType);

		webview.loadUrl(strJavaScript);
	}

	public String GroupCountImge(char StrCount, boolean flag) {
		String strCnt = "";

		if (flag) {
			// 100건�?��????? 경�?? ?????? ??��?��??�?...
			strCnt = "/android_asset/mapicon/frm_map_point_num_0" + StrCount
					+ "_s.png";

			return strCnt;

		} else {
			strCnt = "/android_asset/mapicon/frm_map_point_num_0" + StrCount
					+ ".png";

			return strCnt;
		}

	}

	// ///////////////////////////////////////// POI GRP ???보�????? DIV E

	// ///////////////////////////////////////// Zone ??��?? ???보�????? DIV S

	public void TwsMapAddZoneInfoDetailDIVMarker(int nX, int nY,
			String strImgPath_n, String strImgPath_p, String strTitle,
			int offX, int offY) {

		String strContent = "<div id='ZoneDetailMarker_bg'>"
				+ "<img  id='ZoneDetailMarker_bg' src='"
				+ strImgPath_n
				+ "' />"
				+ "<div id='ZoneMarker_01'>"
				+ "<img class='POIMarker_detail_focus' id='ZoneMarker_01_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div class='POIMarker_detail_title' id='ZoneMarker_01_txt_focus' type='text' style='float:center;font-size:14px;color:#4d4d4d;' >"
				+ strTitle + "</div>" + "</div>" + "</div>"

				// �????
				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addZoneInfoDetailMarker(%d, %d, \"%s\", %d, %d);",
				nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// Zone ??��?? ???보�????? DIV E

	// ///////////////////////////////////////// POI ???�?보기 ???보�????? DIV S

	public void TwsMapAddPOIInfoDetailDIVMarker(int nX, int nY,
			String strImgPath_n, String strImgPath_p, String strTitle,
			int offX, int offY) {

		String strContent = "<div id='POIDetailMarker_bg'>"
				+ "<img  id='POIDetailMarker_bg' src='"
				+ strImgPath_n
				+ "' />"
				+ "<div id='POIMarker_01'>"
				+ "<img class='POIMarker_detail_focus' id='POIMarker_01_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div class='POIMarker_detail_title' id='POIMarker_01_txt_focus' type='text' style='float:center;font-size:14px;color:#4d4d4d;' >"
				+ "경�????????</div>"
				+ "</div>"
				+ "<div id='POIMarker_02'>"
				+ "<img  class='POIMarker_detail_focus' id='POIMarker_02_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div class='POIMarker_detail_title' id='POIMarker_02_txt_focus' type='text' style='float:center;font-size:14px;color:#4d4d4d;' >"
				+ "그룹주�?? ??????</div>"
				+ "</div>"
				+ "<div id='POIMarker_03'>"
				+ "<img  class='POIMarker_detail_focus' id='POIMarker_03_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div class='POIMarker_detail_title' id='POIMarker_03_txt_focus' type='text' style='float:center;font-size:14px;color:#4d4d4d;' >"
				+ "???�? ??��??</div>"
				+ "</div>"

				+ "<div id='POIMarker_04'>"
				+ "<img  class='POIMarker_detail_focus' id='POIMarker_04_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div class='POIMarker_detail_title' id='POIMarker_04_txt_focus' type='text' style='float:center;font-size:14px;color:#4d4d4d;' >"
				+ "???�? 보�?�기</div>" + "</div>"

				+ "</div>"

				// �????
				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addPOIInfoDetailMarker(%d, %d, \"%s\", %d, %d);",
				nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// POI ???�?보기 ???보�????? DIV E

	// ///////////////////////////////////////// POI TAP PLACE ???보�????? DIV S

	public void TwsMapAddPOIInfoTapDIVMarker(int nX, int nY,
			String strImgPath_n, String strImgPath_p, String strTitle,
			int offX, int offY, String arrayIndex) {

		String strContent = "<div>"
				+ "<img  id='CCTVMarker_bg' src='"
				+ strImgPath_n
				+ "' />"
				+ "<img  id='CCTVMarker_focus' src='"
				+ strImgPath_p
				+ "'/>"
				+ "<div id='CCTVMarker_title' type='text' style='float:center;font-size:11px'>"
				+ strTitle + "</div>"

				+ "</div>";

		String strJavaScript = String
				.format("javascript:addPOIInfoTapMarker(%d, %d, \"%s\", %d, %d, \"%s\");",
						nX, nY, strContent, offX, offY, arrayIndex);

		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// POI TAP PLACE ???보�????? DIV E

	// ///////////////////////////////////////// 목�??�? ???보�????? DIV S
	/**
	 * 목�??�? ???�? Div ??????
	 * 
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 * @param strTitle
	 * @param nTextSize
	 * @param offX
	 * @param offY
	 */
	public void TwsMapAddGoalInfoDIVMarker(int nX, int nY, String strImgPath,
			String strTitle, int nTextSize, int offX, int offY) {

		String strContent = "<div>"
				+ "<img  id='DivOnair_Marker_bg' src='"
				+ strImgPath
				+ "' />"
				+ "<div id='DivOnair_Marker_title' type='text' style='float:center;font-size:"
				+ nTextSize + "px' >" + strTitle + "</div>" + "</div>";

		String strJavaScript = String.format(
				"javascript:addGOALInfoMarker(%d, %d, \"%s\", %d, %d);", nX,
				nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * 목�??�? ???�? ?????? Div ??????(경�????????)
	 * 
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 * @param strTitle
	 * @param nTextSize
	 * @param offX
	 * @param offY
	 */

	public void TwsMapAddGoalInfoExtDIVMarker(int nX, int nY,
			String strImgPath, String strTitle, int nTextSize, int offX,
			int offY) {
		String strSubTitle = "경�????????";

		String strContent = "<div>"
				+ "<img  id='DivOnair_Marker_bg_ext' src='"
				+ strImgPath
				+ "' />"
				+ "<div id='DivOnair_Marker_title_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px' >"
				+ strTitle
				+ "</div>"

				// �????
				+ "<div id='DivOnair_Marker_btn_ext_bg' align='center'>"
				+ "<img id='DivOnair_Marker_btn_ext' src='/android_asset/mapicon/frm_postion_bubble_goal_btn_n.png'/>"
				+ "<img id='DivOnair_Marker_btn_ext_focus' src='/android_asset/mapicon/frm_postion_bubble_goal_btn_p.png'/>"
				+ "<div id='DivOnair_Marker_btn_ext_title' type='text' style='float:center;font-size:12px;color:#01b6e7;' >"
				+ strSubTitle + "</div>" + "</div>"

				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addGOALInfoExtMarker(%d, %d, \"%s\", %d, %d);", nX,
				nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * 목�??�? ???�? ?????? Div ??????(경�????????)
	 */
	public void TwsMapRemoveGOALInfoExtMarker() {

		String strJavaScript = String
				.format("javascript:removeGOALInfoExtMarker();");
		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// 목�??�? ???보�????? DIV E

	// ///////////////////////////////////////// 참�?��?? ???보�????? DIV S
	/**
	 * 참�?��?? ???�? Div ??????
	 * 
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 * @param strTitle
	 * @param nTextSize
	 * @param offX
	 * @param offY
	 */
	public void TwsMapAddJoinInfoDIVMarker(String id, int nX, int nY,
			String strImgPath, String strTitle, int nTextSize, int offX,
			int offY) {

		String strContent = "<div id='"
				+ id
				+ "'>"
				+ "<img  id='DivOnair_Marker_name_bg' src='"
				+ strImgPath
				+ "' />"
				+ "<div id='DivOnair_Marker_name_title' type='text' style='float:center;font-size:"
				+ nTextSize + "px' >" + strTitle + "</div>" + "</div>";

		String strJavaScript = String
				.format("javascript:addJoinInfoMarker(\"%s\", %d, %d, \"%s\", %d, %d);",
						id, nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * 참�?��?? ???�? ?????? Div ??????
	 * 
	 * @param nIndex
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 * @param strName
	 * @param strTime
	 * @param strAddr
	 * @param strDistance
	 * @param nTextSize
	 * @param offX
	 * @param offY
	 */

	public void TwsMapAddJoinInfoExtDIVMarker(int nIndex, int nX, int nY,
			String strImgPath, String strName, String strTime, String strAddr,
			String strDistance, int nTextSize, int offX, int offY) {
		String strTimeImgPath = "/android_asset/mapicon/frm_icon_time.png";

		String strContent = "<div>"
				+ "<img  id='DivOnair_Marker_join_bg_ext' src='"
				+ strImgPath
				+ "' />"

				// ??��?? + ???�?
				+ "<div id='DivOnair_Marker_join_sub_bg_ext' align='center'>"

				+ "<div id='DivOnair_Marker_join_name_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px' >"
				+ strName
				+ "<img  id='DivOnair_Marker_join_timeicon_ext' src='"
				+ strTimeImgPath
				+ "' align='center' />"
				+ strTime
				+ "</div>"

				+ "</div>"

				// 주�??
				+ "<div id='DivOnair_Marker_join_addr_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px;color:#01b6e7;' >"
				+ strAddr
				+ "</div>"

				// 거리
				+ "<div id='DivOnair_Marker_join_distance_ext' type='text' style='float:center;font-size:"
				+ nTextSize + "px' >" + strDistance + "</div>"

				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addJoinInfoExtMarker(%d, %d, %d, \"%s\", %d, %d);",
				nIndex, nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * 참�?��?? ???�? ?????? Div ?????? + 보�??�?
	 * 
	 * @param nIndex
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 * @param strName
	 * @param strTime
	 * @param strAddr
	 * @param strDistance
	 * @param nTextSize
	 * @param offX
	 * @param offY
	 */
	public void TwsMapAddJoinInfoExtMsgDIVMarker(int nIndex, int nX, int nY,
			String strImgPath, String strName, String strTime, String strAddr,
			String strDistance, int nTextSize, int offX, int offY) {
		String strTimeImgPath = "/android_asset/mapicon/frm_icon_time.png";
		String strSubTitle = "보�??�?";

		String strContent = "<div>"
				+ "<img  id='DivOnair_Marker_join_msg_bg_ext' src='"
				+ strImgPath
				+ "' />"

				// ??��?? + ???�?
				+ "<div id='DivOnair_Marker_join_sub_bg_ext' align='center'>"

				+ "<div id='DivOnair_Marker_join_name_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px' >"
				+ strName
				+ "<img  id='DivOnair_Marker_join_timeicon_ext' src='"
				+ strTimeImgPath
				+ "' align='center' />"
				+ strTime
				+ "</div>"

				+ "</div>"

				// 주�??
				+ "<div id='DivOnair_Marker_join_addr_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px;color:#01b6e7;' >"
				+ strAddr
				+ "</div>"

				// 거리
				+ "<div id='DivOnair_Marker_join_distance_ext' type='text' style='float:center;font-size:"
				+ nTextSize
				+ "px' >"
				+ strDistance
				+ "</div>"

				// �????
				+ "<div id='DivOnair_Marker_btn_ext_bg' align='center' style='margin-top:6px;'>"
				+ "<img id='DivOnair_Marker_btn_ext' src='/android_asset/mapicon/frm_postion_bubble_goal_btn_n.png'/>"
				+ "<img class='DivOnair_Marker_btn_ext_msg_focus' id='DivOnair_Marker_btn_ext_focus"
				+ "_"
				+ nIndex
				+ "' src='/android_asset/mapicon/frm_postion_bubble_goal_btn_p.png'/>"
				+ "<div id='DivOnair_Marker_btn_ext_title' type='text' style='float:center;font-size:12px;color:#01b6e7;' >"
				+ strSubTitle + "</div>" + "</div>"

				+ "</div>";

		String strJavaScript = String
				.format("javascript:addJoinInfoExtMsgMarker(%d, %d, %d, \"%s\", %d, %d);",
						nIndex, nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * 참�?��?? ???�? ?????? Div 모�????????
	 */
	public void TwsMapRemoveJoinInfoExtMarker(int nIndex) {

		String strJavaScript = String.format(
				"javascript:removeJoinInfoExtMarker(%d);", nIndex);
		webview.loadUrl(strJavaScript);
	}

	// ///////////////////////////////////////// 참�?��?? ???보�????? DIV E

	public void TwsMapAddFriendInfoExtDIVMarker(int nX, int nY,
			String strImgPath, String strName, String strAddr,
			String strDistance, String strTime, int offX, int offY) {

		String strTimeImgPath = "/android_asset/mapicon/frm_icon_time.png";

		String strContent = "<div>"
				+ "<img  id='DivOnair_Marker_join_bg_ext' src='"
				+ strImgPath
				+ "' />"

				// ??��?? + ???�?
				+ "<div id='DivOnair_Marker_join_sub_bg_ext' align='center'>"

				+ "<div id='DivOnair_Marker_join_name_ext' type='text' style='float:center;font-size:"
				+ 10
				+ "px' >"
				+ strName
				+ "<img  id='DivOnair_Marker_join_timeicon_ext' src='"
				+ strTimeImgPath
				+ "' align='center' />"
				+ strTime
				+ "</div>"

				+ "</div>"

				// 주�??
				+ "<div id='DivFriend_Marker_text_ext' type='text' style='float:center;font-size:"
				+ 10
				+ "px;color:#01b6e7;' >"
				+ strAddr
				+ "</div>"

				// 거리
				+ "<div id='DivFriend_Marker_text_ext' type='text' style='float:center;font-size:"
				+ 10
				+ "px' >????????? 거리 <font color='#bd2727'>"
				+ strDistance
				+ "</font></div>"

				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addFriendInfoExtMarker(%d, %d, \"%s\", %d, %d);",
				nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * ??????�???? ??��??
	 * 
	 * @param nX
	 * @param nY
	 * @param nXX
	 * @param nYY
	 */

	public void TwsMapSetMapExtent(int nX, int nY, int nXX, int nYY) {

		String strPoint = nX + "," + nY + "|" + nXX + "," + nYY;
		String strJavaScript = String.format(
				"javascript:THINKMAP.setMapExtentFromPointStr(\"%s\");",
				strPoint);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * Zone ??��?????�? �?�? ??????
	 */

	public void TwsMapRemoveZoneInfoDetailMarker() {

		String strJavaScript = String
				.format("javascript:removeZoneInfoDetailMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * CCTV ?????��??�? �?�? ??????
	 */
	public void TwsMapRemoveCCTVInfoMarker() {

		String strJavaScript = String
				.format("javascript:removeCCTVInfoMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * POI ?????��??�? �?�? ??????
	 */
	public void TwsMapRemovePOIInfoMarker() {

		String strJavaScript = String
				.format("javascript:removePOIInfoMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * POI GRP �?�? ??????
	 */
	public void TwsMapRemovePOIGRPMarker() {

		String strJavaScript = String
				.format("javascript:removePOIGRPMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * POI TAP PLACE ?????��??�? �?�? ??????
	 */
	public void TwsMapRemovePOIInfoTapMarker() {

		String strJavaScript = String
				.format("javascript:removePOIInfoTapMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * POI ???�?보기 �?�? ??????
	 */
	public void TwsMapRemovePOIInfoDetailMarker() {

		String strJavaScript = String
				.format("javascript:removePOIInfoDetailMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * ?????��?? �?�? ??????
	 */
	public void TwsMapRemoveFriendInfoExtMarker() {

		String strJavaScript = String
				.format("javascript:removeFriendInfoExtMarker();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �????�? 기�????��?? ???�? pixel �???��?�기
	 * 
	 * @param nX
	 * @param nY
	 */
	public void TwsMapGetPixelFromLonLat(int nX, int nY) {

		String strJavaScript = String.format(
				"javascript:TMWA.getPixelFromLonLat(%d,%d);", nX, nY);
		webview.loadUrl(strJavaScript);
	}

	/**
	 * center �?�? ?????? 2013. 4. 25. TwsMapAddCenterMarker void
	 * 
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 */
	public void TwsMapAddCenterMarker(int nX, int nY, String strImgPath,
			int nIconW, int nIconH, String strParam) {

		/*
		 * Parameters: lon 경�??(x�?) lat ?????? imageUrl ?????��????��?��????? Url image_opacity �?�?
		 * ??��?��?? ??��????? (�???? : 0 ~ 1 ) image_width ?????��????? ?????? image_height ?????��????? ?????? param
		 * ??��?��?? ?????��?��??
		 * 
		 * Returns: createCenterFeatureMarkerCB �?백�?��??�? marker_id|param
		 * �?커�??�?id�?|??��?��???????��?��??
		 */
		String strJavaScript = String
				.format("javascript:TMWA.createCenterFeatureMarker(%d, %d, \"%s\", 1, %d, %d, \"%s\");",
						nX, nY, strImgPath, nIconW, nIconH, strParam);

		webview.loadUrl(strJavaScript);

	}

	/**
	 * center �?�? ?????? 2013. 4. 25. TwsMapAddCenterMarker void
	 * 
	 * @param nX
	 * @param nY
	 * @param strImgPath
	 */
	public void TwsMapAddCenterMarker(int nX, int nY, String strImgPath) {

		int image_width = 22;
		int image_height = 22;

		/*
		 * Parameters: lon 경�??(x�?) lat ?????? imageUrl ?????��????��?��????? Url image_opacity �?�?
		 * ??��?��?? ??��????? (�???? : 0 ~ 1 ) image_width ?????��????? ?????? image_height ?????��????? ?????? param
		 * ??��?��?? ?????��?��??
		 * 
		 * Returns: createCenterFeatureMarkerCB �?백�?��??�? marker_id|param
		 * �?커�??�?id�?|??��?��???????��?��??
		 */
		String strJavaScript = String
				.format("javascript:TMWA.createCenterFeatureMarker(%d, %d, \"%s\", 1, %d, %d, '2 ');",
						nX, nY, strImgPath, image_width, image_height);

		webview.loadUrl(strJavaScript);

	}

	/**
	 * �?�? ??????
	 * 
	 * @param nX
	 * @param nY
	 * @param nIconW
	 * @param nIconH
	 * @param strImgUrl
	 * @param strParam
	 */
	public void TwsMapCreateAndAddMarker(int nX, int nY, int nIconW,
			int nIconH, String strImgUrl, String strParam) {
		// createMarkerCB�? �?�? �??????? ??????. marker id | param
		String strJavaScript = String
				.format("javascript:TMWA.createAndAddMarker(%d, %d, %d, %d, \"%s\",\"%s\" );",
						nX, nY, nIconW, nIconH, strImgUrl, strParam);

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapCreateAndAddOffsetDoubleMarker(int nX, int nY,
			int nFiconW, int nFiconH, String strFimgUrl, int nSiconW,
			int nSiconH, String strSimgUrl, String strParam) {

		int nFoffSetX = 0;
		int nFoffSetY = 0;
		int nSoffSetX = 0;
		int nSoffSetY = 15;

		// createAndAddOffsetDoubleMarkerCB �?�? �??????? ??????. marker id | param
		String strJavaScript = String
				.format("javascript:TMWA.createAndAddOffsetDoubleMarker(%d, %d, %d, %d, \"%s\", %d, %d ,"
						+ "%d, %d, \"%s\", %d, %d , \"%s\" );", nX, nY,
						nFiconW, nFiconH, strFimgUrl, nFoffSetX, nFoffSetY,
						nSiconW, nSiconH, strSimgUrl, nSoffSetX, nSoffSetY,
						strParam);

		webview.loadUrl(strJavaScript);
	}

	/**
	 * ??��?? �????�? �???? ??��?? 2013. 4. 25. TwsMapMoveTo void
	 * 
	 * @param nX
	 * @param nY
	 */
	public void TwsMapMoveTo(int nX, int nY) {

		String strJavaScript = String.format(
				"javascript:THINKMAP.setPos(%d,%d);", nX, nY);
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? ??��?? (�??????��??)
	 * 
	 * @param nX
	 * @param nY
	 */
	public void TwsMapPanTo(int nX, int nY) {

		String strJavaScript = String.format(
				"javascript:THINKMAP.panTo(%d,%d);", nX, nY);
		webview.loadUrl(strJavaScript);
	}
	
	/**
	 * panto ??�벤??? ??? �?�?.
	 */
	
	public void TwsMapSetPantoendCB(boolean bType) {

		if (bType)
			webview.loadUrl("javascript:TMWA.setPantoend();");
		else
			webview.loadUrl("javascript:TMWA.removePantoend();");
	}

	/**
	 * �? ??��?? ??�벤??? 2013. 6. 5. TwsMapLongTouch void
	 */
	public void TwsMapSetLongTouch() {
		String strJavaScript = String
				.format("javascript:TMWA.setTouchEvent('longpress');");
		webview.loadUrl(strJavaScript);

	}

	/**
	 * �? ??��?? ??�벤??? ???�? 2013. 9. 23. TwsMapRemoveLongTouch void
	 */
	public void TwsMapRemoveLongTouch() {
		String strJavaScript = String
				.format("javascript:TMWA.removeTouchEvent('longpress');");
		webview.loadUrl(strJavaScript);

	}

	/**
	 * ??��?? start ??�벤??? 2013. 9. 23. TwsMapSetStartTouch void
	 */
	public void TwsMapSetStartTouch() {
		String strJavaScript = String
				.format("javascript:TMWA.setTouchEvent('touchstart');");
		webview.loadUrl(strJavaScript);

	}
	
	public void TwsMapSetEndTouch() {
		String strJavaScript = String
				.format("javascript:TMWA.setTouchEvent('touchend');");
		webview.loadUrl(strJavaScript);
		
	}

	/**
	 * ??��?? start ??�벤??? ???�? 2013. 9. 23. TwsMapRemoveStartTouch void
	 */
	public void TwsMapRemoveStartTouch() {
		String strJavaScript = String
				.format("javascript:TMWA.removeTouchEvent('touchstart');");
		webview.loadUrl(strJavaScript);

	}

	/**
	 * �? ??��?? ??�벤??? ???�? ??��?? 2013. 9. 23. TwsMapSetLongPressTime void
	 */
	public void TwsMapSetLongPressTime(int nTime) {
		String strJavaScript = String.format(
				"javascript:THINKMAP.setLongPressTime(%d);", nTime);
		webview.loadUrl(strJavaScript);

	}

	/**
	 * 모�?? �?�? ??????
	 */
	public void TwsMapClearMarkers() {
		String strJavaScript = String
				.format("javascript:THINKMAP.clearMarkers();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? set move end (Only)
	 */
	public void TwsMapSetOnlyMoveend() {
		String strJavaScript = String
				.format("javascript:TMWA.setOnlyMoveend();");
		webview.loadUrl(strJavaScript);
	}
	
	/**
	 * �???? set move end (Only)
	 */
	public void TwsMapSetMoveend() {
		String strJavaScript = String
				.format("javascript:TMWA.setMoveend();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? remove move end (Only)
	 */
	public void TwsMapRemoveOnlyMoveend() {
		String strJavaScript = String
				.format("javascript:TMWA.removeOnlyMoveend();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? set zoom in event
	 */
	public void TwsMapSetZoomIn() {
		String strJavaScript = String.format("javascript:TMWA.setZoomin();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? remove zoom in event
	 */
	public void TwsMapRemoveZoomIn() {
		String strJavaScript = String.format("javascript:TMWA.removeZoomin();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? set zoom out event
	 */
	public void TwsMapSetZoomOut() {
		String strJavaScript = String.format("javascript:TMWA.setZoomout();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? remove zoom out event
	 */
	public void TwsMapRemoveZoomOut() {
		String strJavaScript = String
				.format("javascript:TMWA.removeZoomout();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? set zoom in/out event
	 */
	public void TwsMapSetZoomend() {
		String strJavaScript = String.format("javascript:TMWA.setZoomend();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * �???? remove zoom in/out event
	 */
	public void TwsMapRemoveZoomend() {
		String strJavaScript = String
				.format("javascript:TMWA.removeZoomend();");
		webview.loadUrl(strJavaScript);
	}

	/**
	 * 참�?��?? ???�? ?????? Div 모�????????
	 */
	public void TwsMapRemoveJoinInfoExtMarkerAll() {

		String strJavaScript = String
				.format("javascript:removeJoinInfoExtMarkerAll();");
		webview.loadUrl(strJavaScript);
	}

	public void TwsMapRemoveMarkerById(int nId) {
		String strJavaScript = String.format(
				"javascript:THINKMAP.removeMarkerById(%d)", nId);

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapRemoveDivPopup(String strId) {
		String strJavaScript = String.format(
				"javascript:removeJoinInfoMarker(\"%s\");", strId);

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapAddSafeZoneDIVMarker(String strID, int nX, int nY,
			String strImgPath, int offX, int offY) {

		String strContent = "<div id='" + strID + "'>"
				+ "<img  id='DivZone_Marker_bg' src='" + strImgPath + "' />"
				+ "</div>";

		String strJavaScript = String.format(
				"javascript:addSafeZoneMarker(%d, %d, \"%s\", %d, %d);", nX,
				nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}
	
	public void TwsMapMarkerAni(String strID, boolean start ,String imgPath, String imgPath_s )
	{
		String strJavaScript ="";
		if( start )
		{
		String img_id = strID+"_img";
		
		//String imgPath = "/android_asset/map/icon_map_i.png";
		//String imgPath_s = "/android_asset/map/icon_map_i_1.png";
		
		 strJavaScript = String
				.format("javascript:startMarkerAni( \"%s\", \"%s\", \"%s\");",img_id, imgPath, imgPath_s);
		
		}
		else
		{
			strJavaScript = String
					.format("javascript:stopMarkerAni();");
		}
		webview.loadUrl(strJavaScript);
	}

	public void TwsMapAddCuzAndAddDivPopup(String strID, int nX, int nY,
			int nIconW, int nIconH, String strImgPath, int offX, int offY) {

		String img_id = strID+"_img";
		
		String strContent = "<div id='" + strID + "'>" + "<img width='"
				+ nIconW + "' height='" + nIconH + "' src='" + strImgPath
				+ "' id='"+img_id+"' />" + "</div>";

		String strJavaScript = String
				.format("javascript:addCuzAndAddDivPopup( \"%s\", %d, %d, \"%s\", %d, %d);",
						strID, nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
		

		
	}
	
	public void TwsMapAddCuzAndAddDivPopupWithText(String strID, int nX, int nY,
			int nIconW, int nIconH, String strImgPath, int offX, int offY,String strTitle) {
		
		
		String strStyle = "width:"+nIconW+"px;float:center;font-size:20px;color:#ffffff;";
		String strContent = "<div id='" + strID + "'>" + "<img width='"
				+ nIconW + "' height='" + nIconH + "' src='" + strImgPath
				+ "' />" 
				+ "<div id='Bbox_distance_title' type='text' style='"+strStyle+"'>"
				+ strTitle + "</div>"
				+ "</div>";

		String strJavaScript = String
				.format("javascript:addCuzAndAddDivPopup( \"%s\", %d, %d, \"%s\", %d, %d);",
						strID, nX, nY, strContent, offX, offY);

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapAddCuzAndAddDivPopupCallBack(String strID, int nX,
			int nY, int nIconW, int nIconH, String strImgPath, int offX,
			int offY, String param) {

		String strContent = "<div id='" + strID + "'>" + "<img width='"
				+ nIconW + "' height='" + nIconH + "' src='" + strImgPath
				+ "' />" + "</div>";

		String strJavaScript = String
				.format("javascript:addCuzAndAddDivPopupCallBack( \"%s\", %d, %d, \"%s\", %d, %d, \"%s\");",
						strID, nX, nY, strContent, offX, offY, param);

		webview.loadUrl(strJavaScript);
	}

	public void TwsMapAddCuzAndAddDivPopupDoubleCallBack(String strID, int nX,
			int nY, int nBgW, int nBgH, String strBGPath, int nImgW, int nImgH,
			String strImgPath, int offX, int offY, String param) {

		String strContent = "<div id='" + strID + "'>"

		+ "<div style='position:absolute;'>"
				+ "<div style='position:relative;top:4px;left:4px;'>"
				+ "<img width='"+nImgW+"'" + " height='"+nImgH+"' src='" + strImgPath
				+ "' />" + "</div>" + "</div>"

				+ "<div style='position:absolute;'>"
				+ "<div style='position:relative;'>" + "<img width='" + nBgW
				+ "' height='" + nBgH + "' src='" + strBGPath + "' />"
				+ "</div>" + "</div>"

				+ "</div>";

		String strJavaScript = String
				.format("javascript:addCuzAndAddDivPopupCallBack( \"%s\", %d, %d, \"%s\", %d, %d, \"%s\");",
						strID, nX, nY, strContent, offX, offY, param);

		webview.loadUrl(strJavaScript);
	}
	
	public void TwsMapOilviewDivPopupWithText(String strID, int nX, int nY,
			int nIconW, int nIconH, String strImgPath, int offX, int offY,String strTitle) {
			String strStyle = "width:"+nIconW+"px;float:center;font-size:14px;color:#000000;";
			String strContent = "<div id='" + strID + "'>" + "<img width='"
			+ nIconW + "' height='" + nIconH + "' src='" + strImgPath
			+ "' />" 
			+ "<div id='Oil_divTitle' type='text' style='"+strStyle+"'>"
			+ strTitle + "</div>"
			+ "</div>";

			String strJavaScript = String
			.format("javascript:addCuzAndAddDivPopup( \"%s\", %d, %d, \"%s\", %d, %d);",
			strID, nX, nY, strContent, offX, offY);

			webview.loadUrl(strJavaScript);
			}
			public void TwsMapOilviewDivPopupWithImgText(String strID, int nX, int nY,
			int nIconW, int nIconH, String strImgPath, int offX, int offY,String strImgIcon, String strPrice) {

			String strContent = "<div>"
			+ "<img  style='position:absolute;' width='" + nIconW + "' height='" + nIconH + "'src='"
			+ strImgPath
			+ "' />"

			+ "<div id='Oil_divLow_ext' align='center'>"

			+ "<div id='Oil_divLowText' type='text' style='float:center;font-size:14px' >"
			+ "<img  id='Oil_divLowImg' src='"
			+ strImgIcon
			+ "' align='center' />"
			+ strPrice
			+ "</div>"
			+ "</div>"
			+ "</div>";

			String strJavaScript = String
			.format("javascript:addCuzAndAddDivPopup( \"%s\", %d, %d, \"%s\", %d, %d);",
			strID, nX, nY, strContent, offX, offY);

			webview.loadUrl(strJavaScript);
			}
			
			public void TwsMapOilviewDivPopupDoubleCallBack(String strID, int nX,
			int nY, int nBgW, int nBgH, String strBGPath, int nImgW, int nImgH,
			String strImgPath, int offX, int offY, String param) {

			String strContent = "<div id='" + strID + "'>"

			+ "<div style='position:absolute;'>"
			+ "<div style='position:relative;top:0px;left:0px;'>"
			+ "<img width='"+nImgW+"'" + " height='"+nImgH+"' src='" + strImgPath
			+ "' />" + "</div>" + "</div>"

			+ "<div style='position:absolute;'>"
			+ "<div style='position:relative;'>" + "<img width='" + nBgW
			+ "' height='" + nBgH + "' src='" + strBGPath + "' />"
			+ "</div>" + "</div>"

			+ "</div>";

			String strJavaScript = String
			.format("javascript:addCuzAndAddDivPopupCallBack( \"%s\", %d, %d, \"%s\", %d, %d, \"%s\");",
			strID, nX, nY, strContent, offX, offY, param);

			webview.loadUrl(strJavaScript);
			}

	
	

}
