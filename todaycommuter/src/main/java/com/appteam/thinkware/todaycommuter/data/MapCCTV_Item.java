package com.appteam.thinkware.todaycommuter.data;

import java.io.Serializable;

/**
 * Created by dskim on 2015-01-13.
 */
public class MapCCTV_Item implements Serializable {
    public String name;
    public String nosun; // 도로명
    public String thumbnail; // 썸네일 이미지 URL
    public String update_time; // 업데이트 시간
    public String location; // 위치정보
    public String stream_url; // CCTV URL
    public String group_id; // GROUP ID
    public String cctv_id; // CCTV ID
    public String longitude; // 경도
    public String latitude; // 위도
    public String available; // 사용가능 여부
    public String size; // CCTV DATA SIZE
    public String offer; // 제공처
    public int marker_idx; // marker index
}