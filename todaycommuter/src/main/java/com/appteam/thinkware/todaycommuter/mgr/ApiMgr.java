package com.appteam.thinkware.todaycommuter.mgr;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.appteam.thinkware.todaycommuter.data.MapCCTV;
import com.appteam.thinkware.todaycommuter.data.MapCCTVNow;
import com.appteam.thinkware.todaycommuter.data.PastCCTV;
import com.appteam.thinkware.todaycommuter.data.RouteExtensionItem;
import com.appteam.thinkware.todaycommuter.data.RouteLoc;
import com.appteam.thinkware.todaycommuter.data.SearchCoordAddr;
import com.appteam.thinkware.todaycommuter.data.SearchRouteAroundMulti;
import com.appteam.thinkware.todaycommuter.data.SearchRouteList;
import com.appteam.thinkware.todaycommuter.data.WeatherNow;
import com.appteam.thinkware.todaycommuter.data.WeatherPast;
import com.appteam.thinkware.todaycommuter.util.DeviceInfo;
import com.appteam.thinkware.todaycommuter.util.LOG;
import com.tws.network.api.ApiBase;
import com.tws.network.lib.AirGsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import CoordLib.CoordClient;
import CoordLib.Position_T;

/**
 * Created by jonychoi on 14. 11. 21..
 */
public class ApiMgr {

    private static final String TAG = "VolleyRequest";

    // TCON API
    private static final String URL_DOMAIN = "http://61.33.249.180:8020";
    private static final String URL_CCTV = "/iNaviAir/air/tcon/cctv";
    private static final String URL_CCTV_DETAIL = "/TodayCCTV/today/todaycctv";
    private static final String URL_CCTV_NOW = "/TodayCCTV/today/nowcctv";
    private static final String URL_WEATHER_NOW = "http://api.openweathermap.org/data/2.5/weather?";
    private static final String URL_WEATHER_PAST = "http://api.openweathermap.org/data/2.5/history/";

    // 경로탐색 API
    private static final String URL_ROAD_LIST = "/iNaviAir/air/searchRouteList";
    private static final String URL_ROAD_AROUND_CCTV = "/iNaviAir/air/sire/searchRouteAroundMulti";
    private static final String URL_SEARCH_COOR_ADDR = "/iNaviAir/air/sire/searchCoordAddr";


    // ui Api S
    private HashMap<String, String> getCommonHeader(Context context)
    {
        HashMap<String,String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("auth_key", DeviceInfo.getAuth(context));
        header.put("source", "Android");
        header.put("device", "LG-F160K");
        header.put("mdn", "01082854250");
//        header.put("device", Build.MODEL);
//        header.put("mdn", DeviceInfo.getMDN(context));

        Log.e("getCommonHeader", "DSKIM : " + DeviceInfo.getAuth(context));
        Log.e("getCommonHeader", "DSKIM : "+ "Android");
        Log.e("getCommonHeader", "DSKIM : "+ "LG-F160K");
        Log.e("getCommonHeader", "DSKIM : "+"01082854250");
        return header;
    }

    /**
     * DetailActivity 과거 이미지를 가져오는 로직
     * 시각은 현재시각이 기본값
     * @param context
     * @param cctvi_id
     * @param time
     * @param succListener
     * @param failListener
     */
    public void apiPastCCTVImage(Context context, String cctvi_id, String time,Response.Listener<PastCCTV> succListener, Response.ErrorListener failListener)
    {
        String url = URL_DOMAIN + URL_CCTV_DETAIL;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

//        header = getCommonHeader(context);
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("auth_key", DeviceInfo.getAuth(context));
        header.put("source", "Android");
        header.put("device", "LG-F160K");

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
            // type 1 : 정규화타입
            jsonParams.put("cctvid", cctvi_id);
            jsonParams.put("time", time);

        } catch (Exception e) {
            LOG.d("apiPublicKey error:" + e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<PastCCTV> gsObjRequest = new AirGsonRequest<PastCCTV>(
                Request.Method.POST,
                url,
                PastCCTV.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }
    /**
     *
     * @param context
     * @param succListener
     * @param failListener
     */
    public void apisearchRouteAroundMulti(Context context, String lonx, String laty, String rid, String startX, String startY, String goalX, String goalY, Response.Listener<SearchRouteAroundMulti> succListener, Response.ErrorListener failListener) {
        String url = URL_DOMAIN + URL_ROAD_AROUND_CCTV;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        header = getCommonHeader(context);
        // set add header E
        // set params S
        JSONObject jsonParams = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject subJsonParams = new JSONObject();
        try {
            jsonParams.put("cutflag", "0");
            jsonParams.put("coordtype", "0");
            jsonParams.put("startposition", "0");
            jsonParams.put("reqcount", "0");
            jsonParams.put("lon", lonx);
            jsonParams.put("lat", laty);//수정
            {
            subJsonParams.put("val", "C3000");
            jsonArray.put(subJsonParams);
            jsonParams.put("category", jsonArray);
            }
            jsonParams.put("rid", rid);

            jsonParams.put("sort", "0");
            jsonParams.put("start_x", startX);
            jsonParams.put("start_y", startY);
            jsonParams.put("goal_x", goalX);
            jsonParams.put("goal_y", goalY);
            jsonParams.put("fav_y", "0");
            jsonParams.put("fav_x", "0");
            jsonParams.put("home_y", "0");
            jsonParams.put("home_x", "0");
            jsonParams.put("cur_y", "0");
            jsonParams.put("cur_x", "0");

        } catch (Exception e) {
            LOG.d("apiPublicKey error:" + e.getMessage());
            jsonParams = null;
        }
        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<SearchRouteAroundMulti> gsObjRequest = new AirGsonRequest<SearchRouteAroundMulti>(
                Request.Method.POST,
                url,
                SearchRouteAroundMulti.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }
    /**
     *
     * @param context
     * @param startLoc
     * @param endLoc
     * @param succListener
     * @param failListener
     */
    public void apisearchRouteList(Context context, RouteLoc startLoc, RouteLoc endLoc, RouteExtensionItem routeExtensionItem, Response.Listener<SearchRouteList> succListener, Response.ErrorListener failListener) {
        String url = URL_DOMAIN + URL_ROAD_LIST;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();
        header = getCommonHeader(context);

        // set add header E
        // set params S
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("stype", "1");
            jsonParams.put("sopt1", "1");
            jsonParams.put("sopt2", "0");
            jsonParams.put("coordinate", "2");
            jsonParams.put("nameflag", "1");
            jsonParams.put("version", "15.03a.AIR");//수정
            jsonParams.put("restype", "1");
            jsonParams.put("maxroutesize", "0");

            jsonParams.put("startloc", getRouteListLocJson(startLoc));
            jsonParams.put("endloc", getRouteListLocJson(endLoc));

            jsonParams.put("poi_catecode", "");
            jsonParams.put("pid", "");
            jsonParams.put("poi_name", "");
            jsonParams.put("poi_admcode", "");
            jsonParams.put("cur_admcode", "");
            jsonParams.put("extension_list", getRouteListExtensionJsonArray(routeExtensionItem));
        } catch (Exception e) {
            LOG.d("apiPublicKey error:" + e.getMessage());
            jsonParams = null;
        }
        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<SearchRouteList> gsObjRequest = new AirGsonRequest<SearchRouteList>(
                Request.Method.POST,
                url,
                SearchRouteList.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }

    private JSONArray getRouteListExtensionJsonArray(RouteExtensionItem routeExtensionItem) throws JSONException {
        JSONArray jsonArrays = new JSONArray();
        JSONObject jsonParamExtensionList = new JSONObject();
        jsonParamExtensionList.put("evas_distance", routeExtensionItem.evas_distance);
        jsonParamExtensionList.put("htype",routeExtensionItem.htype);
        jsonParamExtensionList.put("last_index",routeExtensionItem.last_index);
        jsonParamExtensionList.put("directivity",routeExtensionItem.directivity);
        jsonParamExtensionList.put("srhoptname",routeExtensionItem.srhoptname);
        jsonArrays.put(jsonParamExtensionList);
        return jsonArrays;
    }

    private JSONObject getRouteListLocJson(RouteLoc startLoc) throws JSONException {
        JSONObject jsonSubParam = new JSONObject();
        jsonSubParam.put("linkid", startLoc.linkid);
        jsonSubParam.put("laty", startLoc.laty);
        jsonSubParam.put("lonx", startLoc.lonx);
        jsonSubParam.put("direction", startLoc.direction);
        jsonSubParam.put("mapid", startLoc.mapid);
        jsonSubParam.put("nodeid", startLoc.nodeid);
        jsonSubParam.put("name", startLoc.name);
        return jsonSubParam;
    }

    /**
     *
     * @param context 호출 컨텍스트
     * @param strX 정규화 좌표 X
     * @param strY 정규화 좌표 Y
     * @param strLevel 지도 레벨
     * @param succListener 성공 리스너
     * @param failListener 실패 리스너
     */
    public void apiTconCCTV(Context context, String strX, String strY, String strLevel, Response.Listener<MapCCTV> succListener, Response.ErrorListener failListener) {
        String url = URL_DOMAIN + URL_CCTV;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        header = getCommonHeader(context);

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
            jsonParams.put("lonx", strX);
            jsonParams.put("laty", strY);
            jsonParams.put("level", strLevel);
            // type 1 : 정규화타입
            jsonParams.put("locopt", "1");
            jsonParams.put("loctype", "1");
            CoordClient coordClient = new CoordClient();
            Position_T temp =  coordClient.getConvertLongLati(Long.parseLong(strX), Long.parseLong(strY));
            Log.i("apiTconCCTV", " sLatiSec : "+temp.sLatiSec+" sLongSec : "+temp.sLongSec+" sLati : "+temp.sLati+" sLong : "+temp.sLong);
        } catch (Exception e) {
            LOG.d("apiPublicKey error:" + e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<MapCCTV> gsObjRequest = new AirGsonRequest<MapCCTV>(
                Request.Method.POST,
                url,
                MapCCTV.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }


    /**
     *
     * @param context
     * @param cctvId
     * @param succListener
     * @param failListener
     */
    public void apiTodayCurrentCCTV(Context context, String cctvId, Response.Listener<MapCCTVNow> succListener, Response.ErrorListener failListener) {
        String url = URL_DOMAIN + URL_CCTV_NOW;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("auth_key", DeviceInfo.getAuth(context));
        header.put("source", "Android");
        header.put("device", "LG-F160K");
//        header.put("device", Build.MODEL);

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
            jsonParams.put("cctvid", cctvId);
        } catch (Exception e) {
            LOG.d("apiPublicKey error:" + e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<MapCCTVNow> gsObjRequest = new AirGsonRequest<MapCCTVNow>(
                Request.Method.POST,
                url,
                MapCCTVNow.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }

    /**
     *
     * @param context
     * @param longi
     * @param lati
     * @param succListener
     * @param failListener
     */
    public void apiSearchCoordAddr(Context context, String longi, String lati, Response.Listener<SearchCoordAddr> succListener, Response.ErrorListener failListener) {
        String url = URL_DOMAIN + URL_SEARCH_COOR_ADDR;
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

        header = getCommonHeader(context);

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
            jsonParams.put("coordtype", "0");
            jsonParams.put("lonx", longi);
            jsonParams.put("laty", lati);
        } catch (Exception e) {
            LOG.d("apiSearchCoordAddr error:" + e.getMessage());
            jsonParams = null;
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<SearchCoordAddr> gsObjRequest = new AirGsonRequest<SearchCoordAddr>(
                Request.Method.POST,
                url,
                SearchCoordAddr.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }
    /**
     *
     * @param context
     * @param longi
     * @param lati
     * @param succListener
     * @param failListener
     */
    public void apiWeatherNow(Context context, String lati, String longi, Response.Listener<WeatherNow> succListener, Response.ErrorListener failListener) {
        String url = URL_WEATHER_NOW + "lat="+lati+"&lon="+longi;
        LOG.d("apiWeatherNow requsetURL " + url);
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

//        header = getCommonHeader(context);

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
        } catch (Exception e) {
            LOG.d("apiWeatherNow error:" + e.getMessage());
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<WeatherNow> gsObjRequest = new AirGsonRequest<WeatherNow>(
                Request.Method.GET,
                url,
                WeatherNow.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }

    public void apiWeatherPast(Context context, String id,  String start, String end, Response.Listener<WeatherPast> succListener, Response.ErrorListener failListener) {
        String url = URL_WEATHER_PAST + "city?id="+id+"&type=hour&start="+start+"&end="+end;
        LOG.d("apiWeatherPast requsetURL " + url);
        // set add header S
        HashMap<String, String> header = new HashMap<String, String>();

//        header = getCommonHeader(context);

        // set add header E

        // set params S
        JSONObject jsonParams = new JSONObject();

        try {
        } catch (Exception e) {
            LOG.d("apiWeatherNow error:" + e.getMessage());
        }

        String reqParams = null;

        if (jsonParams != null)
            reqParams = jsonParams.toString();

        LOG.d("apiWeatherPast reqParams " + reqParams);


        // set params E

        // request!
        AirGsonRequest<WeatherPast> gsObjRequest = new AirGsonRequest<WeatherPast>(
                Request.Method.GET,
                url,
                WeatherPast.class, header, reqParams,
                succListener, failListener
        );

        // request queue!
        ApiBase.get(context).addToRequestQueue(gsObjRequest);
    }
    // ui Api E
}
