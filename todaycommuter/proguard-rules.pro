# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

## Square Picasso specific rules ##
## https://square.github.io/picasso/ ##

-dontwarn com.squareup.okhttp.**

-optimizations !code/simplification/cast,!code/allocation/*,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify

# The remainder of this file is identical to the non-optimized version
# of the Proguard configuration file (except that the other file has
# flags to turn off optimization).

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# ADDED
-keep class com.google.zxing.client.android.camera.open.**
#-keep class com.google.zxing.client.android.camera.exposure.**
-keep class com.google.zxing.client.android.common.executor.**

# ADDED
-dontobfuscate
-useuniqueclassmembernames

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

-dontwarn android.**
-keep class android.**{
	*;
}

-dontwarn tws.webview.**
-keep class tws.webview.**{
	*;
}

#-dontwarn tws.onair.map.**
#-keep class tws.onair.map.**{
#	*;
#}


#-keep public class tws.onair.ui.OnAirRoadMapActivity$AndroidBridge2
#-keepclassmembers class tws.onair.ui.OnAirRoadMapActivity$AndroidBridge2 {
#   public *;
#}

#-keep public class tws.safety.activity.ActivitySafetyRoadView$AndroidBridge2
#-keepclassmembers class tws.safety.activity.ActivitySafetyRoadView$AndroidBridge2 {
#   public *;
#}

#-keep class com.android.billing.** { *; }
#-keep class com.android.vending.billing.**

#-dontwarn tws.billing.**
#-keep class tws.billing.**{
#	*;
#}

# air 실행 가능함.
#-dontwarn com.thinkware.**
#-keep class com.thinkware.**{
#	*;
#}

#-dontwarn air.payment.**
#-keep class air.payment.**{
#	*;
#}


#-dontwarn air.traffic.**
#-keep class air.traffic.**{
#	*;
#}

-dontwarn com.android.**
-keep class com.android.**{
	*;
}


#-dontwarn tws.air.activity2.**
#-keep class tws.air.activity2.**{
#	*;
#}
-keepattributes InnerClasses

# wrapper 리소스 처리.
#-keep class **.R
#-keep class **.R$* {
#    <fields>;
#}

#-keeppackagenames com.thinkware.inaviair.*

#-keep public class com.android.vending.billing.IInAppBillingService

##---------------Begin: proguard configuration common for all Android apps ----------

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.app.Fragment
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService


# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
-keepclassmembers class * implements java.io.Serializable {     static final long serialVersionUID;     private static final java.io.ObjectStreamField[] serialPersistentFields;     private void writeObject(java.io.ObjectOutputStream);     private void readObject(java.io.ObjectInputStream);     java.lang.Object writeReplace();     java.lang.Object readResolve(); }
# Preserve all native method names and the names of their classes.
-keepclasseswithmembernames class * {     native <methods>; }
-keepclasseswithmembernames class * {     public <init>(android.content.Context, android.util.AttributeSet); }
-keepclasseswithmembernames class * {     public <init>(android.content.Context, android.util.AttributeSet, int); }
# Preserve static fields of inner classes of R classes that might be accessed # through introspection.
-keepclassmembers class **.R$* {   public static <fields>; }
# Preserve the special static methods that are required in all enumeration classes.
-keepclassmembers enum * {     public static **[] values();     public static ** valueOf(java.lang.String); }
-keep public class * {     public protected *; }
-keep class * implements android.os.Parcelable {   public static final android.os.Parcelable$Creator *; }
##---------------End: proguard configuration common for all Android apps ----------


##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# For using GSON @Expose annotation
-keepattributes *Annotation*
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }
# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
##---------------End: proguard configuration for Gson  ----------


##---------------Start: proguard configuration for Google Play Services  ----------
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

# Keep SafeParcelable value, needed for reflection. This is required to support backwards
# compatibility of some classes.
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

# Keep the names of classes/members we need for client functionality.
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

# Needed for Parcelable/SafeParcelable Creators to not get stripped
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
##---------------End: proguard configuration for Google Play Services  ----------

# Add any project specific keep options here:

-keep class com.google.**
-dontwarn com.google.**

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
